(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"/home/duongnhu/workspace/fe/src/js/components/FindBikePod.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            fountains: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    findDonations: function () {
        var parent = this
        var value = parent.refs.location.getValue()
        actions.findDonations(value.address, value.radius)

        if (window.device.platform == "browser") {
            find()
            $(this.refs.findBtn.getDOMNode()).fadeOut(300, function () {
                parent.setState({
                    currentState: parent.CurrentState.FINDING
                })
            })
        } else {
            parent.setState({
                currentState: parent.CurrentState.FINDING
            })
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_POD) {
                if (storeState.success) {
                    this.setState({fountains: storeState.fountains}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.getMarkers(coords, this.state.fountains, this.refs.googleMap.getDOMNode())
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        navigator.geolocation.getCurrentPosition(function (position) {
            var find = null
            if (parent.isMounted()) {
                var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                window.coords = coords
                parent.setState({coords: coords}, function () {
                    actions.findDonations(coords, rad)
                })
            } else {
                find = null
            }
            //parent.drawMap(coords)
            /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
             function (address) {
             parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
             }, function (error) {
             alert(error)
             })*/
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, {enableHighAccuracy: true})
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            this.setUpComponent()
        }
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "s12", id: "search-wrapper"}, 
                    React.createElement("div", {className: "col s12", id: "google-map", ref: "googleMap"})
                )
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                React.createElement(SpinnerWave, {text: "Finding Pods Requests ..."})
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                React.createElement(DonationResult, {donations: this.state.donations})
            )
        }
    }
})

},{"./buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./find_bikepod/DonationResult":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js","./find_bikepod/LocationSetting":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js","./find_bikepod/MapMarker":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/PostRequest.js":[function(require,module,exports){
/**
 * Created by duognnhu on 27/09/15.
 */
var LocationInput = require("./find_bikepod/LocationInput")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
module.exports = React.createClass({displayName: "exports",
     mixins: [flux.mixins.storeListener],

    watchStores: ['PostRequestStore'],


    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            bloodType: "",
            time: "",
            currentAddress: "",
            organizationName: ""
        }
    },

     CurrentState: {
        POSTING: "posting",
        INITIAL: "initial",
        READY: 'READY'
    },

    storeDidChange: function (storeName) {
        console.log(storeName)

        if (this.isMounted()) {
            var store = dispatcher.getStore(storeName)
            var storeState = store.getState()
            if (storeState.success) {
                alert("Posted Successfully")
            } else {
                alert("Error Posting!")
            }
        }
    },

    setUpComponent: function () {
        console.log(this.state.currentState)
        var parent = this
        navigator.geolocation.getCurrentPosition(function (position) {
            googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                function (address) {
                    if (parent.isMounted()) {
                        parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                    }
                }, function (error) {
                    alert(error)
                })
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, { enableHighAccuracy: true })
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    onBloodTypeChange: function (e) {
        this.setState({bloodType: e.target.value})
    },

    onTimeChange: function (e) {
        this.setState({time: e.target.value})
    },

    onNameChange: function (e) {
        this.setState({organizationName: e.target.value})
    },

    postRequest: function () {
        var opts = {
            bloodType: this.state.bloodType,
            address: this.refs.address.getValue(),
            time: this.state.time,
            organizationName: this.state.organizationName
        }
        this.setState({currentState: this.CurrentState.POSTING })
        actions.postRequest(opts)
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "row card"}, 
                    React.createElement("h4", null, "Posting Request"), 
                    React.createElement("ul", {className: "collection"}, 
                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Organization Name:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onNameChange, ref: "name", type: "text", placeholder: "name"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/location_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Location:"), 
                            React.createElement(LocationInput, {initAddress: this.state.currentAddress, ref: "address"})
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Blood Type:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {ref: "bloodType", onChange: this.onBloodTypeChange, type: "text"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Time:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onTimeChange, ref: "time", placeholder: "MM/dd/yyyy HH:mm:ss", type: "text"})
                            )
                        )
                    ), 
                    React.createElement("div", {className: "form-footer right"}, 
                        React.createElement("a", {href: "javascript:void(0)", onClick: this.postRequest, className: "waves-effect waves-green teal lighten-2 btn"}, 
                            "Save"
                        ), 
                        React.createElement("a", {href: "javascript:void(0)", className: "waves-effect waves-green blue-grey btn"}, 
                            "Cancel"
                        )
                    )
                )
            )
        } else if (this.state.currentState == this.CurrentState.POSTING) {
            return (
                React.createElement(SpinnerWave, {text: "Posting Donation Requests ..."})
            )
        }
    }
})
},{"./find_bikepod/LocationInput":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/SideNav.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    componentDidMount: function() {
        $("#menu-btn").sideNav({
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });
    },

    render: function () {
        /**var links = [
            <li key={1}>
                <a href="#/find_donation" className="waves-effect waves-light">
                    <i className="ion-android-search"></i>
                    Find Donation Requests
                </a>
            </li>,
            <li key={2} className="divider"></li>,
            <li key={3}>
                <a href="#/post_request" className="waves-effect waves-light">
                    <i className="ion-android-unlock"></i>
                    Post Request
                </a>
            </li>,
        ]**/
        var links = []
        return (
            React.createElement("div", null, 
                React.createElement("a", {href: "javascript:void(0)", className: "brand-logo center"}, config.app_name), 
                /*<a href="javascript:void(0)" className="brand-logo right" id="logo"><img src="img/app-logo-96.png"/></a>*/
                React.createElement("ul", {className: "left hide-on-med-and-down"}, 
                    links
                ), 
                React.createElement("ul", {id: "slide-out", className: "side-nav"}, 
                    links
                ), 
                  React.createElement("a", {href: "#", "data-activates": "slide-out", className: "button-collapse", id: "menu-btn"}, React.createElement("i", {className: "mdi-navigation-menu"}))
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/WhereToGo.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            fountains: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_FOUNTAIN) {
                if (storeState.success) {
                    this.setState({fountains: storeState.fountains}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.initSearch(coords, this.refs.googleMap.getDOMNode(), {
                "items": this.state.fountains,
                "icon": config.fountain_icon
            })
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        if (!window.coords) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var find = null
                if (parent.isMounted()) {
                    var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                    parent.setState({coords: coords}, function () {
                        actions.findDrinkingFountain(coords, rad)
                    })
                } else {
                    find = null
                }
                //parent.drawMap(coords)
                /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                 function (address) {
                 parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                 }, function (error) {
                 alert(error)
                 })*/
            }, function (error) {
                if (parent.isMounted()) {
                    alert(error.message)
                }
            }, {enableHighAccuracy: true})
        } else {
            parent.setState({coords: coords}, function () {
                actions.findDrinkingFountain(window.coords, rad)
            })
        }

    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            //this.setUpComponent()
        }
    },

    render: function () {
        console.log(this.state.currentState)
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "s12", id: "search-wrapper"}, 
                    React.createElement("nav", null, 
                        React.createElement("div", {className: "nav-wrapper to-go-nav"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("h5", null, "Where to go next?")
                                ), 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("div", {className: "input-field"}, 
                                        React.createElement("input", {value: "Current Location", type: "search"}), 
                                        React.createElement("label", null, React.createElement("i", {className: ""}, "Start"))
                                    )
                                ), 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("div", {className: "input-field"}, 
                                        React.createElement("input", {id: "place-search", type: "search"}), 
                                        React.createElement("label", null, React.createElement("i", {className: ""}, "End"))
                                    )
                                )
                            )
                        )
                    ), 
                    React.createElement("div", {className: "col s12", id: "google-map", ref: "googleMap"})
                )
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                React.createElement(SpinnerWave, {text: "Finding Pods Requests ..."})
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                React.createElement(DonationResult, {donations: this.state.donations})
            )
        }
    }
})

},{"./buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./find_bikepod/DonationResult":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js","./find_bikepod/LocationSetting":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js","./find_bikepod/MapMarker":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            value: (this.props.value)? this.props.value : 0
        }
    },

    componentDidMount: function() {
        this.activateSlider()
    },

    setValue: function(value) {
        this.setState({value: value})
    },

    activateSlider:  function() {
        var min = (this.props.min)? this.props.min : 0
        var max = (this.props.max)? this.props.max : 50
        var node = $(this.refs.slider.getDOMNode())
        node.noUiSlider({
            start: this.state.value,
            connect: "lower",
            orientation: "horizontal",
            direction: "ltr",
            behaviour: "tap-drag",
            range: {
                'min': min,
                'max': max
            },
            format: wNumb({
                decimals: 0
            })
        });
        var parent = this
        node.on('slide', function() {
            parent.setValue($(this).val())
        })
	node.on('change', function () {
	    if (parent.props.sliderRelease) {
		parent.props.sliderRelease($(this).val())
	    }
	})
    },

    render: function () {
        var unit = (this.props.unit)? " " +this.props.unit : ""
        return (
            React.createElement("div", {className: "col s12 slider-container"}, 
                React.createElement("div", {className: "slider col s10", ref: "slider"}), React.createElement("div", {className: "col s2 slider-value"}, this.state.value + unit)
            )
        )
    }
})

},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationCard.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

module.exports = React.createClass({displayName: "exports",

    render: function () {
        return (
            React.createElement("div", {className: "col s12 m8 offset-m2 l6 offset-l3"}, 
                React.createElement("div", {className: "card-panel grey lighten-5 z-depth-1"}, 
                    React.createElement("div", {className: "row valign-wrapper"}, 
                        React.createElement("div", {className: "col s2"}, 
                            React.createElement("img", {src: this.props.donation.profileImg, alt: "Profile Picture", className: "circle responsive-img"})
                        ), 
                        React.createElement("div", {className: "col s10"}, 
                            React.createElement("p", null), 
                            React.createElement("p", {className: "black-text"}, 
                                this.props.donation.description
                            )
                        )
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

var DonationCard = require("./DonationCard")

module.exports = React.createClass({displayName: "exports",

    getInitialState: function() {
        return {
            donations: (this.props.donations)? this.props.donations : []
        }
    },

    componentWillReceiveProps: function(nextProps) {
        if (nextProps.donations && JSON.stringify(nextProps.donations) != JSON.stringify(this.props.donations)) {
            this.setState({donations: nextProps.donations})
        }
    },

    render: function() {
        if (this.state.donations.length == 0) {
            return (
                React.createElement("h3", null, 
                    "No donation requests are found around your location!"
                )
            )
        } else {
            return (
                React.createElement("div", {className: "busker-list"}, 
                    
                        this.state.donations.map(function(item, id) {
                            return (
                                React.createElement(DonationCard, {donation: item, key: id})
                            )
                        })
                    
                )
            )
        }
    }
})
},{"./DonationCard":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationCard.js"}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            searchBox: undefined
        }
    },

    componentDidMount: function () {
        this.setState({searchBox: new google.maps.places.SearchBox(this.refs.input.getDOMNode())})
    },

    getValue: function () {
        return this.refs.input.getDOMNode().value
    },

    render: function () {
        return (
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "input-field col s12"}, 
                    React.createElement("input", {type: "text", placeholder: "Choose a location", className: "validate", ref: "input", defaultValue: this.props.initAddress})
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
var Slider = require("../buttons/Slider")
var LocationInput = require("./LocationInput")

module.exports = React.createClass({displayName: "exports",

    componentWillUpdate: function (nextProps) {
        if (nextProps.initAddress && nextProps.initAddress != this.props.initAddress) {
            return true
        }
        return false
    },

    getValue: function () {
        return {
            address: this.refs.address.getValue(),
            radius: this.refs.radius.state.value
        }
    },

    render: function () {
        return (
            React.createElement("div", {className: "row", id: "location-setting"}, 
                React.createElement("form", {className: "col s12"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement(LocationInput, {initAddress: this.props.initAddress, ref: "address"}), 
                        React.createElement("div", {className: "col s12 label"}, "Radius"), 
                        React.createElement(Slider, {ref: "radius", value: 10, unit: "km"})
                    )
                )
            )
        )
    }
})
},{"../buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./LocationInput":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js"}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js":[function(require,module,exports){
/**
 * Created by duognnhu on 26/09/15.
 */
var gApi = require("../../utils/google")
module.exports = {
    getMarkers: function (currentLocation, pods, dom, radius, icon) {
        if (!window.map) {
            window.map = new google.maps.Map(dom, {
                center: currentLocation,
                zoom: config.googleMap.zoom,
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: false
            });
        }
        google.maps.visualRefresh = true;
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here',
            icon: config.user_current_location_icon
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })

        pods.forEach(function (pod, id) {
            var coordinates = pod.coordinates.coordinates
            var marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(coordinates["lat"]),
                    lng: parseFloat(coordinates["lon"])
                },
                icon: (icon) ? icon : config.bike_icon
            })
            marker.addListener('click', function () {
                window.popupPodDetails(pod)
            })
            marker.setMap(map)
        })
        new google.maps.Circle({
            strokeColor: 'green',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.0,
            map: map,
            center: currentLocation,
            radius: 1000
        });
    },

    initSearch: function (currentLocation, dom, data) {
        var markers = []
        if (!window.map) {
            window.map = new google.maps.Map(dom, {
                center: currentLocation,
                zoom: config.googleMap.zoom,
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: false
            });
        }
        google.maps.visualRefresh = true;
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here',
            icon: config.user_current_location_icon
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })

        data.items.forEach(function (point) {
            var coordinates = point.coordinates.coordinates
            var mOpts = {
                position: {
                    lat: parseFloat(coordinates["lat"]),
                    lng: parseFloat(coordinates["lon"])
                }
            }
            if (data.icon) {
                mOpts["icon"] = data.icon

            }
            var marker = new google.maps.Marker(mOpts)
            marker.setMap(map)
            markers.push(marker)
        })
        new google.maps.Circle({
            strokeColor: 'green',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.0,
            map: map,
            center: currentLocation,
            radius: 1000
        });
        var searchPlacesMarkers = []
        if (document.getElementById('place-search')) {
            // Create the search box and link it to the UI element.
            var input = document.getElementById('place-search');
            var searchBox = new google.maps.places.SearchBox(input);
            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var directionsDisplay = new google.maps.DirectionsRenderer({
                preserveViewport: true,
            });
            directionsDisplay.setMap(map)
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                searchPlacesMarkers.forEach(function (marker) {
                    marker.setMap(null)
                })
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    var marker = new google.maps.Marker({
                        map: map,
                        title: place.name,
                        position: place.geometry.location
                    })
                    searchPlacesMarkers.push(marker)
                    var mLocationInfo = new google.maps.InfoWindow({
                        content: "<p>" + place.name + "</p>"
                    });
                    marker.addListener('click', function () {
                        mLocationInfo.open(map, marker);
                    })

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }


                    gApi.maps.getDir(currentLocation, place.geometry.location, function (direction) {
                        directionsDisplay.setDirections(direction)
                    });
                })
                currentLocationMarker.setMap(null)
                map.fitBounds(bounds);

            });
        }
    }
}

},{"../../utils/google":"/home/duongnhu/workspace/fe/src/js/utils/google.js"}],"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function() {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-wave"}, 
                        React.createElement("div", {className: "sk-rect1"}), 
                        React.createElement("div", {className: "sk-rect2"}), 
                        React.createElement("div", {className: "sk-rect3"}), 
                        React.createElement("div", {className: "sk-rect4"}), 
                        React.createElement("div", {className: "sk-rect5"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function () {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-three-bounce"}, 
                        React.createElement("div", {className: "sk-bounce1"}), 
                        React.createElement("div", {className: "sk-bounce2"}), 
                        React.createElement("div", {className: "sk-bounce3"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/index.js":[function(require,module,exports){
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var router = require("./router/router")

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //app.receivedEvent('deviceready');
        $(document).ready(function() {
            router.init("/find_donation")
            setTimeout(function() {
                $(".splash-screen").fadeOut(500)
            }, 3000)
            document.addEventListener("backbutton", onBackKeyDown, false);
            function onBackKeyDown() {
                React.unmountComponentAtNode(document.getElementById("pod-details"))
            }
        })
    }
};

app.initialize();

},{"./router/router":"/home/duongnhu/workspace/fe/src/js/router/router.js"}],"/home/duongnhu/workspace/fe/src/js/router/router.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
var FindBikePod = require("../components/FindBikePod.js")
var PostRequest = require("../components/PostRequest")
var WhereToGo = require("../components/WhereToGo")
var SideNav = require("../components/SideNav")
window.renderFindDonation = function () {
    window.map = null
    React.unmountComponentAtNode(document.getElementById("main-content"))
    $("#nav-bar").show()
        $(".modal").hide()

    React.render(React.createElement(SideNav, {dispatcher: dispatcher}), document.getElementById("nav-bar"))
    if (document.getElementById("pod-details")) {
        React.unmountComponentAtNode(document.getElementById("pod-details"))
    }
    React.render(React.createElement(FindBikePod, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

window.renderWhereGo = function () {
    window.map = null
    $("#nav-bar").hide()
    $(".modal").hide()
    $(".lean-overlay").hide()
    $("#lean-overlay").hide()

    React.unmountComponentAtNode(document.getElementById("main-content"))
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(React.createElement(WhereToGo, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

module.exports = {
    init: function(path) {
        var routes = {
            "/find_donation": renderFindDonation,
            "/where-go": renderWhereGo
        }
        window.router = Router(routes);
        router.init(path)
    }
}
},{"../components/FindBikePod.js":"/home/duongnhu/workspace/fe/src/js/components/FindBikePod.js","../components/PostRequest":"/home/duongnhu/workspace/fe/src/js/components/PostRequest.js","../components/SideNav":"/home/duongnhu/workspace/fe/src/js/components/SideNav.js","../components/WhereToGo":"/home/duongnhu/workspace/fe/src/js/components/WhereToGo.js"}],"/home/duongnhu/workspace/fe/src/js/utils/google.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = {
    maps: {
        checkConnection: function () {
            var networkState = navigator.connection.type
            console.log(networkState)
            if (networkState == Connection.NONE) {
                return false
            }
            return true
        },

        reverseGeocoding: function (lat, lng, success, error) {
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        success(results[0].formatted_address)
                    } else {
                        error('No results found');
                    }
                } else {
                    error('Geocoder failed due to: ' + status);
                }
            });
        },

        geocoding: function (address, success, error) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        success(results[0].geometry)
                    } else {
                        error('No results found');
                    }
                } else {
                    error('Geocoder failed due to: ' + status);
                }
            });
        },

        getDir: function (origin, destination, success) {
            var directionsService = new google.maps.DirectionsService();
            var directionsRequest = {
                "origin": origin["lat"] + "," + origin["lng"],
                "destination": origin["lat"] + "," + origin["lng"],
                optimizeWaypoints: true,
                travelMode: 'BICYCLING'
            };
            directionsService.route(directionsRequest, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    //do work with response data
                    console.log(response)
                    success(response)
                }
                else {
                    //Error has occured
                    error(response)
                }
            })

        }
    }
}
},{}]},{},["/home/duongnhu/workspace/fe/src/js/index.js"])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvY29tcG9uZW50cy9GaW5kQmlrZVBvZC5qcyIsInNyYy9qcy9jb21wb25lbnRzL1Bvc3RSZXF1ZXN0LmpzIiwic3JjL2pzL2NvbXBvbmVudHMvU2lkZU5hdi5qcyIsInNyYy9qcy9jb21wb25lbnRzL1doZXJlVG9Hby5qcyIsInNyYy9qcy9jb21wb25lbnRzL2J1dHRvbnMvU2xpZGVyLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvZmluZF9iaWtlcG9kL0RvbmF0aW9uQ2FyZC5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfYmlrZXBvZC9Eb25hdGlvblJlc3VsdC5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfYmlrZXBvZC9Mb2NhdGlvbklucHV0LmpzIiwic3JjL2pzL2NvbXBvbmVudHMvZmluZF9iaWtlcG9kL0xvY2F0aW9uU2V0dGluZy5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfYmlrZXBvZC9NYXBNYXJrZXIuanMiLCJzcmMvanMvY29tcG9uZW50cy9sb2FkaW5nL1NwaW5uZXJXYXZlLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvbG9hZGluZy9UaHJlZUJvdW5jZS5qcyIsInNyYy9qcy9pbmRleC5qcyIsInNyYy9qcy9yb3V0ZXIvcm91dGVyLmpzIiwic3JjL2pzL3V0aWxzL2dvb2dsZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMxQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdEpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDNUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMTAvMDUvMTUuXG4gKi9cbnZhciBTbGlkZXIgPSByZXF1aXJlKFwiLi9idXR0b25zL1NsaWRlclwiKVxudmFyIFNwaW5uZXJXYXZlID0gcmVxdWlyZShcIi4vbG9hZGluZy9TcGlubmVyV2F2ZVwiKVxudmFyIEN1YmVHcmlkID0gcmVxdWlyZShcIi4vbG9hZGluZy9UaHJlZUJvdW5jZVwiKVxudmFyIERvbmF0aW9uUmVzdWx0ID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL0RvbmF0aW9uUmVzdWx0XCIpXG52YXIgTG9jYXRpb25TZXR0aW5nID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL0xvY2F0aW9uU2V0dGluZ1wiKVxudmFyIE1hcE1hcmtlciA9IHJlcXVpcmUoXCIuL2ZpbmRfYmlrZXBvZC9NYXBNYXJrZXJcIilcbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICBtaXhpbnM6IFtmbHV4Lm1peGlucy5zdG9yZUxpc3RlbmVyXSxcblxuICAgIHdhdGNoU3RvcmVzOiBbJ0xvY2F0aW9uU3RvcmUnXSxcblxuICAgIEN1cnJlbnRTdGF0ZToge1xuICAgICAgICBGSU5ESU5HOiBcImZpbmRpbmdcIixcbiAgICAgICAgSU5JVElBTDogXCJpbml0aWFsXCIsXG4gICAgICAgIFJFQURZOiAnUkVBRFknLFxuICAgICAgICBSRVNVTFQ6IFwicmVzdWx0XCJcbiAgICB9LFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjdXJyZW50U3RhdGU6ICh0aGlzLnByb3BzLmN1cnJlbnRTdGF0ZSkgPyB0aGlzLnByb3BzLmN1cnJlbnRTdGF0ZSA6IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwsXG4gICAgICAgICAgICBmb3VudGFpbnM6IFtdLFxuICAgICAgICAgICAgY29vcmRzOiB7fVxuICAgICAgICB9XG4gICAgfSxcblxuXG4gICAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24gKG5leHRQcm9wcykge1xuICAgICAgICBpZiAobmV4dFByb3BzLmN1cnJlbnRTdGF0ZSkge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiBuZXh0UHJvcHMuY3VycmVudFN0YXRlfSlcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBmaW5kRG9uYXRpb25zOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBwYXJlbnQgPSB0aGlzXG4gICAgICAgIHZhciB2YWx1ZSA9IHBhcmVudC5yZWZzLmxvY2F0aW9uLmdldFZhbHVlKClcbiAgICAgICAgYWN0aW9ucy5maW5kRG9uYXRpb25zKHZhbHVlLmFkZHJlc3MsIHZhbHVlLnJhZGl1cylcblxuICAgICAgICBpZiAod2luZG93LmRldmljZS5wbGF0Zm9ybSA9PSBcImJyb3dzZXJcIikge1xuICAgICAgICAgICAgZmluZCgpXG4gICAgICAgICAgICAkKHRoaXMucmVmcy5maW5kQnRuLmdldERPTU5vZGUoKSkuZmFkZU91dCgzMDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50U3RhdGU6IHBhcmVudC5DdXJyZW50U3RhdGUuRklORElOR1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9KVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBjdXJyZW50U3RhdGU6IHBhcmVudC5DdXJyZW50U3RhdGUuRklORElOR1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBzdG9yZURpZENoYW5nZTogZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgIHZhciBzdG9yZSA9IGRpc3BhdGNoZXIuZ2V0U3RvcmUobmFtZSlcbiAgICAgICAgICAgIHZhciBzdG9yZVN0YXRlID0gc3RvcmUuZ2V0U3RhdGUoKVxuXG5cbiAgICAgICAgICAgIGlmIChzdG9yZVN0YXRlLmFjdGlvbiA9PSBzdG9yZS5hY3Rpb25zLkZJTkRfUE9EKSB7XG4gICAgICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtmb3VudGFpbnM6IHN0b3JlU3RhdGUuZm91bnRhaW5zfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kcmF3TWFwKHRoaXMuc3RhdGUuY29vcmRzKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIG5hdmlnYXRvci5ub3RpZmljYXRpb24uYWxlcnQoc3RvcmVTdGF0ZS5lcnJvck1lc3NhZ2UpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogdGhpcy5DdXJyZW50U3RhdGUuUkVBRFl9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBkcmF3TWFwOiBmdW5jdGlvbiAoY29vcmRzKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogdGhpcy5DdXJyZW50U3RhdGUuUkVBRFl9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBNYXBNYXJrZXIuZ2V0TWFya2Vycyhjb29yZHMsIHRoaXMuc3RhdGUuZm91bnRhaW5zLCB0aGlzLnJlZnMuZ29vZ2xlTWFwLmdldERPTU5vZGUoKSlcbiAgICAgICAgfSlcbiAgICB9LFxuXG4gICAgc2V0VXBDb21wb25lbnQ6IGZ1bmN0aW9uIChyYWRpdXMpIHtcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgdmFyIHJhZCA9IChyYWRpdXMpID8gcmFkaXVzIDogNVxuICAgICAgICBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKGZ1bmN0aW9uIChwb3NpdGlvbikge1xuICAgICAgICAgICAgdmFyIGZpbmQgPSBudWxsXG4gICAgICAgICAgICBpZiAocGFyZW50LmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgdmFyIGNvb3JkcyA9IHtsYXQ6IHBvc2l0aW9uLmNvb3Jkcy5sYXRpdHVkZSwgbG5nOiBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlfVxuICAgICAgICAgICAgICAgIHdpbmRvdy5jb29yZHMgPSBjb29yZHNcbiAgICAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe2Nvb3JkczogY29vcmRzfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBhY3Rpb25zLmZpbmREb25hdGlvbnMoY29vcmRzLCByYWQpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZmluZCA9IG51bGxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vcGFyZW50LmRyYXdNYXAoY29vcmRzKVxuICAgICAgICAgICAgLypnb29nbGVVdGlscy5tYXBzLnJldmVyc2VHZW9jb2RpbmcocG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlLFxuICAgICAgICAgICAgIGZ1bmN0aW9uIChhZGRyZXNzKSB7XG4gICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjdXJyZW50QWRkcmVzczogYWRkcmVzcywgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICBhbGVydChlcnJvcilcbiAgICAgICAgICAgICB9KSovXG4gICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgaWYgKHBhcmVudC5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgICAgIGFsZXJ0KGVycm9yLm1lc3NhZ2UpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtlbmFibGVIaWdoQWNjdXJhY3k6IHRydWV9KVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkVXBkYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMKSB7XG4gICAgICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwpIHtcbiAgICAgICAgICAgIHJldHVybiAoUmVhY3QuY3JlYXRlRWxlbWVudChDdWJlR3JpZCwge3RleHQ6IFwiTG9hZGluZyAuLi5cIn0pKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzMTJcIiwgaWQ6IFwic2VhcmNoLXdyYXBwZXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMlwiLCBpZDogXCJnb29nbGUtbWFwXCIsIHJlZjogXCJnb29nbGVNYXBcIn0pXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLkZJTkRJTkcpIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChTcGlubmVyV2F2ZSwge3RleHQ6IFwiRmluZGluZyBQb2RzIFJlcXVlc3RzIC4uLlwifSlcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9IHRoaXMuQ3VycmVudFN0YXRlLlJFU1VMVCkge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KERvbmF0aW9uUmVzdWx0LCB7ZG9uYXRpb25zOiB0aGlzLnN0YXRlLmRvbmF0aW9uc30pXG4gICAgICAgICAgICApXG4gICAgICAgIH1cbiAgICB9XG59KVxuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b2dubmh1IG9uIDI3LzA5LzE1LlxuICovXG52YXIgTG9jYXRpb25JbnB1dCA9IHJlcXVpcmUoXCIuL2ZpbmRfYmlrZXBvZC9Mb2NhdGlvbklucHV0XCIpXG52YXIgU3Bpbm5lcldhdmUgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1NwaW5uZXJXYXZlXCIpXG52YXIgQ3ViZUdyaWQgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1RocmVlQm91bmNlXCIpXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG4gICAgIG1peGluczogW2ZsdXgubWl4aW5zLnN0b3JlTGlzdGVuZXJdLFxuXG4gICAgd2F0Y2hTdG9yZXM6IFsnUG9zdFJlcXVlc3RTdG9yZSddLFxuXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogKHRoaXMucHJvcHMuY3VycmVudFN0YXRlKSA/IHRoaXMucHJvcHMuY3VycmVudFN0YXRlIDogdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCxcbiAgICAgICAgICAgIGJsb29kVHlwZTogXCJcIixcbiAgICAgICAgICAgIHRpbWU6IFwiXCIsXG4gICAgICAgICAgICBjdXJyZW50QWRkcmVzczogXCJcIixcbiAgICAgICAgICAgIG9yZ2FuaXphdGlvbk5hbWU6IFwiXCJcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICAgQ3VycmVudFN0YXRlOiB7XG4gICAgICAgIFBPU1RJTkc6IFwicG9zdGluZ1wiLFxuICAgICAgICBJTklUSUFMOiBcImluaXRpYWxcIixcbiAgICAgICAgUkVBRFk6ICdSRUFEWSdcbiAgICB9LFxuXG4gICAgc3RvcmVEaWRDaGFuZ2U6IGZ1bmN0aW9uIChzdG9yZU5hbWUpIHtcbiAgICAgICAgY29uc29sZS5sb2coc3RvcmVOYW1lKVxuXG4gICAgICAgIGlmICh0aGlzLmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICB2YXIgc3RvcmUgPSBkaXNwYXRjaGVyLmdldFN0b3JlKHN0b3JlTmFtZSlcbiAgICAgICAgICAgIHZhciBzdG9yZVN0YXRlID0gc3RvcmUuZ2V0U3RhdGUoKVxuICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgIGFsZXJ0KFwiUG9zdGVkIFN1Y2Nlc3NmdWxseVwiKVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBhbGVydChcIkVycm9yIFBvc3RpbmchXCIpXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgc2V0VXBDb21wb25lbnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUpXG4gICAgICAgIHZhciBwYXJlbnQgPSB0aGlzXG4gICAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgICAgICAgICBnb29nbGVVdGlscy5tYXBzLnJldmVyc2VHZW9jb2RpbmcocG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlLFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChhZGRyZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7Y3VycmVudEFkZHJlc3M6IGFkZHJlc3MsIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5SRUFEWX0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgYWxlcnQoZXJyb3IpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgfSwgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICBpZiAocGFyZW50LmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgYWxlcnQoZXJyb3IubWVzc2FnZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgeyBlbmFibGVIaWdoQWNjdXJhY3k6IHRydWUgfSlcbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRVcENvbXBvbmVudCgpXG4gICAgfSxcblxuICAgIG9uQmxvb2RUeXBlQ2hhbmdlOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtibG9vZFR5cGU6IGUudGFyZ2V0LnZhbHVlfSlcbiAgICB9LFxuXG4gICAgb25UaW1lQ2hhbmdlOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHt0aW1lOiBlLnRhcmdldC52YWx1ZX0pXG4gICAgfSxcblxuICAgIG9uTmFtZUNoYW5nZTogZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7b3JnYW5pemF0aW9uTmFtZTogZS50YXJnZXQudmFsdWV9KVxuICAgIH0sXG5cbiAgICBwb3N0UmVxdWVzdDogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgb3B0cyA9IHtcbiAgICAgICAgICAgIGJsb29kVHlwZTogdGhpcy5zdGF0ZS5ibG9vZFR5cGUsXG4gICAgICAgICAgICBhZGRyZXNzOiB0aGlzLnJlZnMuYWRkcmVzcy5nZXRWYWx1ZSgpLFxuICAgICAgICAgICAgdGltZTogdGhpcy5zdGF0ZS50aW1lLFxuICAgICAgICAgICAgb3JnYW5pemF0aW9uTmFtZTogdGhpcy5zdGF0ZS5vcmdhbml6YXRpb25OYW1lXG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5QT1NUSU5HIH0pXG4gICAgICAgIGFjdGlvbnMucG9zdFJlcXVlc3Qob3B0cylcbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMKSB7XG4gICAgICAgICAgICByZXR1cm4gKFJlYWN0LmNyZWF0ZUVsZW1lbnQoQ3ViZUdyaWQsIHt0ZXh0OiBcIkxvYWRpbmcgLi4uXCJ9KSlcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWSkge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicm93IGNhcmRcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaDRcIiwgbnVsbCwgXCJQb3N0aW5nIFJlcXVlc3RcIiksIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwidWxcIiwge2NsYXNzTmFtZTogXCJjb2xsZWN0aW9uXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb24taXRlbSBhdmF0YXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogXCJpbWcvYmxvb2R0eXBlX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIk9yZ2FuaXphdGlvbiBOYW1lOlwiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7b25DaGFuZ2U6IHRoaXMub25OYW1lQ2hhbmdlLCByZWY6IFwibmFtZVwiLCB0eXBlOiBcInRleHRcIiwgcGxhY2Vob2xkZXI6IFwibmFtZVwifSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICApLCBcblxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImxpXCIsIHtjbGFzc05hbWU6IFwiY29sbGVjdGlvbi1pdGVtIGF2YXRhclwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImltZ1wiLCB7c3JjOiBcImltZy9sb2NhdGlvbl9sb2dvLnBuZ1wiLCBjbGFzc05hbWU6IFwiY2lyY2xlXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInNwYW5cIiwge2NsYXNzTmFtZTogXCJ0aXRsZVwifSwgXCJMb2NhdGlvbjpcIiksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoTG9jYXRpb25JbnB1dCwge2luaXRBZGRyZXNzOiB0aGlzLnN0YXRlLmN1cnJlbnRBZGRyZXNzLCByZWY6IFwiYWRkcmVzc1wifSlcbiAgICAgICAgICAgICAgICAgICAgICAgICksIFxuXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwge2NsYXNzTmFtZTogXCJjb2xsZWN0aW9uLWl0ZW0gYXZhdGFyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW1nXCIsIHtzcmM6IFwiaW1nL2Jsb29kdHlwZV9sb2dvLnBuZ1wiLCBjbGFzc05hbWU6IFwiY2lyY2xlXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInNwYW5cIiwge2NsYXNzTmFtZTogXCJ0aXRsZVwifSwgXCJCbG9vZCBUeXBlOlwiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7cmVmOiBcImJsb29kVHlwZVwiLCBvbkNoYW5nZTogdGhpcy5vbkJsb29kVHlwZUNoYW5nZSwgdHlwZTogXCJ0ZXh0XCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICksIFxuXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwge2NsYXNzTmFtZTogXCJjb2xsZWN0aW9uLWl0ZW0gYXZhdGFyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW1nXCIsIHtzcmM6IFwiaW1nL2Jsb29kdHlwZV9sb2dvLnBuZ1wiLCBjbGFzc05hbWU6IFwiY2lyY2xlXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInNwYW5cIiwge2NsYXNzTmFtZTogXCJ0aXRsZVwifSwgXCJUaW1lOlwiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7b25DaGFuZ2U6IHRoaXMub25UaW1lQ2hhbmdlLCByZWY6IFwidGltZVwiLCBwbGFjZWhvbGRlcjogXCJNTS9kZC95eXl5IEhIOm1tOnNzXCIsIHR5cGU6IFwidGV4dFwifSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiZm9ybS1mb290ZXIgcmlnaHRcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImFcIiwge2hyZWY6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsIG9uQ2xpY2s6IHRoaXMucG9zdFJlcXVlc3QsIGNsYXNzTmFtZTogXCJ3YXZlcy1lZmZlY3Qgd2F2ZXMtZ3JlZW4gdGVhbCBsaWdodGVuLTIgYnRuXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIlNhdmVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYVwiLCB7aHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIiwgY2xhc3NOYW1lOiBcIndhdmVzLWVmZmVjdCB3YXZlcy1ncmVlbiBibHVlLWdyZXkgYnRuXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIkNhbmNlbFwiXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuUE9TVElORykge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFNwaW5uZXJXYXZlLCB7dGV4dDogXCJQb3N0aW5nIERvbmF0aW9uIFJlcXVlc3RzIC4uLlwifSlcbiAgICAgICAgICAgIClcbiAgICAgICAgfVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDEwLzA1LzE1LlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAkKFwiI21lbnUtYnRuXCIpLnNpZGVOYXYoe1xuICAgICAgICAgICAgZWRnZTogJ2xlZnQnLCAvLyBDaG9vc2UgdGhlIGhvcml6b250YWwgb3JpZ2luXG4gICAgICAgICAgICBjbG9zZU9uQ2xpY2s6IHRydWUgLy8gQ2xvc2VzIHNpZGUtbmF2IG9uIDxhPiBjbGlja3MsIHVzZWZ1bCBmb3IgQW5ndWxhci9NZXRlb3JcbiAgICAgICAgfSk7XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICAvKip2YXIgbGlua3MgPSBbXG4gICAgICAgICAgICA8bGkga2V5PXsxfT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiIy9maW5kX2RvbmF0aW9uXCIgY2xhc3NOYW1lPVwid2F2ZXMtZWZmZWN0IHdhdmVzLWxpZ2h0XCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImlvbi1hbmRyb2lkLXNlYXJjaFwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgRmluZCBEb25hdGlvbiBSZXF1ZXN0c1xuICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvbGk+LFxuICAgICAgICAgICAgPGxpIGtleT17Mn0gY2xhc3NOYW1lPVwiZGl2aWRlclwiPjwvbGk+LFxuICAgICAgICAgICAgPGxpIGtleT17M30+XG4gICAgICAgICAgICAgICAgPGEgaHJlZj1cIiMvcG9zdF9yZXF1ZXN0XCIgY2xhc3NOYW1lPVwid2F2ZXMtZWZmZWN0IHdhdmVzLWxpZ2h0XCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImlvbi1hbmRyb2lkLXVubG9ja1wiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgUG9zdCBSZXF1ZXN0XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9saT4sXG4gICAgICAgIF0qKi9cbiAgICAgICAgdmFyIGxpbmtzID0gW11cbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImFcIiwge2hyZWY6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsIGNsYXNzTmFtZTogXCJicmFuZC1sb2dvIGNlbnRlclwifSwgY29uZmlnLmFwcF9uYW1lKSwgXG4gICAgICAgICAgICAgICAgLyo8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3NOYW1lPVwiYnJhbmQtbG9nbyByaWdodFwiIGlkPVwibG9nb1wiPjxpbWcgc3JjPVwiaW1nL2FwcC1sb2dvLTk2LnBuZ1wiLz48L2E+Ki9cbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwidWxcIiwge2NsYXNzTmFtZTogXCJsZWZ0IGhpZGUtb24tbWVkLWFuZC1kb3duXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgbGlua3NcbiAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwidWxcIiwge2lkOiBcInNsaWRlLW91dFwiLCBjbGFzc05hbWU6IFwic2lkZS1uYXZcIn0sIFxuICAgICAgICAgICAgICAgICAgICBsaW5rc1xuICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImFcIiwge2hyZWY6IFwiI1wiLCBcImRhdGEtYWN0aXZhdGVzXCI6IFwic2xpZGUtb3V0XCIsIGNsYXNzTmFtZTogXCJidXR0b24tY29sbGFwc2VcIiwgaWQ6IFwibWVudS1idG5cIn0sIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpXCIsIHtjbGFzc05hbWU6IFwibWRpLW5hdmlnYXRpb24tbWVudVwifSkpXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAxMC8wNS8xNS5cbiAqL1xudmFyIFNsaWRlciA9IHJlcXVpcmUoXCIuL2J1dHRvbnMvU2xpZGVyXCIpXG52YXIgU3Bpbm5lcldhdmUgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1NwaW5uZXJXYXZlXCIpXG52YXIgQ3ViZUdyaWQgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1RocmVlQm91bmNlXCIpXG52YXIgRG9uYXRpb25SZXN1bHQgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvRG9uYXRpb25SZXN1bHRcIilcbnZhciBMb2NhdGlvblNldHRpbmcgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvTG9jYXRpb25TZXR0aW5nXCIpXG52YXIgTWFwTWFya2VyID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL01hcE1hcmtlclwiKVxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuICAgIG1peGluczogW2ZsdXgubWl4aW5zLnN0b3JlTGlzdGVuZXJdLFxuXG4gICAgd2F0Y2hTdG9yZXM6IFsnTG9jYXRpb25TdG9yZSddLFxuXG4gICAgQ3VycmVudFN0YXRlOiB7XG4gICAgICAgIEZJTkRJTkc6IFwiZmluZGluZ1wiLFxuICAgICAgICBJTklUSUFMOiBcImluaXRpYWxcIixcbiAgICAgICAgUkVBRFk6ICdSRUFEWScsXG4gICAgICAgIFJFU1VMVDogXCJyZXN1bHRcIlxuICAgIH0sXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogKHRoaXMucHJvcHMuY3VycmVudFN0YXRlKSA/IHRoaXMucHJvcHMuY3VycmVudFN0YXRlIDogdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCxcbiAgICAgICAgICAgIGZvdW50YWluczogW10sXG4gICAgICAgICAgICBjb29yZHM6IHt9XG4gICAgICAgIH1cbiAgICB9LFxuXG5cbiAgICBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzOiBmdW5jdGlvbiAobmV4dFByb3BzKSB7XG4gICAgICAgIGlmIChuZXh0UHJvcHMuY3VycmVudFN0YXRlKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IG5leHRQcm9wcy5jdXJyZW50U3RhdGV9KVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHN0b3JlRGlkQ2hhbmdlOiBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICBpZiAodGhpcy5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgdmFyIHN0b3JlID0gZGlzcGF0Y2hlci5nZXRTdG9yZShuYW1lKVxuICAgICAgICAgICAgdmFyIHN0b3JlU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpXG5cblxuICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuYWN0aW9uID09IHN0b3JlLmFjdGlvbnMuRklORF9GT1VOVEFJTikge1xuICAgICAgICAgICAgICAgIGlmIChzdG9yZVN0YXRlLnN1Y2Nlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Zm91bnRhaW5zOiBzdG9yZVN0YXRlLmZvdW50YWluc30sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZHJhd01hcCh0aGlzLnN0YXRlLmNvb3JkcylcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBuYXZpZ2F0b3Iubm90aWZpY2F0aW9uLmFsZXJ0KHN0b3JlU3RhdGUuZXJyb3JNZXNzYWdlKVxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgZHJhd01hcDogZnVuY3Rpb24gKGNvb3Jkcykge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgTWFwTWFya2VyLmluaXRTZWFyY2goY29vcmRzLCB0aGlzLnJlZnMuZ29vZ2xlTWFwLmdldERPTU5vZGUoKSwge1xuICAgICAgICAgICAgICAgIFwiaXRlbXNcIjogdGhpcy5zdGF0ZS5mb3VudGFpbnMsXG4gICAgICAgICAgICAgICAgXCJpY29uXCI6IGNvbmZpZy5mb3VudGFpbl9pY29uXG4gICAgICAgICAgICB9KVxuICAgICAgICB9KVxuICAgIH0sXG5cbiAgICBzZXRVcENvbXBvbmVudDogZnVuY3Rpb24gKHJhZGl1cykge1xuICAgICAgICB2YXIgcGFyZW50ID0gdGhpc1xuICAgICAgICB2YXIgcmFkID0gKHJhZGl1cykgPyByYWRpdXMgOiA1XG4gICAgICAgIGlmICghd2luZG93LmNvb3Jkcykge1xuICAgICAgICAgICAgbmF2aWdhdG9yLmdlb2xvY2F0aW9uLmdldEN1cnJlbnRQb3NpdGlvbihmdW5jdGlvbiAocG9zaXRpb24pIHtcbiAgICAgICAgICAgICAgICB2YXIgZmluZCA9IG51bGxcbiAgICAgICAgICAgICAgICBpZiAocGFyZW50LmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBjb29yZHMgPSB7bGF0OiBwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUsIGxuZzogcG9zaXRpb24uY29vcmRzLmxvbmdpdHVkZX1cbiAgICAgICAgICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjb29yZHM6IGNvb3Jkc30sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbnMuZmluZERyaW5raW5nRm91bnRhaW4oY29vcmRzLCByYWQpXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZmluZCA9IG51bGxcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLy9wYXJlbnQuZHJhd01hcChjb29yZHMpXG4gICAgICAgICAgICAgICAgLypnb29nbGVVdGlscy5tYXBzLnJldmVyc2VHZW9jb2RpbmcocG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlLFxuICAgICAgICAgICAgICAgICBmdW5jdGlvbiAoYWRkcmVzcykge1xuICAgICAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe2N1cnJlbnRBZGRyZXNzOiBhZGRyZXNzLCBjdXJyZW50U3RhdGU6IHBhcmVudC5DdXJyZW50U3RhdGUuUkVBRFl9KVxuICAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgYWxlcnQoZXJyb3IpXG4gICAgICAgICAgICAgICAgIH0pKi9cbiAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgYWxlcnQoZXJyb3IubWVzc2FnZSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCB7ZW5hYmxlSGlnaEFjY3VyYWN5OiB0cnVlfSlcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7Y29vcmRzOiBjb29yZHN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgYWN0aW9ucy5maW5kRHJpbmtpbmdGb3VudGFpbih3aW5kb3cuY29vcmRzLCByYWQpXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG5cbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRVcENvbXBvbmVudCgpXG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZFVwZGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCkge1xuICAgICAgICAgICAgLy90aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUpXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMKSB7XG4gICAgICAgICAgICByZXR1cm4gKFJlYWN0LmNyZWF0ZUVsZW1lbnQoQ3ViZUdyaWQsIHt0ZXh0OiBcIkxvYWRpbmcgLi4uXCJ9KSlcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWSkge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiczEyXCIsIGlkOiBcInNlYXJjaC13cmFwcGVyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcIm5hdlwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJuYXYtd3JhcHBlciB0by1nby1uYXZcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3dcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMVwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaDVcIiwgbnVsbCwgXCJXaGVyZSB0byBnbyBuZXh0P1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTFcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImlucHV0LWZpZWxkXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW5wdXRcIiwge3ZhbHVlOiBcIkN1cnJlbnQgTG9jYXRpb25cIiwgdHlwZTogXCJzZWFyY2hcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIiwgbnVsbCwgUmVhY3QuY3JlYXRlRWxlbWVudChcImlcIiwge2NsYXNzTmFtZTogXCJcIn0sIFwiU3RhcnRcIikpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMVwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiaW5wdXQtZmllbGRcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7aWQ6IFwicGxhY2Utc2VhcmNoXCIsIHR5cGU6IFwic2VhcmNoXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImxhYmVsXCIsIG51bGwsIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpXCIsIHtjbGFzc05hbWU6IFwiXCJ9LCBcIkVuZFwiKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEyXCIsIGlkOiBcImdvb2dsZS1tYXBcIiwgcmVmOiBcImdvb2dsZU1hcFwifSlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuRklORElORykge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFNwaW5uZXJXYXZlLCB7dGV4dDogXCJGaW5kaW5nIFBvZHMgUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID0gdGhpcy5DdXJyZW50U3RhdGUuUkVTVUxUKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRG9uYXRpb25SZXN1bHQsIHtkb25hdGlvbnM6IHRoaXMuc3RhdGUuZG9uYXRpb25zfSlcbiAgICAgICAgICAgIClcbiAgICAgICAgfVxuICAgIH1cbn0pXG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjMvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdmFsdWU6ICh0aGlzLnByb3BzLnZhbHVlKT8gdGhpcy5wcm9wcy52YWx1ZSA6IDBcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHRoaXMuYWN0aXZhdGVTbGlkZXIoKVxuICAgIH0sXG5cbiAgICBzZXRWYWx1ZTogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dmFsdWU6IHZhbHVlfSlcbiAgICB9LFxuXG4gICAgYWN0aXZhdGVTbGlkZXI6ICBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIG1pbiA9ICh0aGlzLnByb3BzLm1pbik/IHRoaXMucHJvcHMubWluIDogMFxuICAgICAgICB2YXIgbWF4ID0gKHRoaXMucHJvcHMubWF4KT8gdGhpcy5wcm9wcy5tYXggOiA1MFxuICAgICAgICB2YXIgbm9kZSA9ICQodGhpcy5yZWZzLnNsaWRlci5nZXRET01Ob2RlKCkpXG4gICAgICAgIG5vZGUubm9VaVNsaWRlcih7XG4gICAgICAgICAgICBzdGFydDogdGhpcy5zdGF0ZS52YWx1ZSxcbiAgICAgICAgICAgIGNvbm5lY3Q6IFwibG93ZXJcIixcbiAgICAgICAgICAgIG9yaWVudGF0aW9uOiBcImhvcml6b250YWxcIixcbiAgICAgICAgICAgIGRpcmVjdGlvbjogXCJsdHJcIixcbiAgICAgICAgICAgIGJlaGF2aW91cjogXCJ0YXAtZHJhZ1wiLFxuICAgICAgICAgICAgcmFuZ2U6IHtcbiAgICAgICAgICAgICAgICAnbWluJzogbWluLFxuICAgICAgICAgICAgICAgICdtYXgnOiBtYXhcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBmb3JtYXQ6IHdOdW1iKHtcbiAgICAgICAgICAgICAgICBkZWNpbWFsczogMFxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSk7XG4gICAgICAgIHZhciBwYXJlbnQgPSB0aGlzXG4gICAgICAgIG5vZGUub24oJ3NsaWRlJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBwYXJlbnQuc2V0VmFsdWUoJCh0aGlzKS52YWwoKSlcbiAgICAgICAgfSlcblx0bm9kZS5vbignY2hhbmdlJywgZnVuY3Rpb24gKCkge1xuXHQgICAgaWYgKHBhcmVudC5wcm9wcy5zbGlkZXJSZWxlYXNlKSB7XG5cdFx0cGFyZW50LnByb3BzLnNsaWRlclJlbGVhc2UoJCh0aGlzKS52YWwoKSlcblx0ICAgIH1cblx0fSlcbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciB1bml0ID0gKHRoaXMucHJvcHMudW5pdCk/IFwiIFwiICt0aGlzLnByb3BzLnVuaXQgOiBcIlwiXG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMiBzbGlkZXItY29udGFpbmVyXCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2xpZGVyIGNvbCBzMTBcIiwgcmVmOiBcInNsaWRlclwifSksIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczIgc2xpZGVyLXZhbHVlXCJ9LCB0aGlzLnN0YXRlLnZhbHVlICsgdW5pdClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pXG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjAvMDUvMTUuXG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMiBtOCBvZmZzZXQtbTIgbDYgb2Zmc2V0LWwzXCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY2FyZC1wYW5lbCBncmV5IGxpZ2h0ZW4tNSB6LWRlcHRoLTFcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicm93IHZhbGlnbi13cmFwcGVyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogdGhpcy5wcm9wcy5kb25hdGlvbi5wcm9maWxlSW1nLCBhbHQ6IFwiUHJvZmlsZSBQaWN0dXJlXCIsIGNsYXNzTmFtZTogXCJjaXJjbGUgcmVzcG9uc2l2ZS1pbWdcIn0pXG4gICAgICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEwXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwge2NsYXNzTmFtZTogXCJibGFjay10ZXh0XCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5kb25hdGlvbi5kZXNjcmlwdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIwLzA1LzE1LlxuICovXG5cbnZhciBEb25hdGlvbkNhcmQgPSByZXF1aXJlKFwiLi9Eb25hdGlvbkNhcmRcIilcblxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGRvbmF0aW9uczogKHRoaXMucHJvcHMuZG9uYXRpb25zKT8gdGhpcy5wcm9wcy5kb25hdGlvbnMgOiBbXVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHM6IGZ1bmN0aW9uKG5leHRQcm9wcykge1xuICAgICAgICBpZiAobmV4dFByb3BzLmRvbmF0aW9ucyAmJiBKU09OLnN0cmluZ2lmeShuZXh0UHJvcHMuZG9uYXRpb25zKSAhPSBKU09OLnN0cmluZ2lmeSh0aGlzLnByb3BzLmRvbmF0aW9ucykpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2RvbmF0aW9uczogbmV4dFByb3BzLmRvbmF0aW9uc30pXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZG9uYXRpb25zLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJoM1wiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgXCJObyBkb25hdGlvbiByZXF1ZXN0cyBhcmUgZm91bmQgYXJvdW5kIHlvdXIgbG9jYXRpb24hXCJcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJidXNrZXItbGlzdFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5kb25hdGlvbnMubWFwKGZ1bmN0aW9uKGl0ZW0sIGlkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChEb25hdGlvbkNhcmQsIHtkb25hdGlvbjogaXRlbSwga2V5OiBpZH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICB9XG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjMvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgc2VhcmNoQm94OiB1bmRlZmluZWRcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtzZWFyY2hCb3g6IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuU2VhcmNoQm94KHRoaXMucmVmcy5pbnB1dC5nZXRET01Ob2RlKCkpfSlcbiAgICB9LFxuXG4gICAgZ2V0VmFsdWU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVmcy5pbnB1dC5nZXRET01Ob2RlKCkudmFsdWVcbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicm93XCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiaW5wdXQtZmllbGQgY29sIHMxMlwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7dHlwZTogXCJ0ZXh0XCIsIHBsYWNlaG9sZGVyOiBcIkNob29zZSBhIGxvY2F0aW9uXCIsIGNsYXNzTmFtZTogXCJ2YWxpZGF0ZVwiLCByZWY6IFwiaW5wdXRcIiwgZGVmYXVsdFZhbHVlOiB0aGlzLnByb3BzLmluaXRBZGRyZXNzfSlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMy8wNS8xNS5cbiAqL1xudmFyIFNsaWRlciA9IHJlcXVpcmUoXCIuLi9idXR0b25zL1NsaWRlclwiKVxudmFyIExvY2F0aW9uSW5wdXQgPSByZXF1aXJlKFwiLi9Mb2NhdGlvbklucHV0XCIpXG5cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcblxuICAgIGNvbXBvbmVudFdpbGxVcGRhdGU6IGZ1bmN0aW9uIChuZXh0UHJvcHMpIHtcbiAgICAgICAgaWYgKG5leHRQcm9wcy5pbml0QWRkcmVzcyAmJiBuZXh0UHJvcHMuaW5pdEFkZHJlc3MgIT0gdGhpcy5wcm9wcy5pbml0QWRkcmVzcykge1xuICAgICAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2VcbiAgICB9LFxuXG4gICAgZ2V0VmFsdWU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGFkZHJlc3M6IHRoaXMucmVmcy5hZGRyZXNzLmdldFZhbHVlKCksXG4gICAgICAgICAgICByYWRpdXM6IHRoaXMucmVmcy5yYWRpdXMuc3RhdGUudmFsdWVcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3dcIiwgaWQ6IFwibG9jYXRpb24tc2V0dGluZ1wifSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImZvcm1cIiwge2NsYXNzTmFtZTogXCJjb2wgczEyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInJvd1wifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KExvY2F0aW9uSW5wdXQsIHtpbml0QWRkcmVzczogdGhpcy5wcm9wcy5pbml0QWRkcmVzcywgcmVmOiBcImFkZHJlc3NcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEyIGxhYmVsXCJ9LCBcIlJhZGl1c1wiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFNsaWRlciwge3JlZjogXCJyYWRpdXNcIiwgdmFsdWU6IDEwLCB1bml0OiBcImttXCJ9KVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvZ25uaHUgb24gMjYvMDkvMTUuXG4gKi9cbnZhciBnQXBpID0gcmVxdWlyZShcIi4uLy4uL3V0aWxzL2dvb2dsZVwiKVxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgZ2V0TWFya2VyczogZnVuY3Rpb24gKGN1cnJlbnRMb2NhdGlvbiwgcG9kcywgZG9tLCByYWRpdXMsIGljb24pIHtcbiAgICAgICAgaWYgKCF3aW5kb3cubWFwKSB7XG4gICAgICAgICAgICB3aW5kb3cubWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcChkb20sIHtcbiAgICAgICAgICAgICAgICBjZW50ZXI6IGN1cnJlbnRMb2NhdGlvbixcbiAgICAgICAgICAgICAgICB6b29tOiBjb25maWcuZ29vZ2xlTWFwLnpvb20sXG4gICAgICAgICAgICAgICAgbWFwVHlwZUlkOiBnb29nbGUubWFwcy5NYXBUeXBlSWQuVEVSUkFJTixcbiAgICAgICAgICAgICAgICBtYXBUeXBlQ29udHJvbDogZmFsc2VcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGdvb2dsZS5tYXBzLnZpc3VhbFJlZnJlc2ggPSB0cnVlO1xuICAgICAgICB2YXIgY3VycmVudExvY2F0aW9uTWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICBwb3NpdGlvbjogY3VycmVudExvY2F0aW9uLFxuICAgICAgICAgICAgdGl0bGU6ICdZb3UgYXJlIGhlcmUnLFxuICAgICAgICAgICAgaWNvbjogY29uZmlnLnVzZXJfY3VycmVudF9sb2NhdGlvbl9pY29uXG4gICAgICAgIH0pO1xuICAgICAgICBjdXJyZW50TG9jYXRpb25NYXJrZXIuc2V0TWFwKG1hcCk7XG4gICAgICAgIHZhciBjdXJyZW50TG9jYXRpb25JbmZvV2luZG93ID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3coe1xuICAgICAgICAgICAgY29udGVudDogXCI8cD5Zb3UgYXJlIGhlcmUhPC9wPlwiXG4gICAgICAgIH0pO1xuICAgICAgICBjdXJyZW50TG9jYXRpb25NYXJrZXIuYWRkTGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY3VycmVudExvY2F0aW9uSW5mb1dpbmRvdy5vcGVuKG1hcCwgY3VycmVudExvY2F0aW9uTWFya2VyKTtcbiAgICAgICAgfSlcblxuICAgICAgICBwb2RzLmZvckVhY2goZnVuY3Rpb24gKHBvZCwgaWQpIHtcbiAgICAgICAgICAgIHZhciBjb29yZGluYXRlcyA9IHBvZC5jb29yZGluYXRlcy5jb29yZGluYXRlc1xuICAgICAgICAgICAgdmFyIG1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiB7XG4gICAgICAgICAgICAgICAgICAgIGxhdDogcGFyc2VGbG9hdChjb29yZGluYXRlc1tcImxhdFwiXSksXG4gICAgICAgICAgICAgICAgICAgIGxuZzogcGFyc2VGbG9hdChjb29yZGluYXRlc1tcImxvblwiXSlcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGljb246IChpY29uKSA/IGljb24gOiBjb25maWcuYmlrZV9pY29uXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgbWFya2VyLmFkZExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB3aW5kb3cucG9wdXBQb2REZXRhaWxzKHBvZClcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBtYXJrZXIuc2V0TWFwKG1hcClcbiAgICAgICAgfSlcbiAgICAgICAgbmV3IGdvb2dsZS5tYXBzLkNpcmNsZSh7XG4gICAgICAgICAgICBzdHJva2VDb2xvcjogJ2dyZWVuJyxcbiAgICAgICAgICAgIHN0cm9rZU9wYWNpdHk6IDAuOCxcbiAgICAgICAgICAgIHN0cm9rZVdlaWdodDogMixcbiAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAwLjAsXG4gICAgICAgICAgICBtYXA6IG1hcCxcbiAgICAgICAgICAgIGNlbnRlcjogY3VycmVudExvY2F0aW9uLFxuICAgICAgICAgICAgcmFkaXVzOiAxMDAwXG4gICAgICAgIH0pO1xuICAgIH0sXG5cbiAgICBpbml0U2VhcmNoOiBmdW5jdGlvbiAoY3VycmVudExvY2F0aW9uLCBkb20sIGRhdGEpIHtcbiAgICAgICAgdmFyIG1hcmtlcnMgPSBbXVxuICAgICAgICBpZiAoIXdpbmRvdy5tYXApIHtcbiAgICAgICAgICAgIHdpbmRvdy5tYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGRvbSwge1xuICAgICAgICAgICAgICAgIGNlbnRlcjogY3VycmVudExvY2F0aW9uLFxuICAgICAgICAgICAgICAgIHpvb206IGNvbmZpZy5nb29nbGVNYXAuem9vbSxcbiAgICAgICAgICAgICAgICBtYXBUeXBlSWQ6IGdvb2dsZS5tYXBzLk1hcFR5cGVJZC5URVJSQUlOLFxuICAgICAgICAgICAgICAgIG1hcFR5cGVDb250cm9sOiBmYWxzZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgZ29vZ2xlLm1hcHMudmlzdWFsUmVmcmVzaCA9IHRydWU7XG4gICAgICAgIHZhciBjdXJyZW50TG9jYXRpb25NYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBjdXJyZW50TG9jYXRpb24sXG4gICAgICAgICAgICB0aXRsZTogJ1lvdSBhcmUgaGVyZScsXG4gICAgICAgICAgICBpY29uOiBjb25maWcudXNlcl9jdXJyZW50X2xvY2F0aW9uX2ljb25cbiAgICAgICAgfSk7XG4gICAgICAgIGN1cnJlbnRMb2NhdGlvbk1hcmtlci5zZXRNYXAobWFwKTtcbiAgICAgICAgdmFyIGN1cnJlbnRMb2NhdGlvbkluZm9XaW5kb3cgPSBuZXcgZ29vZ2xlLm1hcHMuSW5mb1dpbmRvdyh7XG4gICAgICAgICAgICBjb250ZW50OiBcIjxwPllvdSBhcmUgaGVyZSE8L3A+XCJcbiAgICAgICAgfSk7XG4gICAgICAgIGN1cnJlbnRMb2NhdGlvbk1hcmtlci5hZGRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBjdXJyZW50TG9jYXRpb25JbmZvV2luZG93Lm9wZW4obWFwLCBjdXJyZW50TG9jYXRpb25NYXJrZXIpO1xuICAgICAgICB9KVxuXG4gICAgICAgIGRhdGEuaXRlbXMuZm9yRWFjaChmdW5jdGlvbiAocG9pbnQpIHtcbiAgICAgICAgICAgIHZhciBjb29yZGluYXRlcyA9IHBvaW50LmNvb3JkaW5hdGVzLmNvb3JkaW5hdGVzXG4gICAgICAgICAgICB2YXIgbU9wdHMgPSB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHtcbiAgICAgICAgICAgICAgICAgICAgbGF0OiBwYXJzZUZsb2F0KGNvb3JkaW5hdGVzW1wibGF0XCJdKSxcbiAgICAgICAgICAgICAgICAgICAgbG5nOiBwYXJzZUZsb2F0KGNvb3JkaW5hdGVzW1wibG9uXCJdKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChkYXRhLmljb24pIHtcbiAgICAgICAgICAgICAgICBtT3B0c1tcImljb25cIl0gPSBkYXRhLmljb25cblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIG1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIobU9wdHMpXG4gICAgICAgICAgICBtYXJrZXIuc2V0TWFwKG1hcClcbiAgICAgICAgICAgIG1hcmtlcnMucHVzaChtYXJrZXIpXG4gICAgICAgIH0pXG4gICAgICAgIG5ldyBnb29nbGUubWFwcy5DaXJjbGUoe1xuICAgICAgICAgICAgc3Ryb2tlQ29sb3I6ICdncmVlbicsXG4gICAgICAgICAgICBzdHJva2VPcGFjaXR5OiAwLjgsXG4gICAgICAgICAgICBzdHJva2VXZWlnaHQ6IDIsXG4gICAgICAgICAgICBmaWxsT3BhY2l0eTogMC4wLFxuICAgICAgICAgICAgbWFwOiBtYXAsXG4gICAgICAgICAgICBjZW50ZXI6IGN1cnJlbnRMb2NhdGlvbixcbiAgICAgICAgICAgIHJhZGl1czogMTAwMFxuICAgICAgICB9KTtcbiAgICAgICAgdmFyIHNlYXJjaFBsYWNlc01hcmtlcnMgPSBbXVxuICAgICAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3BsYWNlLXNlYXJjaCcpKSB7XG4gICAgICAgICAgICAvLyBDcmVhdGUgdGhlIHNlYXJjaCBib3ggYW5kIGxpbmsgaXQgdG8gdGhlIFVJIGVsZW1lbnQuXG4gICAgICAgICAgICB2YXIgaW5wdXQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncGxhY2Utc2VhcmNoJyk7XG4gICAgICAgICAgICB2YXIgc2VhcmNoQm94ID0gbmV3IGdvb2dsZS5tYXBzLnBsYWNlcy5TZWFyY2hCb3goaW5wdXQpO1xuICAgICAgICAgICAgLy9tYXAuY29udHJvbHNbZ29vZ2xlLm1hcHMuQ29udHJvbFBvc2l0aW9uLlRPUF9MRUZUXS5wdXNoKGlucHV0KTtcblxuICAgICAgICAgICAgdmFyIGRpcmVjdGlvbnNEaXNwbGF5ID0gbmV3IGdvb2dsZS5tYXBzLkRpcmVjdGlvbnNSZW5kZXJlcih7XG4gICAgICAgICAgICAgICAgcHJlc2VydmVWaWV3cG9ydDogdHJ1ZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgZGlyZWN0aW9uc0Rpc3BsYXkuc2V0TWFwKG1hcClcbiAgICAgICAgICAgIHNlYXJjaEJveC5hZGRMaXN0ZW5lcigncGxhY2VzX2NoYW5nZWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIHBsYWNlcyA9IHNlYXJjaEJveC5nZXRQbGFjZXMoKTtcblxuICAgICAgICAgICAgICAgIGlmIChwbGFjZXMubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBzZWFyY2hQbGFjZXNNYXJrZXJzLmZvckVhY2goZnVuY3Rpb24gKG1hcmtlcikge1xuICAgICAgICAgICAgICAgICAgICBtYXJrZXIuc2V0TWFwKG51bGwpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAvLyBGb3IgZWFjaCBwbGFjZSwgZ2V0IHRoZSBpY29uLCBuYW1lIGFuZCBsb2NhdGlvbi5cbiAgICAgICAgICAgICAgICB2YXIgYm91bmRzID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZ0JvdW5kcygpO1xuICAgICAgICAgICAgICAgIHBsYWNlcy5mb3JFYWNoKGZ1bmN0aW9uIChwbGFjZSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIXBsYWNlLmdlb21ldHJ5KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlJldHVybmVkIHBsYWNlIGNvbnRhaW5zIG5vIGdlb21ldHJ5XCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHZhciBpY29uID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiBwbGFjZS5pY29uLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoNzEsIDcxKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG9yaWdpbjogbmV3IGdvb2dsZS5tYXBzLlBvaW50KDAsIDApLFxuICAgICAgICAgICAgICAgICAgICAgICAgYW5jaG9yOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMTcsIDM0KSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjYWxlZFNpemU6IG5ldyBnb29nbGUubWFwcy5TaXplKDI1LCAyNSlcbiAgICAgICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBDcmVhdGUgYSBtYXJrZXIgZm9yIGVhY2ggcGxhY2UuXG4gICAgICAgICAgICAgICAgICAgIHZhciBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcDogbWFwLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IHBsYWNlLm5hbWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcGxhY2UuZ2VvbWV0cnkubG9jYXRpb25cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoUGxhY2VzTWFya2Vycy5wdXNoKG1hcmtlcilcbiAgICAgICAgICAgICAgICAgICAgdmFyIG1Mb2NhdGlvbkluZm8gPSBuZXcgZ29vZ2xlLm1hcHMuSW5mb1dpbmRvdyh7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50OiBcIjxwPlwiICsgcGxhY2UubmFtZSArIFwiPC9wPlwiXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBtYXJrZXIuYWRkTGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbUxvY2F0aW9uSW5mby5vcGVuKG1hcCwgbWFya2VyKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgICAgICAgICBpZiAocGxhY2UuZ2VvbWV0cnkudmlld3BvcnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIE9ubHkgZ2VvY29kZXMgaGF2ZSB2aWV3cG9ydC5cbiAgICAgICAgICAgICAgICAgICAgICAgIGJvdW5kcy51bmlvbihwbGFjZS5nZW9tZXRyeS52aWV3cG9ydCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBib3VuZHMuZXh0ZW5kKHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgICAgICAgICAgZ0FwaS5tYXBzLmdldERpcihjdXJyZW50TG9jYXRpb24sIHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLCBmdW5jdGlvbiAoZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3Rpb25zRGlzcGxheS5zZXREaXJlY3Rpb25zKGRpcmVjdGlvbilcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICBjdXJyZW50TG9jYXRpb25NYXJrZXIuc2V0TWFwKG51bGwpXG4gICAgICAgICAgICAgICAgbWFwLmZpdEJvdW5kcyhib3VuZHMpO1xuXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cbn1cbiIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMC8wNS8xNS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic3Bpbm5lci13cmFwcGVyXCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic3Bpbm5lci1jZWxsXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXNwaW5uZXIgc2stc3Bpbm5lci13YXZlXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1yZWN0MVwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXJlY3QyXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stcmVjdDNcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1yZWN0NFwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXJlY3Q1XCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCB0aGlzLnByb3BzLnRleHQpXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMy8wNS8xNS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNwaW5uZXItd3JhcHBlclwifSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNwaW5uZXItY2VsbFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1zcGlubmVyIHNrLXNwaW5uZXItdGhyZWUtYm91bmNlXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1ib3VuY2UxXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stYm91bmNlMlwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLWJvdW5jZTNcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwXCIsIG51bGwsIHRoaXMucHJvcHMudGV4dClcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLypcbiAqIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiAqIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuICogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiAqIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiAqIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiAqIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuICogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiAqIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4gKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuICogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4gKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4gKiB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG52YXIgcm91dGVyID0gcmVxdWlyZShcIi4vcm91dGVyL3JvdXRlclwiKVxuXG52YXIgYXBwID0ge1xuICAgIC8vIEFwcGxpY2F0aW9uIENvbnN0cnVjdG9yXG4gICAgaW5pdGlhbGl6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgIHRoaXMuYmluZEV2ZW50cygpO1xuICAgIH0sXG4gICAgLy8gQmluZCBFdmVudCBMaXN0ZW5lcnNcbiAgICAvL1xuICAgIC8vIEJpbmQgYW55IGV2ZW50cyB0aGF0IGFyZSByZXF1aXJlZCBvbiBzdGFydHVwLiBDb21tb24gZXZlbnRzIGFyZTpcbiAgICAvLyAnbG9hZCcsICdkZXZpY2VyZWFkeScsICdvZmZsaW5lJywgYW5kICdvbmxpbmUnLlxuICAgIGJpbmRFdmVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdkZXZpY2VyZWFkeScsIHRoaXMub25EZXZpY2VSZWFkeSwgZmFsc2UpO1xuICAgIH0sXG4gICAgLy8gZGV2aWNlcmVhZHkgRXZlbnQgSGFuZGxlclxuICAgIC8vXG4gICAgLy8gVGhlIHNjb3BlIG9mICd0aGlzJyBpcyB0aGUgZXZlbnQuIEluIG9yZGVyIHRvIGNhbGwgdGhlICdyZWNlaXZlZEV2ZW50J1xuICAgIC8vIGZ1bmN0aW9uLCB3ZSBtdXN0IGV4cGxpY2l0bHkgY2FsbCAnYXBwLnJlY2VpdmVkRXZlbnQoLi4uKTsnXG4gICAgb25EZXZpY2VSZWFkeTogZnVuY3Rpb24oKSB7XG4gICAgICAgIC8vYXBwLnJlY2VpdmVkRXZlbnQoJ2RldmljZXJlYWR5Jyk7XG4gICAgICAgICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcm91dGVyLmluaXQoXCIvZmluZF9kb25hdGlvblwiKVxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAkKFwiLnNwbGFzaC1zY3JlZW5cIikuZmFkZU91dCg1MDApXG4gICAgICAgICAgICB9LCAzMDAwKVxuICAgICAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImJhY2tidXR0b25cIiwgb25CYWNrS2V5RG93biwgZmFsc2UpO1xuICAgICAgICAgICAgZnVuY3Rpb24gb25CYWNrS2V5RG93bigpIHtcbiAgICAgICAgICAgICAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9kLWRldGFpbHNcIikpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgfVxufTtcblxuYXBwLmluaXRpYWxpemUoKTtcbiIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMC8wNS8xNS5cbiAqL1xudmFyIEZpbmRCaWtlUG9kID0gcmVxdWlyZShcIi4uL2NvbXBvbmVudHMvRmluZEJpa2VQb2QuanNcIilcbnZhciBQb3N0UmVxdWVzdCA9IHJlcXVpcmUoXCIuLi9jb21wb25lbnRzL1Bvc3RSZXF1ZXN0XCIpXG52YXIgV2hlcmVUb0dvID0gcmVxdWlyZShcIi4uL2NvbXBvbmVudHMvV2hlcmVUb0dvXCIpXG52YXIgU2lkZU5hdiA9IHJlcXVpcmUoXCIuLi9jb21wb25lbnRzL1NpZGVOYXZcIilcbndpbmRvdy5yZW5kZXJGaW5kRG9uYXRpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgd2luZG93Lm1hcCA9IG51bGxcbiAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFpbi1jb250ZW50XCIpKVxuICAgICQoXCIjbmF2LWJhclwiKS5zaG93KClcbiAgICAgICAgJChcIi5tb2RhbFwiKS5oaWRlKClcblxuICAgIFJlYWN0LnJlbmRlcihSZWFjdC5jcmVhdGVFbGVtZW50KFNpZGVOYXYsIHtkaXNwYXRjaGVyOiBkaXNwYXRjaGVyfSksIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibmF2LWJhclwiKSlcbiAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwb2QtZGV0YWlsc1wiKSkge1xuICAgICAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9kLWRldGFpbHNcIikpXG4gICAgfVxuICAgIFJlYWN0LnJlbmRlcihSZWFjdC5jcmVhdGVFbGVtZW50KEZpbmRCaWtlUG9kLCB7Y3VycmVudFN0YXRlOiBcImluaXRpYWxcIiwgZGlzcGF0Y2hlcjogZGlzcGF0Y2hlcn0pLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbn1cblxud2luZG93LnJlbmRlcldoZXJlR28gPSBmdW5jdGlvbiAoKSB7XG4gICAgd2luZG93Lm1hcCA9IG51bGxcbiAgICAkKFwiI25hdi1iYXJcIikuaGlkZSgpXG4gICAgJChcIi5tb2RhbFwiKS5oaWRlKClcbiAgICAkKFwiLmxlYW4tb3ZlcmxheVwiKS5oaWRlKClcbiAgICAkKFwiI2xlYW4tb3ZlcmxheVwiKS5oaWRlKClcblxuICAgIFJlYWN0LnVubW91bnRDb21wb25lbnRBdE5vZGUoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYWluLWNvbnRlbnRcIikpXG4gICAgUmVhY3QudW5tb3VudENvbXBvbmVudEF0Tm9kZShkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBvZC1kZXRhaWxzXCIpKVxuICAgIFJlYWN0LnJlbmRlcihSZWFjdC5jcmVhdGVFbGVtZW50KFdoZXJlVG9Hbywge2N1cnJlbnRTdGF0ZTogXCJpbml0aWFsXCIsIGRpc3BhdGNoZXI6IGRpc3BhdGNoZXJ9KSwgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYWluLWNvbnRlbnRcIikpXG59XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uKHBhdGgpIHtcbiAgICAgICAgdmFyIHJvdXRlcyA9IHtcbiAgICAgICAgICAgIFwiL2ZpbmRfZG9uYXRpb25cIjogcmVuZGVyRmluZERvbmF0aW9uLFxuICAgICAgICAgICAgXCIvd2hlcmUtZ29cIjogcmVuZGVyV2hlcmVHb1xuICAgICAgICB9XG4gICAgICAgIHdpbmRvdy5yb3V0ZXIgPSBSb3V0ZXIocm91dGVzKTtcbiAgICAgICAgcm91dGVyLmluaXQocGF0aClcbiAgICB9XG59IiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIzLzA1LzE1LlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBtYXBzOiB7XG4gICAgICAgIGNoZWNrQ29ubmVjdGlvbjogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIG5ldHdvcmtTdGF0ZSA9IG5hdmlnYXRvci5jb25uZWN0aW9uLnR5cGVcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKG5ldHdvcmtTdGF0ZSlcbiAgICAgICAgICAgIGlmIChuZXR3b3JrU3RhdGUgPT0gQ29ubmVjdGlvbi5OT05FKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICB9LFxuXG4gICAgICAgIHJldmVyc2VHZW9jb2Rpbmc6IGZ1bmN0aW9uIChsYXQsIGxuZywgc3VjY2VzcywgZXJyb3IpIHtcbiAgICAgICAgICAgIHZhciBnZW9jb2RlciA9IG5ldyBnb29nbGUubWFwcy5HZW9jb2RlcjtcbiAgICAgICAgICAgIHZhciBsYXRsbmcgPSB7bGF0OiBwYXJzZUZsb2F0KGxhdCksIGxuZzogcGFyc2VGbG9hdChsbmcpfTtcbiAgICAgICAgICAgIGdlb2NvZGVyLmdlb2NvZGUoeydsb2NhdGlvbic6IGxhdGxuZ30sIGZ1bmN0aW9uIChyZXN1bHRzLCBzdGF0dXMpIHtcbiAgICAgICAgICAgICAgICBpZiAoc3RhdHVzID09PSBnb29nbGUubWFwcy5HZW9jb2RlclN0YXR1cy5PSykge1xuICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0c1swXSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzcyhyZXN1bHRzWzBdLmZvcm1hdHRlZF9hZGRyZXNzKVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3IoJ05vIHJlc3VsdHMgZm91bmQnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGVycm9yKCdHZW9jb2RlciBmYWlsZWQgZHVlIHRvOiAnICsgc3RhdHVzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBnZW9jb2Rpbmc6IGZ1bmN0aW9uIChhZGRyZXNzLCBzdWNjZXNzLCBlcnJvcikge1xuICAgICAgICAgICAgdmFyIGdlb2NvZGVyID0gbmV3IGdvb2dsZS5tYXBzLkdlb2NvZGVyO1xuICAgICAgICAgICAgZ2VvY29kZXIuZ2VvY29kZSh7J2FkZHJlc3MnOiBhZGRyZXNzfSwgZnVuY3Rpb24gKHJlc3VsdHMsIHN0YXR1cykge1xuICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPT09IGdvb2dsZS5tYXBzLkdlb2NvZGVyU3RhdHVzLk9LKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHRzWzBdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3VsdHNbMF0uZ2VvbWV0cnkpXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcignTm8gcmVzdWx0cyBmb3VuZCcpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IoJ0dlb2NvZGVyIGZhaWxlZCBkdWUgdG86ICcgKyBzdGF0dXMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldERpcjogZnVuY3Rpb24gKG9yaWdpbiwgZGVzdGluYXRpb24sIHN1Y2Nlc3MpIHtcbiAgICAgICAgICAgIHZhciBkaXJlY3Rpb25zU2VydmljZSA9IG5ldyBnb29nbGUubWFwcy5EaXJlY3Rpb25zU2VydmljZSgpO1xuICAgICAgICAgICAgdmFyIGRpcmVjdGlvbnNSZXF1ZXN0ID0ge1xuICAgICAgICAgICAgICAgIFwib3JpZ2luXCI6IG9yaWdpbltcImxhdFwiXSArIFwiLFwiICsgb3JpZ2luW1wibG5nXCJdLFxuICAgICAgICAgICAgICAgIFwiZGVzdGluYXRpb25cIjogb3JpZ2luW1wibGF0XCJdICsgXCIsXCIgKyBvcmlnaW5bXCJsbmdcIl0sXG4gICAgICAgICAgICAgICAgb3B0aW1pemVXYXlwb2ludHM6IHRydWUsXG4gICAgICAgICAgICAgICAgdHJhdmVsTW9kZTogJ0JJQ1lDTElORydcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBkaXJlY3Rpb25zU2VydmljZS5yb3V0ZShkaXJlY3Rpb25zUmVxdWVzdCwgZnVuY3Rpb24gKHJlc3BvbnNlLCBzdGF0dXMpIHtcbiAgICAgICAgICAgICAgICBpZiAoc3RhdHVzID09IGdvb2dsZS5tYXBzLkRpcmVjdGlvbnNTdGF0dXMuT0spIHtcbiAgICAgICAgICAgICAgICAgICAgLy9kbyB3b3JrIHdpdGggcmVzcG9uc2UgZGF0YVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSlcbiAgICAgICAgICAgICAgICAgICAgc3VjY2VzcyhyZXNwb25zZSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIC8vRXJyb3IgaGFzIG9jY3VyZWRcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IocmVzcG9uc2UpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcblxuICAgICAgICB9XG4gICAgfVxufSJdfQ==
