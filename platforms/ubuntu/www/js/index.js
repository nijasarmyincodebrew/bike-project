(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"/home/duongnhu/workspace/fe/src/js/components/FindBikePod.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            pods: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    findDonations: function () {
        var parent = this
        var value = parent.refs.location.getValue()
        actions.findDonations(value.address, value.radius)

        if (window.device.platform == "browser") {
            find()
            $(this.refs.findBtn.getDOMNode()).fadeOut(300, function () {
                parent.setState({
                    currentState: parent.CurrentState.FINDING
                })
            })
        } else {
            parent.setState({
                currentState: parent.CurrentState.FINDING
            })
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_POD) {
                if (storeState.success) {
                    this.setState({pods: storeState.pods}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.getMarkers(coords, this.state.pods, this.refs.googleMap.getDOMNode())
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        navigator.geolocation.getCurrentPosition(function (position) {
            var find = null
            if (parent.isMounted()) {
                var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                parent.setState({coords: coords}, function () {
                    actions.findDonations(coords, rad)
                })
            } else {
                find = null
            }
            //parent.drawMap(coords)
            /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
             function (address) {
             parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
             }, function (error) {
             alert(error)
             })*/
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, {enableHighAccuracy: true})
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            this.setUpComponent()
        }
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "s12", id: "search-wrapper"}, 
                    React.createElement("div", {className: "col s12", id: "google-map", ref: "googleMap"}), 
                    React.createElement("div", {className: "col s12 input-field"}, 
                      React.createElement("input", {type: "search"}), 
                      React.createElement("label", null, React.createElement("i", {className: "material-icons"}, "search")), 
                      React.createElement("i", {className: "material-icons"}, "close")
                    )
                )
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                React.createElement(SpinnerWave, {text: "Finding Pods Requests ..."})
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                React.createElement(DonationResult, {donations: this.state.donations})
            )
        }
    }
})

},{"./buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./find_bikepod/DonationResult":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js","./find_bikepod/LocationSetting":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js","./find_bikepod/MapMarker":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/PostRequest.js":[function(require,module,exports){
/**
 * Created by duognnhu on 27/09/15.
 */
var LocationInput = require("./find_bikepod/LocationInput")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
module.exports = React.createClass({displayName: "exports",
     mixins: [flux.mixins.storeListener],

    watchStores: ['PostRequestStore'],


    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            bloodType: "",
            time: "",
            currentAddress: "",
            organizationName: ""
        }
    },

     CurrentState: {
        POSTING: "posting",
        INITIAL: "initial",
        READY: 'READY'
    },

    storeDidChange: function (storeName) {
        console.log(storeName)

        if (this.isMounted()) {
            var store = dispatcher.getStore(storeName)
            var storeState = store.getState()
            if (storeState.success) {
                alert("Posted Successfully")
            } else {
                alert("Error Posting!")
            }
        }
    },

    setUpComponent: function () {
        console.log(this.state.currentState)
        var parent = this
        navigator.geolocation.getCurrentPosition(function (position) {
            googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                function (address) {
                    if (parent.isMounted()) {
                        parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                    }
                }, function (error) {
                    alert(error)
                })
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, { enableHighAccuracy: true })
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    onBloodTypeChange: function (e) {
        this.setState({bloodType: e.target.value})
    },

    onTimeChange: function (e) {
        this.setState({time: e.target.value})
    },

    onNameChange: function (e) {
        this.setState({organizationName: e.target.value})
    },

    postRequest: function () {
        var opts = {
            bloodType: this.state.bloodType,
            address: this.refs.address.getValue(),
            time: this.state.time,
            organizationName: this.state.organizationName
        }
        this.setState({currentState: this.CurrentState.POSTING })
        actions.postRequest(opts)
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "row card"}, 
                    React.createElement("h4", null, "Posting Request"), 
                    React.createElement("ul", {className: "collection"}, 
                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Organization Name:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onNameChange, ref: "name", type: "text", placeholder: "name"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/location_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Location:"), 
                            React.createElement(LocationInput, {initAddress: this.state.currentAddress, ref: "address"})
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Blood Type:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {ref: "bloodType", onChange: this.onBloodTypeChange, type: "text"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Time:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onTimeChange, ref: "time", placeholder: "MM/dd/yyyy HH:mm:ss", type: "text"})
                            )
                        )
                    ), 
                    React.createElement("div", {className: "form-footer right"}, 
                        React.createElement("a", {href: "javascript:void(0)", onClick: this.postRequest, className: "waves-effect waves-green teal lighten-2 btn"}, 
                            "Save"
                        ), 
                        React.createElement("a", {href: "javascript:void(0)", className: "waves-effect waves-green blue-grey btn"}, 
                            "Cancel"
                        )
                    )
                )
            )
        } else if (this.state.currentState == this.CurrentState.POSTING) {
            return (
                React.createElement(SpinnerWave, {text: "Posting Donation Requests ..."})
            )
        }
    }
})
},{"./find_bikepod/LocationInput":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/SideNav.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    componentDidMount: function() {
        $("#menu-btn").sideNav({
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });
    },

    render: function () {
        /**var links = [
            <li key={1}>
                <a href="#/find_donation" className="waves-effect waves-light">
                    <i className="ion-android-search"></i>
                    Find Donation Requests
                </a>
            </li>,
            <li key={2} className="divider"></li>,
            <li key={3}>
                <a href="#/post_request" className="waves-effect waves-light">
                    <i className="ion-android-unlock"></i>
                    Post Request
                </a>
            </li>,
        ]**/
        var links = []
        return (
            React.createElement("div", null, 
                React.createElement("a", {href: "javascript:void(0)", className: "brand-logo center"}, config.app_name), 
                /*<a href="javascript:void(0)" className="brand-logo right" id="logo"><img src="img/app-logo-96.png"/></a>*/
                React.createElement("ul", {className: "left hide-on-med-and-down"}, 
                    links
                ), 
                React.createElement("ul", {id: "slide-out", className: "side-nav"}, 
                    links
                ), 
                  React.createElement("a", {href: "#", "data-activates": "slide-out", className: "button-collapse", id: "menu-btn"}, React.createElement("i", {className: "mdi-navigation-menu"}))
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/WhereToGo.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            pods: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    findDonations: function () {
        var parent = this
        var value = parent.refs.location.getValue()
        actions.findDonations(value.address, value.radius)

        if (window.device.platform == "browser") {
            find()
            $(this.refs.findBtn.getDOMNode()).fadeOut(300, function () {
                parent.setState({
                    currentState: parent.CurrentState.FINDING
                })
            })
        } else {
            parent.setState({
                currentState: parent.CurrentState.FINDING
            })
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_POD) {
                if (storeState.success) {
                    this.setState({pods: storeState.pods}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.getMarkers(coords, this.state.pods, this.refs.googleMap.getDOMNode())
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        navigator.geolocation.getCurrentPosition(function (position) {
            var find = null
            if (parent.isMounted()) {
                var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                parent.setState({coords: coords}, function () {
                    actions.findDonations(coords, rad)
                })
            } else {
                find = null
            }
            //parent.drawMap(coords)
            /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
             function (address) {
             parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
             }, function (error) {
             alert(error)
             })*/
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, {enableHighAccuracy: true})
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            this.setUpComponent()
        }
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "s12", id: "search-wrapper"}, 
                    React.createElement("h5", null, "Where to go"), 
                    React.createElement("div", {className: "col s12", id: "google-map", ref: "googleMap"}), 
                    React.createElement("div", {className: "col s12 input-field"}, 
                      React.createElement("input", {type: "search"}), 
                      React.createElement("label", null, React.createElement("i", {className: "material-icons"}, "search")), 
                      React.createElement("i", {className: "material-icons"}, "close")
                    )
                )
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                React.createElement(SpinnerWave, {text: "Finding Pods Requests ..."})
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                React.createElement(DonationResult, {donations: this.state.donations})
            )
        }
    }
})

},{"./buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./find_bikepod/DonationResult":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js","./find_bikepod/LocationSetting":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js","./find_bikepod/MapMarker":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            value: (this.props.value)? this.props.value : 0
        }
    },

    componentDidMount: function() {
        this.activateSlider()
    },

    setValue: function(value) {
        this.setState({value: value})
    },

    activateSlider:  function() {
        var min = (this.props.min)? this.props.min : 0
        var max = (this.props.max)? this.props.max : 50
        var node = $(this.refs.slider.getDOMNode())
        node.noUiSlider({
            start: this.state.value,
            connect: "lower",
            orientation: "horizontal",
            direction: "ltr",
            behaviour: "tap-drag",
            range: {
                'min': min,
                'max': max
            },
            format: wNumb({
                decimals: 0
            })
        });
        var parent = this
        node.on('slide', function() {
            parent.setValue($(this).val())
        })
	node.on('change', function () {
	    if (parent.props.sliderRelease) {
		parent.props.sliderRelease($(this).val())
	    }
	})
    },

    render: function () {
        var unit = (this.props.unit)? " " +this.props.unit : ""
        return (
            React.createElement("div", {className: "col s12 slider-container"}, 
                React.createElement("div", {className: "slider col s10", ref: "slider"}), React.createElement("div", {className: "col s2 slider-value"}, this.state.value + unit)
            )
        )
    }
})

},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationCard.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

module.exports = React.createClass({displayName: "exports",

    render: function () {
        return (
            React.createElement("div", {className: "col s12 m8 offset-m2 l6 offset-l3"}, 
                React.createElement("div", {className: "card-panel grey lighten-5 z-depth-1"}, 
                    React.createElement("div", {className: "row valign-wrapper"}, 
                        React.createElement("div", {className: "col s2"}, 
                            React.createElement("img", {src: this.props.donation.profileImg, alt: "Profile Picture", className: "circle responsive-img"})
                        ), 
                        React.createElement("div", {className: "col s10"}, 
                            React.createElement("p", null), 
                            React.createElement("p", {className: "black-text"}, 
                                this.props.donation.description
                            )
                        )
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

var DonationCard = require("./DonationCard")

module.exports = React.createClass({displayName: "exports",

    getInitialState: function() {
        return {
            donations: (this.props.donations)? this.props.donations : []
        }
    },

    componentWillReceiveProps: function(nextProps) {
        if (nextProps.donations && JSON.stringify(nextProps.donations) != JSON.stringify(this.props.donations)) {
            this.setState({donations: nextProps.donations})
        }
    },

    render: function() {
        if (this.state.donations.length == 0) {
            return (
                React.createElement("h3", null, 
                    "No donation requests are found around your location!"
                )
            )
        } else {
            return (
                React.createElement("div", {className: "busker-list"}, 
                    
                        this.state.donations.map(function(item, id) {
                            return (
                                React.createElement(DonationCard, {donation: item, key: id})
                            )
                        })
                    
                )
            )
        }
    }
})
},{"./DonationCard":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationCard.js"}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            searchBox: undefined
        }
    },

    componentDidMount: function () {
        this.setState({searchBox: new google.maps.places.SearchBox(this.refs.input.getDOMNode())})
    },

    getValue: function () {
        return this.refs.input.getDOMNode().value
    },

    render: function () {
        return (
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "input-field col s12"}, 
                    React.createElement("input", {type: "text", placeholder: "Choose a location", className: "validate", ref: "input", defaultValue: this.props.initAddress})
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
var Slider = require("../buttons/Slider")
var LocationInput = require("./LocationInput")

module.exports = React.createClass({displayName: "exports",

    componentWillUpdate: function (nextProps) {
        if (nextProps.initAddress && nextProps.initAddress != this.props.initAddress) {
            return true
        }
        return false
    },

    getValue: function () {
        return {
            address: this.refs.address.getValue(),
            radius: this.refs.radius.state.value
        }
    },

    render: function () {
        return (
            React.createElement("div", {className: "row", id: "location-setting"}, 
                React.createElement("form", {className: "col s12"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement(LocationInput, {initAddress: this.props.initAddress, ref: "address"}), 
                        React.createElement("div", {className: "col s12 label"}, "Radius"), 
                        React.createElement(Slider, {ref: "radius", value: 10, unit: "km"})
                    )
                )
            )
        )
    }
})
},{"../buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./LocationInput":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js"}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js":[function(require,module,exports){
/**
 * Created by duognnhu on 26/09/15.
 */
module.exports = {
    getMarkers: function (currentLocation, pods, dom, radius, hea) {
        var map = new google.maps.Map(dom, {
            center: currentLocation,
            zoom: config.googleMap.zoom,
            mapTypeId: google.maps.MapTypeId.TERRAIN,
            mapTypeControl: false
        });
        google.maps.visualRefresh = true;
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here',
            icon: config.user_current_location_icon
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })

        pods.forEach(function (pod, id) {
            var coordinates = pod.coordinates.coordinates
            var marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(coordinates["lat"]),
                    lng: parseFloat(coordinates["lon"])
                },
                icon: config.bike_icon
            })
            marker.addListener('click', function () {
                window.popupPodDetails(pod)
            })
            marker.setMap(map)
        })
        new google.maps.Circle({
            strokeColor: 'green',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.0,
            map: map,
            center: map.getCenter(),
            radius: 1000
          });
    }
}

},{}],"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function() {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-wave"}, 
                        React.createElement("div", {className: "sk-rect1"}), 
                        React.createElement("div", {className: "sk-rect2"}), 
                        React.createElement("div", {className: "sk-rect3"}), 
                        React.createElement("div", {className: "sk-rect4"}), 
                        React.createElement("div", {className: "sk-rect5"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function () {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-three-bounce"}, 
                        React.createElement("div", {className: "sk-bounce1"}), 
                        React.createElement("div", {className: "sk-bounce2"}), 
                        React.createElement("div", {className: "sk-bounce3"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/index.js":[function(require,module,exports){
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var router = require("./router/router")
var SideNav = require("./components/SideNav")

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //app.receivedEvent('deviceready');
        $(document).ready(function() {
            router.init("/find_donation")
            React.render(React.createElement(SideNav, {dispatcher: dispatcher}), document.getElementById("nav-bar"))
            setTimeout(function() {
                $(".splash-screen").fadeOut(500)
            }, 3000)
            document.addEventListener("backbutton", onBackKeyDown, false);
            function onBackKeyDown() {
                React.unmountComponentAtNode(document.getElementById("pod-details"))
            }
        })
    }
};

app.initialize();

},{"./components/SideNav":"/home/duongnhu/workspace/fe/src/js/components/SideNav.js","./router/router":"/home/duongnhu/workspace/fe/src/js/router/router.js"}],"/home/duongnhu/workspace/fe/src/js/router/router.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
var FindBikePod = require("../components/FindBikePod.js")
var PostRequest = require("../components/PostRequest")
var WhereToGo = require("../components/WhereToGo")
window.renderFindDonation = function () {
    React.unmountComponentAtNode(document.getElementById("main-content"))
    if (document.getElementById("pod-details")) {
        React.unmountComponentAtNode(document.getElementById("pod-details"))
    }
    React.render(React.createElement(FindBikePod, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

window.renderWhereGo = function () {
    React.unmountComponentAtNode(document.getElementById("main-content"))
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(React.createElement(WhereToGo, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

module.exports = {
    init: function(path) {
        var routes = {
            "/find_donation": renderFindDonation,
            "/where-go": renderWhereGo
        }
        window.router = Router(routes);
        router.init(path)
    }
}
},{"../components/FindBikePod.js":"/home/duongnhu/workspace/fe/src/js/components/FindBikePod.js","../components/PostRequest":"/home/duongnhu/workspace/fe/src/js/components/PostRequest.js","../components/WhereToGo":"/home/duongnhu/workspace/fe/src/js/components/WhereToGo.js"}]},{},["/home/duongnhu/workspace/fe/src/js/index.js"])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvY29tcG9uZW50cy9GaW5kQmlrZVBvZC5qcyIsInNyYy9qcy9jb21wb25lbnRzL1Bvc3RSZXF1ZXN0LmpzIiwic3JjL2pzL2NvbXBvbmVudHMvU2lkZU5hdi5qcyIsInNyYy9qcy9jb21wb25lbnRzL1doZXJlVG9Hby5qcyIsInNyYy9qcy9jb21wb25lbnRzL2J1dHRvbnMvU2xpZGVyLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvZmluZF9iaWtlcG9kL0RvbmF0aW9uQ2FyZC5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfYmlrZXBvZC9Eb25hdGlvblJlc3VsdC5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfYmlrZXBvZC9Mb2NhdGlvbklucHV0LmpzIiwic3JjL2pzL2NvbXBvbmVudHMvZmluZF9iaWtlcG9kL0xvY2F0aW9uU2V0dGluZy5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfYmlrZXBvZC9NYXBNYXJrZXIuanMiLCJzcmMvanMvY29tcG9uZW50cy9sb2FkaW5nL1NwaW5uZXJXYXZlLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvbG9hZGluZy9UaHJlZUJvdW5jZS5qcyIsInNyYy9qcy9pbmRleC5qcyIsInNyYy9qcy9yb3V0ZXIvcm91dGVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMvSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdkRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMTAvMDUvMTUuXG4gKi9cbnZhciBTbGlkZXIgPSByZXF1aXJlKFwiLi9idXR0b25zL1NsaWRlclwiKVxudmFyIFNwaW5uZXJXYXZlID0gcmVxdWlyZShcIi4vbG9hZGluZy9TcGlubmVyV2F2ZVwiKVxudmFyIEN1YmVHcmlkID0gcmVxdWlyZShcIi4vbG9hZGluZy9UaHJlZUJvdW5jZVwiKVxudmFyIERvbmF0aW9uUmVzdWx0ID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL0RvbmF0aW9uUmVzdWx0XCIpXG52YXIgTG9jYXRpb25TZXR0aW5nID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL0xvY2F0aW9uU2V0dGluZ1wiKVxudmFyIE1hcE1hcmtlciA9IHJlcXVpcmUoXCIuL2ZpbmRfYmlrZXBvZC9NYXBNYXJrZXJcIilcbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICBtaXhpbnM6IFtmbHV4Lm1peGlucy5zdG9yZUxpc3RlbmVyXSxcblxuICAgIHdhdGNoU3RvcmVzOiBbJ0xvY2F0aW9uU3RvcmUnXSxcblxuICAgIEN1cnJlbnRTdGF0ZToge1xuICAgICAgICBGSU5ESU5HOiBcImZpbmRpbmdcIixcbiAgICAgICAgSU5JVElBTDogXCJpbml0aWFsXCIsXG4gICAgICAgIFJFQURZOiAnUkVBRFknLFxuICAgICAgICBSRVNVTFQ6IFwicmVzdWx0XCJcbiAgICB9LFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjdXJyZW50U3RhdGU6ICh0aGlzLnByb3BzLmN1cnJlbnRTdGF0ZSkgPyB0aGlzLnByb3BzLmN1cnJlbnRTdGF0ZSA6IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwsXG4gICAgICAgICAgICBwb2RzOiBbXSxcbiAgICAgICAgICAgIGNvb3Jkczoge31cbiAgICAgICAgfVxuICAgIH0sXG5cblxuICAgIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHM6IGZ1bmN0aW9uIChuZXh0UHJvcHMpIHtcbiAgICAgICAgaWYgKG5leHRQcm9wcy5jdXJyZW50U3RhdGUpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogbmV4dFByb3BzLmN1cnJlbnRTdGF0ZX0pXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgZmluZERvbmF0aW9uczogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgcGFyZW50ID0gdGhpc1xuICAgICAgICB2YXIgdmFsdWUgPSBwYXJlbnQucmVmcy5sb2NhdGlvbi5nZXRWYWx1ZSgpXG4gICAgICAgIGFjdGlvbnMuZmluZERvbmF0aW9ucyh2YWx1ZS5hZGRyZXNzLCB2YWx1ZS5yYWRpdXMpXG5cbiAgICAgICAgaWYgKHdpbmRvdy5kZXZpY2UucGxhdGZvcm0gPT0gXCJicm93c2VyXCIpIHtcbiAgICAgICAgICAgIGZpbmQoKVxuICAgICAgICAgICAgJCh0aGlzLnJlZnMuZmluZEJ0bi5nZXRET01Ob2RlKCkpLmZhZGVPdXQoMzAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLkZJTkRJTkdcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLkZJTkRJTkdcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgc3RvcmVEaWRDaGFuZ2U6IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICAgIGlmICh0aGlzLmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICB2YXIgc3RvcmUgPSBkaXNwYXRjaGVyLmdldFN0b3JlKG5hbWUpXG4gICAgICAgICAgICB2YXIgc3RvcmVTdGF0ZSA9IHN0b3JlLmdldFN0YXRlKClcblxuXG4gICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5hY3Rpb24gPT0gc3RvcmUuYWN0aW9ucy5GSU5EX1BPRCkge1xuICAgICAgICAgICAgICAgIGlmIChzdG9yZVN0YXRlLnN1Y2Nlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cG9kczogc3RvcmVTdGF0ZS5wb2RzfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kcmF3TWFwKHRoaXMuc3RhdGUuY29vcmRzKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIG5hdmlnYXRvci5ub3RpZmljYXRpb24uYWxlcnQoc3RvcmVTdGF0ZS5lcnJvck1lc3NhZ2UpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogdGhpcy5DdXJyZW50U3RhdGUuUkVBRFl9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBkcmF3TWFwOiBmdW5jdGlvbiAoY29vcmRzKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogdGhpcy5DdXJyZW50U3RhdGUuUkVBRFl9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBNYXBNYXJrZXIuZ2V0TWFya2Vycyhjb29yZHMsIHRoaXMuc3RhdGUucG9kcywgdGhpcy5yZWZzLmdvb2dsZU1hcC5nZXRET01Ob2RlKCkpXG4gICAgICAgIH0pXG4gICAgfSxcblxuICAgIHNldFVwQ29tcG9uZW50OiBmdW5jdGlvbiAocmFkaXVzKSB7XG4gICAgICAgIHZhciBwYXJlbnQgPSB0aGlzXG4gICAgICAgIHZhciByYWQgPSAocmFkaXVzKSA/IHJhZGl1cyA6IDVcbiAgICAgICAgbmF2aWdhdG9yLmdlb2xvY2F0aW9uLmdldEN1cnJlbnRQb3NpdGlvbihmdW5jdGlvbiAocG9zaXRpb24pIHtcbiAgICAgICAgICAgIHZhciBmaW5kID0gbnVsbFxuICAgICAgICAgICAgaWYgKHBhcmVudC5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgICAgIHZhciBjb29yZHMgPSB7bGF0OiBwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUsIGxuZzogcG9zaXRpb24uY29vcmRzLmxvbmdpdHVkZX1cbiAgICAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe2Nvb3JkczogY29vcmRzfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBhY3Rpb25zLmZpbmREb25hdGlvbnMoY29vcmRzLCByYWQpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZmluZCA9IG51bGxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vcGFyZW50LmRyYXdNYXAoY29vcmRzKVxuICAgICAgICAgICAgLypnb29nbGVVdGlscy5tYXBzLnJldmVyc2VHZW9jb2RpbmcocG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlLFxuICAgICAgICAgICAgIGZ1bmN0aW9uIChhZGRyZXNzKSB7XG4gICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjdXJyZW50QWRkcmVzczogYWRkcmVzcywgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICBhbGVydChlcnJvcilcbiAgICAgICAgICAgICB9KSovXG4gICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgaWYgKHBhcmVudC5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgICAgIGFsZXJ0KGVycm9yLm1lc3NhZ2UpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtlbmFibGVIaWdoQWNjdXJhY3k6IHRydWV9KVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkVXBkYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMKSB7XG4gICAgICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwpIHtcbiAgICAgICAgICAgIHJldHVybiAoUmVhY3QuY3JlYXRlRWxlbWVudChDdWJlR3JpZCwge3RleHQ6IFwiTG9hZGluZyAuLi5cIn0pKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzMTJcIiwgaWQ6IFwic2VhcmNoLXdyYXBwZXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMlwiLCBpZDogXCJnb29nbGUtbWFwXCIsIHJlZjogXCJnb29nbGVNYXBcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTIgaW5wdXQtZmllbGRcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7dHlwZTogXCJzZWFyY2hcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIiwgbnVsbCwgUmVhY3QuY3JlYXRlRWxlbWVudChcImlcIiwge2NsYXNzTmFtZTogXCJtYXRlcmlhbC1pY29uc1wifSwgXCJzZWFyY2hcIikpLCBcbiAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaVwiLCB7Y2xhc3NOYW1lOiBcIm1hdGVyaWFsLWljb25zXCJ9LCBcImNsb3NlXCIpXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuRklORElORykge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFNwaW5uZXJXYXZlLCB7dGV4dDogXCJGaW5kaW5nIFBvZHMgUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID0gdGhpcy5DdXJyZW50U3RhdGUuUkVTVUxUKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRG9uYXRpb25SZXN1bHQsIHtkb25hdGlvbnM6IHRoaXMuc3RhdGUuZG9uYXRpb25zfSlcbiAgICAgICAgICAgIClcbiAgICAgICAgfVxuICAgIH1cbn0pXG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvZ25uaHUgb24gMjcvMDkvMTUuXG4gKi9cbnZhciBMb2NhdGlvbklucHV0ID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL0xvY2F0aW9uSW5wdXRcIilcbnZhciBTcGlubmVyV2F2ZSA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvU3Bpbm5lcldhdmVcIilcbnZhciBDdWJlR3JpZCA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvVGhyZWVCb3VuY2VcIilcbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICAgbWl4aW5zOiBbZmx1eC5taXhpbnMuc3RvcmVMaXN0ZW5lcl0sXG5cbiAgICB3YXRjaFN0b3JlczogWydQb3N0UmVxdWVzdFN0b3JlJ10sXG5cblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY3VycmVudFN0YXRlOiAodGhpcy5wcm9wcy5jdXJyZW50U3RhdGUpID8gdGhpcy5wcm9wcy5jdXJyZW50U3RhdGUgOiB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMLFxuICAgICAgICAgICAgYmxvb2RUeXBlOiBcIlwiLFxuICAgICAgICAgICAgdGltZTogXCJcIixcbiAgICAgICAgICAgIGN1cnJlbnRBZGRyZXNzOiBcIlwiLFxuICAgICAgICAgICAgb3JnYW5pemF0aW9uTmFtZTogXCJcIlxuICAgICAgICB9XG4gICAgfSxcblxuICAgICBDdXJyZW50U3RhdGU6IHtcbiAgICAgICAgUE9TVElORzogXCJwb3N0aW5nXCIsXG4gICAgICAgIElOSVRJQUw6IFwiaW5pdGlhbFwiLFxuICAgICAgICBSRUFEWTogJ1JFQURZJ1xuICAgIH0sXG5cbiAgICBzdG9yZURpZENoYW5nZTogZnVuY3Rpb24gKHN0b3JlTmFtZSkge1xuICAgICAgICBjb25zb2xlLmxvZyhzdG9yZU5hbWUpXG5cbiAgICAgICAgaWYgKHRoaXMuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgIHZhciBzdG9yZSA9IGRpc3BhdGNoZXIuZ2V0U3RvcmUoc3RvcmVOYW1lKVxuICAgICAgICAgICAgdmFyIHN0b3JlU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpXG4gICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgYWxlcnQoXCJQb3N0ZWQgU3VjY2Vzc2Z1bGx5XCIpXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGFsZXJ0KFwiRXJyb3IgUG9zdGluZyFcIilcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBzZXRVcENvbXBvbmVudDogZnVuY3Rpb24gKCkge1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSlcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgbmF2aWdhdG9yLmdlb2xvY2F0aW9uLmdldEN1cnJlbnRQb3NpdGlvbihmdW5jdGlvbiAocG9zaXRpb24pIHtcbiAgICAgICAgICAgIGdvb2dsZVV0aWxzLm1hcHMucmV2ZXJzZUdlb2NvZGluZyhwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUsIHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGUsXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKGFkZHJlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBhcmVudC5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjdXJyZW50QWRkcmVzczogYWRkcmVzcywgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBhbGVydChlcnJvcilcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICBhbGVydChlcnJvci5tZXNzYWdlKVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7IGVuYWJsZUhpZ2hBY2N1cmFjeTogdHJ1ZSB9KVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICB9LFxuXG4gICAgb25CbG9vZFR5cGVDaGFuZ2U6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2Jsb29kVHlwZTogZS50YXJnZXQudmFsdWV9KVxuICAgIH0sXG5cbiAgICBvblRpbWVDaGFuZ2U6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3RpbWU6IGUudGFyZ2V0LnZhbHVlfSlcbiAgICB9LFxuXG4gICAgb25OYW1lQ2hhbmdlOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtvcmdhbml6YXRpb25OYW1lOiBlLnRhcmdldC52YWx1ZX0pXG4gICAgfSxcblxuICAgIHBvc3RSZXF1ZXN0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBvcHRzID0ge1xuICAgICAgICAgICAgYmxvb2RUeXBlOiB0aGlzLnN0YXRlLmJsb29kVHlwZSxcbiAgICAgICAgICAgIGFkZHJlc3M6IHRoaXMucmVmcy5hZGRyZXNzLmdldFZhbHVlKCksXG4gICAgICAgICAgICB0aW1lOiB0aGlzLnN0YXRlLnRpbWUsXG4gICAgICAgICAgICBvcmdhbml6YXRpb25OYW1lOiB0aGlzLnN0YXRlLm9yZ2FuaXphdGlvbk5hbWVcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IHRoaXMuQ3VycmVudFN0YXRlLlBPU1RJTkcgfSlcbiAgICAgICAgYWN0aW9ucy5wb3N0UmVxdWVzdChvcHRzKVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwpIHtcbiAgICAgICAgICAgIHJldHVybiAoUmVhY3QuY3JlYXRlRWxlbWVudChDdWJlR3JpZCwge3RleHQ6IFwiTG9hZGluZyAuLi5cIn0pKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3cgY2FyZFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJoNFwiLCBudWxsLCBcIlBvc3RpbmcgUmVxdWVzdFwiKSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb25cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImxpXCIsIHtjbGFzc05hbWU6IFwiY29sbGVjdGlvbi1pdGVtIGF2YXRhclwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImltZ1wiLCB7c3JjOiBcImltZy9ibG9vZHR5cGVfbG9nby5wbmdcIiwgY2xhc3NOYW1lOiBcImNpcmNsZVwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHtjbGFzc05hbWU6IFwidGl0bGVcIn0sIFwiT3JnYW5pemF0aW9uIE5hbWU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtvbkNoYW5nZTogdGhpcy5vbk5hbWVDaGFuZ2UsIHJlZjogXCJuYW1lXCIsIHR5cGU6IFwidGV4dFwiLCBwbGFjZWhvbGRlcjogXCJuYW1lXCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICksIFxuXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwge2NsYXNzTmFtZTogXCJjb2xsZWN0aW9uLWl0ZW0gYXZhdGFyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW1nXCIsIHtzcmM6IFwiaW1nL2xvY2F0aW9uX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIkxvY2F0aW9uOlwiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChMb2NhdGlvbklucHV0LCB7aW5pdEFkZHJlc3M6IHRoaXMuc3RhdGUuY3VycmVudEFkZHJlc3MsIHJlZjogXCJhZGRyZXNzXCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb24taXRlbSBhdmF0YXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogXCJpbWcvYmxvb2R0eXBlX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIkJsb29kIFR5cGU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtyZWY6IFwiYmxvb2RUeXBlXCIsIG9uQ2hhbmdlOiB0aGlzLm9uQmxvb2RUeXBlQ2hhbmdlLCB0eXBlOiBcInRleHRcIn0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb24taXRlbSBhdmF0YXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogXCJpbWcvYmxvb2R0eXBlX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIlRpbWU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtvbkNoYW5nZTogdGhpcy5vblRpbWVDaGFuZ2UsIHJlZjogXCJ0aW1lXCIsIHBsYWNlaG9sZGVyOiBcIk1NL2RkL3l5eXkgSEg6bW06c3NcIiwgdHlwZTogXCJ0ZXh0XCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJmb3JtLWZvb3RlciByaWdodFwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYVwiLCB7aHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIiwgb25DbGljazogdGhpcy5wb3N0UmVxdWVzdCwgY2xhc3NOYW1lOiBcIndhdmVzLWVmZmVjdCB3YXZlcy1ncmVlbiB0ZWFsIGxpZ2h0ZW4tMiBidG5cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiU2F2ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJhXCIsIHtocmVmOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiLCBjbGFzc05hbWU6IFwid2F2ZXMtZWZmZWN0IHdhdmVzLWdyZWVuIGJsdWUtZ3JleSBidG5cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQ2FuY2VsXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5QT1NUSU5HKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU3Bpbm5lcldhdmUsIHt0ZXh0OiBcIlBvc3RpbmcgRG9uYXRpb24gUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9XG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMTAvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICQoXCIjbWVudS1idG5cIikuc2lkZU5hdih7XG4gICAgICAgICAgICBlZGdlOiAnbGVmdCcsIC8vIENob29zZSB0aGUgaG9yaXpvbnRhbCBvcmlnaW5cbiAgICAgICAgICAgIGNsb3NlT25DbGljazogdHJ1ZSAvLyBDbG9zZXMgc2lkZS1uYXYgb24gPGE+IGNsaWNrcywgdXNlZnVsIGZvciBBbmd1bGFyL01ldGVvclxuICAgICAgICB9KTtcbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8qKnZhciBsaW5rcyA9IFtcbiAgICAgICAgICAgIDxsaSBrZXk9ezF9PlxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjL2ZpbmRfZG9uYXRpb25cIiBjbGFzc05hbWU9XCJ3YXZlcy1lZmZlY3Qgd2F2ZXMtbGlnaHRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaW9uLWFuZHJvaWQtc2VhcmNoXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICBGaW5kIERvbmF0aW9uIFJlcXVlc3RzXG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9saT4sXG4gICAgICAgICAgICA8bGkga2V5PXsyfSBjbGFzc05hbWU9XCJkaXZpZGVyXCI+PC9saT4sXG4gICAgICAgICAgICA8bGkga2V5PXszfT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiIy9wb3N0X3JlcXVlc3RcIiBjbGFzc05hbWU9XCJ3YXZlcy1lZmZlY3Qgd2F2ZXMtbGlnaHRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaW9uLWFuZHJvaWQtdW5sb2NrXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICBQb3N0IFJlcXVlc3RcbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICA8L2xpPixcbiAgICAgICAgXSoqL1xuICAgICAgICB2YXIgbGlua3MgPSBbXVxuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYVwiLCB7aHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIiwgY2xhc3NOYW1lOiBcImJyYW5kLWxvZ28gY2VudGVyXCJ9LCBjb25maWcuYXBwX25hbWUpLCBcbiAgICAgICAgICAgICAgICAvKjxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzc05hbWU9XCJicmFuZC1sb2dvIHJpZ2h0XCIgaWQ9XCJsb2dvXCI+PGltZyBzcmM9XCJpbWcvYXBwLWxvZ28tOTYucG5nXCIvPjwvYT4qL1xuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiLCB7Y2xhc3NOYW1lOiBcImxlZnQgaGlkZS1vbi1tZWQtYW5kLWRvd25cIn0sIFxuICAgICAgICAgICAgICAgICAgICBsaW5rc1xuICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiLCB7aWQ6IFwic2xpZGUtb3V0XCIsIGNsYXNzTmFtZTogXCJzaWRlLW5hdlwifSwgXG4gICAgICAgICAgICAgICAgICAgIGxpbmtzXG4gICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYVwiLCB7aHJlZjogXCIjXCIsIFwiZGF0YS1hY3RpdmF0ZXNcIjogXCJzbGlkZS1vdXRcIiwgY2xhc3NOYW1lOiBcImJ1dHRvbi1jb2xsYXBzZVwiLCBpZDogXCJtZW51LWJ0blwifSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImlcIiwge2NsYXNzTmFtZTogXCJtZGktbmF2aWdhdGlvbi1tZW51XCJ9KSlcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDEwLzA1LzE1LlxuICovXG52YXIgU2xpZGVyID0gcmVxdWlyZShcIi4vYnV0dG9ucy9TbGlkZXJcIilcbnZhciBTcGlubmVyV2F2ZSA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvU3Bpbm5lcldhdmVcIilcbnZhciBDdWJlR3JpZCA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvVGhyZWVCb3VuY2VcIilcbnZhciBEb25hdGlvblJlc3VsdCA9IHJlcXVpcmUoXCIuL2ZpbmRfYmlrZXBvZC9Eb25hdGlvblJlc3VsdFwiKVxudmFyIExvY2F0aW9uU2V0dGluZyA9IHJlcXVpcmUoXCIuL2ZpbmRfYmlrZXBvZC9Mb2NhdGlvblNldHRpbmdcIilcbnZhciBNYXBNYXJrZXIgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvTWFwTWFya2VyXCIpXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG4gICAgbWl4aW5zOiBbZmx1eC5taXhpbnMuc3RvcmVMaXN0ZW5lcl0sXG5cbiAgICB3YXRjaFN0b3JlczogWydMb2NhdGlvblN0b3JlJ10sXG5cbiAgICBDdXJyZW50U3RhdGU6IHtcbiAgICAgICAgRklORElORzogXCJmaW5kaW5nXCIsXG4gICAgICAgIElOSVRJQUw6IFwiaW5pdGlhbFwiLFxuICAgICAgICBSRUFEWTogJ1JFQURZJyxcbiAgICAgICAgUkVTVUxUOiBcInJlc3VsdFwiXG4gICAgfSxcblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY3VycmVudFN0YXRlOiAodGhpcy5wcm9wcy5jdXJyZW50U3RhdGUpID8gdGhpcy5wcm9wcy5jdXJyZW50U3RhdGUgOiB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMLFxuICAgICAgICAgICAgcG9kczogW10sXG4gICAgICAgICAgICBjb29yZHM6IHt9XG4gICAgICAgIH1cbiAgICB9LFxuXG5cbiAgICBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzOiBmdW5jdGlvbiAobmV4dFByb3BzKSB7XG4gICAgICAgIGlmIChuZXh0UHJvcHMuY3VycmVudFN0YXRlKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IG5leHRQcm9wcy5jdXJyZW50U3RhdGV9KVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGZpbmREb25hdGlvbnM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgdmFyIHZhbHVlID0gcGFyZW50LnJlZnMubG9jYXRpb24uZ2V0VmFsdWUoKVxuICAgICAgICBhY3Rpb25zLmZpbmREb25hdGlvbnModmFsdWUuYWRkcmVzcywgdmFsdWUucmFkaXVzKVxuXG4gICAgICAgIGlmICh3aW5kb3cuZGV2aWNlLnBsYXRmb3JtID09IFwiYnJvd3NlclwiKSB7XG4gICAgICAgICAgICBmaW5kKClcbiAgICAgICAgICAgICQodGhpcy5yZWZzLmZpbmRCdG4uZ2V0RE9NTm9kZSgpKS5mYWRlT3V0KDMwMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5GSU5ESU5HXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5GSU5ESU5HXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHN0b3JlRGlkQ2hhbmdlOiBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICBpZiAodGhpcy5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgdmFyIHN0b3JlID0gZGlzcGF0Y2hlci5nZXRTdG9yZShuYW1lKVxuICAgICAgICAgICAgdmFyIHN0b3JlU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpXG5cblxuICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuYWN0aW9uID09IHN0b3JlLmFjdGlvbnMuRklORF9QT0QpIHtcbiAgICAgICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3BvZHM6IHN0b3JlU3RhdGUucG9kc30sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZHJhd01hcCh0aGlzLnN0YXRlLmNvb3JkcylcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBuYXZpZ2F0b3Iubm90aWZpY2F0aW9uLmFsZXJ0KHN0b3JlU3RhdGUuZXJyb3JNZXNzYWdlKVxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgZHJhd01hcDogZnVuY3Rpb24gKGNvb3Jkcykge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgTWFwTWFya2VyLmdldE1hcmtlcnMoY29vcmRzLCB0aGlzLnN0YXRlLnBvZHMsIHRoaXMucmVmcy5nb29nbGVNYXAuZ2V0RE9NTm9kZSgpKVxuICAgICAgICB9KVxuICAgIH0sXG5cbiAgICBzZXRVcENvbXBvbmVudDogZnVuY3Rpb24gKHJhZGl1cykge1xuICAgICAgICB2YXIgcGFyZW50ID0gdGhpc1xuICAgICAgICB2YXIgcmFkID0gKHJhZGl1cykgPyByYWRpdXMgOiA1XG4gICAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgICAgICAgICB2YXIgZmluZCA9IG51bGxcbiAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICB2YXIgY29vcmRzID0ge2xhdDogcG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBsbmc6IHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGV9XG4gICAgICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjb29yZHM6IGNvb3Jkc30sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9ucy5maW5kRG9uYXRpb25zKGNvb3JkcywgcmFkKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGZpbmQgPSBudWxsXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvL3BhcmVudC5kcmF3TWFwKGNvb3JkcylcbiAgICAgICAgICAgIC8qZ29vZ2xlVXRpbHMubWFwcy5yZXZlcnNlR2VvY29kaW5nKHBvc2l0aW9uLmNvb3Jkcy5sYXRpdHVkZSwgcG9zaXRpb24uY29vcmRzLmxvbmdpdHVkZSxcbiAgICAgICAgICAgICBmdW5jdGlvbiAoYWRkcmVzcykge1xuICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7Y3VycmVudEFkZHJlc3M6IGFkZHJlc3MsIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5SRUFEWX0pXG4gICAgICAgICAgICAgfSwgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgYWxlcnQoZXJyb3IpXG4gICAgICAgICAgICAgfSkqL1xuICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICBhbGVydChlcnJvci5tZXNzYWdlKVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7ZW5hYmxlSGlnaEFjY3VyYWN5OiB0cnVlfSlcbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRVcENvbXBvbmVudCgpXG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZFVwZGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCkge1xuICAgICAgICAgICAgdGhpcy5zZXRVcENvbXBvbmVudCgpXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMKSB7XG4gICAgICAgICAgICByZXR1cm4gKFJlYWN0LmNyZWF0ZUVsZW1lbnQoQ3ViZUdyaWQsIHt0ZXh0OiBcIkxvYWRpbmcgLi4uXCJ9KSlcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWSkge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiczEyXCIsIGlkOiBcInNlYXJjaC13cmFwcGVyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImg1XCIsIG51bGwsIFwiV2hlcmUgdG8gZ29cIiksIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMlwiLCBpZDogXCJnb29nbGUtbWFwXCIsIHJlZjogXCJnb29nbGVNYXBcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTIgaW5wdXQtZmllbGRcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7dHlwZTogXCJzZWFyY2hcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIiwgbnVsbCwgUmVhY3QuY3JlYXRlRWxlbWVudChcImlcIiwge2NsYXNzTmFtZTogXCJtYXRlcmlhbC1pY29uc1wifSwgXCJzZWFyY2hcIikpLCBcbiAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaVwiLCB7Y2xhc3NOYW1lOiBcIm1hdGVyaWFsLWljb25zXCJ9LCBcImNsb3NlXCIpXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuRklORElORykge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFNwaW5uZXJXYXZlLCB7dGV4dDogXCJGaW5kaW5nIFBvZHMgUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID0gdGhpcy5DdXJyZW50U3RhdGUuUkVTVUxUKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRG9uYXRpb25SZXN1bHQsIHtkb25hdGlvbnM6IHRoaXMuc3RhdGUuZG9uYXRpb25zfSlcbiAgICAgICAgICAgIClcbiAgICAgICAgfVxuICAgIH1cbn0pXG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjMvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdmFsdWU6ICh0aGlzLnByb3BzLnZhbHVlKT8gdGhpcy5wcm9wcy52YWx1ZSA6IDBcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHRoaXMuYWN0aXZhdGVTbGlkZXIoKVxuICAgIH0sXG5cbiAgICBzZXRWYWx1ZTogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7dmFsdWU6IHZhbHVlfSlcbiAgICB9LFxuXG4gICAgYWN0aXZhdGVTbGlkZXI6ICBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIG1pbiA9ICh0aGlzLnByb3BzLm1pbik/IHRoaXMucHJvcHMubWluIDogMFxuICAgICAgICB2YXIgbWF4ID0gKHRoaXMucHJvcHMubWF4KT8gdGhpcy5wcm9wcy5tYXggOiA1MFxuICAgICAgICB2YXIgbm9kZSA9ICQodGhpcy5yZWZzLnNsaWRlci5nZXRET01Ob2RlKCkpXG4gICAgICAgIG5vZGUubm9VaVNsaWRlcih7XG4gICAgICAgICAgICBzdGFydDogdGhpcy5zdGF0ZS52YWx1ZSxcbiAgICAgICAgICAgIGNvbm5lY3Q6IFwibG93ZXJcIixcbiAgICAgICAgICAgIG9yaWVudGF0aW9uOiBcImhvcml6b250YWxcIixcbiAgICAgICAgICAgIGRpcmVjdGlvbjogXCJsdHJcIixcbiAgICAgICAgICAgIGJlaGF2aW91cjogXCJ0YXAtZHJhZ1wiLFxuICAgICAgICAgICAgcmFuZ2U6IHtcbiAgICAgICAgICAgICAgICAnbWluJzogbWluLFxuICAgICAgICAgICAgICAgICdtYXgnOiBtYXhcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBmb3JtYXQ6IHdOdW1iKHtcbiAgICAgICAgICAgICAgICBkZWNpbWFsczogMFxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSk7XG4gICAgICAgIHZhciBwYXJlbnQgPSB0aGlzXG4gICAgICAgIG5vZGUub24oJ3NsaWRlJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBwYXJlbnQuc2V0VmFsdWUoJCh0aGlzKS52YWwoKSlcbiAgICAgICAgfSlcblx0bm9kZS5vbignY2hhbmdlJywgZnVuY3Rpb24gKCkge1xuXHQgICAgaWYgKHBhcmVudC5wcm9wcy5zbGlkZXJSZWxlYXNlKSB7XG5cdFx0cGFyZW50LnByb3BzLnNsaWRlclJlbGVhc2UoJCh0aGlzKS52YWwoKSlcblx0ICAgIH1cblx0fSlcbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciB1bml0ID0gKHRoaXMucHJvcHMudW5pdCk/IFwiIFwiICt0aGlzLnByb3BzLnVuaXQgOiBcIlwiXG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMiBzbGlkZXItY29udGFpbmVyXCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2xpZGVyIGNvbCBzMTBcIiwgcmVmOiBcInNsaWRlclwifSksIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczIgc2xpZGVyLXZhbHVlXCJ9LCB0aGlzLnN0YXRlLnZhbHVlICsgdW5pdClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pXG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjAvMDUvMTUuXG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMiBtOCBvZmZzZXQtbTIgbDYgb2Zmc2V0LWwzXCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY2FyZC1wYW5lbCBncmV5IGxpZ2h0ZW4tNSB6LWRlcHRoLTFcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicm93IHZhbGlnbi13cmFwcGVyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogdGhpcy5wcm9wcy5kb25hdGlvbi5wcm9maWxlSW1nLCBhbHQ6IFwiUHJvZmlsZSBQaWN0dXJlXCIsIGNsYXNzTmFtZTogXCJjaXJjbGUgcmVzcG9uc2l2ZS1pbWdcIn0pXG4gICAgICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEwXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwge2NsYXNzTmFtZTogXCJibGFjay10ZXh0XCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5kb25hdGlvbi5kZXNjcmlwdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIwLzA1LzE1LlxuICovXG5cbnZhciBEb25hdGlvbkNhcmQgPSByZXF1aXJlKFwiLi9Eb25hdGlvbkNhcmRcIilcblxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGRvbmF0aW9uczogKHRoaXMucHJvcHMuZG9uYXRpb25zKT8gdGhpcy5wcm9wcy5kb25hdGlvbnMgOiBbXVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHM6IGZ1bmN0aW9uKG5leHRQcm9wcykge1xuICAgICAgICBpZiAobmV4dFByb3BzLmRvbmF0aW9ucyAmJiBKU09OLnN0cmluZ2lmeShuZXh0UHJvcHMuZG9uYXRpb25zKSAhPSBKU09OLnN0cmluZ2lmeSh0aGlzLnByb3BzLmRvbmF0aW9ucykpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2RvbmF0aW9uczogbmV4dFByb3BzLmRvbmF0aW9uc30pXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZG9uYXRpb25zLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJoM1wiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgXCJObyBkb25hdGlvbiByZXF1ZXN0cyBhcmUgZm91bmQgYXJvdW5kIHlvdXIgbG9jYXRpb24hXCJcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJidXNrZXItbGlzdFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5kb25hdGlvbnMubWFwKGZ1bmN0aW9uKGl0ZW0sIGlkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChEb25hdGlvbkNhcmQsIHtkb25hdGlvbjogaXRlbSwga2V5OiBpZH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICB9XG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjMvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgc2VhcmNoQm94OiB1bmRlZmluZWRcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtzZWFyY2hCb3g6IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuU2VhcmNoQm94KHRoaXMucmVmcy5pbnB1dC5nZXRET01Ob2RlKCkpfSlcbiAgICB9LFxuXG4gICAgZ2V0VmFsdWU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVmcy5pbnB1dC5nZXRET01Ob2RlKCkudmFsdWVcbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicm93XCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiaW5wdXQtZmllbGQgY29sIHMxMlwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7dHlwZTogXCJ0ZXh0XCIsIHBsYWNlaG9sZGVyOiBcIkNob29zZSBhIGxvY2F0aW9uXCIsIGNsYXNzTmFtZTogXCJ2YWxpZGF0ZVwiLCByZWY6IFwiaW5wdXRcIiwgZGVmYXVsdFZhbHVlOiB0aGlzLnByb3BzLmluaXRBZGRyZXNzfSlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMy8wNS8xNS5cbiAqL1xudmFyIFNsaWRlciA9IHJlcXVpcmUoXCIuLi9idXR0b25zL1NsaWRlclwiKVxudmFyIExvY2F0aW9uSW5wdXQgPSByZXF1aXJlKFwiLi9Mb2NhdGlvbklucHV0XCIpXG5cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcblxuICAgIGNvbXBvbmVudFdpbGxVcGRhdGU6IGZ1bmN0aW9uIChuZXh0UHJvcHMpIHtcbiAgICAgICAgaWYgKG5leHRQcm9wcy5pbml0QWRkcmVzcyAmJiBuZXh0UHJvcHMuaW5pdEFkZHJlc3MgIT0gdGhpcy5wcm9wcy5pbml0QWRkcmVzcykge1xuICAgICAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2VcbiAgICB9LFxuXG4gICAgZ2V0VmFsdWU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGFkZHJlc3M6IHRoaXMucmVmcy5hZGRyZXNzLmdldFZhbHVlKCksXG4gICAgICAgICAgICByYWRpdXM6IHRoaXMucmVmcy5yYWRpdXMuc3RhdGUudmFsdWVcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3dcIiwgaWQ6IFwibG9jYXRpb24tc2V0dGluZ1wifSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImZvcm1cIiwge2NsYXNzTmFtZTogXCJjb2wgczEyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInJvd1wifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KExvY2F0aW9uSW5wdXQsIHtpbml0QWRkcmVzczogdGhpcy5wcm9wcy5pbml0QWRkcmVzcywgcmVmOiBcImFkZHJlc3NcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEyIGxhYmVsXCJ9LCBcIlJhZGl1c1wiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFNsaWRlciwge3JlZjogXCJyYWRpdXNcIiwgdmFsdWU6IDEwLCB1bml0OiBcImttXCJ9KVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvZ25uaHUgb24gMjYvMDkvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0ge1xuICAgIGdldE1hcmtlcnM6IGZ1bmN0aW9uIChjdXJyZW50TG9jYXRpb24sIHBvZHMsIGRvbSwgcmFkaXVzLCBoZWEpIHtcbiAgICAgICAgdmFyIG1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9tLCB7XG4gICAgICAgICAgICBjZW50ZXI6IGN1cnJlbnRMb2NhdGlvbixcbiAgICAgICAgICAgIHpvb206IGNvbmZpZy5nb29nbGVNYXAuem9vbSxcbiAgICAgICAgICAgIG1hcFR5cGVJZDogZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLlRFUlJBSU4sXG4gICAgICAgICAgICBtYXBUeXBlQ29udHJvbDogZmFsc2VcbiAgICAgICAgfSk7XG4gICAgICAgIGdvb2dsZS5tYXBzLnZpc3VhbFJlZnJlc2ggPSB0cnVlO1xuICAgICAgICB2YXIgY3VycmVudExvY2F0aW9uTWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICBwb3NpdGlvbjogY3VycmVudExvY2F0aW9uLFxuICAgICAgICAgICAgdGl0bGU6ICdZb3UgYXJlIGhlcmUnLFxuICAgICAgICAgICAgaWNvbjogY29uZmlnLnVzZXJfY3VycmVudF9sb2NhdGlvbl9pY29uXG4gICAgICAgIH0pO1xuICAgICAgICBjdXJyZW50TG9jYXRpb25NYXJrZXIuc2V0TWFwKG1hcCk7XG4gICAgICAgIHZhciBjdXJyZW50TG9jYXRpb25JbmZvV2luZG93ID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3coe1xuICAgICAgICAgICAgY29udGVudDogXCI8cD5Zb3UgYXJlIGhlcmUhPC9wPlwiXG4gICAgICAgIH0pO1xuICAgICAgICBjdXJyZW50TG9jYXRpb25NYXJrZXIuYWRkTGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY3VycmVudExvY2F0aW9uSW5mb1dpbmRvdy5vcGVuKG1hcCwgY3VycmVudExvY2F0aW9uTWFya2VyKTtcbiAgICAgICAgfSlcblxuICAgICAgICBwb2RzLmZvckVhY2goZnVuY3Rpb24gKHBvZCwgaWQpIHtcbiAgICAgICAgICAgIHZhciBjb29yZGluYXRlcyA9IHBvZC5jb29yZGluYXRlcy5jb29yZGluYXRlc1xuICAgICAgICAgICAgdmFyIG1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiB7XG4gICAgICAgICAgICAgICAgICAgIGxhdDogcGFyc2VGbG9hdChjb29yZGluYXRlc1tcImxhdFwiXSksXG4gICAgICAgICAgICAgICAgICAgIGxuZzogcGFyc2VGbG9hdChjb29yZGluYXRlc1tcImxvblwiXSlcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGljb246IGNvbmZpZy5iaWtlX2ljb25cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBtYXJrZXIuYWRkTGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHdpbmRvdy5wb3B1cFBvZERldGFpbHMocG9kKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIG1hcmtlci5zZXRNYXAobWFwKVxuICAgICAgICB9KVxuICAgICAgICBuZXcgZ29vZ2xlLm1hcHMuQ2lyY2xlKHtcbiAgICAgICAgICAgIHN0cm9rZUNvbG9yOiAnZ3JlZW4nLFxuICAgICAgICAgICAgc3Ryb2tlT3BhY2l0eTogMC44LFxuICAgICAgICAgICAgc3Ryb2tlV2VpZ2h0OiAyLFxuICAgICAgICAgICAgZmlsbE9wYWNpdHk6IDAuMCxcbiAgICAgICAgICAgIG1hcDogbWFwLFxuICAgICAgICAgICAgY2VudGVyOiBtYXAuZ2V0Q2VudGVyKCksXG4gICAgICAgICAgICByYWRpdXM6IDEwMDBcbiAgICAgICAgICB9KTtcbiAgICB9XG59XG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjAvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNwaW5uZXItd3JhcHBlclwifSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNwaW5uZXItY2VsbFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1zcGlubmVyIHNrLXNwaW5uZXItd2F2ZVwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stcmVjdDFcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1yZWN0MlwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXJlY3QzXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stcmVjdDRcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1yZWN0NVwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwgbnVsbCwgdGhpcy5wcm9wcy50ZXh0KVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjMvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzcGlubmVyLXdyYXBwZXJcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzcGlubmVyLWNlbGxcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stc3Bpbm5lciBzay1zcGlubmVyLXRocmVlLWJvdW5jZVwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stYm91bmNlMVwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLWJvdW5jZTJcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1ib3VuY2UzXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCB0aGlzLnByb3BzLnRleHQpXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qXG4gKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4gKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiAqIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4gKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4gKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4gKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2VcbiAqIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4gKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuICogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiAqIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuICogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuICogdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxudmFyIHJvdXRlciA9IHJlcXVpcmUoXCIuL3JvdXRlci9yb3V0ZXJcIilcbnZhciBTaWRlTmF2ID0gcmVxdWlyZShcIi4vY29tcG9uZW50cy9TaWRlTmF2XCIpXG5cbnZhciBhcHAgPSB7XG4gICAgLy8gQXBwbGljYXRpb24gQ29uc3RydWN0b3JcbiAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgdGhpcy5iaW5kRXZlbnRzKCk7XG4gICAgfSxcbiAgICAvLyBCaW5kIEV2ZW50IExpc3RlbmVyc1xuICAgIC8vXG4gICAgLy8gQmluZCBhbnkgZXZlbnRzIHRoYXQgYXJlIHJlcXVpcmVkIG9uIHN0YXJ0dXAuIENvbW1vbiBldmVudHMgYXJlOlxuICAgIC8vICdsb2FkJywgJ2RldmljZXJlYWR5JywgJ29mZmxpbmUnLCBhbmQgJ29ubGluZScuXG4gICAgYmluZEV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2RldmljZXJlYWR5JywgdGhpcy5vbkRldmljZVJlYWR5LCBmYWxzZSk7XG4gICAgfSxcbiAgICAvLyBkZXZpY2VyZWFkeSBFdmVudCBIYW5kbGVyXG4gICAgLy9cbiAgICAvLyBUaGUgc2NvcGUgb2YgJ3RoaXMnIGlzIHRoZSBldmVudC4gSW4gb3JkZXIgdG8gY2FsbCB0aGUgJ3JlY2VpdmVkRXZlbnQnXG4gICAgLy8gZnVuY3Rpb24sIHdlIG11c3QgZXhwbGljaXRseSBjYWxsICdhcHAucmVjZWl2ZWRFdmVudCguLi4pOydcbiAgICBvbkRldmljZVJlYWR5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgLy9hcHAucmVjZWl2ZWRFdmVudCgnZGV2aWNlcmVhZHknKTtcbiAgICAgICAgJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByb3V0ZXIuaW5pdChcIi9maW5kX2RvbmF0aW9uXCIpXG4gICAgICAgICAgICBSZWFjdC5yZW5kZXIoUmVhY3QuY3JlYXRlRWxlbWVudChTaWRlTmF2LCB7ZGlzcGF0Y2hlcjogZGlzcGF0Y2hlcn0pLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm5hdi1iYXJcIikpXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICQoXCIuc3BsYXNoLXNjcmVlblwiKS5mYWRlT3V0KDUwMClcbiAgICAgICAgICAgIH0sIDMwMDApXG4gICAgICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiYmFja2J1dHRvblwiLCBvbkJhY2tLZXlEb3duLCBmYWxzZSk7XG4gICAgICAgICAgICBmdW5jdGlvbiBvbkJhY2tLZXlEb3duKCkge1xuICAgICAgICAgICAgICAgIFJlYWN0LnVubW91bnRDb21wb25lbnRBdE5vZGUoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwb2QtZGV0YWlsc1wiKSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9XG59O1xuXG5hcHAuaW5pdGlhbGl6ZSgpO1xuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIwLzA1LzE1LlxuICovXG52YXIgRmluZEJpa2VQb2QgPSByZXF1aXJlKFwiLi4vY29tcG9uZW50cy9GaW5kQmlrZVBvZC5qc1wiKVxudmFyIFBvc3RSZXF1ZXN0ID0gcmVxdWlyZShcIi4uL2NvbXBvbmVudHMvUG9zdFJlcXVlc3RcIilcbnZhciBXaGVyZVRvR28gPSByZXF1aXJlKFwiLi4vY29tcG9uZW50cy9XaGVyZVRvR29cIilcbndpbmRvdy5yZW5kZXJGaW5kRG9uYXRpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgUmVhY3QudW5tb3VudENvbXBvbmVudEF0Tm9kZShkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbiAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwb2QtZGV0YWlsc1wiKSkge1xuICAgICAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9kLWRldGFpbHNcIikpXG4gICAgfVxuICAgIFJlYWN0LnJlbmRlcihSZWFjdC5jcmVhdGVFbGVtZW50KEZpbmRCaWtlUG9kLCB7Y3VycmVudFN0YXRlOiBcImluaXRpYWxcIiwgZGlzcGF0Y2hlcjogZGlzcGF0Y2hlcn0pLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbn1cblxud2luZG93LnJlbmRlcldoZXJlR28gPSBmdW5jdGlvbiAoKSB7XG4gICAgUmVhY3QudW5tb3VudENvbXBvbmVudEF0Tm9kZShkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbiAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9kLWRldGFpbHNcIikpXG4gICAgUmVhY3QucmVuZGVyKFJlYWN0LmNyZWF0ZUVsZW1lbnQoV2hlcmVUb0dvLCB7Y3VycmVudFN0YXRlOiBcImluaXRpYWxcIiwgZGlzcGF0Y2hlcjogZGlzcGF0Y2hlcn0pLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgaW5pdDogZnVuY3Rpb24ocGF0aCkge1xuICAgICAgICB2YXIgcm91dGVzID0ge1xuICAgICAgICAgICAgXCIvZmluZF9kb25hdGlvblwiOiByZW5kZXJGaW5kRG9uYXRpb24sXG4gICAgICAgICAgICBcIi93aGVyZS1nb1wiOiByZW5kZXJXaGVyZUdvXG4gICAgICAgIH1cbiAgICAgICAgd2luZG93LnJvdXRlciA9IFJvdXRlcihyb3V0ZXMpO1xuICAgICAgICByb3V0ZXIuaW5pdChwYXRoKVxuICAgIH1cbn0iXX0=
