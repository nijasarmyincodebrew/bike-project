(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"/home/duognnhu/workspace/codebrew/fe/src/js/components/FindDonation.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_donation/DonationResult")
var LocationSetting = require("./find_donation/LocationSetting")
var MapMarker = require("./find_donation/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            donations: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    findDonations: function () {
        var parent = this
        var value = parent.refs.location.getValue()
        actions.findDonations(value.address, value.radius)

        if (window.device.platform == "browser") {
            find()
            $(this.refs.findBtn.getDOMNode()).fadeOut(300, function () {
                parent.setState({
                    currentState: parent.CurrentState.FINDING
                })
            })
        } else {
            parent.setState({
                currentState: parent.CurrentState.FINDING
            })
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()

            console.log(storeState.donations)

            if (storeState.action == store.actions.FIND_DONATION) {
                if (storeState.success) {
                    this.setState({ donations: storeState.donations}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        console.log(this.state.donations)
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.getMarkers(coords, this.state.donations, this.refs.googleMap.getDOMNode())
        })
    },

    setUpComponent: function (radius) {
        var parent = this
	var rad = (radius)? radius : 5
	console.log(rad)
        navigator.geolocation.getCurrentPosition(function (position) {
           console.log(position) 
	   if (parent.isMounted()) {
                var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                parent.setState({coords: coords}, function () {
                    actions.findDonations(coords, rad)
                })
            }
            //parent.drawMap(coords)
            /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                function (address) {
                    parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                }, function (error) {
                    alert(error)
                })*/
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, { enableHighAccuracy: true })
	console.log("o28eu0dua9u")
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            this.setUpComponent()
        }
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "s12", id: "search-wrapper"}, 
                    React.createElement("div", {id: "slider-wrapper"}, 
                        React.createElement(Slider, {value: 10, unit: "km", sliderRelease: this.setUpComponent})
                    ), 
                    React.createElement("div", {className: "col s12", id: "google-map", ref: "googleMap"})
                )
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                React.createElement(SpinnerWave, {text: "Finding Donation Requests ..."})
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                React.createElement(DonationResult, {donations: this.state.donations})
            )
        }
    }
})

},{"./buttons/Slider":"/home/duognnhu/workspace/codebrew/fe/src/js/components/buttons/Slider.js","./find_donation/DonationResult":"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/DonationResult.js","./find_donation/LocationSetting":"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/LocationSetting.js","./find_donation/MapMarker":"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/MapMarker.js","./loading/SpinnerWave":"/home/duognnhu/workspace/codebrew/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duognnhu/workspace/codebrew/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/PostRequest.js":[function(require,module,exports){
/**
 * Created by duognnhu on 27/09/15.
 */
var LocationInput = require("./find_donation/LocationInput")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
module.exports = React.createClass({displayName: "exports",
     mixins: [flux.mixins.storeListener],

    watchStores: ['PostRequestStore'],


    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            bloodType: "",
            time: "",
            currentAddress: "",
            organizationName: ""
        }
    },

     CurrentState: {
        POSTING: "posting",
        INITIAL: "initial",
        READY: 'READY'
    },

    storeDidChange: function (storeName) {
        console.log(storeName)

        if (this.isMounted()) {
            var store = dispatcher.getStore(storeName)
            var storeState = store.getState()
            if (storeState.success) {
                alert("Posted Successfully")
            } else {
                alert("Error Posting!")
            }
        }
    },

    setUpComponent: function () {
        console.log(this.state.currentState)
        var parent = this
        navigator.geolocation.getCurrentPosition(function (position) {
            googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                function (address) {
                    if (parent.isMounted()) {
                        parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                    }
                }, function (error) {
                    alert(error)
                })
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, { enableHighAccuracy: true })
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    onBloodTypeChange: function (e) {
        this.setState({bloodType: e.target.value})
    },

    onTimeChange: function (e) {
        this.setState({time: e.target.value})
    },

    onNameChange: function (e) {
        this.setState({organizationName: e.target.value})
    },

    postRequest: function () {
        var opts = {
            bloodType: this.state.bloodType,
            address: this.refs.address.getValue(),
            time: this.state.time,
            organizationName: this.state.organizationName
        }
        this.setState({currentState: this.CurrentState.POSTING })
        actions.postRequest(opts)
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "row card"}, 
                    React.createElement("h4", null, "Posting Request"), 
                    React.createElement("ul", {className: "collection"}, 
                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Organization Name:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onNameChange, ref: "name", type: "text", placeholder: "name"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/location_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Location:"), 
                            React.createElement(LocationInput, {initAddress: this.state.currentAddress, ref: "address"})
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Blood Type:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {ref: "bloodType", onChange: this.onBloodTypeChange, ype: "text"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Time:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onTimeChange, ref: "time", placeholder: "MM/dd/yyyy HH:mm:ss", type: "text"})
                            )
                        )
                    ), 
                    React.createElement("div", {className: "form-footer right"}, 
                        React.createElement("a", {href: "javascript:void(0)", onClick: this.postRequest, className: "waves-effect waves-green teal lighten-2 btn"}, 
                            "Save"
                        ), 
                        React.createElement("a", {href: "javascript:void(0)", className: "waves-effect waves-green blue-grey btn"}, 
                            "Cancel"
                        )
                    )
                )
            )
        } else if (this.state.currentState == this.CurrentState.POSTING) {
            return (
                React.createElement(SpinnerWave, {text: "Posting Donation Requests ..."})
            )
        }
    }
})
},{"./find_donation/LocationInput":"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/LocationInput.js","./loading/SpinnerWave":"/home/duognnhu/workspace/codebrew/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duognnhu/workspace/codebrew/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/SideNav.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    componentDidMount: function() {
        $("#menu-btn").sideNav({
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });
        console.log("mounted")
    },

    render: function () {
        var links = [
            React.createElement("li", {key: 1}, 
                React.createElement("a", {href: "#/find_donation", className: "waves-effect waves-light"}, 
                    React.createElement("i", {className: "ion-android-search"}), 
                    "Find Donation Requests"
                )
            ),
            React.createElement("li", {key: 2, className: "divider"}),
            React.createElement("li", {key: 3}, 
                React.createElement("a", {href: "#/post_request", className: "waves-effect waves-light"}, 
                    React.createElement("i", {className: "ion-android-unlock"}), 
                    "Post Request"
                )
            ),
        ]
        return (
            React.createElement("div", null, 
                React.createElement("a", {href: "javascript:void(0)", className: "brand-logo center"}, config.app_name), 
                React.createElement("a", {href: "javascript:void(0)", className: "brand-logo right", id: "logo"}, React.createElement("img", {src: "img/app-logo-96.png"})), 
                React.createElement("ul", {className: "left hide-on-med-and-down"}, 
                    links
                ), 
                React.createElement("ul", {id: "slide-out", className: "side-nav"}, 
                    links
                ), 
                  React.createElement("a", {href: "#", "data-activates": "slide-out", className: "button-collapse", id: "menu-btn"}, React.createElement("i", {className: "mdi-navigation-menu"}))
            )
        )
    }
})
},{}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/buttons/Slider.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            value: (this.props.value)? this.props.value : 0
        }
    },

    componentDidMount: function() {
        this.activateSlider()
    },

    setValue: function(value) {
        this.setState({value: value})
    },

    activateSlider:  function() {
        var min = (this.props.min)? this.props.min : 0
        var max = (this.props.max)? this.props.max : 50
        var node = $(this.refs.slider.getDOMNode())
        node.noUiSlider({
            start: this.state.value,
            connect: "lower",
            orientation: "horizontal",
            direction: "ltr",
            behaviour: "tap-drag",
            range: {
                'min': min,
                'max': max
            },
            format: wNumb({
                decimals: 0
            })
        });
        var parent = this
        node.on('slide', function() {
            parent.setValue($(this).val())
        })
	node.on('change', function () {
	    if (parent.props.sliderRelease) {
		parent.props.sliderRelease($(this).val())
	    }
	})
    },

    render: function () {
        var unit = (this.props.unit)? " " +this.props.unit : ""
        return (
            React.createElement("div", {className: "col s12 slider-container"}, 
                React.createElement("div", {className: "slider col s10", ref: "slider"}), React.createElement("div", {className: "col s2 slider-value"}, this.state.value + unit)
            )
        )
    }
})

},{}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/DonationCard.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

module.exports = React.createClass({displayName: "exports",

    render: function () {
        return (
            React.createElement("div", {className: "col s12 m8 offset-m2 l6 offset-l3"}, 
                React.createElement("div", {className: "card-panel grey lighten-5 z-depth-1"}, 
                    React.createElement("div", {className: "row valign-wrapper"}, 
                        React.createElement("div", {className: "col s2"}, 
                            React.createElement("img", {src: this.props.donation.profileImg, alt: "Profile Picture", className: "circle responsive-img"})
                        ), 
                        React.createElement("div", {className: "col s10"}, 
                            React.createElement("p", null), 
                            React.createElement("p", {className: "black-text"}, 
                                this.props.donation.description
                            )
                        )
                    )
                )
            )
        )
    }
})
},{}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/DonationResult.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

var DonationCard = require("./DonationCard")

module.exports = React.createClass({displayName: "exports",

    getInitialState: function() {
        return {
            donations: (this.props.donations)? this.props.donations : []
        }
    },

    componentWillReceiveProps: function(nextProps) {
        if (nextProps.donations && JSON.stringify(nextProps.donations) != JSON.stringify(this.props.donations)) {
            this.setState({donations: nextProps.donations})
        }
    },

    render: function() {
        if (this.state.donations.length == 0) {
            return (
                React.createElement("h3", null, 
                    "No donation requests are found around your location!"
                )
            )
        } else {
            return (
                React.createElement("div", {className: "busker-list"}, 
                    
                        this.state.donations.map(function(item, id) {
                            return (
                                React.createElement(DonationCard, {donation: item, key: id})
                            )
                        })
                    
                )
            )
        }
    }
})
},{"./DonationCard":"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/DonationCard.js"}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/LocationInput.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            searchBox: undefined
        }
    },

    componentDidMount: function () {
        this.setState({searchBox: new google.maps.places.SearchBox(this.refs.input.getDOMNode())})
    },

    getValue: function () {
        return this.refs.input.getDOMNode().value
    },

    render: function () {
        return (
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "input-field col s12"}, 
                    React.createElement("input", {type: "text", placeholder: "Choose a location", className: "validate", ref: "input", defaultValue: this.props.initAddress})
                )
            )
        )
    }
})
},{}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/LocationSetting.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
var Slider = require("../buttons/Slider")
var LocationInput = require("./LocationInput")

module.exports = React.createClass({displayName: "exports",

    componentWillUpdate: function (nextProps) {
        if (nextProps.initAddress && nextProps.initAddress != this.props.initAddress) {
            return true
        }
        return false
    },

    getValue: function () {
        return {
            address: this.refs.address.getValue(),
            radius: this.refs.radius.state.value
        }
    },

    render: function () {
        return (
            React.createElement("div", {className: "row", id: "location-setting"}, 
                React.createElement("form", {className: "col s12"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement(LocationInput, {initAddress: this.props.initAddress, ref: "address"}), 
                        React.createElement("div", {className: "col s12 label"}, "Radius"), 
                        React.createElement(Slider, {ref: "radius", value: 10, unit: "km"})
                    )
                )
            )
        )
    }
})
},{"../buttons/Slider":"/home/duognnhu/workspace/codebrew/fe/src/js/components/buttons/Slider.js","./LocationInput":"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/LocationInput.js"}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/find_donation/MapMarker.js":[function(require,module,exports){
/**
 * Created by duognnhu on 26/09/15.
 */
module.exports = {
    getMarkers: function (currentLocation, donations, dom, radius) {
        console.log(donations)
        var map = new google.maps.Map(dom, {
            center: currentLocation,
            zoom: config.googleMap.zoom
        });
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here'
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })


        donations.forEach(function (donation, id) {
            console.log(id)
            if (donation.location) {
                var marker = new google.maps.Marker({
                    position: {lat: parseFloat(donation.latitude), lng: parseFloat(donation.longitude)},
                    icon: config.hospital_icon,
                    map: map
                })
                marker.addListener('click', function () {
                    window.popupDonationDetails(donation)
                })
            }
        })
    }
}

},{}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/loading/SpinnerWave.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function() {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-wave"}, 
                        React.createElement("div", {className: "sk-rect1"}), 
                        React.createElement("div", {className: "sk-rect2"}), 
                        React.createElement("div", {className: "sk-rect3"}), 
                        React.createElement("div", {className: "sk-rect4"}), 
                        React.createElement("div", {className: "sk-rect5"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duognnhu/workspace/codebrew/fe/src/js/components/loading/ThreeBounce.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function () {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-three-bounce"}, 
                        React.createElement("div", {className: "sk-bounce1"}), 
                        React.createElement("div", {className: "sk-bounce2"}), 
                        React.createElement("div", {className: "sk-bounce3"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duognnhu/workspace/codebrew/fe/src/js/index.js":[function(require,module,exports){
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var router = require("./router/router")
var SideNav = require("./components/SideNav")

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //app.receivedEvent('deviceready');
        $(document).ready(function() {
            router.init("/find_donation")
            React.render(React.createElement(SideNav, {dispatcher: dispatcher}), document.getElementById("nav-bar"))
        })
    }
};

app.initialize();

},{"./components/SideNav":"/home/duognnhu/workspace/codebrew/fe/src/js/components/SideNav.js","./router/router":"/home/duognnhu/workspace/codebrew/fe/src/js/router/router.js"}],"/home/duognnhu/workspace/codebrew/fe/src/js/router/router.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
var FindDonation = require("../components/FindDonation")
var PostRequest = require("../components/PostRequest")
window.renderFindDonation = function () {
    React.unmountComponentAtNode(document.getElementById("main-content"))
    React.render(React.createElement(FindDonation, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

window.renderPostRequest = function () {
    console.log("render post request")
    React.unmountComponentAtNode(document.getElementById("main-content"))
    React.render(React.createElement(PostRequest, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

module.exports = {
    init: function(path) {
        var routes = {
            "/find_donation": renderFindDonation,
            "/post_request": renderPostRequest
        }
        window.router = Router(routes);
        router.init(path)
    }
}
},{"../components/FindDonation":"/home/duognnhu/workspace/codebrew/fe/src/js/components/FindDonation.js","../components/PostRequest":"/home/duognnhu/workspace/codebrew/fe/src/js/components/PostRequest.js"}]},{},["/home/duognnhu/workspace/codebrew/fe/src/js/index.js"])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvY29tcG9uZW50cy9GaW5kRG9uYXRpb24uanMiLCJzcmMvanMvY29tcG9uZW50cy9Qb3N0UmVxdWVzdC5qcyIsInNyYy9qcy9jb21wb25lbnRzL1NpZGVOYXYuanMiLCJzcmMvanMvY29tcG9uZW50cy9idXR0b25zL1NsaWRlci5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfZG9uYXRpb24vRG9uYXRpb25DYXJkLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvZmluZF9kb25hdGlvbi9Eb25hdGlvblJlc3VsdC5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfZG9uYXRpb24vTG9jYXRpb25JbnB1dC5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfZG9uYXRpb24vTG9jYXRpb25TZXR0aW5nLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvZmluZF9kb25hdGlvbi9NYXBNYXJrZXIuanMiLCJzcmMvanMvY29tcG9uZW50cy9sb2FkaW5nL1NwaW5uZXJXYXZlLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvbG9hZGluZy9UaHJlZUJvdW5jZS5qcyIsInNyYy9qcy9pbmRleC5qcyIsInNyYy9qcy9yb3V0ZXIvcm91dGVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDNUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMTAvMDUvMTUuXG4gKi9cbnZhciBTbGlkZXIgPSByZXF1aXJlKFwiLi9idXR0b25zL1NsaWRlclwiKVxudmFyIFNwaW5uZXJXYXZlID0gcmVxdWlyZShcIi4vbG9hZGluZy9TcGlubmVyV2F2ZVwiKVxudmFyIEN1YmVHcmlkID0gcmVxdWlyZShcIi4vbG9hZGluZy9UaHJlZUJvdW5jZVwiKVxudmFyIERvbmF0aW9uUmVzdWx0ID0gcmVxdWlyZShcIi4vZmluZF9kb25hdGlvbi9Eb25hdGlvblJlc3VsdFwiKVxudmFyIExvY2F0aW9uU2V0dGluZyA9IHJlcXVpcmUoXCIuL2ZpbmRfZG9uYXRpb24vTG9jYXRpb25TZXR0aW5nXCIpXG52YXIgTWFwTWFya2VyID0gcmVxdWlyZShcIi4vZmluZF9kb25hdGlvbi9NYXBNYXJrZXJcIilcbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICBtaXhpbnM6IFtmbHV4Lm1peGlucy5zdG9yZUxpc3RlbmVyXSxcblxuICAgIHdhdGNoU3RvcmVzOiBbJ0xvY2F0aW9uU3RvcmUnXSxcblxuICAgIEN1cnJlbnRTdGF0ZToge1xuICAgICAgICBGSU5ESU5HOiBcImZpbmRpbmdcIixcbiAgICAgICAgSU5JVElBTDogXCJpbml0aWFsXCIsXG4gICAgICAgIFJFQURZOiAnUkVBRFknLFxuICAgICAgICBSRVNVTFQ6IFwicmVzdWx0XCJcbiAgICB9LFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjdXJyZW50U3RhdGU6ICh0aGlzLnByb3BzLmN1cnJlbnRTdGF0ZSkgPyB0aGlzLnByb3BzLmN1cnJlbnRTdGF0ZSA6IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwsXG4gICAgICAgICAgICBkb25hdGlvbnM6IFtdLFxuICAgICAgICAgICAgY29vcmRzOiB7fVxuICAgICAgICB9XG4gICAgfSxcblxuXG4gICAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24gKG5leHRQcm9wcykge1xuICAgICAgICBpZiAobmV4dFByb3BzLmN1cnJlbnRTdGF0ZSkge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiBuZXh0UHJvcHMuY3VycmVudFN0YXRlfSlcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBmaW5kRG9uYXRpb25zOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBwYXJlbnQgPSB0aGlzXG4gICAgICAgIHZhciB2YWx1ZSA9IHBhcmVudC5yZWZzLmxvY2F0aW9uLmdldFZhbHVlKClcbiAgICAgICAgYWN0aW9ucy5maW5kRG9uYXRpb25zKHZhbHVlLmFkZHJlc3MsIHZhbHVlLnJhZGl1cylcblxuICAgICAgICBpZiAod2luZG93LmRldmljZS5wbGF0Zm9ybSA9PSBcImJyb3dzZXJcIikge1xuICAgICAgICAgICAgZmluZCgpXG4gICAgICAgICAgICAkKHRoaXMucmVmcy5maW5kQnRuLmdldERPTU5vZGUoKSkuZmFkZU91dCgzMDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50U3RhdGU6IHBhcmVudC5DdXJyZW50U3RhdGUuRklORElOR1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9KVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBjdXJyZW50U3RhdGU6IHBhcmVudC5DdXJyZW50U3RhdGUuRklORElOR1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBzdG9yZURpZENoYW5nZTogZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgIHZhciBzdG9yZSA9IGRpc3BhdGNoZXIuZ2V0U3RvcmUobmFtZSlcbiAgICAgICAgICAgIHZhciBzdG9yZVN0YXRlID0gc3RvcmUuZ2V0U3RhdGUoKVxuXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhzdG9yZVN0YXRlLmRvbmF0aW9ucylcblxuICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuYWN0aW9uID09IHN0b3JlLmFjdGlvbnMuRklORF9ET05BVElPTikge1xuICAgICAgICAgICAgICAgIGlmIChzdG9yZVN0YXRlLnN1Y2Nlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGRvbmF0aW9uczogc3RvcmVTdGF0ZS5kb25hdGlvbnN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRyYXdNYXAodGhpcy5zdGF0ZS5jb29yZHMpXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbmF2aWdhdG9yLm5vdGlmaWNhdGlvbi5hbGVydChzdG9yZVN0YXRlLmVycm9yTWVzc2FnZSlcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWX0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGRyYXdNYXA6IGZ1bmN0aW9uIChjb29yZHMpIHtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5zdGF0ZS5kb25hdGlvbnMpXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogdGhpcy5DdXJyZW50U3RhdGUuUkVBRFl9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBNYXBNYXJrZXIuZ2V0TWFya2Vycyhjb29yZHMsIHRoaXMuc3RhdGUuZG9uYXRpb25zLCB0aGlzLnJlZnMuZ29vZ2xlTWFwLmdldERPTU5vZGUoKSlcbiAgICAgICAgfSlcbiAgICB9LFxuXG4gICAgc2V0VXBDb21wb25lbnQ6IGZ1bmN0aW9uIChyYWRpdXMpIHtcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcblx0dmFyIHJhZCA9IChyYWRpdXMpPyByYWRpdXMgOiA1XG5cdGNvbnNvbGUubG9nKHJhZClcbiAgICAgICAgbmF2aWdhdG9yLmdlb2xvY2F0aW9uLmdldEN1cnJlbnRQb3NpdGlvbihmdW5jdGlvbiAocG9zaXRpb24pIHtcbiAgICAgICAgICAgY29uc29sZS5sb2cocG9zaXRpb24pIFxuXHQgICBpZiAocGFyZW50LmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgdmFyIGNvb3JkcyA9IHtsYXQ6IHBvc2l0aW9uLmNvb3Jkcy5sYXRpdHVkZSwgbG5nOiBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlfVxuICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7Y29vcmRzOiBjb29yZHN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbnMuZmluZERvbmF0aW9ucyhjb29yZHMsIHJhZClcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy9wYXJlbnQuZHJhd01hcChjb29yZHMpXG4gICAgICAgICAgICAvKmdvb2dsZVV0aWxzLm1hcHMucmV2ZXJzZUdlb2NvZGluZyhwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUsIHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGUsXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKGFkZHJlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjdXJyZW50QWRkcmVzczogYWRkcmVzcywgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgYWxlcnQoZXJyb3IpXG4gICAgICAgICAgICAgICAgfSkqL1xuICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICBhbGVydChlcnJvci5tZXNzYWdlKVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7IGVuYWJsZUhpZ2hBY2N1cmFjeTogdHJ1ZSB9KVxuXHRjb25zb2xlLmxvZyhcIm8yOGV1MGR1YTl1XCIpXG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0VXBDb21wb25lbnQoKVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRVcGRhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0VXBDb21wb25lbnQoKVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCkge1xuICAgICAgICAgICAgcmV0dXJuIChSZWFjdC5jcmVhdGVFbGVtZW50KEN1YmVHcmlkLCB7dGV4dDogXCJMb2FkaW5nIC4uLlwifSkpXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuUkVBRFkpIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInMxMlwiLCBpZDogXCJzZWFyY2gtd3JhcHBlclwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2lkOiBcInNsaWRlci13cmFwcGVyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU2xpZGVyLCB7dmFsdWU6IDEwLCB1bml0OiBcImttXCIsIHNsaWRlclJlbGVhc2U6IHRoaXMuc2V0VXBDb21wb25lbnR9KVxuICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTJcIiwgaWQ6IFwiZ29vZ2xlLW1hcFwiLCByZWY6IFwiZ29vZ2xlTWFwXCJ9KVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5GSU5ESU5HKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU3Bpbm5lcldhdmUsIHt0ZXh0OiBcIkZpbmRpbmcgRG9uYXRpb24gUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID0gdGhpcy5DdXJyZW50U3RhdGUuUkVTVUxUKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRG9uYXRpb25SZXN1bHQsIHtkb25hdGlvbnM6IHRoaXMuc3RhdGUuZG9uYXRpb25zfSlcbiAgICAgICAgICAgIClcbiAgICAgICAgfVxuICAgIH1cbn0pXG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvZ25uaHUgb24gMjcvMDkvMTUuXG4gKi9cbnZhciBMb2NhdGlvbklucHV0ID0gcmVxdWlyZShcIi4vZmluZF9kb25hdGlvbi9Mb2NhdGlvbklucHV0XCIpXG52YXIgU3Bpbm5lcldhdmUgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1NwaW5uZXJXYXZlXCIpXG52YXIgQ3ViZUdyaWQgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1RocmVlQm91bmNlXCIpXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG4gICAgIG1peGluczogW2ZsdXgubWl4aW5zLnN0b3JlTGlzdGVuZXJdLFxuXG4gICAgd2F0Y2hTdG9yZXM6IFsnUG9zdFJlcXVlc3RTdG9yZSddLFxuXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogKHRoaXMucHJvcHMuY3VycmVudFN0YXRlKSA/IHRoaXMucHJvcHMuY3VycmVudFN0YXRlIDogdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCxcbiAgICAgICAgICAgIGJsb29kVHlwZTogXCJcIixcbiAgICAgICAgICAgIHRpbWU6IFwiXCIsXG4gICAgICAgICAgICBjdXJyZW50QWRkcmVzczogXCJcIixcbiAgICAgICAgICAgIG9yZ2FuaXphdGlvbk5hbWU6IFwiXCJcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICAgQ3VycmVudFN0YXRlOiB7XG4gICAgICAgIFBPU1RJTkc6IFwicG9zdGluZ1wiLFxuICAgICAgICBJTklUSUFMOiBcImluaXRpYWxcIixcbiAgICAgICAgUkVBRFk6ICdSRUFEWSdcbiAgICB9LFxuXG4gICAgc3RvcmVEaWRDaGFuZ2U6IGZ1bmN0aW9uIChzdG9yZU5hbWUpIHtcbiAgICAgICAgY29uc29sZS5sb2coc3RvcmVOYW1lKVxuXG4gICAgICAgIGlmICh0aGlzLmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICB2YXIgc3RvcmUgPSBkaXNwYXRjaGVyLmdldFN0b3JlKHN0b3JlTmFtZSlcbiAgICAgICAgICAgIHZhciBzdG9yZVN0YXRlID0gc3RvcmUuZ2V0U3RhdGUoKVxuICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgIGFsZXJ0KFwiUG9zdGVkIFN1Y2Nlc3NmdWxseVwiKVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBhbGVydChcIkVycm9yIFBvc3RpbmchXCIpXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgc2V0VXBDb21wb25lbnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUpXG4gICAgICAgIHZhciBwYXJlbnQgPSB0aGlzXG4gICAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgICAgICAgICBnb29nbGVVdGlscy5tYXBzLnJldmVyc2VHZW9jb2RpbmcocG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlLFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChhZGRyZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7Y3VycmVudEFkZHJlc3M6IGFkZHJlc3MsIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5SRUFEWX0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgYWxlcnQoZXJyb3IpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgfSwgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICBpZiAocGFyZW50LmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgYWxlcnQoZXJyb3IubWVzc2FnZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgeyBlbmFibGVIaWdoQWNjdXJhY3k6IHRydWUgfSlcbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRVcENvbXBvbmVudCgpXG4gICAgfSxcblxuICAgIG9uQmxvb2RUeXBlQ2hhbmdlOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtibG9vZFR5cGU6IGUudGFyZ2V0LnZhbHVlfSlcbiAgICB9LFxuXG4gICAgb25UaW1lQ2hhbmdlOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHt0aW1lOiBlLnRhcmdldC52YWx1ZX0pXG4gICAgfSxcblxuICAgIG9uTmFtZUNoYW5nZTogZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7b3JnYW5pemF0aW9uTmFtZTogZS50YXJnZXQudmFsdWV9KVxuICAgIH0sXG5cbiAgICBwb3N0UmVxdWVzdDogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgb3B0cyA9IHtcbiAgICAgICAgICAgIGJsb29kVHlwZTogdGhpcy5zdGF0ZS5ibG9vZFR5cGUsXG4gICAgICAgICAgICBhZGRyZXNzOiB0aGlzLnJlZnMuYWRkcmVzcy5nZXRWYWx1ZSgpLFxuICAgICAgICAgICAgdGltZTogdGhpcy5zdGF0ZS50aW1lLFxuICAgICAgICAgICAgb3JnYW5pemF0aW9uTmFtZTogdGhpcy5zdGF0ZS5vcmdhbml6YXRpb25OYW1lXG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5QT1NUSU5HIH0pXG4gICAgICAgIGFjdGlvbnMucG9zdFJlcXVlc3Qob3B0cylcbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMKSB7XG4gICAgICAgICAgICByZXR1cm4gKFJlYWN0LmNyZWF0ZUVsZW1lbnQoQ3ViZUdyaWQsIHt0ZXh0OiBcIkxvYWRpbmcgLi4uXCJ9KSlcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWSkge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicm93IGNhcmRcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaDRcIiwgbnVsbCwgXCJQb3N0aW5nIFJlcXVlc3RcIiksIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwidWxcIiwge2NsYXNzTmFtZTogXCJjb2xsZWN0aW9uXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb24taXRlbSBhdmF0YXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogXCJpbWcvYmxvb2R0eXBlX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIk9yZ2FuaXphdGlvbiBOYW1lOlwiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7b25DaGFuZ2U6IHRoaXMub25OYW1lQ2hhbmdlLCByZWY6IFwibmFtZVwiLCB0eXBlOiBcInRleHRcIiwgcGxhY2Vob2xkZXI6IFwibmFtZVwifSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICApLCBcblxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImxpXCIsIHtjbGFzc05hbWU6IFwiY29sbGVjdGlvbi1pdGVtIGF2YXRhclwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImltZ1wiLCB7c3JjOiBcImltZy9sb2NhdGlvbl9sb2dvLnBuZ1wiLCBjbGFzc05hbWU6IFwiY2lyY2xlXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInNwYW5cIiwge2NsYXNzTmFtZTogXCJ0aXRsZVwifSwgXCJMb2NhdGlvbjpcIiksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoTG9jYXRpb25JbnB1dCwge2luaXRBZGRyZXNzOiB0aGlzLnN0YXRlLmN1cnJlbnRBZGRyZXNzLCByZWY6IFwiYWRkcmVzc1wifSlcbiAgICAgICAgICAgICAgICAgICAgICAgICksIFxuXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwge2NsYXNzTmFtZTogXCJjb2xsZWN0aW9uLWl0ZW0gYXZhdGFyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW1nXCIsIHtzcmM6IFwiaW1nL2Jsb29kdHlwZV9sb2dvLnBuZ1wiLCBjbGFzc05hbWU6IFwiY2lyY2xlXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInNwYW5cIiwge2NsYXNzTmFtZTogXCJ0aXRsZVwifSwgXCJCbG9vZCBUeXBlOlwiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7cmVmOiBcImJsb29kVHlwZVwiLCBvbkNoYW5nZTogdGhpcy5vbkJsb29kVHlwZUNoYW5nZSwgeXBlOiBcInRleHRcIn0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb24taXRlbSBhdmF0YXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogXCJpbWcvYmxvb2R0eXBlX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIlRpbWU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtvbkNoYW5nZTogdGhpcy5vblRpbWVDaGFuZ2UsIHJlZjogXCJ0aW1lXCIsIHBsYWNlaG9sZGVyOiBcIk1NL2RkL3l5eXkgSEg6bW06c3NcIiwgdHlwZTogXCJ0ZXh0XCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJmb3JtLWZvb3RlciByaWdodFwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYVwiLCB7aHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIiwgb25DbGljazogdGhpcy5wb3N0UmVxdWVzdCwgY2xhc3NOYW1lOiBcIndhdmVzLWVmZmVjdCB3YXZlcy1ncmVlbiB0ZWFsIGxpZ2h0ZW4tMiBidG5cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiU2F2ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJhXCIsIHtocmVmOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiLCBjbGFzc05hbWU6IFwid2F2ZXMtZWZmZWN0IHdhdmVzLWdyZWVuIGJsdWUtZ3JleSBidG5cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQ2FuY2VsXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5QT1NUSU5HKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU3Bpbm5lcldhdmUsIHt0ZXh0OiBcIlBvc3RpbmcgRG9uYXRpb24gUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9XG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMTAvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICQoXCIjbWVudS1idG5cIikuc2lkZU5hdih7XG4gICAgICAgICAgICBlZGdlOiAnbGVmdCcsIC8vIENob29zZSB0aGUgaG9yaXpvbnRhbCBvcmlnaW5cbiAgICAgICAgICAgIGNsb3NlT25DbGljazogdHJ1ZSAvLyBDbG9zZXMgc2lkZS1uYXYgb24gPGE+IGNsaWNrcywgdXNlZnVsIGZvciBBbmd1bGFyL01ldGVvclxuICAgICAgICB9KTtcbiAgICAgICAgY29uc29sZS5sb2coXCJtb3VudGVkXCIpXG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgbGlua3MgPSBbXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwge2tleTogMX0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJhXCIsIHtocmVmOiBcIiMvZmluZF9kb25hdGlvblwiLCBjbGFzc05hbWU6IFwid2F2ZXMtZWZmZWN0IHdhdmVzLWxpZ2h0XCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlcIiwge2NsYXNzTmFtZTogXCJpb24tYW5kcm9pZC1zZWFyY2hcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgXCJGaW5kIERvbmF0aW9uIFJlcXVlc3RzXCJcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApLFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImxpXCIsIHtrZXk6IDIsIGNsYXNzTmFtZTogXCJkaXZpZGVyXCJ9KSxcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7a2V5OiAzfSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImFcIiwge2hyZWY6IFwiIy9wb3N0X3JlcXVlc3RcIiwgY2xhc3NOYW1lOiBcIndhdmVzLWVmZmVjdCB3YXZlcy1saWdodFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpXCIsIHtjbGFzc05hbWU6IFwiaW9uLWFuZHJvaWQtdW5sb2NrXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgIFwiUG9zdCBSZXF1ZXN0XCJcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApLFxuICAgICAgICBdXG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIG51bGwsIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJhXCIsIHtocmVmOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiLCBjbGFzc05hbWU6IFwiYnJhbmQtbG9nbyBjZW50ZXJcIn0sIGNvbmZpZy5hcHBfbmFtZSksIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJhXCIsIHtocmVmOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiLCBjbGFzc05hbWU6IFwiYnJhbmQtbG9nbyByaWdodFwiLCBpZDogXCJsb2dvXCJ9LCBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW1nXCIsIHtzcmM6IFwiaW1nL2FwcC1sb2dvLTk2LnBuZ1wifSkpLCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwidWxcIiwge2NsYXNzTmFtZTogXCJsZWZ0IGhpZGUtb24tbWVkLWFuZC1kb3duXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgbGlua3NcbiAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwidWxcIiwge2lkOiBcInNsaWRlLW91dFwiLCBjbGFzc05hbWU6IFwic2lkZS1uYXZcIn0sIFxuICAgICAgICAgICAgICAgICAgICBsaW5rc1xuICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImFcIiwge2hyZWY6IFwiI1wiLCBcImRhdGEtYWN0aXZhdGVzXCI6IFwic2xpZGUtb3V0XCIsIGNsYXNzTmFtZTogXCJidXR0b24tY29sbGFwc2VcIiwgaWQ6IFwibWVudS1idG5cIn0sIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpXCIsIHtjbGFzc05hbWU6IFwibWRpLW5hdmlnYXRpb24tbWVudVwifSkpXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMy8wNS8xNS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICB2YWx1ZTogKHRoaXMucHJvcHMudmFsdWUpPyB0aGlzLnByb3BzLnZhbHVlIDogMFxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcbiAgICAgICAgdGhpcy5hY3RpdmF0ZVNsaWRlcigpXG4gICAgfSxcblxuICAgIHNldFZhbHVlOiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHt2YWx1ZTogdmFsdWV9KVxuICAgIH0sXG5cbiAgICBhY3RpdmF0ZVNsaWRlcjogIGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgbWluID0gKHRoaXMucHJvcHMubWluKT8gdGhpcy5wcm9wcy5taW4gOiAwXG4gICAgICAgIHZhciBtYXggPSAodGhpcy5wcm9wcy5tYXgpPyB0aGlzLnByb3BzLm1heCA6IDUwXG4gICAgICAgIHZhciBub2RlID0gJCh0aGlzLnJlZnMuc2xpZGVyLmdldERPTU5vZGUoKSlcbiAgICAgICAgbm9kZS5ub1VpU2xpZGVyKHtcbiAgICAgICAgICAgIHN0YXJ0OiB0aGlzLnN0YXRlLnZhbHVlLFxuICAgICAgICAgICAgY29ubmVjdDogXCJsb3dlclwiLFxuICAgICAgICAgICAgb3JpZW50YXRpb246IFwiaG9yaXpvbnRhbFwiLFxuICAgICAgICAgICAgZGlyZWN0aW9uOiBcImx0clwiLFxuICAgICAgICAgICAgYmVoYXZpb3VyOiBcInRhcC1kcmFnXCIsXG4gICAgICAgICAgICByYW5nZToge1xuICAgICAgICAgICAgICAgICdtaW4nOiBtaW4sXG4gICAgICAgICAgICAgICAgJ21heCc6IG1heFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGZvcm1hdDogd051bWIoe1xuICAgICAgICAgICAgICAgIGRlY2ltYWxzOiAwXG4gICAgICAgICAgICB9KVxuICAgICAgICB9KTtcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgbm9kZS5vbignc2xpZGUnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHBhcmVudC5zZXRWYWx1ZSgkKHRoaXMpLnZhbCgpKVxuICAgICAgICB9KVxuXHRub2RlLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbiAoKSB7XG5cdCAgICBpZiAocGFyZW50LnByb3BzLnNsaWRlclJlbGVhc2UpIHtcblx0XHRwYXJlbnQucHJvcHMuc2xpZGVyUmVsZWFzZSgkKHRoaXMpLnZhbCgpKVxuXHQgICAgfVxuXHR9KVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHVuaXQgPSAodGhpcy5wcm9wcy51bml0KT8gXCIgXCIgK3RoaXMucHJvcHMudW5pdCA6IFwiXCJcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEyIHNsaWRlci1jb250YWluZXJcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzbGlkZXIgY29sIHMxMFwiLCByZWY6IFwic2xpZGVyXCJ9KSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMiBzbGlkZXItdmFsdWVcIn0sIHRoaXMuc3RhdGUudmFsdWUgKyB1bml0KVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSlcbiIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMC8wNS8xNS5cbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEyIG04IG9mZnNldC1tMiBsNiBvZmZzZXQtbDNcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjYXJkLXBhbmVsIGdyZXkgbGlnaHRlbi01IHotZGVwdGgtMVwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3cgdmFsaWduLXdyYXBwZXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMlwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImltZ1wiLCB7c3JjOiB0aGlzLnByb3BzLmRvbmF0aW9uLnByb2ZpbGVJbWcsIGFsdDogXCJQcm9maWxlIFBpY3R1cmVcIiwgY2xhc3NOYW1lOiBcImNpcmNsZSByZXNwb25zaXZlLWltZ1wifSlcbiAgICAgICAgICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTBcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwXCIsIG51bGwpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCB7Y2xhc3NOYW1lOiBcImJsYWNrLXRleHRcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmRvbmF0aW9uLmRlc2NyaXB0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjAvMDUvMTUuXG4gKi9cblxudmFyIERvbmF0aW9uQ2FyZCA9IHJlcXVpcmUoXCIuL0RvbmF0aW9uQ2FyZFwiKVxuXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgZG9uYXRpb25zOiAodGhpcy5wcm9wcy5kb25hdGlvbnMpPyB0aGlzLnByb3BzLmRvbmF0aW9ucyA6IFtdXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24obmV4dFByb3BzKSB7XG4gICAgICAgIGlmIChuZXh0UHJvcHMuZG9uYXRpb25zICYmIEpTT04uc3RyaW5naWZ5KG5leHRQcm9wcy5kb25hdGlvbnMpICE9IEpTT04uc3RyaW5naWZ5KHRoaXMucHJvcHMuZG9uYXRpb25zKSkge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZG9uYXRpb25zOiBuZXh0UHJvcHMuZG9uYXRpb25zfSlcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5kb25hdGlvbnMubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImgzXCIsIG51bGwsIFxuICAgICAgICAgICAgICAgICAgICBcIk5vIGRvbmF0aW9uIHJlcXVlc3RzIGFyZSBmb3VuZCBhcm91bmQgeW91ciBsb2NhdGlvbiFcIlxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImJ1c2tlci1saXN0XCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmRvbmF0aW9ucy5tYXAoZnVuY3Rpb24oaXRlbSwgaWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KERvbmF0aW9uQ2FyZCwge2RvbmF0aW9uOiBpdGVtLCBrZXk6IGlkfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH1cbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMy8wNS8xNS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBzZWFyY2hCb3g6IHVuZGVmaW5lZFxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3NlYXJjaEJveDogbmV3IGdvb2dsZS5tYXBzLnBsYWNlcy5TZWFyY2hCb3godGhpcy5yZWZzLmlucHV0LmdldERPTU5vZGUoKSl9KVxuICAgIH0sXG5cbiAgICBnZXRWYWx1ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5yZWZzLmlucHV0LmdldERPTU5vZGUoKS52YWx1ZVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3dcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJpbnB1dC1maWVsZCBjb2wgczEyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHt0eXBlOiBcInRleHRcIiwgcGxhY2Vob2xkZXI6IFwiQ2hvb3NlIGEgbG9jYXRpb25cIiwgY2xhc3NOYW1lOiBcInZhbGlkYXRlXCIsIHJlZjogXCJpbnB1dFwiLCBkZWZhdWx0VmFsdWU6IHRoaXMucHJvcHMuaW5pdEFkZHJlc3N9KVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIzLzA1LzE1LlxuICovXG52YXIgU2xpZGVyID0gcmVxdWlyZShcIi4uL2J1dHRvbnMvU2xpZGVyXCIpXG52YXIgTG9jYXRpb25JbnB1dCA9IHJlcXVpcmUoXCIuL0xvY2F0aW9uSW5wdXRcIilcblxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgY29tcG9uZW50V2lsbFVwZGF0ZTogZnVuY3Rpb24gKG5leHRQcm9wcykge1xuICAgICAgICBpZiAobmV4dFByb3BzLmluaXRBZGRyZXNzICYmIG5leHRQcm9wcy5pbml0QWRkcmVzcyAhPSB0aGlzLnByb3BzLmluaXRBZGRyZXNzKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgIH0sXG5cbiAgICBnZXRWYWx1ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgYWRkcmVzczogdGhpcy5yZWZzLmFkZHJlc3MuZ2V0VmFsdWUoKSxcbiAgICAgICAgICAgIHJhZGl1czogdGhpcy5yZWZzLnJhZGl1cy5zdGF0ZS52YWx1ZVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInJvd1wiLCBpZDogXCJsb2NhdGlvbi1zZXR0aW5nXCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZm9ybVwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTJcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicm93XCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoTG9jYXRpb25JbnB1dCwge2luaXRBZGRyZXNzOiB0aGlzLnByb3BzLmluaXRBZGRyZXNzLCByZWY6IFwiYWRkcmVzc1wifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTIgbGFiZWxcIn0sIFwiUmFkaXVzXCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU2xpZGVyLCB7cmVmOiBcInJhZGl1c1wiLCB2YWx1ZTogMTAsIHVuaXQ6IFwia21cIn0pXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9nbm5odSBvbiAyNi8wOS8xNS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgZ2V0TWFya2VyczogZnVuY3Rpb24gKGN1cnJlbnRMb2NhdGlvbiwgZG9uYXRpb25zLCBkb20sIHJhZGl1cykge1xuICAgICAgICBjb25zb2xlLmxvZyhkb25hdGlvbnMpXG4gICAgICAgIHZhciBtYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGRvbSwge1xuICAgICAgICAgICAgY2VudGVyOiBjdXJyZW50TG9jYXRpb24sXG4gICAgICAgICAgICB6b29tOiBjb25maWcuZ29vZ2xlTWFwLnpvb21cbiAgICAgICAgfSk7XG4gICAgICAgIHZhciBjdXJyZW50TG9jYXRpb25NYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBjdXJyZW50TG9jYXRpb24sXG4gICAgICAgICAgICB0aXRsZTogJ1lvdSBhcmUgaGVyZSdcbiAgICAgICAgfSk7XG4gICAgICAgIGN1cnJlbnRMb2NhdGlvbk1hcmtlci5zZXRNYXAobWFwKTtcbiAgICAgICAgdmFyIGN1cnJlbnRMb2NhdGlvbkluZm9XaW5kb3cgPSBuZXcgZ29vZ2xlLm1hcHMuSW5mb1dpbmRvdyh7XG4gICAgICAgICAgICBjb250ZW50OiBcIjxwPllvdSBhcmUgaGVyZSE8L3A+XCJcbiAgICAgICAgfSk7XG4gICAgICAgIGN1cnJlbnRMb2NhdGlvbk1hcmtlci5hZGRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBjdXJyZW50TG9jYXRpb25JbmZvV2luZG93Lm9wZW4obWFwLCBjdXJyZW50TG9jYXRpb25NYXJrZXIpO1xuICAgICAgICB9KVxuXG5cbiAgICAgICAgZG9uYXRpb25zLmZvckVhY2goZnVuY3Rpb24gKGRvbmF0aW9uLCBpZCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coaWQpXG4gICAgICAgICAgICBpZiAoZG9uYXRpb24ubG9jYXRpb24pIHtcbiAgICAgICAgICAgICAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiB7bGF0OiBwYXJzZUZsb2F0KGRvbmF0aW9uLmxhdGl0dWRlKSwgbG5nOiBwYXJzZUZsb2F0KGRvbmF0aW9uLmxvbmdpdHVkZSl9LFxuICAgICAgICAgICAgICAgICAgICBpY29uOiBjb25maWcuaG9zcGl0YWxfaWNvbixcbiAgICAgICAgICAgICAgICAgICAgbWFwOiBtYXBcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIG1hcmtlci5hZGRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5wb3B1cERvbmF0aW9uRGV0YWlscyhkb25hdGlvbilcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuICAgIH1cbn1cbiIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMC8wNS8xNS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic3Bpbm5lci13cmFwcGVyXCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic3Bpbm5lci1jZWxsXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXNwaW5uZXIgc2stc3Bpbm5lci13YXZlXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1yZWN0MVwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXJlY3QyXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stcmVjdDNcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1yZWN0NFwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXJlY3Q1XCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCB0aGlzLnByb3BzLnRleHQpXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMy8wNS8xNS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNwaW5uZXItd3JhcHBlclwifSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNwaW5uZXItY2VsbFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1zcGlubmVyIHNrLXNwaW5uZXItdGhyZWUtYm91bmNlXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1ib3VuY2UxXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stYm91bmNlMlwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLWJvdW5jZTNcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwXCIsIG51bGwsIHRoaXMucHJvcHMudGV4dClcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLypcbiAqIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiAqIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuICogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiAqIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiAqIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiAqIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuICogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiAqIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4gKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuICogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4gKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4gKiB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG52YXIgcm91dGVyID0gcmVxdWlyZShcIi4vcm91dGVyL3JvdXRlclwiKVxudmFyIFNpZGVOYXYgPSByZXF1aXJlKFwiLi9jb21wb25lbnRzL1NpZGVOYXZcIilcblxudmFyIGFwcCA9IHtcbiAgICAvLyBBcHBsaWNhdGlvbiBDb25zdHJ1Y3RvclxuICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICB0aGlzLmJpbmRFdmVudHMoKTtcbiAgICB9LFxuICAgIC8vIEJpbmQgRXZlbnQgTGlzdGVuZXJzXG4gICAgLy9cbiAgICAvLyBCaW5kIGFueSBldmVudHMgdGhhdCBhcmUgcmVxdWlyZWQgb24gc3RhcnR1cC4gQ29tbW9uIGV2ZW50cyBhcmU6XG4gICAgLy8gJ2xvYWQnLCAnZGV2aWNlcmVhZHknLCAnb2ZmbGluZScsIGFuZCAnb25saW5lJy5cbiAgICBiaW5kRXZlbnRzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignZGV2aWNlcmVhZHknLCB0aGlzLm9uRGV2aWNlUmVhZHksIGZhbHNlKTtcbiAgICB9LFxuICAgIC8vIGRldmljZXJlYWR5IEV2ZW50IEhhbmRsZXJcbiAgICAvL1xuICAgIC8vIFRoZSBzY29wZSBvZiAndGhpcycgaXMgdGhlIGV2ZW50LiBJbiBvcmRlciB0byBjYWxsIHRoZSAncmVjZWl2ZWRFdmVudCdcbiAgICAvLyBmdW5jdGlvbiwgd2UgbXVzdCBleHBsaWNpdGx5IGNhbGwgJ2FwcC5yZWNlaXZlZEV2ZW50KC4uLik7J1xuICAgIG9uRGV2aWNlUmVhZHk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAvL2FwcC5yZWNlaXZlZEV2ZW50KCdkZXZpY2VyZWFkeScpO1xuICAgICAgICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJvdXRlci5pbml0KFwiL2ZpbmRfZG9uYXRpb25cIilcbiAgICAgICAgICAgIFJlYWN0LnJlbmRlcihSZWFjdC5jcmVhdGVFbGVtZW50KFNpZGVOYXYsIHtkaXNwYXRjaGVyOiBkaXNwYXRjaGVyfSksIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibmF2LWJhclwiKSlcbiAgICAgICAgfSlcbiAgICB9XG59O1xuXG5hcHAuaW5pdGlhbGl6ZSgpO1xuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIwLzA1LzE1LlxuICovXG52YXIgRmluZERvbmF0aW9uID0gcmVxdWlyZShcIi4uL2NvbXBvbmVudHMvRmluZERvbmF0aW9uXCIpXG52YXIgUG9zdFJlcXVlc3QgPSByZXF1aXJlKFwiLi4vY29tcG9uZW50cy9Qb3N0UmVxdWVzdFwiKVxud2luZG93LnJlbmRlckZpbmREb25hdGlvbiA9IGZ1bmN0aW9uICgpIHtcbiAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFpbi1jb250ZW50XCIpKVxuICAgIFJlYWN0LnJlbmRlcihSZWFjdC5jcmVhdGVFbGVtZW50KEZpbmREb25hdGlvbiwge2N1cnJlbnRTdGF0ZTogXCJpbml0aWFsXCIsIGRpc3BhdGNoZXI6IGRpc3BhdGNoZXJ9KSwgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYWluLWNvbnRlbnRcIikpXG59XG5cbndpbmRvdy5yZW5kZXJQb3N0UmVxdWVzdCA9IGZ1bmN0aW9uICgpIHtcbiAgICBjb25zb2xlLmxvZyhcInJlbmRlciBwb3N0IHJlcXVlc3RcIilcbiAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFpbi1jb250ZW50XCIpKVxuICAgIFJlYWN0LnJlbmRlcihSZWFjdC5jcmVhdGVFbGVtZW50KFBvc3RSZXF1ZXN0LCB7Y3VycmVudFN0YXRlOiBcImluaXRpYWxcIiwgZGlzcGF0Y2hlcjogZGlzcGF0Y2hlcn0pLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgaW5pdDogZnVuY3Rpb24ocGF0aCkge1xuICAgICAgICB2YXIgcm91dGVzID0ge1xuICAgICAgICAgICAgXCIvZmluZF9kb25hdGlvblwiOiByZW5kZXJGaW5kRG9uYXRpb24sXG4gICAgICAgICAgICBcIi9wb3N0X3JlcXVlc3RcIjogcmVuZGVyUG9zdFJlcXVlc3RcbiAgICAgICAgfVxuICAgICAgICB3aW5kb3cucm91dGVyID0gUm91dGVyKHJvdXRlcyk7XG4gICAgICAgIHJvdXRlci5pbml0KHBhdGgpXG4gICAgfVxufSJdfQ==
