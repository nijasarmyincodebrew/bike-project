/****************************************************************************
** Meta object code from reading C++ file 'notification.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../build/src/plugins/cordova-plugin-dialogs/notification.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'notification.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Dialogs_t {
    QByteArrayData data[17];
    char stringdata0[148];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Dialogs_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Dialogs_t qt_meta_stringdata_Dialogs = {
    {
QT_MOC_LITERAL(0, 0, 7), // "Dialogs"
QT_MOC_LITERAL(1, 8, 4), // "beep"
QT_MOC_LITERAL(2, 13, 0), // ""
QT_MOC_LITERAL(3, 14, 4), // "scId"
QT_MOC_LITERAL(4, 19, 4), // "ecId"
QT_MOC_LITERAL(5, 24, 5), // "times"
QT_MOC_LITERAL(6, 30, 5), // "alert"
QT_MOC_LITERAL(7, 36, 7), // "message"
QT_MOC_LITERAL(8, 44, 5), // "title"
QT_MOC_LITERAL(9, 50, 11), // "buttonLabel"
QT_MOC_LITERAL(10, 62, 7), // "confirm"
QT_MOC_LITERAL(11, 70, 12), // "buttonLabels"
QT_MOC_LITERAL(12, 83, 6), // "prompt"
QT_MOC_LITERAL(13, 90, 11), // "defaultText"
QT_MOC_LITERAL(14, 102, 31), // "notificationDialogButtonPressed"
QT_MOC_LITERAL(15, 134, 8), // "buttonId"
QT_MOC_LITERAL(16, 143, 4) // "text"

    },
    "Dialogs\0beep\0\0scId\0ecId\0times\0alert\0"
    "message\0title\0buttonLabel\0confirm\0"
    "buttonLabels\0prompt\0defaultText\0"
    "notificationDialogButtonPressed\0"
    "buttonId\0text"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Dialogs[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    3,   39,    2, 0x0a /* Public */,
       6,    5,   46,    2, 0x0a /* Public */,
      10,    5,   57,    2, 0x0a /* Public */,
      12,    6,   68,    2, 0x0a /* Public */,
      14,    3,   81,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,    3,    4,    5,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,    4,    7,    8,    9,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QStringList,    3,    4,    7,    8,   11,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QStringList, QMetaType::QString,    3,    4,    7,    8,   11,   13,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::Bool,   15,   16,   12,

       0        // eod
};

void Dialogs::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Dialogs *_t = static_cast<Dialogs *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->beep((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->alert((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QString(*)>(_a[5]))); break;
        case 2: _t->confirm((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QStringList(*)>(_a[5]))); break;
        case 3: _t->prompt((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QStringList(*)>(_a[5])),(*reinterpret_cast< const QString(*)>(_a[6]))); break;
        case 4: _t->notificationDialogButtonPressed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObject Dialogs::staticMetaObject = {
    { &CPlugin::staticMetaObject, qt_meta_stringdata_Dialogs.data,
      qt_meta_data_Dialogs,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Dialogs::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Dialogs::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Dialogs.stringdata0))
        return static_cast<void*>(const_cast< Dialogs*>(this));
    return CPlugin::qt_metacast(_clname);
}

int Dialogs::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CPlugin::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
