/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            fountains: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    findDonations: function () {
        var parent = this
        var value = parent.refs.location.getValue()
        actions.findDonations(value.address, value.radius)

        if (window.device.platform == "browser") {
            find()
            $(this.refs.findBtn.getDOMNode()).fadeOut(300, function () {
                parent.setState({
                    currentState: parent.CurrentState.FINDING
                })
            })
        } else {
            parent.setState({
                currentState: parent.CurrentState.FINDING
            })
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_POD) {
                if (storeState.success) {
                    this.setState({fountains: storeState.fountains}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.getMarkers(coords, this.state.fountains, this.refs.googleMap.getDOMNode())
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        navigator.geolocation.getCurrentPosition(function (position) {
            var find = null
            if (parent.isMounted()) {
                var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                window.coords = coords
                parent.setState({coords: coords}, function () {
                    actions.findDonations(coords, rad)
                })
            } else {
                find = null
            }
            //parent.drawMap(coords)
            /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
             function (address) {
             parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
             }, function (error) {
             alert(error)
             })*/
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, {enableHighAccuracy: true})
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            this.setUpComponent()
        }
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (<CubeGrid text="Loading ..."/>)
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                <div className="s12" id="search-wrapper">
                    <div className="col s12" id="google-map" ref="googleMap"></div>
                </div>
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                <SpinnerWave text="Finding Pods Requests ..."/>
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                <DonationResult donations={this.state.donations}/>
            )
        }
    }
})
