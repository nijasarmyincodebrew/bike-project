/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            fountains: [],
            merchants: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_FOUNTAIN) {
                if (storeState.success) {
                    this.setState({fountains: storeState.fountains}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            } else if (storeState.action == store.actions.FIND_MERCHANT){
                 if (storeState.success) {
                    this.setState({merchants: storeState.merchants}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.initSearch(coords, this.refs.googleMap.getDOMNode(), {
                "items": this.state.fountains,
                "icon": config.fountain_icon
            })
        })

        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.initSearch(coords, this.refs.googleMap.getDOMNode(), {
                "items": this.state.merchants,
                "icon": config.retail_icon
            })
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        if (!window.coords) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var find = null
                if (parent.isMounted()) {
                    var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                    parent.setState({coords: coords}, function () {
                        actions.findDrinkingFountain(coords, rad)
                    })
                } else {
                    find = null
                }
                //parent.drawMap(coords)
                /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                 function (address) {
                 parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                 }, function (error) {
                 alert(error)
                 })*/
            }, function (error) {
                if (parent.isMounted()) {
                    alert(error.message)
                }
            }, {enableHighAccuracy: true})
        } else {
            parent.setState({coords: coords}, function () {
                actions.findDrinkingFountain(window.coords, rad)
                actions.findMerchants(window.coords, rad)
            })
        }

    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            //this.setUpComponent()
        }
    },

    render: function () {
        console.log(this.state.currentState)
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (<CubeGrid text="Loading ..."/>)
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                <div className="s12" id="search-wrapper">
                    <nav>
                        <div className="nav-wrapper to-go-nav">
                            <div className="row">
                                <div className="col s11">
                                    <h5>Where to go next?</h5>
                                </div>
                                <div className="col s11">
                                    <div className="input-field">
                                        <input value="Current Location" type="search"/>
                                        <label><i className="">Start</i></label>
                                    </div>
                                </div>
                                <div className="col s11">
                                    <div className="input-field">
                                        <input id="place-search" type="search"/>
                                        <label><i className="">End</i></label>
                                    </div>
                                </div>
                                <div className="col s11">
                                    <a href="#/rewards" className="btn btn-primary red" style={{marginBottom: "10px"}}>
                                        Go
                                    </a>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div className="col s12" id="google-map" ref="googleMap"></div>
                </div>
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                <SpinnerWave text="Finding Pods Requests ..."/>
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                <DonationResult donations={this.state.donations}/>
            )
        }
    }
})
