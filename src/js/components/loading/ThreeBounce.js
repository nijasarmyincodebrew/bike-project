/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({
    render: function () {
        return (
            <div className="spinner-wrapper">
                <div className="spinner-cell">
                    <div className="sk-spinner sk-spinner-three-bounce">
                        <div className="sk-bounce1"></div>
                        <div className="sk-bounce2"></div>
                        <div className="sk-bounce3"></div>
                        <p>{this.props.text}</p>
                    </div>
                </div>
            </div>
        )
    }
})