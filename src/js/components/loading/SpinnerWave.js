/**
 * Created by duongnhu on 20/05/15.
 */
module.exports = React.createClass({
    render: function() {
        return (
            <div className="spinner-wrapper">
                <div className="spinner-cell">
                    <div className="sk-spinner sk-spinner-wave">
                        <div className="sk-rect1"></div>
                        <div className="sk-rect2"></div>
                        <div className="sk-rect3"></div>
                        <div className="sk-rect4"></div>
                        <div className="sk-rect5"></div>
                        <p>{this.props.text}</p>
                    </div>
                </div>
            </div>
        )
    }
})