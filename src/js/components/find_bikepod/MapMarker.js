/**
 * Created by duognnhu on 26/09/15.
 */
var gApi = require("../../utils/google")
module.exports = {
    getMarkers: function (currentLocation, pods, dom, radius, icon) {
        if (!window.map) {
            window.map = new google.maps.Map(dom, {
                center: currentLocation,
                zoom: config.googleMap.zoom,
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: false
            });
        }
        google.maps.visualRefresh = true;
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here',
            icon: config.user_current_location_icon
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })

        pods.forEach(function (pod, id) {
            var coordinates = pod.coordinates.coordinates
            var marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(coordinates["lat"]),
                    lng: parseFloat(coordinates["lon"])
                },
                icon: (icon) ? icon : config.bike_icon
            })
            marker.addListener('click', function () {
                window.popupPodDetails(pod)
            })
            marker.setMap(map)
        })
        new google.maps.Circle({
            strokeColor: 'green',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.0,
            map: map,
            center: currentLocation,
            radius: 1000
        });
    },

    initSearch: function (currentLocation, dom, data) {
        var markers = []
        if (!window.map) {
            window.map = new google.maps.Map(dom, {
                center: currentLocation,
                zoom: config.googleMap.zoom,
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: false
            });
        }
        google.maps.visualRefresh = true;
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here',
            icon: config.user_current_location_icon
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })

        data.items.forEach(function (point) {
            var coordinates = point.coordinates.coordinates
            var mOpts = {
                position: {
                    lat: parseFloat(coordinates["lat"]),
                    lng: parseFloat(coordinates["lon"])
                }
            }
            if (data.icon) {
                mOpts["icon"] = data.icon

            }
            var marker = new google.maps.Marker(mOpts)
            marker.setMap(map)
            markers.push(marker)
        })
        new google.maps.Circle({
            strokeColor: 'green',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.0,
            map: map,
            center: currentLocation,
            radius: 1000
        });
        var searchPlacesMarkers = []
        if (document.getElementById('place-search')) {
            // Create the search box and link it to the UI element.
            var input = document.getElementById('place-search');
            var searchBox = new google.maps.places.SearchBox(input);
            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var directionsDisplay = new google.maps.DirectionsRenderer({
                preserveViewport: true,
            });
            directionsDisplay.setMap(map)
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                searchPlacesMarkers.forEach(function (marker) {
                    marker.setMap(null)
                })
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    var marker = new google.maps.Marker({
                        map: map,
                        title: place.name,
                        position: place.geometry.location
                    })
                    searchPlacesMarkers.push(marker)
                    var mLocationInfo = new google.maps.InfoWindow({
                        content: "<p>" + place.name + "</p>"
                    });
                    marker.addListener('click', function () {
                        mLocationInfo.open(map, marker);
                    })

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }


                    gApi.maps.getDir(currentLocation, place.geometry.location, function (direction) {
                        directionsDisplay.setDirections(direction)
                    });
                })
                currentLocationMarker.setMap(null)
                map.fitBounds(bounds);

            });
        }
    }
}
