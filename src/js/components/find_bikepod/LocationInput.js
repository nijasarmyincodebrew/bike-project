/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({

    getInitialState: function () {
        return {
            searchBox: undefined
        }
    },

    componentDidMount: function () {
        this.setState({searchBox: new google.maps.places.SearchBox(this.refs.input.getDOMNode())})
    },

    getValue: function () {
        return this.refs.input.getDOMNode().value
    },

    render: function () {
        return (
            <div className="row">
                <div className="input-field col s12">
                    <input type="text"  placeholder="Choose a location" className="validate" ref="input" defaultValue={this.props.initAddress}/>
                </div>
            </div>
        )
    }
})