/**
 * Created by duognnhu on 27/09/15.
 */

module.exports = React.createClass({

    componentDidMount: function () {
        $(this.refs.modal.getDOMNode()).openModal()
    },

    makePayment: function () {

    },

    render: function () {
        var devicePlatorm = window.device.platform;
        var backbtn = (<a href="javascript:void(0)"
                          className="modal-action modal-close waves-effect waves-green btn-flat glyphicon ion-android-arrow-back"></a>)
        if (devicePlatorm != "browser") {
            backbtn = null
        }
        return (
            <div className="modal donation-details" ref="modal">
                <div className="modal-content">
                    <div className="modal-header">
                        <div className="col s12 header">
                            <img src="https://s3-ap-southeast-2.amazonaws.com/imm-travel-app/bike-highlight.png"/>
                            <h6>{this.props.name}</h6>
                        </div>
                    </div>
                    <div className="modal-body">
                        <div className="col s12 pod-info">
                            <div className="circle green">
                                {this.props.bikes_no}
                            </div>
                            <div className="rectangle blue">
                            </div>
                            <div className="circle blue">
                                {this.props.empty}
                            </div>
                            <table className="table">
                                <tr>
                                    <td>
                                        Cyles
                                    </td>
                                    <td>
                                        Docks
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div className=" col s12modal-button-container">
                            <button className="waves-effect waves-light btn btn-primary green book-btn" onClick={window.popupPayment}>Book Online
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
})
