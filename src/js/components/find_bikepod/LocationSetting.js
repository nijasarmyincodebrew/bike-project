/**
 * Created by duongnhu on 23/05/15.
 */
var Slider = require("../buttons/Slider")
var LocationInput = require("./LocationInput")

module.exports = React.createClass({

    componentWillUpdate: function (nextProps) {
        if (nextProps.initAddress && nextProps.initAddress != this.props.initAddress) {
            return true
        }
        return false
    },

    getValue: function () {
        return {
            address: this.refs.address.getValue(),
            radius: this.refs.radius.state.value
        }
    },

    render: function () {
        return (
            <div className="row" id="location-setting">
                <form className="col s12">
                    <div className="row">
                        <LocationInput initAddress={this.props.initAddress} ref="address"/>
                        <div className="col s12 label">Radius</div>
                        <Slider ref="radius" value={10} unit="km"/>
                    </div>
                </form>
            </div>
        )
    }
})