/**
 * Created by duognnhu on 27/09/15.
 */
module.exports = React.createClass({
    componentDidMount: function () {
        $(this.refs.modal.getDOMNode()).openModal()
    },

    render: function () {
        return (
            <div className="modal donation-details" ref="modal">
                <div class="modal-content">
                    <div className="row modal-header">
                        <div className="col s1">
                            <a href="javascript:void(0)" className="modal-action modal-close waves-effect waves-green btn-flat glyphicon ion-android-arrow-back"></a>
                        </div>
                        <div className="col s11 header">
                            <h4>{this.props.organizationName}</h4>
                        </div>
                    </div>
                    <div>
                        <p className="center tick-img"><img src="/img/confirm_icon.png"/></p>
                        <p className="center tnk-text">Successfully Scheduled!</p>
                    </div>
                    <ul className="collection">
                        <li className="collection-item avatar">
                            <img src="img/location_logo.png" className="circle"/>
                            <span className="title">Location</span>
                            <p>{this.props.location}</p>
                        </li>

                        <li className="collection-item avatar">
                            <img src="img/endtime_logo.png" className="circle"/>
                            <span className="title">Time:</span>
                            <p>{this.props.endTime}</p>
                        </li>
                    </ul>
                    <div className="modal-footer">
                            <a href="javascript:void(0)" className="modal-action modal-close waves-effect waves-green btn">Close</a>
                    </div>
                </div>
            </div>
        )
    }
})