/**
 * Created by duongnhu on 20/05/15.
 */

module.exports = React.createClass({

    render: function () {
        return (
            <div className="col s12 m8 offset-m2 l6 offset-l3">
                <div className="card-panel grey lighten-5 z-depth-1">
                    <div className="row valign-wrapper">
                        <div className="col s2">
                            <img src={this.props.donation.profileImg} alt="Profile Picture" className="circle responsive-img"/>
                        </div>
                        <div className="col s10">
                            <p></p>
                            <p className="black-text">
                                {this.props.donation.description}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
})