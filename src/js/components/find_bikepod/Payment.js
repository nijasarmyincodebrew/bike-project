/**
 * Created by duongnhu on 9/10/16.
 */
/**
 * Created by duognnhu on 27/09/15.
 */

module.exports = React.createClass({

    componentDidMount: function () {
        $(this.refs.modal.getDOMNode()).openModal()
         PaymentSession.configure({
            fields: {
                // ATTACH HOSTED FIELDS TO YOUR PAYMENT PAGE FOR A CREDIT CARD
                card: {
                    number: "#card-number",
                    securityCode: "#security-code",
                    expiryMonth: "#expiry-month",
                    expiryYear: "#expiry-year"
                }
            },
            //SPECIFY YOUR MITIGATION OPTION HERE
            frameEmbeddingMitigation: ["javascript"],
            callbacks: {
                initialized: function (response) {
                    // HANDLE INITIALIZATION RESPONSE
                    console.log(response)
                },
                formSessionUpdate: function (response) {
                    // HANDLE RESPONSE FOR UPDATE SESSION
                    if (response.status) {
                        if ("ok" == response.status) {
                            window.location.href = '#/where-go'
                            console.log("Session updated with data: " + response.session.id);

                            //check if the security code was provided by the user
                            if (response.sourceOfFunds.provided.card.securityCode) {
                                console.log("Security code was provided.");
                            }

                            //check if the user entered a MasterCard credit card
                            if (response.sourceOfFunds.provided.card.scheme == 'MASTERCARD') {
                                console.log("The user entered a MasterCard credit card.")
                            }
                        } else if ("fields_in_error" == response.status) {

                            console.log("Session update failed with field errors.");
                            if (response.errors.cardNumber) {
                                console.log("Card number invalid or missing.");
                            }
                            if (response.errors.expiryYear) {
                                console.log("Expiry year invalid or missing.");
                            }
                            if (response.errors.expiryMonth) {
                                console.log("Expiry month invalid or missing.");
                            }
                            if (response.errors.securityCode) {
                                console.log("Security code invalid.");
                            }
                        } else if ("request_timeout" == response.status) {
                            console.log("Session update failed with request timeout: " + response.errors.message);
                        } else if ("system_error" == response.status) {
                            console.log("Session update failed with system error: " + response.errors.message);
                        }
                    } else {
                        console.log("Session update failed: " + response);
                    }
                }
            }
        });
    },

    makePayment: function () {
        PaymentSession.updateSessionFromForm('card');
    },

    render: function () {
        var devicePlatorm = window.device.platform;
        var backbtn = (<a href="javascript:void(0)"
                          className="modal-action modal-close waves-effect waves-green btn-flat glyphicon ion-android-arrow-back"></a>)
        if (devicePlatorm != "browser") {
            backbtn = null
        }
        return (
            <div className="modal donation-details" ref="modal">
                <div className="modal-content">
                    <div className="modal-header">
                        <div className="col s12 header">
                            <img src="https://s3-ap-southeast-2.amazonaws.com/imm-travel-app/bike-highlight.png"/>
                            <h5>Please enter your payment details:</h5>
                        </div>
                    </div>
                    <div className="modal-body">
                        <div className="col s12">
                            <div className="row">
                                <div className="input-field col s12">
                                    <input id="card-number" placeholder="Card Number: XXXX-XXXX-XXXX-XXXX-XXXX" type="text" readOnly="true"/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s6">
                                    <input id="expiry-month" placeholder="Expiry Month: mm" type="text" className="validate"/>
                                </div>
                                <div className="input-field col s6">
                                    <input id="expiry-year" placeholder="Expiry Year: yy" type="text"/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input id="security-code" placeholder="Security Code" type="text" readOnly="true"/>
                                </div>
                            </div>
                            <div className=" col s12modal-button-container">
                                <button className="waves-effect waves-light btn btn-primary green book-btn"
                                        onClick={this.makePayment}>Pay Now
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
})
