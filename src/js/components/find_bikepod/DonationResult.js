/**
 * Created by duongnhu on 20/05/15.
 */

var DonationCard = require("./DonationCard")

module.exports = React.createClass({

    getInitialState: function() {
        return {
            donations: (this.props.donations)? this.props.donations : []
        }
    },

    componentWillReceiveProps: function(nextProps) {
        if (nextProps.donations && JSON.stringify(nextProps.donations) != JSON.stringify(this.props.donations)) {
            this.setState({donations: nextProps.donations})
        }
    },

    render: function() {
        if (this.state.donations.length == 0) {
            return (
                <h3>
                    No donation requests are found around your location!
                </h3>
            )
        } else {
            return (
                <div className="busker-list">
                    {
                        this.state.donations.map(function(item, id) {
                            return (
                                <DonationCard donation={item} key={id}/>
                            )
                        })
                    }
                </div>
            )
        }
    }
})