/**
 * Created by duongnhu on 10/05/15.
 */
module.exports = React.createClass({
    componentDidMount: function() {
        $("#menu-btn").sideNav({
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });
    },

    render: function () {
        /**var links = [
            <li key={1}>
                <a href="#/find_donation" className="waves-effect waves-light">
                    <i className="ion-android-search"></i>
                    Find Donation Requests
                </a>
            </li>,
            <li key={2} className="divider"></li>,
            <li key={3}>
                <a href="#/post_request" className="waves-effect waves-light">
                    <i className="ion-android-unlock"></i>
                    Post Request
                </a>
            </li>,
        ]**/
        var links = []
        return (
            <div>
                <a href="javascript:void(0)" className="brand-logo center">{config.app_name}</a>
                {/*<a href="javascript:void(0)" className="brand-logo right" id="logo"><img src="img/app-logo-96.png"/></a>*/}
                <ul className="left hide-on-med-and-down">
                    {links}
                </ul>
                <ul id="slide-out" className="side-nav">
                    {links}
                </ul>
                  <a href="#" data-activates="slide-out" className="button-collapse" id="menu-btn"><i className="mdi-navigation-menu"></i></a>
            </div>
        )
    }
})