/**
 * Created by duognnhu on 27/09/15.
 */
var LocationInput = require("./find_bikepod/LocationInput")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
module.exports = React.createClass({
     mixins: [flux.mixins.storeListener],

    watchStores: ['PostRequestStore'],


    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            bloodType: "",
            time: "",
            currentAddress: "",
            organizationName: ""
        }
    },

     CurrentState: {
        POSTING: "posting",
        INITIAL: "initial",
        READY: 'READY'
    },

    storeDidChange: function (storeName) {
        console.log(storeName)

        if (this.isMounted()) {
            var store = dispatcher.getStore(storeName)
            var storeState = store.getState()
            if (storeState.success) {
                alert("Posted Successfully")
            } else {
                alert("Error Posting!")
            }
        }
    },

    setUpComponent: function () {
        console.log(this.state.currentState)
        var parent = this
        navigator.geolocation.getCurrentPosition(function (position) {
            googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                function (address) {
                    if (parent.isMounted()) {
                        parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                    }
                }, function (error) {
                    alert(error)
                })
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, { enableHighAccuracy: true })
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    onBloodTypeChange: function (e) {
        this.setState({bloodType: e.target.value})
    },

    onTimeChange: function (e) {
        this.setState({time: e.target.value})
    },

    onNameChange: function (e) {
        this.setState({organizationName: e.target.value})
    },

    postRequest: function () {
        var opts = {
            bloodType: this.state.bloodType,
            address: this.refs.address.getValue(),
            time: this.state.time,
            organizationName: this.state.organizationName
        }
        this.setState({currentState: this.CurrentState.POSTING })
        actions.postRequest(opts)
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (<CubeGrid text="Loading ..."/>)
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                <div className="row card">
                    <h4>Posting Request</h4>
                    <ul className="collection">
                        <li className="collection-item avatar">
                            <img src="img/bloodtype_logo.png" className="circle"/>
                            <span className="title">Organization Name:</span>
                            <p>
                                <input onChange={this.onNameChange} ref="name" type="text" placeholder="name"/>
                            </p>
                        </li>

                        <li className="collection-item avatar">
                            <img src="img/location_logo.png" className="circle"/>
                            <span className="title">Location:</span>
                            <LocationInput initAddress={this.state.currentAddress} ref="address"/>
                        </li>

                        <li className="collection-item avatar">
                            <img src="img/bloodtype_logo.png" className="circle"/>
                            <span className="title">Blood Type:</span>
                            <p>
                                <input ref="bloodType" onChange={this.onBloodTypeChange} type="text"/>
                            </p>
                        </li>

                        <li className="collection-item avatar">
                            <img src="img/bloodtype_logo.png" className="circle"/>
                            <span className="title">Time:</span>
                            <p>
                                <input onChange={this.onTimeChange} ref="time" placeholder="MM/dd/yyyy HH:mm:ss" type="text"/>
                            </p>
                        </li>
                    </ul>
                    <div className="form-footer right">
                        <a href="javascript:void(0)" onClick={this.postRequest} className="waves-effect waves-green teal lighten-2 btn">
                            Save
                        </a>
                        <a href="javascript:void(0)" className="waves-effect waves-green blue-grey btn">
                            Cancel
                        </a>
                    </div>
                </div>
            )
        } else if (this.state.currentState == this.CurrentState.POSTING) {
            return (
                <SpinnerWave text="Posting Donation Requests ..."/>
            )
        }
    }
})