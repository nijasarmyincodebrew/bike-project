/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({

    getInitialState: function () {
        return {
            value: ""
        }
    },

    render: function () {
        return (
            <div className="input-field col s12">
                <select defaultValue={this.state.value} value={this.state.value}>
                    {
                        this.props.items.map(function (item, id) {
                            return (
                                <option value={item.value}>{item.text}</option>
                            )
                        })
                    }
                </select>
                <label>Materialize Select</label>
            </div>
        )
    }
})