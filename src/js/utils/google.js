/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = {
    maps: {
        checkConnection: function () {
            var networkState = navigator.connection.type
            console.log(networkState)
            if (networkState == Connection.NONE) {
                return false
            }
            return true
        },

        reverseGeocoding: function (lat, lng, success, error) {
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        success(results[0].formatted_address)
                    } else {
                        error('No results found');
                    }
                } else {
                    error('Geocoder failed due to: ' + status);
                }
            });
        },

        geocoding: function (address, success, error) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        success(results[0].geometry)
                    } else {
                        error('No results found');
                    }
                } else {
                    error('Geocoder failed due to: ' + status);
                }
            });
        },

        getDir: function (origin, destination, success) {
            var directionsService = new google.maps.DirectionsService();
            var directionsRequest = {
                "origin": origin["lat"] + "," + origin["lng"],
                "destination": origin["lat"] + "," + origin["lng"],
                optimizeWaypoints: true,
                travelMode: 'BICYCLING'
            };
            directionsService.route(directionsRequest, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    //do work with response data
                    console.log(response)
                    success(response)
                }
                else {
                    //Error has occured
                    error(response)
                }
            })

        }
    }
}