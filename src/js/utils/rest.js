/**
 * Created by duongnhu on 17/05/15.
 */

module.exports = {

    checkConnection: function (opts, callback) {
        var networkState = navigator.connection.type
        if (networkState == Connection.NONE) {
            opts.error("No network available!")
        } else {
            callback(opts)
        }
    },

    verify_auth: function (xhr, callback) {
        if (xhr.statusCode() == 401) {
            window.location.hash = "#/log-in"
        } else {
            callback(xhr.responseText)
        }
    },

    /**
     * Find closest buskers
     * @param opts {
     *  success: success callback,
     *  error: error callback
     * }
     */
    _findDonations: function (opts) {
        console.log(opts)
        var query = '{\
            "query": {\
                "bool": {\
                    "filter": {\
                        "geo_distance": {\
                            "distance": "1000m",\
                            "coordinates.coordinates": {\
                                "lat":'+ opts.latitude+',\
                                "lon":'+ opts.longitude+'\
                            }\
                        }\
                    }\
                }\
            }\
        }'


        $.ajax({
            url: "http://52.42.243.209:9200/myke/bike_pods/_search",
            type: "GET",
            data: {"source":query},
            headers: {"Content-type": "application/json"},
            success: function (data) {
                var pods = []
                data["hits"]["hits"].forEach(function (pod) {
                    pods = pods.concat(pod["_source"])
                })
                opts.success(pods)
            },
            error: function (xhr) {
                opts.error("Cannot connect to server!")
            }

        })
    },

    /**
     * Find closest buskers
     * @param opts {
     *  success: success callback,
     *  error: error callback
     * }
     */
    _findDrinkingFountain: function (opts) {
        console.log(opts)
        var query = '{\
            "query": {\
                "bool": {\
                    "filter": {\
                        "geo_distance": {\
                            "distance": "1km",\
                            "coordinates.coordinates": {\
                                "lat":'+ opts.latitude+',\
                                "lon":'+ opts.longitude+'\
                            }\
                        }\
                    }\
                }\
            }\
        }'


        $.ajax({
            url: "http://52.42.243.209:9200/citymike/fountain/_search",
            type: "GET",
            data: {"source":query},
            headers: {"Content-type": "application/json"},
            success: function (data) {
                var pods = []
                data["hits"]["hits"].forEach(function (pod) {
                    pods = pods.concat(pod["_source"])
                })
                opts.success(pods)
            },
            error: function (xhr) {
                opts.error("Cannot connect to server!")
            }

        })
    },


    findDonations: function (opts) {
        this.checkConnection(opts, this._findDonations)
    },

    _findMerchants: function (opts) {
        console.log(opts)
        var query = '{\
            "query": {\
                "bool": {\
                    "filter": {\
                        "geo_distance": {\
                            "distance": "1km",\
                            "coordinates.coordinates": {\
                                "lat":'+ opts.latitude+',\
                                "lon":'+ opts.longitude+'\
                            }\
                        }\
                    }\
                }\
            }\
        }'


        $.ajax({
            url: "http://52.42.243.209:9200/citymike3/mastercard/_search",
            type: "GET",
            data: {"source":query},
            headers: {"Content-type": "application/json"},
            success: function (data) {
                var pods = []
                data["hits"]["hits"].forEach(function (pod) {
                    pods = pods.concat(pod["_source"])
                })
                opts.success(pods)
            },
            error: function (xhr) {
                opts.error("Cannot connect to server!")
            }

        })
    },

    findMerchants: function (opts) {
        this.checkConnection(opts, this._findMerchants)

    },

    findDrinkingFountain: function (opts) {
        this.checkConnection(opts, this._findDrinkingFountain)
    },

    postRequest: function (opts) {
        var url = config.base_url + "/rest/donation"
        console.log(window.googleUtils.geocoding)
        window.googleUtils.maps.geocoding(opts.address, function (dt) {
            console.log(dt.location.L)
            var data = {
                longitude: dt.location.L,
                latitude: dt.location.H,
                bloodGroup: opts.bloodType,
                organizationName: opts.organizationName
            }
            var str = Object.keys(data).map(function(key){
                return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
            }).join('&');
            $.ajax({
                url: url + "?" + str,
                type: "POST",
                headers: {"Content-type": "application/json"},
                success: opts.success,
                error: function (xhr) {
                    opts.error(xhr.textResponse)
                }
            })
        }, opts.error)
    }
}
