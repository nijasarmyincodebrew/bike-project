/**
 * Created by duongnhu on 17/05/15.
 */
module.exports = {
    findDonations: function(coords, radius) {
        rest.findDonations({
            latitude: coords.lat,
            longitude: coords.lng,
            radius: radius,
            success: function (data) {
                dispatcher.findPods(true, data)
            },

            error: function (errorMessage) {
                dispatcher.findPods(false, null, errorMessage)
            }
        })
    },
    findMerchants: function(coords, radius) {
        rest.findMerchants({
            latitude: coords.lat,
            longitude: coords.lng,
            radius: radius,
            success: function (data) {
                dispatcher.findMerchants(true, data)
            },

            error: function (errorMessage) {
                dispatcher.findMerchants(false, null, errorMessage)
            }
        })
    },

    findDrinkingFountain: function(coords, radius) {
        rest.findDrinkingFountain({
            latitude: coords.lat,
            longitude: coords.lng,
            radius: radius,
            success: function (data) {
                dispatcher.findDrinkingFountain(true, data)
            },

            error: function (errorMessage) {
                dispatcher.findDrinkingFountain(false, null, errorMessage)
            }
        })
    },

    postRequest: function (opts) {
        opts.success = function (data) {
            dispatcher.postRequest(true, data)
        }
        opts.error = function (msg) {
            dispatcher.postRequest(false, null, msg)
        }
        rest.postRequest(opts)
    }
}