/**
 * Created by duongnhu on 20/05/15.
 */
var FindBikePod = require("../components/FindBikePod.js")
var PostRequest = require("../components/PostRequest")
var WhereToGo = require("../components/WhereToGo")
var SideNav = require("../components/SideNav")
var Rewards = require("../components/Rewards")

window.renderFindDonation = function () {
    window.map = null
    React.unmountComponentAtNode(document.getElementById("main-content"))
    $("#nav-bar").show()
        $(".modal").hide()

    React.render(<SideNav dispatcher={dispatcher}/>, document.getElementById("nav-bar"))
    if (document.getElementById("pod-details")) {
        React.unmountComponentAtNode(document.getElementById("pod-details"))
    }
    React.render(<FindBikePod currentState="initial" dispatcher={dispatcher}/>, document.getElementById("main-content"))
}

window.renderWhereGo = function () {
    window.map = null
    $("#nav-bar").hide()
    $(".modal").hide()
    $(".lean-overlay").hide()
    $("#lean-overlay").hide()

    React.unmountComponentAtNode(document.getElementById("main-content"))
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(<WhereToGo currentState="initial" dispatcher={dispatcher}/>, document.getElementById("main-content"))
}

window.rewards = function () {
    $("#pes-container").hide()
     React.unmountComponentAtNode(document.getElementById("main-content"))
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(<Rewards currentState="initial" dispatcher={dispatcher}/>, document.getElementById("main-content"))
}

module.exports = {
    init: function(path) {
        var routes = {
            "/find_donation": renderFindDonation,
            "/where-go": renderWhereGo,
            "/rewards": rewards
        }
        window.router = Router(routes);
        router.init(path)
    }
}