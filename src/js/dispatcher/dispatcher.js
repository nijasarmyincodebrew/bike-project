/**
 * Created by duongnhu on 17/05/15.
 */
var LocationStore = require("../stores/LocationStore")
var PostRequest = require("../stores/PostRequestStore")
module.exports = flux.createDispatcher({

    findPods: function(success, data, errorMessage) {
        this.dispatch("FIND_POD", {success: success, data:data, errorMessage: errorMessage})
    },

    findMerchants: function(success, data, errorMessage) {
        this.dispatch("FIND_MERCHANT", {success: success, data:data, errorMessage: errorMessage})
    },

    findDrinkingFountain: function(success, data, errorMessage) {
        this.dispatch("FIND_FOUNTAIN", {success: success, data:data, errorMessage: errorMessage})
    },

    postRequest: function (success, data, errorMessage) {
        this.dispatch("POST_REQUEST", {success: success, data:data, errorMessage: errorMessage})
    },

    getStores: function() {
        return {
            LocationStore: new LocationStore(),
            PostRequest: new PostRequest()
        }
    }
})