module.exports = {
    base_url: "http://52.64.148.4:8080/SpringMVC",
    google_api_key: "AIzaSyD29jhUWlezLx3tvwemw_u4a--SpQ5J1NA",
    app_name: "City Mike",

    bike_icon: "https://s3-ap-southeast-2.amazonaws.com/imm-travel-app/Bike_Icon-01.png",
    user_current_location_icon: "https://s3-ap-southeast-2.amazonaws.com/imm-travel-app/geo-location.png",
    fountain_icon: "https://s3-ap-southeast-2.amazonaws.com/imm-travel-app/Map_marker_icon_%E2%80%93_Nicolas_Mollet_%E2%80%93_Drinking_fountain_%E2%80%93_Offices_%E2%80%93_Light.png",
    retail_icon: "https://s3-ap-southeast-2.amazonaws.com/imm-travel-app/gw_icon_mobile_shop-online.png",
    reset_interval:  60000,
    googleMap: {
        zoom: 14
    }
}
