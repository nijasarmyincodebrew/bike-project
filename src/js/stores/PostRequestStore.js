/**
 * Created by duongnhu on 17/05/15.
 */
module.exports = flux.createStore({

    actions: {
        POST_REQUEST: "postRequest"
    },

    getState: function() {
        return {
            success: this.success,
            errorMessage: this.errorMessage,
            action: this.action
        }
    },

    postRequest: function(opts) {
        this.action = this.actions.POST_REQUEST
        this.success = opts.success
        this.errorMessage = opts.errorMessage
        this.emitChange()
    }
})