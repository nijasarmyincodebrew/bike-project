/**
 * Created by duongnhu on 17/05/15.
 */
module.exports = flux.createStore({

    actions: {
        FIND_POD: "findPods",
        FIND_FOUNTAIN: "findDrinkingFountains",
        FIND_MERCHANT: "findMerchants"
    },

    initialize: function() {
        this.fountains = []
    },

    getState: function() {
        return {
            merchants: this.merchants,
            fountains: this.fountains,
            success: this.success,
            errorMessage: this.errorMessage,
            action: this.action
        }
    },

    findPods: function(opts) {
        this.action = this.actions.FIND_POD
        this.success = opts.success
        if (!opts.success) {
            this.errorMessage = opts.errorMessage
        }
        this.fountains = opts.data
        this.emitChange()
    },

    findDrinkingFountains:  function(opts) {
        this.action = this.actions.FIND_FOUNTAIN
        this.success = opts.success
        if (!opts.success) {
            this.errorMessage = opts.errorMessage
        }
        this.fountains = opts.data
        this.emitChange()
    },

    findMerchants:  function(opts) {
        this.action = this.actions.FIND_MERCHANT
        this.success = opts.success
        if (!opts.success) {
            this.errorMessage = opts.errorMessage
        }
        this.merchants = opts.data
        this.emitChange()
    }
})