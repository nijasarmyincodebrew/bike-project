/**
 * Created by duongnhu on 10/05/15.
 */
var Chance = require("chance").Chance
window.chance = new Chance()
window.jQuery = require("jquery")
window.$ = jQuery
require("./lib/materialize")
window.React = require("react")
window.flux = require("delorean").Flux
window.director = require("director")
window.Router = window.director.Router

window.config = require("./config/config")

window.googleUtils = require("./utils/google")

window.rest = require("./utils/rest")
window.dispatcher = require("./dispatcher/dispatcher")
window.actions = require("./actions/actions")

require("./lib/jquery.nouislider.all")


var PodDetails = require("./components/find_bikepod/PodDetails")
window.popupPodDetails = function (opts) {
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(<PodDetails name={opts.featurename} empty={opts.nbemptydoc} bikes_no={opts.nbbikes}/>, document.getElementById("pod-details"))
}

var ConfirmDonation = require("./components/find_bikepod/ConfirmDonation")
window.popupConfirmDonation = function (opts) {
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(<ConfirmDonation organizationName={opts.organizationName} bloodType={opts.bloodType} distance={opts.distance} endTime={opts.endTime} location={opts.location}/>, document.getElementById("pod-details"))
}

var Payment = require("./components/find_bikepod/Payment")
window.popupPayment = function (opts) {
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(<Payment/>, document.getElementById("pod-details"))
}