(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"/home/duongnhu/workspace/fe/src/js/components/FindBikePod.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            fountains: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    findDonations: function () {
        var parent = this
        var value = parent.refs.location.getValue()
        actions.findDonations(value.address, value.radius)

        if (window.device.platform == "browser") {
            find()
            $(this.refs.findBtn.getDOMNode()).fadeOut(300, function () {
                parent.setState({
                    currentState: parent.CurrentState.FINDING
                })
            })
        } else {
            parent.setState({
                currentState: parent.CurrentState.FINDING
            })
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_POD) {
                if (storeState.success) {
                    this.setState({fountains: storeState.fountains}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.getMarkers(coords, this.state.fountains, this.refs.googleMap.getDOMNode())
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        navigator.geolocation.getCurrentPosition(function (position) {
            var find = null
            if (parent.isMounted()) {
                var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                window.coords = coords
                parent.setState({coords: coords}, function () {
                    actions.findDonations(coords, rad)
                })
            } else {
                find = null
            }
            //parent.drawMap(coords)
            /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
             function (address) {
             parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
             }, function (error) {
             alert(error)
             })*/
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, {enableHighAccuracy: true})
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            this.setUpComponent()
        }
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "s12", id: "search-wrapper"}, 
                    React.createElement("div", {className: "col s12", id: "google-map", ref: "googleMap"})
                )
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                React.createElement(SpinnerWave, {text: "Finding Pods Requests ..."})
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                React.createElement(DonationResult, {donations: this.state.donations})
            )
        }
    }
})

},{"./buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./find_bikepod/DonationResult":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js","./find_bikepod/LocationSetting":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js","./find_bikepod/MapMarker":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/PostRequest.js":[function(require,module,exports){
/**
 * Created by duognnhu on 27/09/15.
 */
var LocationInput = require("./find_bikepod/LocationInput")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
module.exports = React.createClass({displayName: "exports",
     mixins: [flux.mixins.storeListener],

    watchStores: ['PostRequestStore'],


    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            bloodType: "",
            time: "",
            currentAddress: "",
            organizationName: ""
        }
    },

     CurrentState: {
        POSTING: "posting",
        INITIAL: "initial",
        READY: 'READY'
    },

    storeDidChange: function (storeName) {
        console.log(storeName)

        if (this.isMounted()) {
            var store = dispatcher.getStore(storeName)
            var storeState = store.getState()
            if (storeState.success) {
                alert("Posted Successfully")
            } else {
                alert("Error Posting!")
            }
        }
    },

    setUpComponent: function () {
        console.log(this.state.currentState)
        var parent = this
        navigator.geolocation.getCurrentPosition(function (position) {
            googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                function (address) {
                    if (parent.isMounted()) {
                        parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                    }
                }, function (error) {
                    alert(error)
                })
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, { enableHighAccuracy: true })
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    onBloodTypeChange: function (e) {
        this.setState({bloodType: e.target.value})
    },

    onTimeChange: function (e) {
        this.setState({time: e.target.value})
    },

    onNameChange: function (e) {
        this.setState({organizationName: e.target.value})
    },

    postRequest: function () {
        var opts = {
            bloodType: this.state.bloodType,
            address: this.refs.address.getValue(),
            time: this.state.time,
            organizationName: this.state.organizationName
        }
        this.setState({currentState: this.CurrentState.POSTING })
        actions.postRequest(opts)
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "row card"}, 
                    React.createElement("h4", null, "Posting Request"), 
                    React.createElement("ul", {className: "collection"}, 
                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Organization Name:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onNameChange, ref: "name", type: "text", placeholder: "name"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/location_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Location:"), 
                            React.createElement(LocationInput, {initAddress: this.state.currentAddress, ref: "address"})
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Blood Type:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {ref: "bloodType", onChange: this.onBloodTypeChange, type: "text"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Time:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onTimeChange, ref: "time", placeholder: "MM/dd/yyyy HH:mm:ss", type: "text"})
                            )
                        )
                    ), 
                    React.createElement("div", {className: "form-footer right"}, 
                        React.createElement("a", {href: "javascript:void(0)", onClick: this.postRequest, className: "waves-effect waves-green teal lighten-2 btn"}, 
                            "Save"
                        ), 
                        React.createElement("a", {href: "javascript:void(0)", className: "waves-effect waves-green blue-grey btn"}, 
                            "Cancel"
                        )
                    )
                )
            )
        } else if (this.state.currentState == this.CurrentState.POSTING) {
            return (
                React.createElement(SpinnerWave, {text: "Posting Donation Requests ..."})
            )
        }
    }
})
},{"./find_bikepod/LocationInput":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/SideNav.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    componentDidMount: function() {
        $("#menu-btn").sideNav({
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });
    },

    render: function () {
        /**var links = [
            <li key={1}>
                <a href="#/find_donation" className="waves-effect waves-light">
                    <i className="ion-android-search"></i>
                    Find Donation Requests
                </a>
            </li>,
            <li key={2} className="divider"></li>,
            <li key={3}>
                <a href="#/post_request" className="waves-effect waves-light">
                    <i className="ion-android-unlock"></i>
                    Post Request
                </a>
            </li>,
        ]**/
        var links = []
        return (
            React.createElement("div", null, 
                React.createElement("a", {href: "javascript:void(0)", className: "brand-logo center"}, config.app_name), 
                /*<a href="javascript:void(0)" className="brand-logo right" id="logo"><img src="img/app-logo-96.png"/></a>*/
                React.createElement("ul", {className: "left hide-on-med-and-down"}, 
                    links
                ), 
                React.createElement("ul", {id: "slide-out", className: "side-nav"}, 
                    links
                ), 
                  React.createElement("a", {href: "#", "data-activates": "slide-out", className: "button-collapse", id: "menu-btn"}, React.createElement("i", {className: "mdi-navigation-menu"}))
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/WhereToGo.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            fountains: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_FOUNTAIN) {
                if (storeState.success) {
                    this.setState({fountains: storeState.fountains}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.initSearch(coords, this.refs.googleMap.getDOMNode(), {
                "items": this.state.fountains,
                "icon": config.fountain_icon
            })
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        if (!window.coords) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var find = null
                if (parent.isMounted()) {
                    var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                    parent.setState({coords: coords}, function () {
                        actions.findDrinkingFountain(coords, rad)
                    })
                } else {
                    find = null
                }
                //parent.drawMap(coords)
                /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                 function (address) {
                 parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                 }, function (error) {
                 alert(error)
                 })*/
            }, function (error) {
                if (parent.isMounted()) {
                    alert(error.message)
                }
            }, {enableHighAccuracy: true})
        } else {
            parent.setState({coords: coords}, function () {
                actions.findDrinkingFountain(window.coords, rad)
            })
        }

    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            //this.setUpComponent()
        }
    },

    render: function () {
        console.log(this.state.currentState)
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "s12", id: "search-wrapper"}, 
                    React.createElement("nav", null, 
                        React.createElement("div", {className: "nav-wrapper to-go-nav"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("h5", null, "Where to go next?")
                                ), 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("div", {className: "input-field"}, 
                                        React.createElement("input", {value: "Current Location", type: "search"}), 
                                        React.createElement("label", null, React.createElement("i", {className: ""}, "Start"))
                                    )
                                ), 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("div", {className: "input-field"}, 
                                        React.createElement("input", {id: "place-search", type: "search"}), 
                                        React.createElement("label", null, React.createElement("i", {className: ""}, "End"))
                                    )
                                )
                            )
                        )
                    ), 
                    React.createElement("div", {style: {position: "absolute", left: "0", bottom: "20%", overflow: "scroll", height: "20%", width: "10%"}}, 
                        React.createElement("ul", null, 
                            React.createElement("li", null, 
                                "Town Hall"
                            ), 
                            React.createElement("li", null, 
                                "Bourke Street"
                            )
                        )
                    ), 
                    React.createElement("div", {className: "col s12", id: "google-map", ref: "googleMap"})
                )
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                React.createElement(SpinnerWave, {text: "Finding Pods Requests ..."})
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                React.createElement(DonationResult, {donations: this.state.donations})
            )
        }
    }
})

},{"./buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./find_bikepod/DonationResult":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js","./find_bikepod/LocationSetting":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js","./find_bikepod/MapMarker":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            value: (this.props.value)? this.props.value : 0
        }
    },

    componentDidMount: function() {
        this.activateSlider()
    },

    setValue: function(value) {
        this.setState({value: value})
    },

    activateSlider:  function() {
        var min = (this.props.min)? this.props.min : 0
        var max = (this.props.max)? this.props.max : 50
        var node = $(this.refs.slider.getDOMNode())
        node.noUiSlider({
            start: this.state.value,
            connect: "lower",
            orientation: "horizontal",
            direction: "ltr",
            behaviour: "tap-drag",
            range: {
                'min': min,
                'max': max
            },
            format: wNumb({
                decimals: 0
            })
        });
        var parent = this
        node.on('slide', function() {
            parent.setValue($(this).val())
        })
	node.on('change', function () {
	    if (parent.props.sliderRelease) {
		parent.props.sliderRelease($(this).val())
	    }
	})
    },

    render: function () {
        var unit = (this.props.unit)? " " +this.props.unit : ""
        return (
            React.createElement("div", {className: "col s12 slider-container"}, 
                React.createElement("div", {className: "slider col s10", ref: "slider"}), React.createElement("div", {className: "col s2 slider-value"}, this.state.value + unit)
            )
        )
    }
})

},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationCard.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

module.exports = React.createClass({displayName: "exports",

    render: function () {
        return (
            React.createElement("div", {className: "col s12 m8 offset-m2 l6 offset-l3"}, 
                React.createElement("div", {className: "card-panel grey lighten-5 z-depth-1"}, 
                    React.createElement("div", {className: "row valign-wrapper"}, 
                        React.createElement("div", {className: "col s2"}, 
                            React.createElement("img", {src: this.props.donation.profileImg, alt: "Profile Picture", className: "circle responsive-img"})
                        ), 
                        React.createElement("div", {className: "col s10"}, 
                            React.createElement("p", null), 
                            React.createElement("p", {className: "black-text"}, 
                                this.props.donation.description
                            )
                        )
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

var DonationCard = require("./DonationCard")

module.exports = React.createClass({displayName: "exports",

    getInitialState: function() {
        return {
            donations: (this.props.donations)? this.props.donations : []
        }
    },

    componentWillReceiveProps: function(nextProps) {
        if (nextProps.donations && JSON.stringify(nextProps.donations) != JSON.stringify(this.props.donations)) {
            this.setState({donations: nextProps.donations})
        }
    },

    render: function() {
        if (this.state.donations.length == 0) {
            return (
                React.createElement("h3", null, 
                    "No donation requests are found around your location!"
                )
            )
        } else {
            return (
                React.createElement("div", {className: "busker-list"}, 
                    
                        this.state.donations.map(function(item, id) {
                            return (
                                React.createElement(DonationCard, {donation: item, key: id})
                            )
                        })
                    
                )
            )
        }
    }
})
},{"./DonationCard":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationCard.js"}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            searchBox: undefined
        }
    },

    componentDidMount: function () {
        this.setState({searchBox: new google.maps.places.SearchBox(this.refs.input.getDOMNode())})
    },

    getValue: function () {
        return this.refs.input.getDOMNode().value
    },

    render: function () {
        return (
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "input-field col s12"}, 
                    React.createElement("input", {type: "text", placeholder: "Choose a location", className: "validate", ref: "input", defaultValue: this.props.initAddress})
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
var Slider = require("../buttons/Slider")
var LocationInput = require("./LocationInput")

module.exports = React.createClass({displayName: "exports",

    componentWillUpdate: function (nextProps) {
        if (nextProps.initAddress && nextProps.initAddress != this.props.initAddress) {
            return true
        }
        return false
    },

    getValue: function () {
        return {
            address: this.refs.address.getValue(),
            radius: this.refs.radius.state.value
        }
    },

    render: function () {
        return (
            React.createElement("div", {className: "row", id: "location-setting"}, 
                React.createElement("form", {className: "col s12"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement(LocationInput, {initAddress: this.props.initAddress, ref: "address"}), 
                        React.createElement("div", {className: "col s12 label"}, "Radius"), 
                        React.createElement(Slider, {ref: "radius", value: 10, unit: "km"})
                    )
                )
            )
        )
    }
})
},{"../buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./LocationInput":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js"}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js":[function(require,module,exports){
/**
 * Created by duognnhu on 26/09/15.
 */
var gApi = require("../../utils/google")
module.exports = {
    getMarkers: function (currentLocation, pods, dom, radius, icon) {
        if (!window.map) {
            window.map = new google.maps.Map(dom, {
                center: currentLocation,
                zoom: config.googleMap.zoom,
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: false
            });
        }
        google.maps.visualRefresh = true;
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here',
            icon: config.user_current_location_icon
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })

        pods.forEach(function (pod, id) {
            var coordinates = pod.coordinates.coordinates
            var marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(coordinates["lat"]),
                    lng: parseFloat(coordinates["lon"])
                },
                icon: (icon) ? icon : config.bike_icon
            })
            marker.addListener('click', function () {
                window.popupPodDetails(pod)
            })
            marker.setMap(map)
        })
        new google.maps.Circle({
            strokeColor: 'green',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.0,
            map: map,
            center: currentLocation,
            radius: 1000
        });
    },

    initSearch: function (currentLocation, dom, data) {
        var markers = []
        if (!window.map) {
            window.map = new google.maps.Map(dom, {
                center: currentLocation,
                zoom: config.googleMap.zoom,
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: false
            });
        }
        google.maps.visualRefresh = true;
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here',
            icon: config.user_current_location_icon
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })

        data.items.forEach(function (point) {
            var coordinates = point.coordinates.coordinates
            var mOpts = {
                position: {
                    lat: parseFloat(coordinates["lat"]),
                    lng: parseFloat(coordinates["lon"])
                }
            }
            if (data.icon) {
                mOpts["icon"] = data.icon

            }
            var marker = new google.maps.Marker(mOpts)
            marker.setMap(map)
            markers.push(marker)
        })
        new google.maps.Circle({
            strokeColor: 'green',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.0,
            map: map,
            center: currentLocation,
            radius: 1000
        });
        var searchPlacesMarkers = []
        if (document.getElementById('place-search')) {
            // Create the search box and link it to the UI element.
            var input = document.getElementById('place-search');
            var searchBox = new google.maps.places.SearchBox(input);
            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var directionsDisplay = new google.maps.DirectionsRenderer({
                preserveViewport: true,
            });
            directionsDisplay.setMap(map)
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                searchPlacesMarkers.forEach(function (marker) {
                    marker.setMap(null)
                })
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    var marker = new google.maps.Marker({
                        map: map,
                        title: place.name,
                        position: place.geometry.location
                    })
                    searchPlacesMarkers.push(marker)
                    var mLocationInfo = new google.maps.InfoWindow({
                        content: "<p>" + place.name + "</p>"
                    });
                    marker.addListener('click', function () {
                        mLocationInfo.open(map, marker);
                    })

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }


                    gApi.maps.getDir(currentLocation, place.geometry.location, function (direction) {
                        directionsDisplay.setDirections(direction)
                    });
                })
                currentLocationMarker.setMap(null)
                map.fitBounds(bounds);

            });
        }
    }
}

},{"../../utils/google":"/home/duongnhu/workspace/fe/src/js/utils/google.js"}],"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function() {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-wave"}, 
                        React.createElement("div", {className: "sk-rect1"}), 
                        React.createElement("div", {className: "sk-rect2"}), 
                        React.createElement("div", {className: "sk-rect3"}), 
                        React.createElement("div", {className: "sk-rect4"}), 
                        React.createElement("div", {className: "sk-rect5"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function () {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-three-bounce"}, 
                        React.createElement("div", {className: "sk-bounce1"}), 
                        React.createElement("div", {className: "sk-bounce2"}), 
                        React.createElement("div", {className: "sk-bounce3"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/index.js":[function(require,module,exports){
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var router = require("./router/router")

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //app.receivedEvent('deviceready');
        $(document).ready(function() {
            router.init("/find_donation")
            setTimeout(function() {
                $(".splash-screen").fadeOut(500)
            }, 3000)
            document.addEventListener("backbutton", onBackKeyDown, false);
            function onBackKeyDown() {
                React.unmountComponentAtNode(document.getElementById("pod-details"))
            }
        })
    }
};

app.initialize();

},{"./router/router":"/home/duongnhu/workspace/fe/src/js/router/router.js"}],"/home/duongnhu/workspace/fe/src/js/router/router.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
var FindBikePod = require("../components/FindBikePod.js")
var PostRequest = require("../components/PostRequest")
var WhereToGo = require("../components/WhereToGo")
var SideNav = require("../components/SideNav")
window.renderFindDonation = function () {
    window.map = null
    React.unmountComponentAtNode(document.getElementById("main-content"))
    $("#nav-bar").show()
        $(".modal").hide()

    React.render(React.createElement(SideNav, {dispatcher: dispatcher}), document.getElementById("nav-bar"))
    if (document.getElementById("pod-details")) {
        React.unmountComponentAtNode(document.getElementById("pod-details"))
    }
    React.render(React.createElement(FindBikePod, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

window.renderWhereGo = function () {
    window.map = null
    $("#nav-bar").hide()
    $(".modal").hide()
    $(".lean-overlay").hide()
    $("#lean-overlay").hide()

    React.unmountComponentAtNode(document.getElementById("main-content"))
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(React.createElement(WhereToGo, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

module.exports = {
    init: function(path) {
        var routes = {
            "/find_donation": renderFindDonation,
            "/where-go": renderWhereGo
        }
        window.router = Router(routes);
        router.init(path)
    }
}
},{"../components/FindBikePod.js":"/home/duongnhu/workspace/fe/src/js/components/FindBikePod.js","../components/PostRequest":"/home/duongnhu/workspace/fe/src/js/components/PostRequest.js","../components/SideNav":"/home/duongnhu/workspace/fe/src/js/components/SideNav.js","../components/WhereToGo":"/home/duongnhu/workspace/fe/src/js/components/WhereToGo.js"}],"/home/duongnhu/workspace/fe/src/js/utils/google.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = {
    maps: {
        checkConnection: function () {
            var networkState = navigator.connection.type
            console.log(networkState)
            if (networkState == Connection.NONE) {
                return false
            }
            return true
        },

        reverseGeocoding: function (lat, lng, success, error) {
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        success(results[0].formatted_address)
                    } else {
                        error('No results found');
                    }
                } else {
                    error('Geocoder failed due to: ' + status);
                }
            });
        },

        geocoding: function (address, success, error) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        success(results[0].geometry)
                    } else {
                        error('No results found');
                    }
                } else {
                    error('Geocoder failed due to: ' + status);
                }
            });
        },

        getDir: function (origin, destination, success) {
            var directionsService = new google.maps.DirectionsService();
            var directionsRequest = {
                "origin": origin["lat"] + "," + origin["lng"],
                "destination": origin["lat"] + "," + origin["lng"],
                optimizeWaypoints: true,
                travelMode: 'BICYCLING'
            };
            directionsService.route(directionsRequest, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    //do work with response data
                    console.log(response)
                    success(response)
                }
                else {
                    //Error has occured
                    error(response)
                }
            })

        }
    }
}
},{}]},{},["/home/duongnhu/workspace/fe/src/js/index.js"])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL25vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIuLi9qcy9jb21wb25lbnRzL0ZpbmRCaWtlUG9kLmpzIiwiLi4vanMvY29tcG9uZW50cy9Qb3N0UmVxdWVzdC5qcyIsIi4uL2pzL2NvbXBvbmVudHMvU2lkZU5hdi5qcyIsIi4uL2pzL2NvbXBvbmVudHMvV2hlcmVUb0dvLmpzIiwiLi4vanMvY29tcG9uZW50cy9idXR0b25zL1NsaWRlci5qcyIsIi4uL2pzL2NvbXBvbmVudHMvZmluZF9iaWtlcG9kL0RvbmF0aW9uQ2FyZC5qcyIsIi4uL2pzL2NvbXBvbmVudHMvZmluZF9iaWtlcG9kL0RvbmF0aW9uUmVzdWx0LmpzIiwiLi4vanMvY29tcG9uZW50cy9maW5kX2Jpa2Vwb2QvTG9jYXRpb25JbnB1dC5qcyIsIi4uL2pzL2NvbXBvbmVudHMvZmluZF9iaWtlcG9kL0xvY2F0aW9uU2V0dGluZy5qcyIsIi4uL2pzL2NvbXBvbmVudHMvZmluZF9iaWtlcG9kL01hcE1hcmtlci5qcyIsIi4uL2pzL2NvbXBvbmVudHMvbG9hZGluZy9TcGlubmVyV2F2ZS5qcyIsIi4uL2pzL2NvbXBvbmVudHMvbG9hZGluZy9UaHJlZUJvdW5jZS5qcyIsIi4uL2pzL2luZGV4LmpzIiwiLi4vanMvcm91dGVyL3JvdXRlci5qcyIsIi4uL2pzL3V0aWxzL2dvb2dsZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMxQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAxMC8wNS8xNS5cbiAqL1xudmFyIFNsaWRlciA9IHJlcXVpcmUoXCIuL2J1dHRvbnMvU2xpZGVyXCIpXG52YXIgU3Bpbm5lcldhdmUgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1NwaW5uZXJXYXZlXCIpXG52YXIgQ3ViZUdyaWQgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1RocmVlQm91bmNlXCIpXG52YXIgRG9uYXRpb25SZXN1bHQgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvRG9uYXRpb25SZXN1bHRcIilcbnZhciBMb2NhdGlvblNldHRpbmcgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvTG9jYXRpb25TZXR0aW5nXCIpXG52YXIgTWFwTWFya2VyID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL01hcE1hcmtlclwiKVxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuICAgIG1peGluczogW2ZsdXgubWl4aW5zLnN0b3JlTGlzdGVuZXJdLFxuXG4gICAgd2F0Y2hTdG9yZXM6IFsnTG9jYXRpb25TdG9yZSddLFxuXG4gICAgQ3VycmVudFN0YXRlOiB7XG4gICAgICAgIEZJTkRJTkc6IFwiZmluZGluZ1wiLFxuICAgICAgICBJTklUSUFMOiBcImluaXRpYWxcIixcbiAgICAgICAgUkVBRFk6ICdSRUFEWScsXG4gICAgICAgIFJFU1VMVDogXCJyZXN1bHRcIlxuICAgIH0sXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogKHRoaXMucHJvcHMuY3VycmVudFN0YXRlKSA/IHRoaXMucHJvcHMuY3VycmVudFN0YXRlIDogdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCxcbiAgICAgICAgICAgIGZvdW50YWluczogW10sXG4gICAgICAgICAgICBjb29yZHM6IHt9XG4gICAgICAgIH1cbiAgICB9LFxuXG5cbiAgICBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzOiBmdW5jdGlvbiAobmV4dFByb3BzKSB7XG4gICAgICAgIGlmIChuZXh0UHJvcHMuY3VycmVudFN0YXRlKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IG5leHRQcm9wcy5jdXJyZW50U3RhdGV9KVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGZpbmREb25hdGlvbnM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgdmFyIHZhbHVlID0gcGFyZW50LnJlZnMubG9jYXRpb24uZ2V0VmFsdWUoKVxuICAgICAgICBhY3Rpb25zLmZpbmREb25hdGlvbnModmFsdWUuYWRkcmVzcywgdmFsdWUucmFkaXVzKVxuXG4gICAgICAgIGlmICh3aW5kb3cuZGV2aWNlLnBsYXRmb3JtID09IFwiYnJvd3NlclwiKSB7XG4gICAgICAgICAgICBmaW5kKClcbiAgICAgICAgICAgICQodGhpcy5yZWZzLmZpbmRCdG4uZ2V0RE9NTm9kZSgpKS5mYWRlT3V0KDMwMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5GSU5ESU5HXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5GSU5ESU5HXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHN0b3JlRGlkQ2hhbmdlOiBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICBpZiAodGhpcy5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgdmFyIHN0b3JlID0gZGlzcGF0Y2hlci5nZXRTdG9yZShuYW1lKVxuICAgICAgICAgICAgdmFyIHN0b3JlU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpXG5cblxuICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuYWN0aW9uID09IHN0b3JlLmFjdGlvbnMuRklORF9QT0QpIHtcbiAgICAgICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2ZvdW50YWluczogc3RvcmVTdGF0ZS5mb3VudGFpbnN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRyYXdNYXAodGhpcy5zdGF0ZS5jb29yZHMpXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbmF2aWdhdG9yLm5vdGlmaWNhdGlvbi5hbGVydChzdG9yZVN0YXRlLmVycm9yTWVzc2FnZSlcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWX0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGRyYXdNYXA6IGZ1bmN0aW9uIChjb29yZHMpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWX0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIE1hcE1hcmtlci5nZXRNYXJrZXJzKGNvb3JkcywgdGhpcy5zdGF0ZS5mb3VudGFpbnMsIHRoaXMucmVmcy5nb29nbGVNYXAuZ2V0RE9NTm9kZSgpKVxuICAgICAgICB9KVxuICAgIH0sXG5cbiAgICBzZXRVcENvbXBvbmVudDogZnVuY3Rpb24gKHJhZGl1cykge1xuICAgICAgICB2YXIgcGFyZW50ID0gdGhpc1xuICAgICAgICB2YXIgcmFkID0gKHJhZGl1cykgPyByYWRpdXMgOiA1XG4gICAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgICAgICAgICB2YXIgZmluZCA9IG51bGxcbiAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICB2YXIgY29vcmRzID0ge2xhdDogcG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBsbmc6IHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGV9XG4gICAgICAgICAgICAgICAgd2luZG93LmNvb3JkcyA9IGNvb3Jkc1xuICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7Y29vcmRzOiBjb29yZHN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbnMuZmluZERvbmF0aW9ucyhjb29yZHMsIHJhZClcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBmaW5kID0gbnVsbFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy9wYXJlbnQuZHJhd01hcChjb29yZHMpXG4gICAgICAgICAgICAvKmdvb2dsZVV0aWxzLm1hcHMucmV2ZXJzZUdlb2NvZGluZyhwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUsIHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGUsXG4gICAgICAgICAgICAgZnVuY3Rpb24gKGFkZHJlc3MpIHtcbiAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe2N1cnJlbnRBZGRyZXNzOiBhZGRyZXNzLCBjdXJyZW50U3RhdGU6IHBhcmVudC5DdXJyZW50U3RhdGUuUkVBRFl9KVxuICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgIGFsZXJ0KGVycm9yKVxuICAgICAgICAgICAgIH0pKi9cbiAgICAgICAgfSwgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICBpZiAocGFyZW50LmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgYWxlcnQoZXJyb3IubWVzc2FnZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge2VuYWJsZUhpZ2hBY2N1cmFjeTogdHJ1ZX0pXG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0VXBDb21wb25lbnQoKVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRVcGRhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0VXBDb21wb25lbnQoKVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCkge1xuICAgICAgICAgICAgcmV0dXJuIChSZWFjdC5jcmVhdGVFbGVtZW50KEN1YmVHcmlkLCB7dGV4dDogXCJMb2FkaW5nIC4uLlwifSkpXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuUkVBRFkpIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInMxMlwiLCBpZDogXCJzZWFyY2gtd3JhcHBlclwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEyXCIsIGlkOiBcImdvb2dsZS1tYXBcIiwgcmVmOiBcImdvb2dsZU1hcFwifSlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuRklORElORykge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFNwaW5uZXJXYXZlLCB7dGV4dDogXCJGaW5kaW5nIFBvZHMgUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID0gdGhpcy5DdXJyZW50U3RhdGUuUkVTVUxUKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRG9uYXRpb25SZXN1bHQsIHtkb25hdGlvbnM6IHRoaXMuc3RhdGUuZG9uYXRpb25zfSlcbiAgICAgICAgICAgIClcbiAgICAgICAgfVxuICAgIH1cbn0pXG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvZ25uaHUgb24gMjcvMDkvMTUuXG4gKi9cbnZhciBMb2NhdGlvbklucHV0ID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL0xvY2F0aW9uSW5wdXRcIilcbnZhciBTcGlubmVyV2F2ZSA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvU3Bpbm5lcldhdmVcIilcbnZhciBDdWJlR3JpZCA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvVGhyZWVCb3VuY2VcIilcbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICAgbWl4aW5zOiBbZmx1eC5taXhpbnMuc3RvcmVMaXN0ZW5lcl0sXG5cbiAgICB3YXRjaFN0b3JlczogWydQb3N0UmVxdWVzdFN0b3JlJ10sXG5cblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY3VycmVudFN0YXRlOiAodGhpcy5wcm9wcy5jdXJyZW50U3RhdGUpID8gdGhpcy5wcm9wcy5jdXJyZW50U3RhdGUgOiB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMLFxuICAgICAgICAgICAgYmxvb2RUeXBlOiBcIlwiLFxuICAgICAgICAgICAgdGltZTogXCJcIixcbiAgICAgICAgICAgIGN1cnJlbnRBZGRyZXNzOiBcIlwiLFxuICAgICAgICAgICAgb3JnYW5pemF0aW9uTmFtZTogXCJcIlxuICAgICAgICB9XG4gICAgfSxcblxuICAgICBDdXJyZW50U3RhdGU6IHtcbiAgICAgICAgUE9TVElORzogXCJwb3N0aW5nXCIsXG4gICAgICAgIElOSVRJQUw6IFwiaW5pdGlhbFwiLFxuICAgICAgICBSRUFEWTogJ1JFQURZJ1xuICAgIH0sXG5cbiAgICBzdG9yZURpZENoYW5nZTogZnVuY3Rpb24gKHN0b3JlTmFtZSkge1xuICAgICAgICBjb25zb2xlLmxvZyhzdG9yZU5hbWUpXG5cbiAgICAgICAgaWYgKHRoaXMuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgIHZhciBzdG9yZSA9IGRpc3BhdGNoZXIuZ2V0U3RvcmUoc3RvcmVOYW1lKVxuICAgICAgICAgICAgdmFyIHN0b3JlU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpXG4gICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgYWxlcnQoXCJQb3N0ZWQgU3VjY2Vzc2Z1bGx5XCIpXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGFsZXJ0KFwiRXJyb3IgUG9zdGluZyFcIilcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBzZXRVcENvbXBvbmVudDogZnVuY3Rpb24gKCkge1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSlcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgbmF2aWdhdG9yLmdlb2xvY2F0aW9uLmdldEN1cnJlbnRQb3NpdGlvbihmdW5jdGlvbiAocG9zaXRpb24pIHtcbiAgICAgICAgICAgIGdvb2dsZVV0aWxzLm1hcHMucmV2ZXJzZUdlb2NvZGluZyhwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUsIHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGUsXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKGFkZHJlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBhcmVudC5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjdXJyZW50QWRkcmVzczogYWRkcmVzcywgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBhbGVydChlcnJvcilcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICBhbGVydChlcnJvci5tZXNzYWdlKVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7IGVuYWJsZUhpZ2hBY2N1cmFjeTogdHJ1ZSB9KVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICB9LFxuXG4gICAgb25CbG9vZFR5cGVDaGFuZ2U6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2Jsb29kVHlwZTogZS50YXJnZXQudmFsdWV9KVxuICAgIH0sXG5cbiAgICBvblRpbWVDaGFuZ2U6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3RpbWU6IGUudGFyZ2V0LnZhbHVlfSlcbiAgICB9LFxuXG4gICAgb25OYW1lQ2hhbmdlOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtvcmdhbml6YXRpb25OYW1lOiBlLnRhcmdldC52YWx1ZX0pXG4gICAgfSxcblxuICAgIHBvc3RSZXF1ZXN0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBvcHRzID0ge1xuICAgICAgICAgICAgYmxvb2RUeXBlOiB0aGlzLnN0YXRlLmJsb29kVHlwZSxcbiAgICAgICAgICAgIGFkZHJlc3M6IHRoaXMucmVmcy5hZGRyZXNzLmdldFZhbHVlKCksXG4gICAgICAgICAgICB0aW1lOiB0aGlzLnN0YXRlLnRpbWUsXG4gICAgICAgICAgICBvcmdhbml6YXRpb25OYW1lOiB0aGlzLnN0YXRlLm9yZ2FuaXphdGlvbk5hbWVcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IHRoaXMuQ3VycmVudFN0YXRlLlBPU1RJTkcgfSlcbiAgICAgICAgYWN0aW9ucy5wb3N0UmVxdWVzdChvcHRzKVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwpIHtcbiAgICAgICAgICAgIHJldHVybiAoUmVhY3QuY3JlYXRlRWxlbWVudChDdWJlR3JpZCwge3RleHQ6IFwiTG9hZGluZyAuLi5cIn0pKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3cgY2FyZFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJoNFwiLCBudWxsLCBcIlBvc3RpbmcgUmVxdWVzdFwiKSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb25cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImxpXCIsIHtjbGFzc05hbWU6IFwiY29sbGVjdGlvbi1pdGVtIGF2YXRhclwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImltZ1wiLCB7c3JjOiBcImltZy9ibG9vZHR5cGVfbG9nby5wbmdcIiwgY2xhc3NOYW1lOiBcImNpcmNsZVwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHtjbGFzc05hbWU6IFwidGl0bGVcIn0sIFwiT3JnYW5pemF0aW9uIE5hbWU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtvbkNoYW5nZTogdGhpcy5vbk5hbWVDaGFuZ2UsIHJlZjogXCJuYW1lXCIsIHR5cGU6IFwidGV4dFwiLCBwbGFjZWhvbGRlcjogXCJuYW1lXCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICksIFxuXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwge2NsYXNzTmFtZTogXCJjb2xsZWN0aW9uLWl0ZW0gYXZhdGFyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW1nXCIsIHtzcmM6IFwiaW1nL2xvY2F0aW9uX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIkxvY2F0aW9uOlwiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChMb2NhdGlvbklucHV0LCB7aW5pdEFkZHJlc3M6IHRoaXMuc3RhdGUuY3VycmVudEFkZHJlc3MsIHJlZjogXCJhZGRyZXNzXCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb24taXRlbSBhdmF0YXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogXCJpbWcvYmxvb2R0eXBlX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIkJsb29kIFR5cGU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtyZWY6IFwiYmxvb2RUeXBlXCIsIG9uQ2hhbmdlOiB0aGlzLm9uQmxvb2RUeXBlQ2hhbmdlLCB0eXBlOiBcInRleHRcIn0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb24taXRlbSBhdmF0YXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogXCJpbWcvYmxvb2R0eXBlX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIlRpbWU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtvbkNoYW5nZTogdGhpcy5vblRpbWVDaGFuZ2UsIHJlZjogXCJ0aW1lXCIsIHBsYWNlaG9sZGVyOiBcIk1NL2RkL3l5eXkgSEg6bW06c3NcIiwgdHlwZTogXCJ0ZXh0XCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJmb3JtLWZvb3RlciByaWdodFwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYVwiLCB7aHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIiwgb25DbGljazogdGhpcy5wb3N0UmVxdWVzdCwgY2xhc3NOYW1lOiBcIndhdmVzLWVmZmVjdCB3YXZlcy1ncmVlbiB0ZWFsIGxpZ2h0ZW4tMiBidG5cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiU2F2ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJhXCIsIHtocmVmOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiLCBjbGFzc05hbWU6IFwid2F2ZXMtZWZmZWN0IHdhdmVzLWdyZWVuIGJsdWUtZ3JleSBidG5cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQ2FuY2VsXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5QT1NUSU5HKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU3Bpbm5lcldhdmUsIHt0ZXh0OiBcIlBvc3RpbmcgRG9uYXRpb24gUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9XG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMTAvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICQoXCIjbWVudS1idG5cIikuc2lkZU5hdih7XG4gICAgICAgICAgICBlZGdlOiAnbGVmdCcsIC8vIENob29zZSB0aGUgaG9yaXpvbnRhbCBvcmlnaW5cbiAgICAgICAgICAgIGNsb3NlT25DbGljazogdHJ1ZSAvLyBDbG9zZXMgc2lkZS1uYXYgb24gPGE+IGNsaWNrcywgdXNlZnVsIGZvciBBbmd1bGFyL01ldGVvclxuICAgICAgICB9KTtcbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8qKnZhciBsaW5rcyA9IFtcbiAgICAgICAgICAgIDxsaSBrZXk9ezF9PlxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjL2ZpbmRfZG9uYXRpb25cIiBjbGFzc05hbWU9XCJ3YXZlcy1lZmZlY3Qgd2F2ZXMtbGlnaHRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaW9uLWFuZHJvaWQtc2VhcmNoXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICBGaW5kIERvbmF0aW9uIFJlcXVlc3RzXG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9saT4sXG4gICAgICAgICAgICA8bGkga2V5PXsyfSBjbGFzc05hbWU9XCJkaXZpZGVyXCI+PC9saT4sXG4gICAgICAgICAgICA8bGkga2V5PXszfT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiIy9wb3N0X3JlcXVlc3RcIiBjbGFzc05hbWU9XCJ3YXZlcy1lZmZlY3Qgd2F2ZXMtbGlnaHRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaW9uLWFuZHJvaWQtdW5sb2NrXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICBQb3N0IFJlcXVlc3RcbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICA8L2xpPixcbiAgICAgICAgXSoqL1xuICAgICAgICB2YXIgbGlua3MgPSBbXVxuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYVwiLCB7aHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIiwgY2xhc3NOYW1lOiBcImJyYW5kLWxvZ28gY2VudGVyXCJ9LCBjb25maWcuYXBwX25hbWUpLCBcbiAgICAgICAgICAgICAgICAvKjxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzc05hbWU9XCJicmFuZC1sb2dvIHJpZ2h0XCIgaWQ9XCJsb2dvXCI+PGltZyBzcmM9XCJpbWcvYXBwLWxvZ28tOTYucG5nXCIvPjwvYT4qL1xuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiLCB7Y2xhc3NOYW1lOiBcImxlZnQgaGlkZS1vbi1tZWQtYW5kLWRvd25cIn0sIFxuICAgICAgICAgICAgICAgICAgICBsaW5rc1xuICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiLCB7aWQ6IFwic2xpZGUtb3V0XCIsIGNsYXNzTmFtZTogXCJzaWRlLW5hdlwifSwgXG4gICAgICAgICAgICAgICAgICAgIGxpbmtzXG4gICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYVwiLCB7aHJlZjogXCIjXCIsIFwiZGF0YS1hY3RpdmF0ZXNcIjogXCJzbGlkZS1vdXRcIiwgY2xhc3NOYW1lOiBcImJ1dHRvbi1jb2xsYXBzZVwiLCBpZDogXCJtZW51LWJ0blwifSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImlcIiwge2NsYXNzTmFtZTogXCJtZGktbmF2aWdhdGlvbi1tZW51XCJ9KSlcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDEwLzA1LzE1LlxuICovXG52YXIgU2xpZGVyID0gcmVxdWlyZShcIi4vYnV0dG9ucy9TbGlkZXJcIilcbnZhciBTcGlubmVyV2F2ZSA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvU3Bpbm5lcldhdmVcIilcbnZhciBDdWJlR3JpZCA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvVGhyZWVCb3VuY2VcIilcbnZhciBEb25hdGlvblJlc3VsdCA9IHJlcXVpcmUoXCIuL2ZpbmRfYmlrZXBvZC9Eb25hdGlvblJlc3VsdFwiKVxudmFyIExvY2F0aW9uU2V0dGluZyA9IHJlcXVpcmUoXCIuL2ZpbmRfYmlrZXBvZC9Mb2NhdGlvblNldHRpbmdcIilcbnZhciBNYXBNYXJrZXIgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvTWFwTWFya2VyXCIpXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG4gICAgbWl4aW5zOiBbZmx1eC5taXhpbnMuc3RvcmVMaXN0ZW5lcl0sXG5cbiAgICB3YXRjaFN0b3JlczogWydMb2NhdGlvblN0b3JlJ10sXG5cbiAgICBDdXJyZW50U3RhdGU6IHtcbiAgICAgICAgRklORElORzogXCJmaW5kaW5nXCIsXG4gICAgICAgIElOSVRJQUw6IFwiaW5pdGlhbFwiLFxuICAgICAgICBSRUFEWTogJ1JFQURZJyxcbiAgICAgICAgUkVTVUxUOiBcInJlc3VsdFwiXG4gICAgfSxcblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY3VycmVudFN0YXRlOiAodGhpcy5wcm9wcy5jdXJyZW50U3RhdGUpID8gdGhpcy5wcm9wcy5jdXJyZW50U3RhdGUgOiB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMLFxuICAgICAgICAgICAgZm91bnRhaW5zOiBbXSxcbiAgICAgICAgICAgIGNvb3Jkczoge31cbiAgICAgICAgfVxuICAgIH0sXG5cblxuICAgIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHM6IGZ1bmN0aW9uIChuZXh0UHJvcHMpIHtcbiAgICAgICAgaWYgKG5leHRQcm9wcy5jdXJyZW50U3RhdGUpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogbmV4dFByb3BzLmN1cnJlbnRTdGF0ZX0pXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgc3RvcmVEaWRDaGFuZ2U6IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICAgIGlmICh0aGlzLmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICB2YXIgc3RvcmUgPSBkaXNwYXRjaGVyLmdldFN0b3JlKG5hbWUpXG4gICAgICAgICAgICB2YXIgc3RvcmVTdGF0ZSA9IHN0b3JlLmdldFN0YXRlKClcblxuXG4gICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5hY3Rpb24gPT0gc3RvcmUuYWN0aW9ucy5GSU5EX0ZPVU5UQUlOKSB7XG4gICAgICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtmb3VudGFpbnM6IHN0b3JlU3RhdGUuZm91bnRhaW5zfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kcmF3TWFwKHRoaXMuc3RhdGUuY29vcmRzKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIG5hdmlnYXRvci5ub3RpZmljYXRpb24uYWxlcnQoc3RvcmVTdGF0ZS5lcnJvck1lc3NhZ2UpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogdGhpcy5DdXJyZW50U3RhdGUuUkVBRFl9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBkcmF3TWFwOiBmdW5jdGlvbiAoY29vcmRzKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogdGhpcy5DdXJyZW50U3RhdGUuUkVBRFl9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBNYXBNYXJrZXIuaW5pdFNlYXJjaChjb29yZHMsIHRoaXMucmVmcy5nb29nbGVNYXAuZ2V0RE9NTm9kZSgpLCB7XG4gICAgICAgICAgICAgICAgXCJpdGVtc1wiOiB0aGlzLnN0YXRlLmZvdW50YWlucyxcbiAgICAgICAgICAgICAgICBcImljb25cIjogY29uZmlnLmZvdW50YWluX2ljb25cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pXG4gICAgfSxcblxuICAgIHNldFVwQ29tcG9uZW50OiBmdW5jdGlvbiAocmFkaXVzKSB7XG4gICAgICAgIHZhciBwYXJlbnQgPSB0aGlzXG4gICAgICAgIHZhciByYWQgPSAocmFkaXVzKSA/IHJhZGl1cyA6IDVcbiAgICAgICAgaWYgKCF3aW5kb3cuY29vcmRzKSB7XG4gICAgICAgICAgICBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKGZ1bmN0aW9uIChwb3NpdGlvbikge1xuICAgICAgICAgICAgICAgIHZhciBmaW5kID0gbnVsbFxuICAgICAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGNvb3JkcyA9IHtsYXQ6IHBvc2l0aW9uLmNvb3Jkcy5sYXRpdHVkZSwgbG5nOiBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlfVxuICAgICAgICAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe2Nvb3JkczogY29vcmRzfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9ucy5maW5kRHJpbmtpbmdGb3VudGFpbihjb29yZHMsIHJhZClcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBmaW5kID0gbnVsbFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvL3BhcmVudC5kcmF3TWFwKGNvb3JkcylcbiAgICAgICAgICAgICAgICAvKmdvb2dsZVV0aWxzLm1hcHMucmV2ZXJzZUdlb2NvZGluZyhwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUsIHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGUsXG4gICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChhZGRyZXNzKSB7XG4gICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7Y3VycmVudEFkZHJlc3M6IGFkZHJlc3MsIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5SRUFEWX0pXG4gICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgICBhbGVydChlcnJvcilcbiAgICAgICAgICAgICAgICAgfSkqL1xuICAgICAgICAgICAgfSwgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgaWYgKHBhcmVudC5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgICAgICAgICBhbGVydChlcnJvci5tZXNzYWdlKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIHtlbmFibGVIaWdoQWNjdXJhY3k6IHRydWV9KVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjb29yZHM6IGNvb3Jkc30sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBhY3Rpb25zLmZpbmREcmlua2luZ0ZvdW50YWluKHdpbmRvdy5jb29yZHMsIHJhZClcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cblxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkVXBkYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMKSB7XG4gICAgICAgICAgICAvL3RoaXMuc2V0VXBDb21wb25lbnQoKVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSlcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwpIHtcbiAgICAgICAgICAgIHJldHVybiAoUmVhY3QuY3JlYXRlRWxlbWVudChDdWJlR3JpZCwge3RleHQ6IFwiTG9hZGluZyAuLi5cIn0pKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzMTJcIiwgaWQ6IFwic2VhcmNoLXdyYXBwZXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibmF2XCIsIG51bGwsIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcIm5hdi13cmFwcGVyIHRvLWdvLW5hdlwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInJvd1wifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczExXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJoNVwiLCBudWxsLCBcIldoZXJlIHRvIGdvIG5leHQ/XCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMVwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiaW5wdXQtZmllbGRcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7dmFsdWU6IFwiQ3VycmVudCBMb2NhdGlvblwiLCB0eXBlOiBcInNlYXJjaFwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsYWJlbFwiLCBudWxsLCBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaVwiLCB7Y2xhc3NOYW1lOiBcIlwifSwgXCJTdGFydFwiKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczExXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJpbnB1dC1maWVsZFwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtpZDogXCJwbGFjZS1zZWFyY2hcIiwgdHlwZTogXCJzZWFyY2hcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIiwgbnVsbCwgUmVhY3QuY3JlYXRlRWxlbWVudChcImlcIiwge2NsYXNzTmFtZTogXCJcIn0sIFwiRW5kXCIpKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7c3R5bGU6IHtwb3NpdGlvbjogXCJhYnNvbHV0ZVwiLCBsZWZ0OiBcIjBcIiwgYm90dG9tOiBcIjIwJVwiLCBvdmVyZmxvdzogXCJzY3JvbGxcIiwgaGVpZ2h0OiBcIjIwJVwiLCB3aWR0aDogXCIxMCVcIn19LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiVG93biBIYWxsXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQm91cmtlIFN0cmVldFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTJcIiwgaWQ6IFwiZ29vZ2xlLW1hcFwiLCByZWY6IFwiZ29vZ2xlTWFwXCJ9KVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5GSU5ESU5HKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU3Bpbm5lcldhdmUsIHt0ZXh0OiBcIkZpbmRpbmcgUG9kcyBSZXF1ZXN0cyAuLi5cIn0pXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPSB0aGlzLkN1cnJlbnRTdGF0ZS5SRVNVTFQpIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChEb25hdGlvblJlc3VsdCwge2RvbmF0aW9uczogdGhpcy5zdGF0ZS5kb25hdGlvbnN9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9XG4gICAgfVxufSlcbiIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMy8wNS8xNS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICB2YWx1ZTogKHRoaXMucHJvcHMudmFsdWUpPyB0aGlzLnByb3BzLnZhbHVlIDogMFxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcbiAgICAgICAgdGhpcy5hY3RpdmF0ZVNsaWRlcigpXG4gICAgfSxcblxuICAgIHNldFZhbHVlOiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHt2YWx1ZTogdmFsdWV9KVxuICAgIH0sXG5cbiAgICBhY3RpdmF0ZVNsaWRlcjogIGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgbWluID0gKHRoaXMucHJvcHMubWluKT8gdGhpcy5wcm9wcy5taW4gOiAwXG4gICAgICAgIHZhciBtYXggPSAodGhpcy5wcm9wcy5tYXgpPyB0aGlzLnByb3BzLm1heCA6IDUwXG4gICAgICAgIHZhciBub2RlID0gJCh0aGlzLnJlZnMuc2xpZGVyLmdldERPTU5vZGUoKSlcbiAgICAgICAgbm9kZS5ub1VpU2xpZGVyKHtcbiAgICAgICAgICAgIHN0YXJ0OiB0aGlzLnN0YXRlLnZhbHVlLFxuICAgICAgICAgICAgY29ubmVjdDogXCJsb3dlclwiLFxuICAgICAgICAgICAgb3JpZW50YXRpb246IFwiaG9yaXpvbnRhbFwiLFxuICAgICAgICAgICAgZGlyZWN0aW9uOiBcImx0clwiLFxuICAgICAgICAgICAgYmVoYXZpb3VyOiBcInRhcC1kcmFnXCIsXG4gICAgICAgICAgICByYW5nZToge1xuICAgICAgICAgICAgICAgICdtaW4nOiBtaW4sXG4gICAgICAgICAgICAgICAgJ21heCc6IG1heFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGZvcm1hdDogd051bWIoe1xuICAgICAgICAgICAgICAgIGRlY2ltYWxzOiAwXG4gICAgICAgICAgICB9KVxuICAgICAgICB9KTtcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgbm9kZS5vbignc2xpZGUnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHBhcmVudC5zZXRWYWx1ZSgkKHRoaXMpLnZhbCgpKVxuICAgICAgICB9KVxuXHRub2RlLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbiAoKSB7XG5cdCAgICBpZiAocGFyZW50LnByb3BzLnNsaWRlclJlbGVhc2UpIHtcblx0XHRwYXJlbnQucHJvcHMuc2xpZGVyUmVsZWFzZSgkKHRoaXMpLnZhbCgpKVxuXHQgICAgfVxuXHR9KVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHVuaXQgPSAodGhpcy5wcm9wcy51bml0KT8gXCIgXCIgK3RoaXMucHJvcHMudW5pdCA6IFwiXCJcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEyIHNsaWRlci1jb250YWluZXJcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzbGlkZXIgY29sIHMxMFwiLCByZWY6IFwic2xpZGVyXCJ9KSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMiBzbGlkZXItdmFsdWVcIn0sIHRoaXMuc3RhdGUudmFsdWUgKyB1bml0KVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSlcbiIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMC8wNS8xNS5cbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEyIG04IG9mZnNldC1tMiBsNiBvZmZzZXQtbDNcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjYXJkLXBhbmVsIGdyZXkgbGlnaHRlbi01IHotZGVwdGgtMVwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3cgdmFsaWduLXdyYXBwZXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMlwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImltZ1wiLCB7c3JjOiB0aGlzLnByb3BzLmRvbmF0aW9uLnByb2ZpbGVJbWcsIGFsdDogXCJQcm9maWxlIFBpY3R1cmVcIiwgY2xhc3NOYW1lOiBcImNpcmNsZSByZXNwb25zaXZlLWltZ1wifSlcbiAgICAgICAgICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTBcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwXCIsIG51bGwpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCB7Y2xhc3NOYW1lOiBcImJsYWNrLXRleHRcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmRvbmF0aW9uLmRlc2NyaXB0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjAvMDUvMTUuXG4gKi9cblxudmFyIERvbmF0aW9uQ2FyZCA9IHJlcXVpcmUoXCIuL0RvbmF0aW9uQ2FyZFwiKVxuXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgZG9uYXRpb25zOiAodGhpcy5wcm9wcy5kb25hdGlvbnMpPyB0aGlzLnByb3BzLmRvbmF0aW9ucyA6IFtdXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24obmV4dFByb3BzKSB7XG4gICAgICAgIGlmIChuZXh0UHJvcHMuZG9uYXRpb25zICYmIEpTT04uc3RyaW5naWZ5KG5leHRQcm9wcy5kb25hdGlvbnMpICE9IEpTT04uc3RyaW5naWZ5KHRoaXMucHJvcHMuZG9uYXRpb25zKSkge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZG9uYXRpb25zOiBuZXh0UHJvcHMuZG9uYXRpb25zfSlcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5kb25hdGlvbnMubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImgzXCIsIG51bGwsIFxuICAgICAgICAgICAgICAgICAgICBcIk5vIGRvbmF0aW9uIHJlcXVlc3RzIGFyZSBmb3VuZCBhcm91bmQgeW91ciBsb2NhdGlvbiFcIlxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImJ1c2tlci1saXN0XCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmRvbmF0aW9ucy5tYXAoZnVuY3Rpb24oaXRlbSwgaWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KERvbmF0aW9uQ2FyZCwge2RvbmF0aW9uOiBpdGVtLCBrZXk6IGlkfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH1cbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMy8wNS8xNS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBzZWFyY2hCb3g6IHVuZGVmaW5lZFxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3NlYXJjaEJveDogbmV3IGdvb2dsZS5tYXBzLnBsYWNlcy5TZWFyY2hCb3godGhpcy5yZWZzLmlucHV0LmdldERPTU5vZGUoKSl9KVxuICAgIH0sXG5cbiAgICBnZXRWYWx1ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5yZWZzLmlucHV0LmdldERPTU5vZGUoKS52YWx1ZVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3dcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJpbnB1dC1maWVsZCBjb2wgczEyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHt0eXBlOiBcInRleHRcIiwgcGxhY2Vob2xkZXI6IFwiQ2hvb3NlIGEgbG9jYXRpb25cIiwgY2xhc3NOYW1lOiBcInZhbGlkYXRlXCIsIHJlZjogXCJpbnB1dFwiLCBkZWZhdWx0VmFsdWU6IHRoaXMucHJvcHMuaW5pdEFkZHJlc3N9KVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIzLzA1LzE1LlxuICovXG52YXIgU2xpZGVyID0gcmVxdWlyZShcIi4uL2J1dHRvbnMvU2xpZGVyXCIpXG52YXIgTG9jYXRpb25JbnB1dCA9IHJlcXVpcmUoXCIuL0xvY2F0aW9uSW5wdXRcIilcblxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuXG4gICAgY29tcG9uZW50V2lsbFVwZGF0ZTogZnVuY3Rpb24gKG5leHRQcm9wcykge1xuICAgICAgICBpZiAobmV4dFByb3BzLmluaXRBZGRyZXNzICYmIG5leHRQcm9wcy5pbml0QWRkcmVzcyAhPSB0aGlzLnByb3BzLmluaXRBZGRyZXNzKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgIH0sXG5cbiAgICBnZXRWYWx1ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgYWRkcmVzczogdGhpcy5yZWZzLmFkZHJlc3MuZ2V0VmFsdWUoKSxcbiAgICAgICAgICAgIHJhZGl1czogdGhpcy5yZWZzLnJhZGl1cy5zdGF0ZS52YWx1ZVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInJvd1wiLCBpZDogXCJsb2NhdGlvbi1zZXR0aW5nXCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZm9ybVwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTJcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicm93XCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoTG9jYXRpb25JbnB1dCwge2luaXRBZGRyZXNzOiB0aGlzLnByb3BzLmluaXRBZGRyZXNzLCByZWY6IFwiYWRkcmVzc1wifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTIgbGFiZWxcIn0sIFwiUmFkaXVzXCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU2xpZGVyLCB7cmVmOiBcInJhZGl1c1wiLCB2YWx1ZTogMTAsIHVuaXQ6IFwia21cIn0pXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9nbm5odSBvbiAyNi8wOS8xNS5cbiAqL1xudmFyIGdBcGkgPSByZXF1aXJlKFwiLi4vLi4vdXRpbHMvZ29vZ2xlXCIpXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBnZXRNYXJrZXJzOiBmdW5jdGlvbiAoY3VycmVudExvY2F0aW9uLCBwb2RzLCBkb20sIHJhZGl1cywgaWNvbikge1xuICAgICAgICBpZiAoIXdpbmRvdy5tYXApIHtcbiAgICAgICAgICAgIHdpbmRvdy5tYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGRvbSwge1xuICAgICAgICAgICAgICAgIGNlbnRlcjogY3VycmVudExvY2F0aW9uLFxuICAgICAgICAgICAgICAgIHpvb206IGNvbmZpZy5nb29nbGVNYXAuem9vbSxcbiAgICAgICAgICAgICAgICBtYXBUeXBlSWQ6IGdvb2dsZS5tYXBzLk1hcFR5cGVJZC5URVJSQUlOLFxuICAgICAgICAgICAgICAgIG1hcFR5cGVDb250cm9sOiBmYWxzZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgZ29vZ2xlLm1hcHMudmlzdWFsUmVmcmVzaCA9IHRydWU7XG4gICAgICAgIHZhciBjdXJyZW50TG9jYXRpb25NYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBjdXJyZW50TG9jYXRpb24sXG4gICAgICAgICAgICB0aXRsZTogJ1lvdSBhcmUgaGVyZScsXG4gICAgICAgICAgICBpY29uOiBjb25maWcudXNlcl9jdXJyZW50X2xvY2F0aW9uX2ljb25cbiAgICAgICAgfSk7XG4gICAgICAgIGN1cnJlbnRMb2NhdGlvbk1hcmtlci5zZXRNYXAobWFwKTtcbiAgICAgICAgdmFyIGN1cnJlbnRMb2NhdGlvbkluZm9XaW5kb3cgPSBuZXcgZ29vZ2xlLm1hcHMuSW5mb1dpbmRvdyh7XG4gICAgICAgICAgICBjb250ZW50OiBcIjxwPllvdSBhcmUgaGVyZSE8L3A+XCJcbiAgICAgICAgfSk7XG4gICAgICAgIGN1cnJlbnRMb2NhdGlvbk1hcmtlci5hZGRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBjdXJyZW50TG9jYXRpb25JbmZvV2luZG93Lm9wZW4obWFwLCBjdXJyZW50TG9jYXRpb25NYXJrZXIpO1xuICAgICAgICB9KVxuXG4gICAgICAgIHBvZHMuZm9yRWFjaChmdW5jdGlvbiAocG9kLCBpZCkge1xuICAgICAgICAgICAgdmFyIGNvb3JkaW5hdGVzID0gcG9kLmNvb3JkaW5hdGVzLmNvb3JkaW5hdGVzXG4gICAgICAgICAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHtcbiAgICAgICAgICAgICAgICAgICAgbGF0OiBwYXJzZUZsb2F0KGNvb3JkaW5hdGVzW1wibGF0XCJdKSxcbiAgICAgICAgICAgICAgICAgICAgbG5nOiBwYXJzZUZsb2F0KGNvb3JkaW5hdGVzW1wibG9uXCJdKVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgaWNvbjogKGljb24pID8gaWNvbiA6IGNvbmZpZy5iaWtlX2ljb25cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBtYXJrZXIuYWRkTGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHdpbmRvdy5wb3B1cFBvZERldGFpbHMocG9kKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIG1hcmtlci5zZXRNYXAobWFwKVxuICAgICAgICB9KVxuICAgICAgICBuZXcgZ29vZ2xlLm1hcHMuQ2lyY2xlKHtcbiAgICAgICAgICAgIHN0cm9rZUNvbG9yOiAnZ3JlZW4nLFxuICAgICAgICAgICAgc3Ryb2tlT3BhY2l0eTogMC44LFxuICAgICAgICAgICAgc3Ryb2tlV2VpZ2h0OiAyLFxuICAgICAgICAgICAgZmlsbE9wYWNpdHk6IDAuMCxcbiAgICAgICAgICAgIG1hcDogbWFwLFxuICAgICAgICAgICAgY2VudGVyOiBjdXJyZW50TG9jYXRpb24sXG4gICAgICAgICAgICByYWRpdXM6IDEwMDBcbiAgICAgICAgfSk7XG4gICAgfSxcblxuICAgIGluaXRTZWFyY2g6IGZ1bmN0aW9uIChjdXJyZW50TG9jYXRpb24sIGRvbSwgZGF0YSkge1xuICAgICAgICB2YXIgbWFya2VycyA9IFtdXG4gICAgICAgIGlmICghd2luZG93Lm1hcCkge1xuICAgICAgICAgICAgd2luZG93Lm1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9tLCB7XG4gICAgICAgICAgICAgICAgY2VudGVyOiBjdXJyZW50TG9jYXRpb24sXG4gICAgICAgICAgICAgICAgem9vbTogY29uZmlnLmdvb2dsZU1hcC56b29tLFxuICAgICAgICAgICAgICAgIG1hcFR5cGVJZDogZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLlRFUlJBSU4sXG4gICAgICAgICAgICAgICAgbWFwVHlwZUNvbnRyb2w6IGZhbHNlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBnb29nbGUubWFwcy52aXN1YWxSZWZyZXNoID0gdHJ1ZTtcbiAgICAgICAgdmFyIGN1cnJlbnRMb2NhdGlvbk1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgICAgcG9zaXRpb246IGN1cnJlbnRMb2NhdGlvbixcbiAgICAgICAgICAgIHRpdGxlOiAnWW91IGFyZSBoZXJlJyxcbiAgICAgICAgICAgIGljb246IGNvbmZpZy51c2VyX2N1cnJlbnRfbG9jYXRpb25faWNvblxuICAgICAgICB9KTtcbiAgICAgICAgY3VycmVudExvY2F0aW9uTWFya2VyLnNldE1hcChtYXApO1xuICAgICAgICB2YXIgY3VycmVudExvY2F0aW9uSW5mb1dpbmRvdyA9IG5ldyBnb29nbGUubWFwcy5JbmZvV2luZG93KHtcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiPHA+WW91IGFyZSBoZXJlITwvcD5cIlxuICAgICAgICB9KTtcbiAgICAgICAgY3VycmVudExvY2F0aW9uTWFya2VyLmFkZExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGN1cnJlbnRMb2NhdGlvbkluZm9XaW5kb3cub3BlbihtYXAsIGN1cnJlbnRMb2NhdGlvbk1hcmtlcik7XG4gICAgICAgIH0pXG5cbiAgICAgICAgZGF0YS5pdGVtcy5mb3JFYWNoKGZ1bmN0aW9uIChwb2ludCkge1xuICAgICAgICAgICAgdmFyIGNvb3JkaW5hdGVzID0gcG9pbnQuY29vcmRpbmF0ZXMuY29vcmRpbmF0ZXNcbiAgICAgICAgICAgIHZhciBtT3B0cyA9IHtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjoge1xuICAgICAgICAgICAgICAgICAgICBsYXQ6IHBhcnNlRmxvYXQoY29vcmRpbmF0ZXNbXCJsYXRcIl0pLFxuICAgICAgICAgICAgICAgICAgICBsbmc6IHBhcnNlRmxvYXQoY29vcmRpbmF0ZXNbXCJsb25cIl0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGRhdGEuaWNvbikge1xuICAgICAgICAgICAgICAgIG1PcHRzW1wiaWNvblwiXSA9IGRhdGEuaWNvblxuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcihtT3B0cylcbiAgICAgICAgICAgIG1hcmtlci5zZXRNYXAobWFwKVxuICAgICAgICAgICAgbWFya2Vycy5wdXNoKG1hcmtlcilcbiAgICAgICAgfSlcbiAgICAgICAgbmV3IGdvb2dsZS5tYXBzLkNpcmNsZSh7XG4gICAgICAgICAgICBzdHJva2VDb2xvcjogJ2dyZWVuJyxcbiAgICAgICAgICAgIHN0cm9rZU9wYWNpdHk6IDAuOCxcbiAgICAgICAgICAgIHN0cm9rZVdlaWdodDogMixcbiAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAwLjAsXG4gICAgICAgICAgICBtYXA6IG1hcCxcbiAgICAgICAgICAgIGNlbnRlcjogY3VycmVudExvY2F0aW9uLFxuICAgICAgICAgICAgcmFkaXVzOiAxMDAwXG4gICAgICAgIH0pO1xuICAgICAgICB2YXIgc2VhcmNoUGxhY2VzTWFya2VycyA9IFtdXG4gICAgICAgIGlmIChkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncGxhY2Utc2VhcmNoJykpIHtcbiAgICAgICAgICAgIC8vIENyZWF0ZSB0aGUgc2VhcmNoIGJveCBhbmQgbGluayBpdCB0byB0aGUgVUkgZWxlbWVudC5cbiAgICAgICAgICAgIHZhciBpbnB1dCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwbGFjZS1zZWFyY2gnKTtcbiAgICAgICAgICAgIHZhciBzZWFyY2hCb3ggPSBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLlNlYXJjaEJveChpbnB1dCk7XG4gICAgICAgICAgICAvL21hcC5jb250cm9sc1tnb29nbGUubWFwcy5Db250cm9sUG9zaXRpb24uVE9QX0xFRlRdLnB1c2goaW5wdXQpO1xuXG4gICAgICAgICAgICB2YXIgZGlyZWN0aW9uc0Rpc3BsYXkgPSBuZXcgZ29vZ2xlLm1hcHMuRGlyZWN0aW9uc1JlbmRlcmVyKHtcbiAgICAgICAgICAgICAgICBwcmVzZXJ2ZVZpZXdwb3J0OiB0cnVlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBkaXJlY3Rpb25zRGlzcGxheS5zZXRNYXAobWFwKVxuICAgICAgICAgICAgc2VhcmNoQm94LmFkZExpc3RlbmVyKCdwbGFjZXNfY2hhbmdlZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXIgcGxhY2VzID0gc2VhcmNoQm94LmdldFBsYWNlcygpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHBsYWNlcy5sZW5ndGggPT0gMCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHNlYXJjaFBsYWNlc01hcmtlcnMuZm9yRWFjaChmdW5jdGlvbiAobWFya2VyKSB7XG4gICAgICAgICAgICAgICAgICAgIG1hcmtlci5zZXRNYXAobnVsbClcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC8vIEZvciBlYWNoIHBsYWNlLCBnZXQgdGhlIGljb24sIG5hbWUgYW5kIGxvY2F0aW9uLlxuICAgICAgICAgICAgICAgIHZhciBib3VuZHMgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nQm91bmRzKCk7XG4gICAgICAgICAgICAgICAgcGxhY2VzLmZvckVhY2goZnVuY3Rpb24gKHBsYWNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghcGxhY2UuZ2VvbWV0cnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmV0dXJuZWQgcGxhY2UgY29udGFpbnMgbm8gZ2VvbWV0cnlcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdmFyIGljb24gPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHBsYWNlLmljb24sXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplOiBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSg3MSwgNzEpLFxuICAgICAgICAgICAgICAgICAgICAgICAgb3JpZ2luOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCksXG4gICAgICAgICAgICAgICAgICAgICAgICBhbmNob3I6IG5ldyBnb29nbGUubWFwcy5Qb2ludCgxNywgMzQpLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMjUsIDI1KVxuICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIENyZWF0ZSBhIG1hcmtlciBmb3IgZWFjaCBwbGFjZS5cbiAgICAgICAgICAgICAgICAgICAgdmFyIG1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFwOiBtYXAsXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogcGxhY2UubmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBwbGFjZS5nZW9tZXRyeS5sb2NhdGlvblxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICBzZWFyY2hQbGFjZXNNYXJrZXJzLnB1c2gobWFya2VyKVxuICAgICAgICAgICAgICAgICAgICB2YXIgbUxvY2F0aW9uSW5mbyA9IG5ldyBnb29nbGUubWFwcy5JbmZvV2luZG93KHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiPHA+XCIgKyBwbGFjZS5uYW1lICsgXCI8L3A+XCJcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIG1hcmtlci5hZGRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtTG9jYXRpb25JbmZvLm9wZW4obWFwLCBtYXJrZXIpO1xuICAgICAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgICAgIGlmIChwbGFjZS5nZW9tZXRyeS52aWV3cG9ydCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gT25seSBnZW9jb2RlcyBoYXZlIHZpZXdwb3J0LlxuICAgICAgICAgICAgICAgICAgICAgICAgYm91bmRzLnVuaW9uKHBsYWNlLmdlb21ldHJ5LnZpZXdwb3J0KTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvdW5kcy5leHRlbmQocGxhY2UuZ2VvbWV0cnkubG9jYXRpb24pO1xuICAgICAgICAgICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgICAgICAgICBnQXBpLm1hcHMuZ2V0RGlyKGN1cnJlbnRMb2NhdGlvbiwgcGxhY2UuZ2VvbWV0cnkubG9jYXRpb24sIGZ1bmN0aW9uIChkaXJlY3Rpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGlvbnNEaXNwbGF5LnNldERpcmVjdGlvbnMoZGlyZWN0aW9uKVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIGN1cnJlbnRMb2NhdGlvbk1hcmtlci5zZXRNYXAobnVsbClcbiAgICAgICAgICAgICAgICBtYXAuZml0Qm91bmRzKGJvdW5kcyk7XG5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxufVxuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIwLzA1LzE1LlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzcGlubmVyLXdyYXBwZXJcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzcGlubmVyLWNlbGxcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stc3Bpbm5lciBzay1zcGlubmVyLXdhdmVcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXJlY3QxXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stcmVjdDJcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1yZWN0M1wifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXJlY3Q0XCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stcmVjdDVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwXCIsIG51bGwsIHRoaXMucHJvcHMudGV4dClcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIzLzA1LzE1LlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic3Bpbm5lci13cmFwcGVyXCJ9LCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic3Bpbm5lci1jZWxsXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXNwaW5uZXIgc2stc3Bpbm5lci10aHJlZS1ib3VuY2VcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLWJvdW5jZTFcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1ib3VuY2UyXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stYm91bmNlM1wifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwgbnVsbCwgdGhpcy5wcm9wcy50ZXh0KVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSkiLCIvKlxuICogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuICogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4gKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuICogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuICogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuICogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4gKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuICogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiAqIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4gKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiAqIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiAqIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbnZhciByb3V0ZXIgPSByZXF1aXJlKFwiLi9yb3V0ZXIvcm91dGVyXCIpXG5cbnZhciBhcHAgPSB7XG4gICAgLy8gQXBwbGljYXRpb24gQ29uc3RydWN0b3JcbiAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgdGhpcy5iaW5kRXZlbnRzKCk7XG4gICAgfSxcbiAgICAvLyBCaW5kIEV2ZW50IExpc3RlbmVyc1xuICAgIC8vXG4gICAgLy8gQmluZCBhbnkgZXZlbnRzIHRoYXQgYXJlIHJlcXVpcmVkIG9uIHN0YXJ0dXAuIENvbW1vbiBldmVudHMgYXJlOlxuICAgIC8vICdsb2FkJywgJ2RldmljZXJlYWR5JywgJ29mZmxpbmUnLCBhbmQgJ29ubGluZScuXG4gICAgYmluZEV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2RldmljZXJlYWR5JywgdGhpcy5vbkRldmljZVJlYWR5LCBmYWxzZSk7XG4gICAgfSxcbiAgICAvLyBkZXZpY2VyZWFkeSBFdmVudCBIYW5kbGVyXG4gICAgLy9cbiAgICAvLyBUaGUgc2NvcGUgb2YgJ3RoaXMnIGlzIHRoZSBldmVudC4gSW4gb3JkZXIgdG8gY2FsbCB0aGUgJ3JlY2VpdmVkRXZlbnQnXG4gICAgLy8gZnVuY3Rpb24sIHdlIG11c3QgZXhwbGljaXRseSBjYWxsICdhcHAucmVjZWl2ZWRFdmVudCguLi4pOydcbiAgICBvbkRldmljZVJlYWR5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgLy9hcHAucmVjZWl2ZWRFdmVudCgnZGV2aWNlcmVhZHknKTtcbiAgICAgICAgJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByb3V0ZXIuaW5pdChcIi9maW5kX2RvbmF0aW9uXCIpXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICQoXCIuc3BsYXNoLXNjcmVlblwiKS5mYWRlT3V0KDUwMClcbiAgICAgICAgICAgIH0sIDMwMDApXG4gICAgICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiYmFja2J1dHRvblwiLCBvbkJhY2tLZXlEb3duLCBmYWxzZSk7XG4gICAgICAgICAgICBmdW5jdGlvbiBvbkJhY2tLZXlEb3duKCkge1xuICAgICAgICAgICAgICAgIFJlYWN0LnVubW91bnRDb21wb25lbnRBdE5vZGUoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwb2QtZGV0YWlsc1wiKSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9XG59O1xuXG5hcHAuaW5pdGlhbGl6ZSgpO1xuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIwLzA1LzE1LlxuICovXG52YXIgRmluZEJpa2VQb2QgPSByZXF1aXJlKFwiLi4vY29tcG9uZW50cy9GaW5kQmlrZVBvZC5qc1wiKVxudmFyIFBvc3RSZXF1ZXN0ID0gcmVxdWlyZShcIi4uL2NvbXBvbmVudHMvUG9zdFJlcXVlc3RcIilcbnZhciBXaGVyZVRvR28gPSByZXF1aXJlKFwiLi4vY29tcG9uZW50cy9XaGVyZVRvR29cIilcbnZhciBTaWRlTmF2ID0gcmVxdWlyZShcIi4uL2NvbXBvbmVudHMvU2lkZU5hdlwiKVxud2luZG93LnJlbmRlckZpbmREb25hdGlvbiA9IGZ1bmN0aW9uICgpIHtcbiAgICB3aW5kb3cubWFwID0gbnVsbFxuICAgIFJlYWN0LnVubW91bnRDb21wb25lbnRBdE5vZGUoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYWluLWNvbnRlbnRcIikpXG4gICAgJChcIiNuYXYtYmFyXCIpLnNob3coKVxuICAgICAgICAkKFwiLm1vZGFsXCIpLmhpZGUoKVxuXG4gICAgUmVhY3QucmVuZGVyKFJlYWN0LmNyZWF0ZUVsZW1lbnQoU2lkZU5hdiwge2Rpc3BhdGNoZXI6IGRpc3BhdGNoZXJ9KSwgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJuYXYtYmFyXCIpKVxuICAgIGlmIChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBvZC1kZXRhaWxzXCIpKSB7XG4gICAgICAgIFJlYWN0LnVubW91bnRDb21wb25lbnRBdE5vZGUoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwb2QtZGV0YWlsc1wiKSlcbiAgICB9XG4gICAgUmVhY3QucmVuZGVyKFJlYWN0LmNyZWF0ZUVsZW1lbnQoRmluZEJpa2VQb2QsIHtjdXJyZW50U3RhdGU6IFwiaW5pdGlhbFwiLCBkaXNwYXRjaGVyOiBkaXNwYXRjaGVyfSksIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFpbi1jb250ZW50XCIpKVxufVxuXG53aW5kb3cucmVuZGVyV2hlcmVHbyA9IGZ1bmN0aW9uICgpIHtcbiAgICB3aW5kb3cubWFwID0gbnVsbFxuICAgICQoXCIjbmF2LWJhclwiKS5oaWRlKClcbiAgICAkKFwiLm1vZGFsXCIpLmhpZGUoKVxuICAgICQoXCIubGVhbi1vdmVybGF5XCIpLmhpZGUoKVxuICAgICQoXCIjbGVhbi1vdmVybGF5XCIpLmhpZGUoKVxuXG4gICAgUmVhY3QudW5tb3VudENvbXBvbmVudEF0Tm9kZShkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbiAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9kLWRldGFpbHNcIikpXG4gICAgUmVhY3QucmVuZGVyKFJlYWN0LmNyZWF0ZUVsZW1lbnQoV2hlcmVUb0dvLCB7Y3VycmVudFN0YXRlOiBcImluaXRpYWxcIiwgZGlzcGF0Y2hlcjogZGlzcGF0Y2hlcn0pLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgaW5pdDogZnVuY3Rpb24ocGF0aCkge1xuICAgICAgICB2YXIgcm91dGVzID0ge1xuICAgICAgICAgICAgXCIvZmluZF9kb25hdGlvblwiOiByZW5kZXJGaW5kRG9uYXRpb24sXG4gICAgICAgICAgICBcIi93aGVyZS1nb1wiOiByZW5kZXJXaGVyZUdvXG4gICAgICAgIH1cbiAgICAgICAgd2luZG93LnJvdXRlciA9IFJvdXRlcihyb3V0ZXMpO1xuICAgICAgICByb3V0ZXIuaW5pdChwYXRoKVxuICAgIH1cbn0iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjMvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0ge1xuICAgIG1hcHM6IHtcbiAgICAgICAgY2hlY2tDb25uZWN0aW9uOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgbmV0d29ya1N0YXRlID0gbmF2aWdhdG9yLmNvbm5lY3Rpb24udHlwZVxuICAgICAgICAgICAgY29uc29sZS5sb2cobmV0d29ya1N0YXRlKVxuICAgICAgICAgICAgaWYgKG5ldHdvcmtTdGF0ZSA9PSBDb25uZWN0aW9uLk5PTkUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH0sXG5cbiAgICAgICAgcmV2ZXJzZUdlb2NvZGluZzogZnVuY3Rpb24gKGxhdCwgbG5nLCBzdWNjZXNzLCBlcnJvcikge1xuICAgICAgICAgICAgdmFyIGdlb2NvZGVyID0gbmV3IGdvb2dsZS5tYXBzLkdlb2NvZGVyO1xuICAgICAgICAgICAgdmFyIGxhdGxuZyA9IHtsYXQ6IHBhcnNlRmxvYXQobGF0KSwgbG5nOiBwYXJzZUZsb2F0KGxuZyl9O1xuICAgICAgICAgICAgZ2VvY29kZXIuZ2VvY29kZSh7J2xvY2F0aW9uJzogbGF0bG5nfSwgZnVuY3Rpb24gKHJlc3VsdHMsIHN0YXR1cykge1xuICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPT09IGdvb2dsZS5tYXBzLkdlb2NvZGVyU3RhdHVzLk9LKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHRzWzBdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3VsdHNbMF0uZm9ybWF0dGVkX2FkZHJlc3MpXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcignTm8gcmVzdWx0cyBmb3VuZCcpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IoJ0dlb2NvZGVyIGZhaWxlZCBkdWUgdG86ICcgKyBzdGF0dXMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdlb2NvZGluZzogZnVuY3Rpb24gKGFkZHJlc3MsIHN1Y2Nlc3MsIGVycm9yKSB7XG4gICAgICAgICAgICB2YXIgZ2VvY29kZXIgPSBuZXcgZ29vZ2xlLm1hcHMuR2VvY29kZXI7XG4gICAgICAgICAgICBnZW9jb2Rlci5nZW9jb2RlKHsnYWRkcmVzcyc6IGFkZHJlc3N9LCBmdW5jdGlvbiAocmVzdWx0cywgc3RhdHVzKSB7XG4gICAgICAgICAgICAgICAgaWYgKHN0YXR1cyA9PT0gZ29vZ2xlLm1hcHMuR2VvY29kZXJTdGF0dXMuT0spIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdHNbMF0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzdWx0c1swXS5nZW9tZXRyeSlcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yKCdObyByZXN1bHRzIGZvdW5kJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBlcnJvcignR2VvY29kZXIgZmFpbGVkIGR1ZSB0bzogJyArIHN0YXR1cyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0RGlyOiBmdW5jdGlvbiAob3JpZ2luLCBkZXN0aW5hdGlvbiwgc3VjY2Vzcykge1xuICAgICAgICAgICAgdmFyIGRpcmVjdGlvbnNTZXJ2aWNlID0gbmV3IGdvb2dsZS5tYXBzLkRpcmVjdGlvbnNTZXJ2aWNlKCk7XG4gICAgICAgICAgICB2YXIgZGlyZWN0aW9uc1JlcXVlc3QgPSB7XG4gICAgICAgICAgICAgICAgXCJvcmlnaW5cIjogb3JpZ2luW1wibGF0XCJdICsgXCIsXCIgKyBvcmlnaW5bXCJsbmdcIl0sXG4gICAgICAgICAgICAgICAgXCJkZXN0aW5hdGlvblwiOiBvcmlnaW5bXCJsYXRcIl0gKyBcIixcIiArIG9yaWdpbltcImxuZ1wiXSxcbiAgICAgICAgICAgICAgICBvcHRpbWl6ZVdheXBvaW50czogdHJ1ZSxcbiAgICAgICAgICAgICAgICB0cmF2ZWxNb2RlOiAnQklDWUNMSU5HJ1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGRpcmVjdGlvbnNTZXJ2aWNlLnJvdXRlKGRpcmVjdGlvbnNSZXF1ZXN0LCBmdW5jdGlvbiAocmVzcG9uc2UsIHN0YXR1cykge1xuICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPT0gZ29vZ2xlLm1hcHMuRGlyZWN0aW9uc1N0YXR1cy5PSykge1xuICAgICAgICAgICAgICAgICAgICAvL2RvIHdvcmsgd2l0aCByZXNwb25zZSBkYXRhXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKVxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3BvbnNlKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgLy9FcnJvciBoYXMgb2NjdXJlZFxuICAgICAgICAgICAgICAgICAgICBlcnJvcihyZXNwb25zZSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuXG4gICAgICAgIH1cbiAgICB9XG59Il19
