var gulp = require('gulp')
var browserify = require('browserify')
var watchify = require('watchify')
var reactify = require('reactify')
var uglify = require('gulp-uglify')
var source = require('vinyl-source-stream')
var watchLess = require('gulp-watch-less')
var less = require('gulp-less')
var prefix = require('gulp-autoprefixer')
var factor = require("factor-bundle")
var watch = require("gulp-watch")
var base_dir = "./"
var moreLess = require("gulp-more-less")
var debug = require('gulp-debug');
var fs = require('fs')
var through = require("through2")
var mkdirp = require("mkdirp")
var child_process = require("child_process")

/**
 *
 * Property of params:
 * _ base_dir (required)
 * _ fname (required)
 * _ build (required)
 * _ dist (optional. if specified, uglify-js will be executed)
 */
function watch_browserify(params) {
    var bundler = browserify({
        entries: [params.fname.path], // Only need initial file, browserify finds the deps // nested array so watchify will return only the element

        transform: [reactify], // compile jsx
        debug: true, // Gives us sourcemapping
        cache: {}, packageCache: {}, fullPaths: true // watchify default properties
    });
    var watcher = watchify(bundler)

    function rebundle() {
        return watcher.bundle().pipe(source(params.fname.relative)).pipe(gulp.dest("./www/js/"))
    }

    watcher.on('update', function (ids) {
        console.log("updating " + ids)
        rebundle()
    })

    return rebundle()
}

/**
 * No watchify
 */
function browserify_bundle(params, common) {
    if (typeof(params.entries) == "object") {
        var inputFiles = params.entries.map(function (f) {
            return params.base_dir + params.src + f
        })

        var outputDevFiles = params.entries.map(function (f) {
            return params.base_dir + params.build + f
        })
    }
    else {
        inputFiles = params.base_dir + params.src + params.entries

        outputDevFiles = params.base_dir + params.build + params.entries
    }
    //if target build folder does not exist, create it
    if (!fs.existsSync(params.base_dir + params.build)) {
        mkdirp.sync(params.base_dir + params.build)
    }
    var bundler = browserify({
        entries: inputFiles, // Only need initial file, browserify finds the deps // nested array so watchify will return only the element

        transform: [reactify], // compile jsx
        debug: true, // Gives us sourcemapping
        cache: {}, packageCache: {}, fullPaths: true // watchify default properties
    });
    if (common) {
        var opts = {
            o: outputDevFiles
        }
        return bundler.plugin(factor, opts).bundle().pipe(source(params.common)).pipe(gulp.dest(params.base_dir + params.build))
    } else {
        return bundler.bundle().pipe(source(params.out)).pipe(gulp.dest(params.base_dir + params.build))
    }
}

gulp.task('watch_src', function () {
    return gulp.src(base_dir + "src/js/*.js")
        .pipe(watch(base_dir + "src/js/*.js", function (vinyl) {
            if (vinyl.event == "added" || vinyl.event == undefined) {
                return watch_browserify({
                    fname: vinyl,
                    base_dir: base_dir,
                    src: "src/"
                })
            }
            else {
                console.log(vinyl.relative + " " + vinyl.event)
            }
        }))
});

gulp.task('watch_build_less', function () {
    return gulp.src('./src/css/main.less')
        .pipe(watchLess('./src/css/main.less', function (vinyl, done) {
            // vinyl is actually StreamArray. Watch less documentation is wrong.
            //Watch less only notifies which dependencies is changed and does not say which main file it belongs to.
            // rebuild all less files when there is any change in any dependencies.
            var st = gulp.src('./src/css/main.less').pipe(moreLess({less: true})).pipe(gulp.dest("./www/css"))
            st.on('end', function () {
                done()
            })
        })).pipe(through.obj(function (file, enc, cb) {
            var st = gulp.src(file.path).pipe(moreLess({less: true})).pipe(gulp.dest("./www/css"))
            st.on('end', function () {
                cb()
            })
        }))
})


gulp.task("auto_build", function() {
    return gulp.src(base_dir + "www/**/*.*")
        .pipe(watch(base_dir + "www/**/*.*", function (vinyl) {
            console.log("building cordova")
            if(vinyl.event !== undefined) {
                var cordova = child_process.spawn("./cordova_build.sh")

                cordova.stderr.on("data", function (data) {
                    console.error(data.toString("utf8"))
                })
                cordova.on('close', function() {
                    console.log("finished building cordova")
                })
            }
        }))
})

gulp.task('watch_test', ['watch_src'], function () {
    var bundler = browserify({
        entries: ["./tests/src/test.js"],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    });
    var watcher = watchify(bundler)

    function bundle() {
        return watcher.bundle()
            .pipe(source("test.js")).pipe(gulp.dest("./tests/build"))
    }

    watcher.on('update', function (ids) {
        console.log("updating " + ids)
        bundle()
    })
    return bundle()
})

/**
 *Tasks for deployment
 */
gulp.task('build_css', function () {
    return gulp.src("./src/css/main.less").pipe(moreLess({less: true})).pipe(gulp.dest("./www/css/"))
})

gulp.task("build_js_common", function () {
    var dir = __dirname + "/src/"
    var files = fs.readdirSync(dir)
    var to_build = []
    files.forEach(function (file) {
        var nFile = dir + "/" + file
        var stats = fs.statSync(nFile)
        if (stats.isFile() && file !== "external.js" && file !== "sidebar.js") {
            to_build.push(file)
        }
    })

    return browserify_bundle({
        entries: to_build,
        base_dir: base_dir,
        src: "src/js/",
        build: "www/js/",
        common: "common.js"
    }, true)
})

gulp.task("build_js_external", function () {
    return browserify_bundle({
        entries: "external.js",
        base_dir: base_dir,
        src: "src/js/",
        build: "www/js/",
        out: "external.js"
    }, false)
})

/**
 * group task together
 */
gulp.task('default', ['watch_src', 'watch_build_less', 'watch_test', 'auto_build'])


/***
 * build and uglify
 * run build step first then uglifying all the files.
 */
gulp.task("build", ['build_js_common', 'build_js_external', 'build_css'], function () {
    return gulp.src(base_dir + "./www/js").pipe(uglify({compress: {negate_iife: false}})).pipe(gulp.dest(base_dir + "./www/js/"))
})