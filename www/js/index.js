(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"/home/duongnhu/workspace/fe/src/js/components/FindBikePod.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            fountains: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    findDonations: function () {
        var parent = this
        var value = parent.refs.location.getValue()
        actions.findDonations(value.address, value.radius)

        if (window.device.platform == "browser") {
            find()
            $(this.refs.findBtn.getDOMNode()).fadeOut(300, function () {
                parent.setState({
                    currentState: parent.CurrentState.FINDING
                })
            })
        } else {
            parent.setState({
                currentState: parent.CurrentState.FINDING
            })
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_POD) {
                if (storeState.success) {
                    this.setState({fountains: storeState.fountains}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.getMarkers(coords, this.state.fountains, this.refs.googleMap.getDOMNode())
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        navigator.geolocation.getCurrentPosition(function (position) {
            var find = null
            if (parent.isMounted()) {
                var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                window.coords = coords
                parent.setState({coords: coords}, function () {
                    actions.findDonations(coords, rad)
                })
            } else {
                find = null
            }
            //parent.drawMap(coords)
            /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
             function (address) {
             parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
             }, function (error) {
             alert(error)
             })*/
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, {enableHighAccuracy: true})
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            this.setUpComponent()
        }
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "s12", id: "search-wrapper"}, 
                    React.createElement("div", {className: "col s12", id: "google-map", ref: "googleMap"})
                )
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                React.createElement(SpinnerWave, {text: "Finding Pods Requests ..."})
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                React.createElement(DonationResult, {donations: this.state.donations})
            )
        }
    }
})

},{"./buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./find_bikepod/DonationResult":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js","./find_bikepod/LocationSetting":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js","./find_bikepod/MapMarker":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/PostRequest.js":[function(require,module,exports){
/**
 * Created by duognnhu on 27/09/15.
 */
var LocationInput = require("./find_bikepod/LocationInput")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
module.exports = React.createClass({displayName: "exports",
     mixins: [flux.mixins.storeListener],

    watchStores: ['PostRequestStore'],


    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            bloodType: "",
            time: "",
            currentAddress: "",
            organizationName: ""
        }
    },

     CurrentState: {
        POSTING: "posting",
        INITIAL: "initial",
        READY: 'READY'
    },

    storeDidChange: function (storeName) {
        console.log(storeName)

        if (this.isMounted()) {
            var store = dispatcher.getStore(storeName)
            var storeState = store.getState()
            if (storeState.success) {
                alert("Posted Successfully")
            } else {
                alert("Error Posting!")
            }
        }
    },

    setUpComponent: function () {
        console.log(this.state.currentState)
        var parent = this
        navigator.geolocation.getCurrentPosition(function (position) {
            googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                function (address) {
                    if (parent.isMounted()) {
                        parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                    }
                }, function (error) {
                    alert(error)
                })
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, { enableHighAccuracy: true })
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    onBloodTypeChange: function (e) {
        this.setState({bloodType: e.target.value})
    },

    onTimeChange: function (e) {
        this.setState({time: e.target.value})
    },

    onNameChange: function (e) {
        this.setState({organizationName: e.target.value})
    },

    postRequest: function () {
        var opts = {
            bloodType: this.state.bloodType,
            address: this.refs.address.getValue(),
            time: this.state.time,
            organizationName: this.state.organizationName
        }
        this.setState({currentState: this.CurrentState.POSTING })
        actions.postRequest(opts)
    },

    render: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "row card"}, 
                    React.createElement("h4", null, "Posting Request"), 
                    React.createElement("ul", {className: "collection"}, 
                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Organization Name:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onNameChange, ref: "name", type: "text", placeholder: "name"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/location_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Location:"), 
                            React.createElement(LocationInput, {initAddress: this.state.currentAddress, ref: "address"})
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Blood Type:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {ref: "bloodType", onChange: this.onBloodTypeChange, type: "text"})
                            )
                        ), 

                        React.createElement("li", {className: "collection-item avatar"}, 
                            React.createElement("img", {src: "img/bloodtype_logo.png", className: "circle"}), 
                            React.createElement("span", {className: "title"}, "Time:"), 
                            React.createElement("p", null, 
                                React.createElement("input", {onChange: this.onTimeChange, ref: "time", placeholder: "MM/dd/yyyy HH:mm:ss", type: "text"})
                            )
                        )
                    ), 
                    React.createElement("div", {className: "form-footer right"}, 
                        React.createElement("a", {href: "javascript:void(0)", onClick: this.postRequest, className: "waves-effect waves-green teal lighten-2 btn"}, 
                            "Save"
                        ), 
                        React.createElement("a", {href: "javascript:void(0)", className: "waves-effect waves-green blue-grey btn"}, 
                            "Cancel"
                        )
                    )
                )
            )
        } else if (this.state.currentState == this.CurrentState.POSTING) {
            return (
                React.createElement(SpinnerWave, {text: "Posting Donation Requests ..."})
            )
        }
    }
})
},{"./find_bikepod/LocationInput":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/Rewards.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            fountains: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    findDonations: function () {
        var parent = this
        var value = parent.refs.location.getValue()
        actions.findDonations(value.address, value.radius)

        if (window.device.platform == "browser") {
            find()
            $(this.refs.findBtn.getDOMNode()).fadeOut(300, function () {
                parent.setState({
                    currentState: parent.CurrentState.FINDING
                })
            })
        } else {
            parent.setState({
                currentState: parent.CurrentState.FINDING
            })
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_POD) {
                if (storeState.success) {
                    this.setState({fountains: storeState.fountains}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.getMarkers(coords, this.state.fountains, this.refs.googleMap.getDOMNode())
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        navigator.geolocation.getCurrentPosition(function (position) {
            var find = null
            if (parent.isMounted()) {
                var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                window.coords = coords
                parent.setState({coords: coords}, function () {
                    actions.findDonations(coords, rad)
                })
            } else {
                find = null
            }
            //parent.drawMap(coords)
            /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
             function (address) {
             parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
             }, function (error) {
             alert(error)
             })*/
        }, function (error) {
            if (parent.isMounted()) {
                alert(error.message)
            }
        }, {enableHighAccuracy: true})
    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            this.setUpComponent()
        }
    },

    render: function () {
       return (
           React.createElement("div", {className: "rewards"}, 
               React.createElement("img", {src: "https://s3-ap-southeast-2.amazonaws.com/imm-travel-app/iPhone+6+Copy+13+(1).png"})
           )
       )
    }
})

},{"./buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./find_bikepod/DonationResult":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js","./find_bikepod/LocationSetting":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js","./find_bikepod/MapMarker":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/SideNav.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    componentDidMount: function() {
        $("#menu-btn").sideNav({
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });
    },

    render: function () {
        /**var links = [
            <li key={1}>
                <a href="#/find_donation" className="waves-effect waves-light">
                    <i className="ion-android-search"></i>
                    Find Donation Requests
                </a>
            </li>,
            <li key={2} className="divider"></li>,
            <li key={3}>
                <a href="#/post_request" className="waves-effect waves-light">
                    <i className="ion-android-unlock"></i>
                    Post Request
                </a>
            </li>,
        ]**/
        var links = []
        return (
            React.createElement("div", null, 
                React.createElement("a", {href: "javascript:void(0)", className: "brand-logo center"}, config.app_name), 
                /*<a href="javascript:void(0)" className="brand-logo right" id="logo"><img src="img/app-logo-96.png"/></a>*/
                React.createElement("ul", {className: "left hide-on-med-and-down"}, 
                    links
                ), 
                React.createElement("ul", {id: "slide-out", className: "side-nav"}, 
                    links
                ), 
                  React.createElement("a", {href: "#", "data-activates": "slide-out", className: "button-collapse", id: "menu-btn"}, React.createElement("i", {className: "mdi-navigation-menu"}))
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/WhereToGo.js":[function(require,module,exports){
/**
 * Created by duongnhu on 10/05/15.
 */
var Slider = require("./buttons/Slider")
var SpinnerWave = require("./loading/SpinnerWave")
var CubeGrid = require("./loading/ThreeBounce")
var DonationResult = require("./find_bikepod/DonationResult")
var LocationSetting = require("./find_bikepod/LocationSetting")
var MapMarker = require("./find_bikepod/MapMarker")
module.exports = React.createClass({displayName: "exports",
    mixins: [flux.mixins.storeListener],

    watchStores: ['LocationStore'],

    CurrentState: {
        FINDING: "finding",
        INITIAL: "initial",
        READY: 'READY',
        RESULT: "result"
    },

    getInitialState: function () {
        return {
            currentState: (this.props.currentState) ? this.props.currentState : this.CurrentState.INITIAL,
            fountains: [],
            merchants: [],
            coords: {}
        }
    },


    componentWillReceiveProps: function (nextProps) {
        if (nextProps.currentState) {
            this.setState({currentState: nextProps.currentState})
        }
    },

    storeDidChange: function (name) {
        if (this.isMounted()) {
            var store = dispatcher.getStore(name)
            var storeState = store.getState()


            if (storeState.action == store.actions.FIND_FOUNTAIN) {
                if (storeState.success) {
                    this.setState({fountains: storeState.fountains}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            } else if (storeState.action == store.actions.FIND_MERCHANT){
                 if (storeState.success) {
                    this.setState({merchants: storeState.merchants}, function () {
                        this.drawMap(this.state.coords)
                    })
                } else {
                    navigator.notification.alert(storeState.errorMessage)
                    this.setState({currentState: this.CurrentState.READY})
                }
            }
        }
    },

    drawMap: function (coords) {
        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.initSearch(coords, this.refs.googleMap.getDOMNode(), {
                "items": this.state.fountains,
                "icon": config.fountain_icon
            })
        })

        this.setState({currentState: this.CurrentState.READY}, function () {
            MapMarker.initSearch(coords, this.refs.googleMap.getDOMNode(), {
                "items": this.state.merchants,
                "icon": config.retail_icon
            })
        })
    },

    setUpComponent: function (radius) {
        var parent = this
        var rad = (radius) ? radius : 5
        if (!window.coords) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var find = null
                if (parent.isMounted()) {
                    var coords = {lat: position.coords.latitude, lng: position.coords.longitude}
                    parent.setState({coords: coords}, function () {
                        actions.findDrinkingFountain(coords, rad)
                    })
                } else {
                    find = null
                }
                //parent.drawMap(coords)
                /*googleUtils.maps.reverseGeocoding(position.coords.latitude, position.coords.longitude,
                 function (address) {
                 parent.setState({currentAddress: address, currentState: parent.CurrentState.READY})
                 }, function (error) {
                 alert(error)
                 })*/
            }, function (error) {
                if (parent.isMounted()) {
                    alert(error.message)
                }
            }, {enableHighAccuracy: true})
        } else {
            parent.setState({coords: coords}, function () {
                actions.findDrinkingFountain(window.coords, rad)
                actions.findMerchants(window.coords, rad)
            })
        }

    },

    componentDidMount: function () {
        this.setUpComponent()
    },

    componentDidUpdate: function () {
        if (this.state.currentState == this.CurrentState.INITIAL) {
            //this.setUpComponent()
        }
    },

    render: function () {
        console.log(this.state.currentState)
        if (this.state.currentState == this.CurrentState.INITIAL) {
            return (React.createElement(CubeGrid, {text: "Loading ..."}))
        } else if (this.state.currentState == this.CurrentState.READY) {
            return (
                React.createElement("div", {className: "s12", id: "search-wrapper"}, 
                    React.createElement("nav", null, 
                        React.createElement("div", {className: "nav-wrapper to-go-nav"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("h5", null, "Where to go next?")
                                ), 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("div", {className: "input-field"}, 
                                        React.createElement("input", {value: "Current Location", type: "search"}), 
                                        React.createElement("label", null, React.createElement("i", {className: ""}, "Start"))
                                    )
                                ), 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("div", {className: "input-field"}, 
                                        React.createElement("input", {id: "place-search", type: "search"}), 
                                        React.createElement("label", null, React.createElement("i", {className: ""}, "End"))
                                    )
                                ), 
                                React.createElement("div", {className: "col s11"}, 
                                    React.createElement("a", {href: "#/rewards", className: "btn btn-primary red", style: {marginBottom: "10px"}}, 
                                        "Go"
                                    )
                                )
                            )
                        )
                    ), 
                    React.createElement("div", {className: "col s12", id: "google-map", ref: "googleMap"})
                )
            )
        } else if (this.state.currentState == this.CurrentState.FINDING) {
            return (
                React.createElement(SpinnerWave, {text: "Finding Pods Requests ..."})
            )
        } else if (this.state.currentState = this.CurrentState.RESULT) {
            return (
                React.createElement(DonationResult, {donations: this.state.donations})
            )
        }
    }
})

},{"./buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./find_bikepod/DonationResult":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js","./find_bikepod/LocationSetting":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js","./find_bikepod/MapMarker":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js","./loading/SpinnerWave":"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js","./loading/ThreeBounce":"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js"}],"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            value: (this.props.value)? this.props.value : 0
        }
    },

    componentDidMount: function() {
        this.activateSlider()
    },

    setValue: function(value) {
        this.setState({value: value})
    },

    activateSlider:  function() {
        var min = (this.props.min)? this.props.min : 0
        var max = (this.props.max)? this.props.max : 50
        var node = $(this.refs.slider.getDOMNode())
        node.noUiSlider({
            start: this.state.value,
            connect: "lower",
            orientation: "horizontal",
            direction: "ltr",
            behaviour: "tap-drag",
            range: {
                'min': min,
                'max': max
            },
            format: wNumb({
                decimals: 0
            })
        });
        var parent = this
        node.on('slide', function() {
            parent.setValue($(this).val())
        })
	node.on('change', function () {
	    if (parent.props.sliderRelease) {
		parent.props.sliderRelease($(this).val())
	    }
	})
    },

    render: function () {
        var unit = (this.props.unit)? " " +this.props.unit : ""
        return (
            React.createElement("div", {className: "col s12 slider-container"}, 
                React.createElement("div", {className: "slider col s10", ref: "slider"}), React.createElement("div", {className: "col s2 slider-value"}, this.state.value + unit)
            )
        )
    }
})

},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationCard.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

module.exports = React.createClass({displayName: "exports",

    render: function () {
        return (
            React.createElement("div", {className: "col s12 m8 offset-m2 l6 offset-l3"}, 
                React.createElement("div", {className: "card-panel grey lighten-5 z-depth-1"}, 
                    React.createElement("div", {className: "row valign-wrapper"}, 
                        React.createElement("div", {className: "col s2"}, 
                            React.createElement("img", {src: this.props.donation.profileImg, alt: "Profile Picture", className: "circle responsive-img"})
                        ), 
                        React.createElement("div", {className: "col s10"}, 
                            React.createElement("p", null), 
                            React.createElement("p", {className: "black-text"}, 
                                this.props.donation.description
                            )
                        )
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationResult.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */

var DonationCard = require("./DonationCard")

module.exports = React.createClass({displayName: "exports",

    getInitialState: function() {
        return {
            donations: (this.props.donations)? this.props.donations : []
        }
    },

    componentWillReceiveProps: function(nextProps) {
        if (nextProps.donations && JSON.stringify(nextProps.donations) != JSON.stringify(this.props.donations)) {
            this.setState({donations: nextProps.donations})
        }
    },

    render: function() {
        if (this.state.donations.length == 0) {
            return (
                React.createElement("h3", null, 
                    "No donation requests are found around your location!"
                )
            )
        } else {
            return (
                React.createElement("div", {className: "busker-list"}, 
                    
                        this.state.donations.map(function(item, id) {
                            return (
                                React.createElement(DonationCard, {donation: item, key: id})
                            )
                        })
                    
                )
            )
        }
    }
})
},{"./DonationCard":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/DonationCard.js"}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",

    getInitialState: function () {
        return {
            searchBox: undefined
        }
    },

    componentDidMount: function () {
        this.setState({searchBox: new google.maps.places.SearchBox(this.refs.input.getDOMNode())})
    },

    getValue: function () {
        return this.refs.input.getDOMNode().value
    },

    render: function () {
        return (
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "input-field col s12"}, 
                    React.createElement("input", {type: "text", placeholder: "Choose a location", className: "validate", ref: "input", defaultValue: this.props.initAddress})
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationSetting.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
var Slider = require("../buttons/Slider")
var LocationInput = require("./LocationInput")

module.exports = React.createClass({displayName: "exports",

    componentWillUpdate: function (nextProps) {
        if (nextProps.initAddress && nextProps.initAddress != this.props.initAddress) {
            return true
        }
        return false
    },

    getValue: function () {
        return {
            address: this.refs.address.getValue(),
            radius: this.refs.radius.state.value
        }
    },

    render: function () {
        return (
            React.createElement("div", {className: "row", id: "location-setting"}, 
                React.createElement("form", {className: "col s12"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement(LocationInput, {initAddress: this.props.initAddress, ref: "address"}), 
                        React.createElement("div", {className: "col s12 label"}, "Radius"), 
                        React.createElement(Slider, {ref: "radius", value: 10, unit: "km"})
                    )
                )
            )
        )
    }
})
},{"../buttons/Slider":"/home/duongnhu/workspace/fe/src/js/components/buttons/Slider.js","./LocationInput":"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/LocationInput.js"}],"/home/duongnhu/workspace/fe/src/js/components/find_bikepod/MapMarker.js":[function(require,module,exports){
/**
 * Created by duognnhu on 26/09/15.
 */
var gApi = require("../../utils/google")
module.exports = {
    getMarkers: function (currentLocation, pods, dom, radius, icon) {
        if (!window.map) {
            window.map = new google.maps.Map(dom, {
                center: currentLocation,
                zoom: config.googleMap.zoom,
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: false
            });
        }
        google.maps.visualRefresh = true;
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here',
            icon: config.user_current_location_icon
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })

        pods.forEach(function (pod, id) {
            var coordinates = pod.coordinates.coordinates
            var marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(coordinates["lat"]),
                    lng: parseFloat(coordinates["lon"])
                },
                icon: (icon) ? icon : config.bike_icon
            })
            marker.addListener('click', function () {
                window.popupPodDetails(pod)
            })
            marker.setMap(map)
        })
        new google.maps.Circle({
            strokeColor: 'green',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.0,
            map: map,
            center: currentLocation,
            radius: 1000
        });
    },

    initSearch: function (currentLocation, dom, data) {
        var markers = []
        if (!window.map) {
            window.map = new google.maps.Map(dom, {
                center: currentLocation,
                zoom: config.googleMap.zoom,
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: false
            });
        }
        google.maps.visualRefresh = true;
        var currentLocationMarker = new google.maps.Marker({
            position: currentLocation,
            title: 'You are here',
            icon: config.user_current_location_icon
        });
        currentLocationMarker.setMap(map);
        var currentLocationInfoWindow = new google.maps.InfoWindow({
            content: "<p>You are here!</p>"
        });
        currentLocationMarker.addListener('click', function () {
            currentLocationInfoWindow.open(map, currentLocationMarker);
        })

        data.items.forEach(function (point) {
            var coordinates = point.coordinates.coordinates
            var mOpts = {
                position: {
                    lat: parseFloat(coordinates["lat"]),
                    lng: parseFloat(coordinates["lon"])
                }
            }
            if (data.icon) {
                mOpts["icon"] = data.icon

            }
            var marker = new google.maps.Marker(mOpts)
            marker.setMap(map)
            markers.push(marker)
        })
        new google.maps.Circle({
            strokeColor: 'green',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.0,
            map: map,
            center: currentLocation,
            radius: 1000
        });
        var searchPlacesMarkers = []
        if (document.getElementById('place-search')) {
            // Create the search box and link it to the UI element.
            var input = document.getElementById('place-search');
            var searchBox = new google.maps.places.SearchBox(input);
            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var directionsDisplay = new google.maps.DirectionsRenderer({
                preserveViewport: true,
            });
            directionsDisplay.setMap(map)
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                searchPlacesMarkers.forEach(function (marker) {
                    marker.setMap(null)
                })
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    var marker = new google.maps.Marker({
                        map: map,
                        title: place.name,
                        position: place.geometry.location
                    })
                    searchPlacesMarkers.push(marker)
                    var mLocationInfo = new google.maps.InfoWindow({
                        content: "<p>" + place.name + "</p>"
                    });
                    marker.addListener('click', function () {
                        mLocationInfo.open(map, marker);
                    })

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }


                    gApi.maps.getDir(currentLocation, place.geometry.location, function (direction) {
                        directionsDisplay.setDirections(direction)
                    });
                })
                currentLocationMarker.setMap(null)
                map.fitBounds(bounds);

            });
        }
    }
}

},{"../../utils/google":"/home/duongnhu/workspace/fe/src/js/utils/google.js"}],"/home/duongnhu/workspace/fe/src/js/components/loading/SpinnerWave.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function() {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-wave"}, 
                        React.createElement("div", {className: "sk-rect1"}), 
                        React.createElement("div", {className: "sk-rect2"}), 
                        React.createElement("div", {className: "sk-rect3"}), 
                        React.createElement("div", {className: "sk-rect4"}), 
                        React.createElement("div", {className: "sk-rect5"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/components/loading/ThreeBounce.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = React.createClass({displayName: "exports",
    render: function () {
        return (
            React.createElement("div", {className: "spinner-wrapper"}, 
                React.createElement("div", {className: "spinner-cell"}, 
                    React.createElement("div", {className: "sk-spinner sk-spinner-three-bounce"}, 
                        React.createElement("div", {className: "sk-bounce1"}), 
                        React.createElement("div", {className: "sk-bounce2"}), 
                        React.createElement("div", {className: "sk-bounce3"}), 
                        React.createElement("p", null, this.props.text)
                    )
                )
            )
        )
    }
})
},{}],"/home/duongnhu/workspace/fe/src/js/index.js":[function(require,module,exports){
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var router = require("./router/router")

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //app.receivedEvent('deviceready');
        $(document).ready(function() {
            router.init("/find_donation")
            setTimeout(function() {
                $(".splash-screen").fadeOut(500)
            }, 3000)
            document.addEventListener("backbutton", onBackKeyDown, false);
            function onBackKeyDown() {
                React.unmountComponentAtNode(document.getElementById("pod-details"))
            }
        })
    }
};

app.initialize();

},{"./router/router":"/home/duongnhu/workspace/fe/src/js/router/router.js"}],"/home/duongnhu/workspace/fe/src/js/router/router.js":[function(require,module,exports){
/**
 * Created by duongnhu on 20/05/15.
 */
var FindBikePod = require("../components/FindBikePod.js")
var PostRequest = require("../components/PostRequest")
var WhereToGo = require("../components/WhereToGo")
var SideNav = require("../components/SideNav")
var Rewards = require("../components/Rewards")

window.renderFindDonation = function () {
    window.map = null
    React.unmountComponentAtNode(document.getElementById("main-content"))
    $("#nav-bar").show()
        $(".modal").hide()

    React.render(React.createElement(SideNav, {dispatcher: dispatcher}), document.getElementById("nav-bar"))
    if (document.getElementById("pod-details")) {
        React.unmountComponentAtNode(document.getElementById("pod-details"))
    }
    React.render(React.createElement(FindBikePod, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

window.renderWhereGo = function () {
    window.map = null
    $("#nav-bar").hide()
    $(".modal").hide()
    $(".lean-overlay").hide()
    $("#lean-overlay").hide()

    React.unmountComponentAtNode(document.getElementById("main-content"))
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(React.createElement(WhereToGo, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

window.rewards = function () {
    $("#pes-container").hide()
     React.unmountComponentAtNode(document.getElementById("main-content"))
    React.unmountComponentAtNode(document.getElementById("pod-details"))
    React.render(React.createElement(Rewards, {currentState: "initial", dispatcher: dispatcher}), document.getElementById("main-content"))
}

module.exports = {
    init: function(path) {
        var routes = {
            "/find_donation": renderFindDonation,
            "/where-go": renderWhereGo,
            "/rewards": rewards
        }
        window.router = Router(routes);
        router.init(path)
    }
}
},{"../components/FindBikePod.js":"/home/duongnhu/workspace/fe/src/js/components/FindBikePod.js","../components/PostRequest":"/home/duongnhu/workspace/fe/src/js/components/PostRequest.js","../components/Rewards":"/home/duongnhu/workspace/fe/src/js/components/Rewards.js","../components/SideNav":"/home/duongnhu/workspace/fe/src/js/components/SideNav.js","../components/WhereToGo":"/home/duongnhu/workspace/fe/src/js/components/WhereToGo.js"}],"/home/duongnhu/workspace/fe/src/js/utils/google.js":[function(require,module,exports){
/**
 * Created by duongnhu on 23/05/15.
 */
module.exports = {
    maps: {
        checkConnection: function () {
            var networkState = navigator.connection.type
            console.log(networkState)
            if (networkState == Connection.NONE) {
                return false
            }
            return true
        },

        reverseGeocoding: function (lat, lng, success, error) {
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        success(results[0].formatted_address)
                    } else {
                        error('No results found');
                    }
                } else {
                    error('Geocoder failed due to: ' + status);
                }
            });
        },

        geocoding: function (address, success, error) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        success(results[0].geometry)
                    } else {
                        error('No results found');
                    }
                } else {
                    error('Geocoder failed due to: ' + status);
                }
            });
        },

        getDir: function (origin, destination, success) {
            var directionsService = new google.maps.DirectionsService();
            var directionsRequest = {
                "origin": origin["lat"] + "," + origin["lng"],
                "destination": origin["lat"] + "," + origin["lng"],
                optimizeWaypoints: true,
                travelMode: 'BICYCLING'
            };
            directionsService.route(directionsRequest, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    //do work with response data
                    console.log(response)
                    success(response)
                }
                else {
                    //Error has occured
                    error(response)
                }
            })

        }
    }
}
},{}]},{},["/home/duongnhu/workspace/fe/src/js/index.js"])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvY29tcG9uZW50cy9GaW5kQmlrZVBvZC5qcyIsInNyYy9qcy9jb21wb25lbnRzL1Bvc3RSZXF1ZXN0LmpzIiwic3JjL2pzL2NvbXBvbmVudHMvUmV3YXJkcy5qcyIsInNyYy9qcy9jb21wb25lbnRzL1NpZGVOYXYuanMiLCJzcmMvanMvY29tcG9uZW50cy9XaGVyZVRvR28uanMiLCJzcmMvanMvY29tcG9uZW50cy9idXR0b25zL1NsaWRlci5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfYmlrZXBvZC9Eb25hdGlvbkNhcmQuanMiLCJzcmMvanMvY29tcG9uZW50cy9maW5kX2Jpa2Vwb2QvRG9uYXRpb25SZXN1bHQuanMiLCJzcmMvanMvY29tcG9uZW50cy9maW5kX2Jpa2Vwb2QvTG9jYXRpb25JbnB1dC5qcyIsInNyYy9qcy9jb21wb25lbnRzL2ZpbmRfYmlrZXBvZC9Mb2NhdGlvblNldHRpbmcuanMiLCJzcmMvanMvY29tcG9uZW50cy9maW5kX2Jpa2Vwb2QvTWFwTWFya2VyLmpzIiwic3JjL2pzL2NvbXBvbmVudHMvbG9hZGluZy9TcGlubmVyV2F2ZS5qcyIsInNyYy9qcy9jb21wb25lbnRzL2xvYWRpbmcvVGhyZWVCb3VuY2UuanMiLCJzcmMvanMvaW5kZXguanMiLCJzcmMvanMvcm91dGVyL3JvdXRlci5qcyIsInNyYy9qcy91dGlscy9nb29nbGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMxSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDOUhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDN0tBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDNUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25EQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAxMC8wNS8xNS5cbiAqL1xudmFyIFNsaWRlciA9IHJlcXVpcmUoXCIuL2J1dHRvbnMvU2xpZGVyXCIpXG52YXIgU3Bpbm5lcldhdmUgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1NwaW5uZXJXYXZlXCIpXG52YXIgQ3ViZUdyaWQgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1RocmVlQm91bmNlXCIpXG52YXIgRG9uYXRpb25SZXN1bHQgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvRG9uYXRpb25SZXN1bHRcIilcbnZhciBMb2NhdGlvblNldHRpbmcgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvTG9jYXRpb25TZXR0aW5nXCIpXG52YXIgTWFwTWFya2VyID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL01hcE1hcmtlclwiKVxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuICAgIG1peGluczogW2ZsdXgubWl4aW5zLnN0b3JlTGlzdGVuZXJdLFxuXG4gICAgd2F0Y2hTdG9yZXM6IFsnTG9jYXRpb25TdG9yZSddLFxuXG4gICAgQ3VycmVudFN0YXRlOiB7XG4gICAgICAgIEZJTkRJTkc6IFwiZmluZGluZ1wiLFxuICAgICAgICBJTklUSUFMOiBcImluaXRpYWxcIixcbiAgICAgICAgUkVBRFk6ICdSRUFEWScsXG4gICAgICAgIFJFU1VMVDogXCJyZXN1bHRcIlxuICAgIH0sXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogKHRoaXMucHJvcHMuY3VycmVudFN0YXRlKSA/IHRoaXMucHJvcHMuY3VycmVudFN0YXRlIDogdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCxcbiAgICAgICAgICAgIGZvdW50YWluczogW10sXG4gICAgICAgICAgICBjb29yZHM6IHt9XG4gICAgICAgIH1cbiAgICB9LFxuXG5cbiAgICBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzOiBmdW5jdGlvbiAobmV4dFByb3BzKSB7XG4gICAgICAgIGlmIChuZXh0UHJvcHMuY3VycmVudFN0YXRlKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IG5leHRQcm9wcy5jdXJyZW50U3RhdGV9KVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGZpbmREb25hdGlvbnM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgdmFyIHZhbHVlID0gcGFyZW50LnJlZnMubG9jYXRpb24uZ2V0VmFsdWUoKVxuICAgICAgICBhY3Rpb25zLmZpbmREb25hdGlvbnModmFsdWUuYWRkcmVzcywgdmFsdWUucmFkaXVzKVxuXG4gICAgICAgIGlmICh3aW5kb3cuZGV2aWNlLnBsYXRmb3JtID09IFwiYnJvd3NlclwiKSB7XG4gICAgICAgICAgICBmaW5kKClcbiAgICAgICAgICAgICQodGhpcy5yZWZzLmZpbmRCdG4uZ2V0RE9NTm9kZSgpKS5mYWRlT3V0KDMwMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5GSU5ESU5HXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogcGFyZW50LkN1cnJlbnRTdGF0ZS5GSU5ESU5HXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHN0b3JlRGlkQ2hhbmdlOiBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICBpZiAodGhpcy5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgdmFyIHN0b3JlID0gZGlzcGF0Y2hlci5nZXRTdG9yZShuYW1lKVxuICAgICAgICAgICAgdmFyIHN0b3JlU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpXG5cblxuICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuYWN0aW9uID09IHN0b3JlLmFjdGlvbnMuRklORF9QT0QpIHtcbiAgICAgICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2ZvdW50YWluczogc3RvcmVTdGF0ZS5mb3VudGFpbnN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRyYXdNYXAodGhpcy5zdGF0ZS5jb29yZHMpXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbmF2aWdhdG9yLm5vdGlmaWNhdGlvbi5hbGVydChzdG9yZVN0YXRlLmVycm9yTWVzc2FnZSlcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWX0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGRyYXdNYXA6IGZ1bmN0aW9uIChjb29yZHMpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWX0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIE1hcE1hcmtlci5nZXRNYXJrZXJzKGNvb3JkcywgdGhpcy5zdGF0ZS5mb3VudGFpbnMsIHRoaXMucmVmcy5nb29nbGVNYXAuZ2V0RE9NTm9kZSgpKVxuICAgICAgICB9KVxuICAgIH0sXG5cbiAgICBzZXRVcENvbXBvbmVudDogZnVuY3Rpb24gKHJhZGl1cykge1xuICAgICAgICB2YXIgcGFyZW50ID0gdGhpc1xuICAgICAgICB2YXIgcmFkID0gKHJhZGl1cykgPyByYWRpdXMgOiA1XG4gICAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgICAgICAgICB2YXIgZmluZCA9IG51bGxcbiAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICB2YXIgY29vcmRzID0ge2xhdDogcG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBsbmc6IHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGV9XG4gICAgICAgICAgICAgICAgd2luZG93LmNvb3JkcyA9IGNvb3Jkc1xuICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7Y29vcmRzOiBjb29yZHN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbnMuZmluZERvbmF0aW9ucyhjb29yZHMsIHJhZClcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBmaW5kID0gbnVsbFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy9wYXJlbnQuZHJhd01hcChjb29yZHMpXG4gICAgICAgICAgICAvKmdvb2dsZVV0aWxzLm1hcHMucmV2ZXJzZUdlb2NvZGluZyhwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUsIHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGUsXG4gICAgICAgICAgICAgZnVuY3Rpb24gKGFkZHJlc3MpIHtcbiAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe2N1cnJlbnRBZGRyZXNzOiBhZGRyZXNzLCBjdXJyZW50U3RhdGU6IHBhcmVudC5DdXJyZW50U3RhdGUuUkVBRFl9KVxuICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgIGFsZXJ0KGVycm9yKVxuICAgICAgICAgICAgIH0pKi9cbiAgICAgICAgfSwgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICBpZiAocGFyZW50LmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgYWxlcnQoZXJyb3IubWVzc2FnZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge2VuYWJsZUhpZ2hBY2N1cmFjeTogdHJ1ZX0pXG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0VXBDb21wb25lbnQoKVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRVcGRhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0VXBDb21wb25lbnQoKVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCkge1xuICAgICAgICAgICAgcmV0dXJuIChSZWFjdC5jcmVhdGVFbGVtZW50KEN1YmVHcmlkLCB7dGV4dDogXCJMb2FkaW5nIC4uLlwifSkpXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuUkVBRFkpIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInMxMlwiLCBpZDogXCJzZWFyY2gtd3JhcHBlclwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczEyXCIsIGlkOiBcImdvb2dsZS1tYXBcIiwgcmVmOiBcImdvb2dsZU1hcFwifSlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuRklORElORykge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFNwaW5uZXJXYXZlLCB7dGV4dDogXCJGaW5kaW5nIFBvZHMgUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID0gdGhpcy5DdXJyZW50U3RhdGUuUkVTVUxUKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRG9uYXRpb25SZXN1bHQsIHtkb25hdGlvbnM6IHRoaXMuc3RhdGUuZG9uYXRpb25zfSlcbiAgICAgICAgICAgIClcbiAgICAgICAgfVxuICAgIH1cbn0pXG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvZ25uaHUgb24gMjcvMDkvMTUuXG4gKi9cbnZhciBMb2NhdGlvbklucHV0ID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL0xvY2F0aW9uSW5wdXRcIilcbnZhciBTcGlubmVyV2F2ZSA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvU3Bpbm5lcldhdmVcIilcbnZhciBDdWJlR3JpZCA9IHJlcXVpcmUoXCIuL2xvYWRpbmcvVGhyZWVCb3VuY2VcIilcbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICAgbWl4aW5zOiBbZmx1eC5taXhpbnMuc3RvcmVMaXN0ZW5lcl0sXG5cbiAgICB3YXRjaFN0b3JlczogWydQb3N0UmVxdWVzdFN0b3JlJ10sXG5cblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY3VycmVudFN0YXRlOiAodGhpcy5wcm9wcy5jdXJyZW50U3RhdGUpID8gdGhpcy5wcm9wcy5jdXJyZW50U3RhdGUgOiB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMLFxuICAgICAgICAgICAgYmxvb2RUeXBlOiBcIlwiLFxuICAgICAgICAgICAgdGltZTogXCJcIixcbiAgICAgICAgICAgIGN1cnJlbnRBZGRyZXNzOiBcIlwiLFxuICAgICAgICAgICAgb3JnYW5pemF0aW9uTmFtZTogXCJcIlxuICAgICAgICB9XG4gICAgfSxcblxuICAgICBDdXJyZW50U3RhdGU6IHtcbiAgICAgICAgUE9TVElORzogXCJwb3N0aW5nXCIsXG4gICAgICAgIElOSVRJQUw6IFwiaW5pdGlhbFwiLFxuICAgICAgICBSRUFEWTogJ1JFQURZJ1xuICAgIH0sXG5cbiAgICBzdG9yZURpZENoYW5nZTogZnVuY3Rpb24gKHN0b3JlTmFtZSkge1xuICAgICAgICBjb25zb2xlLmxvZyhzdG9yZU5hbWUpXG5cbiAgICAgICAgaWYgKHRoaXMuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgIHZhciBzdG9yZSA9IGRpc3BhdGNoZXIuZ2V0U3RvcmUoc3RvcmVOYW1lKVxuICAgICAgICAgICAgdmFyIHN0b3JlU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpXG4gICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgYWxlcnQoXCJQb3N0ZWQgU3VjY2Vzc2Z1bGx5XCIpXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGFsZXJ0KFwiRXJyb3IgUG9zdGluZyFcIilcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBzZXRVcENvbXBvbmVudDogZnVuY3Rpb24gKCkge1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSlcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgbmF2aWdhdG9yLmdlb2xvY2F0aW9uLmdldEN1cnJlbnRQb3NpdGlvbihmdW5jdGlvbiAocG9zaXRpb24pIHtcbiAgICAgICAgICAgIGdvb2dsZVV0aWxzLm1hcHMucmV2ZXJzZUdlb2NvZGluZyhwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUsIHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGUsXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKGFkZHJlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBhcmVudC5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjdXJyZW50QWRkcmVzczogYWRkcmVzcywgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBhbGVydChlcnJvcilcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgIGlmIChwYXJlbnQuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgICAgICBhbGVydChlcnJvci5tZXNzYWdlKVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7IGVuYWJsZUhpZ2hBY2N1cmFjeTogdHJ1ZSB9KVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICB9LFxuXG4gICAgb25CbG9vZFR5cGVDaGFuZ2U6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2Jsb29kVHlwZTogZS50YXJnZXQudmFsdWV9KVxuICAgIH0sXG5cbiAgICBvblRpbWVDaGFuZ2U6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3RpbWU6IGUudGFyZ2V0LnZhbHVlfSlcbiAgICB9LFxuXG4gICAgb25OYW1lQ2hhbmdlOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtvcmdhbml6YXRpb25OYW1lOiBlLnRhcmdldC52YWx1ZX0pXG4gICAgfSxcblxuICAgIHBvc3RSZXF1ZXN0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBvcHRzID0ge1xuICAgICAgICAgICAgYmxvb2RUeXBlOiB0aGlzLnN0YXRlLmJsb29kVHlwZSxcbiAgICAgICAgICAgIGFkZHJlc3M6IHRoaXMucmVmcy5hZGRyZXNzLmdldFZhbHVlKCksXG4gICAgICAgICAgICB0aW1lOiB0aGlzLnN0YXRlLnRpbWUsXG4gICAgICAgICAgICBvcmdhbml6YXRpb25OYW1lOiB0aGlzLnN0YXRlLm9yZ2FuaXphdGlvbk5hbWVcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IHRoaXMuQ3VycmVudFN0YXRlLlBPU1RJTkcgfSlcbiAgICAgICAgYWN0aW9ucy5wb3N0UmVxdWVzdChvcHRzKVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwpIHtcbiAgICAgICAgICAgIHJldHVybiAoUmVhY3QuY3JlYXRlRWxlbWVudChDdWJlR3JpZCwge3RleHQ6IFwiTG9hZGluZyAuLi5cIn0pKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3cgY2FyZFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJoNFwiLCBudWxsLCBcIlBvc3RpbmcgUmVxdWVzdFwiKSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb25cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImxpXCIsIHtjbGFzc05hbWU6IFwiY29sbGVjdGlvbi1pdGVtIGF2YXRhclwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImltZ1wiLCB7c3JjOiBcImltZy9ibG9vZHR5cGVfbG9nby5wbmdcIiwgY2xhc3NOYW1lOiBcImNpcmNsZVwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHtjbGFzc05hbWU6IFwidGl0bGVcIn0sIFwiT3JnYW5pemF0aW9uIE5hbWU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtvbkNoYW5nZTogdGhpcy5vbk5hbWVDaGFuZ2UsIHJlZjogXCJuYW1lXCIsIHR5cGU6IFwidGV4dFwiLCBwbGFjZWhvbGRlcjogXCJuYW1lXCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICksIFxuXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwge2NsYXNzTmFtZTogXCJjb2xsZWN0aW9uLWl0ZW0gYXZhdGFyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW1nXCIsIHtzcmM6IFwiaW1nL2xvY2F0aW9uX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIkxvY2F0aW9uOlwiKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChMb2NhdGlvbklucHV0LCB7aW5pdEFkZHJlc3M6IHRoaXMuc3RhdGUuY3VycmVudEFkZHJlc3MsIHJlZjogXCJhZGRyZXNzXCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb24taXRlbSBhdmF0YXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogXCJpbWcvYmxvb2R0eXBlX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIkJsb29kIFR5cGU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtyZWY6IFwiYmxvb2RUeXBlXCIsIG9uQ2hhbmdlOiB0aGlzLm9uQmxvb2RUeXBlQ2hhbmdlLCB0eXBlOiBcInRleHRcIn0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJsaVwiLCB7Y2xhc3NOYW1lOiBcImNvbGxlY3Rpb24taXRlbSBhdmF0YXJcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIiwge3NyYzogXCJpbWcvYmxvb2R0eXBlX2xvZ28ucG5nXCIsIGNsYXNzTmFtZTogXCJjaXJjbGVcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7Y2xhc3NOYW1lOiBcInRpdGxlXCJ9LCBcIlRpbWU6XCIpLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImlucHV0XCIsIHtvbkNoYW5nZTogdGhpcy5vblRpbWVDaGFuZ2UsIHJlZjogXCJ0aW1lXCIsIHBsYWNlaG9sZGVyOiBcIk1NL2RkL3l5eXkgSEg6bW06c3NcIiwgdHlwZTogXCJ0ZXh0XCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJmb3JtLWZvb3RlciByaWdodFwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiYVwiLCB7aHJlZjogXCJqYXZhc2NyaXB0OnZvaWQoMClcIiwgb25DbGljazogdGhpcy5wb3N0UmVxdWVzdCwgY2xhc3NOYW1lOiBcIndhdmVzLWVmZmVjdCB3YXZlcy1ncmVlbiB0ZWFsIGxpZ2h0ZW4tMiBidG5cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiU2F2ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJhXCIsIHtocmVmOiBcImphdmFzY3JpcHQ6dm9pZCgwKVwiLCBjbGFzc05hbWU6IFwid2F2ZXMtZWZmZWN0IHdhdmVzLWdyZWVuIGJsdWUtZ3JleSBidG5cIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiQ2FuY2VsXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5QT1NUSU5HKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU3Bpbm5lcldhdmUsIHt0ZXh0OiBcIlBvc3RpbmcgRG9uYXRpb24gUmVxdWVzdHMgLi4uXCJ9KVxuICAgICAgICAgICAgKVxuICAgICAgICB9XG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMTAvMDUvMTUuXG4gKi9cbnZhciBTbGlkZXIgPSByZXF1aXJlKFwiLi9idXR0b25zL1NsaWRlclwiKVxudmFyIFNwaW5uZXJXYXZlID0gcmVxdWlyZShcIi4vbG9hZGluZy9TcGlubmVyV2F2ZVwiKVxudmFyIEN1YmVHcmlkID0gcmVxdWlyZShcIi4vbG9hZGluZy9UaHJlZUJvdW5jZVwiKVxudmFyIERvbmF0aW9uUmVzdWx0ID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL0RvbmF0aW9uUmVzdWx0XCIpXG52YXIgTG9jYXRpb25TZXR0aW5nID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL0xvY2F0aW9uU2V0dGluZ1wiKVxudmFyIE1hcE1hcmtlciA9IHJlcXVpcmUoXCIuL2ZpbmRfYmlrZXBvZC9NYXBNYXJrZXJcIilcbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICBtaXhpbnM6IFtmbHV4Lm1peGlucy5zdG9yZUxpc3RlbmVyXSxcblxuICAgIHdhdGNoU3RvcmVzOiBbJ0xvY2F0aW9uU3RvcmUnXSxcblxuICAgIEN1cnJlbnRTdGF0ZToge1xuICAgICAgICBGSU5ESU5HOiBcImZpbmRpbmdcIixcbiAgICAgICAgSU5JVElBTDogXCJpbml0aWFsXCIsXG4gICAgICAgIFJFQURZOiAnUkVBRFknLFxuICAgICAgICBSRVNVTFQ6IFwicmVzdWx0XCJcbiAgICB9LFxuXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjdXJyZW50U3RhdGU6ICh0aGlzLnByb3BzLmN1cnJlbnRTdGF0ZSkgPyB0aGlzLnByb3BzLmN1cnJlbnRTdGF0ZSA6IHRoaXMuQ3VycmVudFN0YXRlLklOSVRJQUwsXG4gICAgICAgICAgICBmb3VudGFpbnM6IFtdLFxuICAgICAgICAgICAgY29vcmRzOiB7fVxuICAgICAgICB9XG4gICAgfSxcblxuXG4gICAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24gKG5leHRQcm9wcykge1xuICAgICAgICBpZiAobmV4dFByb3BzLmN1cnJlbnRTdGF0ZSkge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiBuZXh0UHJvcHMuY3VycmVudFN0YXRlfSlcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBmaW5kRG9uYXRpb25zOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBwYXJlbnQgPSB0aGlzXG4gICAgICAgIHZhciB2YWx1ZSA9IHBhcmVudC5yZWZzLmxvY2F0aW9uLmdldFZhbHVlKClcbiAgICAgICAgYWN0aW9ucy5maW5kRG9uYXRpb25zKHZhbHVlLmFkZHJlc3MsIHZhbHVlLnJhZGl1cylcblxuICAgICAgICBpZiAod2luZG93LmRldmljZS5wbGF0Zm9ybSA9PSBcImJyb3dzZXJcIikge1xuICAgICAgICAgICAgZmluZCgpXG4gICAgICAgICAgICAkKHRoaXMucmVmcy5maW5kQnRuLmdldERPTU5vZGUoKSkuZmFkZU91dCgzMDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50U3RhdGU6IHBhcmVudC5DdXJyZW50U3RhdGUuRklORElOR1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9KVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBjdXJyZW50U3RhdGU6IHBhcmVudC5DdXJyZW50U3RhdGUuRklORElOR1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBzdG9yZURpZENoYW5nZTogZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgIHZhciBzdG9yZSA9IGRpc3BhdGNoZXIuZ2V0U3RvcmUobmFtZSlcbiAgICAgICAgICAgIHZhciBzdG9yZVN0YXRlID0gc3RvcmUuZ2V0U3RhdGUoKVxuXG5cbiAgICAgICAgICAgIGlmIChzdG9yZVN0YXRlLmFjdGlvbiA9PSBzdG9yZS5hY3Rpb25zLkZJTkRfUE9EKSB7XG4gICAgICAgICAgICAgICAgaWYgKHN0b3JlU3RhdGUuc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtmb3VudGFpbnM6IHN0b3JlU3RhdGUuZm91bnRhaW5zfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kcmF3TWFwKHRoaXMuc3RhdGUuY29vcmRzKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIG5hdmlnYXRvci5ub3RpZmljYXRpb24uYWxlcnQoc3RvcmVTdGF0ZS5lcnJvck1lc3NhZ2UpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogdGhpcy5DdXJyZW50U3RhdGUuUkVBRFl9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBkcmF3TWFwOiBmdW5jdGlvbiAoY29vcmRzKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2N1cnJlbnRTdGF0ZTogdGhpcy5DdXJyZW50U3RhdGUuUkVBRFl9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBNYXBNYXJrZXIuZ2V0TWFya2Vycyhjb29yZHMsIHRoaXMuc3RhdGUuZm91bnRhaW5zLCB0aGlzLnJlZnMuZ29vZ2xlTWFwLmdldERPTU5vZGUoKSlcbiAgICAgICAgfSlcbiAgICB9LFxuXG4gICAgc2V0VXBDb21wb25lbnQ6IGZ1bmN0aW9uIChyYWRpdXMpIHtcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgdmFyIHJhZCA9IChyYWRpdXMpID8gcmFkaXVzIDogNVxuICAgICAgICBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKGZ1bmN0aW9uIChwb3NpdGlvbikge1xuICAgICAgICAgICAgdmFyIGZpbmQgPSBudWxsXG4gICAgICAgICAgICBpZiAocGFyZW50LmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgdmFyIGNvb3JkcyA9IHtsYXQ6IHBvc2l0aW9uLmNvb3Jkcy5sYXRpdHVkZSwgbG5nOiBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlfVxuICAgICAgICAgICAgICAgIHdpbmRvdy5jb29yZHMgPSBjb29yZHNcbiAgICAgICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe2Nvb3JkczogY29vcmRzfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBhY3Rpb25zLmZpbmREb25hdGlvbnMoY29vcmRzLCByYWQpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZmluZCA9IG51bGxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vcGFyZW50LmRyYXdNYXAoY29vcmRzKVxuICAgICAgICAgICAgLypnb29nbGVVdGlscy5tYXBzLnJldmVyc2VHZW9jb2RpbmcocG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlLFxuICAgICAgICAgICAgIGZ1bmN0aW9uIChhZGRyZXNzKSB7XG4gICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjdXJyZW50QWRkcmVzczogYWRkcmVzcywgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICBhbGVydChlcnJvcilcbiAgICAgICAgICAgICB9KSovXG4gICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgaWYgKHBhcmVudC5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgICAgIGFsZXJ0KGVycm9yLm1lc3NhZ2UpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtlbmFibGVIaWdoQWNjdXJhY3k6IHRydWV9KVxuICAgIH0sXG5cbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkVXBkYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMKSB7XG4gICAgICAgICAgICB0aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICByZXR1cm4gKFxuICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicmV3YXJkc1wifSwgXG4gICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW1nXCIsIHtzcmM6IFwiaHR0cHM6Ly9zMy1hcC1zb3V0aGVhc3QtMi5hbWF6b25hd3MuY29tL2ltbS10cmF2ZWwtYXBwL2lQaG9uZSs2K0NvcHkrMTMrKDEpLnBuZ1wifSlcbiAgICAgICAgICAgKVxuICAgICAgIClcbiAgICB9XG59KVxuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDEwLzA1LzE1LlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAkKFwiI21lbnUtYnRuXCIpLnNpZGVOYXYoe1xuICAgICAgICAgICAgZWRnZTogJ2xlZnQnLCAvLyBDaG9vc2UgdGhlIGhvcml6b250YWwgb3JpZ2luXG4gICAgICAgICAgICBjbG9zZU9uQ2xpY2s6IHRydWUgLy8gQ2xvc2VzIHNpZGUtbmF2IG9uIDxhPiBjbGlja3MsIHVzZWZ1bCBmb3IgQW5ndWxhci9NZXRlb3JcbiAgICAgICAgfSk7XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICAvKip2YXIgbGlua3MgPSBbXG4gICAgICAgICAgICA8bGkga2V5PXsxfT5cbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiIy9maW5kX2RvbmF0aW9uXCIgY2xhc3NOYW1lPVwid2F2ZXMtZWZmZWN0IHdhdmVzLWxpZ2h0XCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImlvbi1hbmRyb2lkLXNlYXJjaFwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgRmluZCBEb25hdGlvbiBSZXF1ZXN0c1xuICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvbGk+LFxuICAgICAgICAgICAgPGxpIGtleT17Mn0gY2xhc3NOYW1lPVwiZGl2aWRlclwiPjwvbGk+LFxuICAgICAgICAgICAgPGxpIGtleT17M30+XG4gICAgICAgICAgICAgICAgPGEgaHJlZj1cIiMvcG9zdF9yZXF1ZXN0XCIgY2xhc3NOYW1lPVwid2F2ZXMtZWZmZWN0IHdhdmVzLWxpZ2h0XCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImlvbi1hbmRyb2lkLXVubG9ja1wiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgUG9zdCBSZXF1ZXN0XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9saT4sXG4gICAgICAgIF0qKi9cbiAgICAgICAgdmFyIGxpbmtzID0gW11cbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImFcIiwge2hyZWY6IFwiamF2YXNjcmlwdDp2b2lkKDApXCIsIGNsYXNzTmFtZTogXCJicmFuZC1sb2dvIGNlbnRlclwifSwgY29uZmlnLmFwcF9uYW1lKSwgXG4gICAgICAgICAgICAgICAgLyo8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCIgY2xhc3NOYW1lPVwiYnJhbmQtbG9nbyByaWdodFwiIGlkPVwibG9nb1wiPjxpbWcgc3JjPVwiaW1nL2FwcC1sb2dvLTk2LnBuZ1wiLz48L2E+Ki9cbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwidWxcIiwge2NsYXNzTmFtZTogXCJsZWZ0IGhpZGUtb24tbWVkLWFuZC1kb3duXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgbGlua3NcbiAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwidWxcIiwge2lkOiBcInNsaWRlLW91dFwiLCBjbGFzc05hbWU6IFwic2lkZS1uYXZcIn0sIFxuICAgICAgICAgICAgICAgICAgICBsaW5rc1xuICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImFcIiwge2hyZWY6IFwiI1wiLCBcImRhdGEtYWN0aXZhdGVzXCI6IFwic2xpZGUtb3V0XCIsIGNsYXNzTmFtZTogXCJidXR0b24tY29sbGFwc2VcIiwgaWQ6IFwibWVudS1idG5cIn0sIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpXCIsIHtjbGFzc05hbWU6IFwibWRpLW5hdmlnYXRpb24tbWVudVwifSkpXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAxMC8wNS8xNS5cbiAqL1xudmFyIFNsaWRlciA9IHJlcXVpcmUoXCIuL2J1dHRvbnMvU2xpZGVyXCIpXG52YXIgU3Bpbm5lcldhdmUgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1NwaW5uZXJXYXZlXCIpXG52YXIgQ3ViZUdyaWQgPSByZXF1aXJlKFwiLi9sb2FkaW5nL1RocmVlQm91bmNlXCIpXG52YXIgRG9uYXRpb25SZXN1bHQgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvRG9uYXRpb25SZXN1bHRcIilcbnZhciBMb2NhdGlvblNldHRpbmcgPSByZXF1aXJlKFwiLi9maW5kX2Jpa2Vwb2QvTG9jYXRpb25TZXR0aW5nXCIpXG52YXIgTWFwTWFya2VyID0gcmVxdWlyZShcIi4vZmluZF9iaWtlcG9kL01hcE1hcmtlclwiKVxubW9kdWxlLmV4cG9ydHMgPSBSZWFjdC5jcmVhdGVDbGFzcyh7ZGlzcGxheU5hbWU6IFwiZXhwb3J0c1wiLFxuICAgIG1peGluczogW2ZsdXgubWl4aW5zLnN0b3JlTGlzdGVuZXJdLFxuXG4gICAgd2F0Y2hTdG9yZXM6IFsnTG9jYXRpb25TdG9yZSddLFxuXG4gICAgQ3VycmVudFN0YXRlOiB7XG4gICAgICAgIEZJTkRJTkc6IFwiZmluZGluZ1wiLFxuICAgICAgICBJTklUSUFMOiBcImluaXRpYWxcIixcbiAgICAgICAgUkVBRFk6ICdSRUFEWScsXG4gICAgICAgIFJFU1VMVDogXCJyZXN1bHRcIlxuICAgIH0sXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGN1cnJlbnRTdGF0ZTogKHRoaXMucHJvcHMuY3VycmVudFN0YXRlKSA/IHRoaXMucHJvcHMuY3VycmVudFN0YXRlIDogdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCxcbiAgICAgICAgICAgIGZvdW50YWluczogW10sXG4gICAgICAgICAgICBtZXJjaGFudHM6IFtdLFxuICAgICAgICAgICAgY29vcmRzOiB7fVxuICAgICAgICB9XG4gICAgfSxcblxuXG4gICAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wczogZnVuY3Rpb24gKG5leHRQcm9wcykge1xuICAgICAgICBpZiAobmV4dFByb3BzLmN1cnJlbnRTdGF0ZSkge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiBuZXh0UHJvcHMuY3VycmVudFN0YXRlfSlcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBzdG9yZURpZENoYW5nZTogZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNNb3VudGVkKCkpIHtcbiAgICAgICAgICAgIHZhciBzdG9yZSA9IGRpc3BhdGNoZXIuZ2V0U3RvcmUobmFtZSlcbiAgICAgICAgICAgIHZhciBzdG9yZVN0YXRlID0gc3RvcmUuZ2V0U3RhdGUoKVxuXG5cbiAgICAgICAgICAgIGlmIChzdG9yZVN0YXRlLmFjdGlvbiA9PSBzdG9yZS5hY3Rpb25zLkZJTkRfRk9VTlRBSU4pIHtcbiAgICAgICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2ZvdW50YWluczogc3RvcmVTdGF0ZS5mb3VudGFpbnN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRyYXdNYXAodGhpcy5zdGF0ZS5jb29yZHMpXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbmF2aWdhdG9yLm5vdGlmaWNhdGlvbi5hbGVydChzdG9yZVN0YXRlLmVycm9yTWVzc2FnZSlcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWX0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIGlmIChzdG9yZVN0YXRlLmFjdGlvbiA9PSBzdG9yZS5hY3Rpb25zLkZJTkRfTUVSQ0hBTlQpe1xuICAgICAgICAgICAgICAgICBpZiAoc3RvcmVTdGF0ZS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe21lcmNoYW50czogc3RvcmVTdGF0ZS5tZXJjaGFudHN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRyYXdNYXAodGhpcy5zdGF0ZS5jb29yZHMpXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbmF2aWdhdG9yLm5vdGlmaWNhdGlvbi5hbGVydChzdG9yZVN0YXRlLmVycm9yTWVzc2FnZSlcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWX0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIGRyYXdNYXA6IGZ1bmN0aW9uIChjb29yZHMpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7Y3VycmVudFN0YXRlOiB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWX0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIE1hcE1hcmtlci5pbml0U2VhcmNoKGNvb3JkcywgdGhpcy5yZWZzLmdvb2dsZU1hcC5nZXRET01Ob2RlKCksIHtcbiAgICAgICAgICAgICAgICBcIml0ZW1zXCI6IHRoaXMuc3RhdGUuZm91bnRhaW5zLFxuICAgICAgICAgICAgICAgIFwiaWNvblwiOiBjb25maWcuZm91bnRhaW5faWNvblxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSlcblxuICAgICAgICB0aGlzLnNldFN0YXRlKHtjdXJyZW50U3RhdGU6IHRoaXMuQ3VycmVudFN0YXRlLlJFQURZfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgTWFwTWFya2VyLmluaXRTZWFyY2goY29vcmRzLCB0aGlzLnJlZnMuZ29vZ2xlTWFwLmdldERPTU5vZGUoKSwge1xuICAgICAgICAgICAgICAgIFwiaXRlbXNcIjogdGhpcy5zdGF0ZS5tZXJjaGFudHMsXG4gICAgICAgICAgICAgICAgXCJpY29uXCI6IGNvbmZpZy5yZXRhaWxfaWNvblxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICB9LFxuXG4gICAgc2V0VXBDb21wb25lbnQ6IGZ1bmN0aW9uIChyYWRpdXMpIHtcbiAgICAgICAgdmFyIHBhcmVudCA9IHRoaXNcbiAgICAgICAgdmFyIHJhZCA9IChyYWRpdXMpID8gcmFkaXVzIDogNVxuICAgICAgICBpZiAoIXdpbmRvdy5jb29yZHMpIHtcbiAgICAgICAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgICAgICAgICAgICAgdmFyIGZpbmQgPSBudWxsXG4gICAgICAgICAgICAgICAgaWYgKHBhcmVudC5pc01vdW50ZWQoKSkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgY29vcmRzID0ge2xhdDogcG9zaXRpb24uY29vcmRzLmxhdGl0dWRlLCBsbmc6IHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGV9XG4gICAgICAgICAgICAgICAgICAgIHBhcmVudC5zZXRTdGF0ZSh7Y29vcmRzOiBjb29yZHN9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb25zLmZpbmREcmlua2luZ0ZvdW50YWluKGNvb3JkcywgcmFkKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGZpbmQgPSBudWxsXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vcGFyZW50LmRyYXdNYXAoY29vcmRzKVxuICAgICAgICAgICAgICAgIC8qZ29vZ2xlVXRpbHMubWFwcy5yZXZlcnNlR2VvY29kaW5nKHBvc2l0aW9uLmNvb3Jkcy5sYXRpdHVkZSwgcG9zaXRpb24uY29vcmRzLmxvbmdpdHVkZSxcbiAgICAgICAgICAgICAgICAgZnVuY3Rpb24gKGFkZHJlc3MpIHtcbiAgICAgICAgICAgICAgICAgcGFyZW50LnNldFN0YXRlKHtjdXJyZW50QWRkcmVzczogYWRkcmVzcywgY3VycmVudFN0YXRlOiBwYXJlbnQuQ3VycmVudFN0YXRlLlJFQURZfSlcbiAgICAgICAgICAgICAgICAgfSwgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgIGFsZXJ0KGVycm9yKVxuICAgICAgICAgICAgICAgICB9KSovXG4gICAgICAgICAgICB9LCBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICBpZiAocGFyZW50LmlzTW91bnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0KGVycm9yLm1lc3NhZ2UpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwge2VuYWJsZUhpZ2hBY2N1cmFjeTogdHJ1ZX0pXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBwYXJlbnQuc2V0U3RhdGUoe2Nvb3JkczogY29vcmRzfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGFjdGlvbnMuZmluZERyaW5raW5nRm91bnRhaW4od2luZG93LmNvb3JkcywgcmFkKVxuICAgICAgICAgICAgICAgIGFjdGlvbnMuZmluZE1lcmNoYW50cyh3aW5kb3cuY29vcmRzLCByYWQpXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG5cbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRVcENvbXBvbmVudCgpXG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZFVwZGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUgPT0gdGhpcy5DdXJyZW50U3RhdGUuSU5JVElBTCkge1xuICAgICAgICAgICAgLy90aGlzLnNldFVwQ29tcG9uZW50KClcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5zdGF0ZS5jdXJyZW50U3RhdGUpXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5JTklUSUFMKSB7XG4gICAgICAgICAgICByZXR1cm4gKFJlYWN0LmNyZWF0ZUVsZW1lbnQoQ3ViZUdyaWQsIHt0ZXh0OiBcIkxvYWRpbmcgLi4uXCJ9KSlcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9PSB0aGlzLkN1cnJlbnRTdGF0ZS5SRUFEWSkge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiczEyXCIsIGlkOiBcInNlYXJjaC13cmFwcGVyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcIm5hdlwiLCBudWxsLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJuYXYtd3JhcHBlciB0by1nby1uYXZcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3dcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMVwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaDVcIiwgbnVsbCwgXCJXaGVyZSB0byBnbyBuZXh0P1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTFcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImlucHV0LWZpZWxkXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW5wdXRcIiwge3ZhbHVlOiBcIkN1cnJlbnQgTG9jYXRpb25cIiwgdHlwZTogXCJzZWFyY2hcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIiwgbnVsbCwgUmVhY3QuY3JlYXRlRWxlbWVudChcImlcIiwge2NsYXNzTmFtZTogXCJcIn0sIFwiU3RhcnRcIikpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMVwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiaW5wdXQtZmllbGRcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiLCB7aWQ6IFwicGxhY2Utc2VhcmNoXCIsIHR5cGU6IFwic2VhcmNoXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImxhYmVsXCIsIG51bGwsIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJpXCIsIHtjbGFzc05hbWU6IFwiXCJ9LCBcIkVuZFwiKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJjb2wgczExXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJhXCIsIHtocmVmOiBcIiMvcmV3YXJkc1wiLCBjbGFzc05hbWU6IFwiYnRuIGJ0bi1wcmltYXJ5IHJlZFwiLCBzdHlsZToge21hcmdpbkJvdHRvbTogXCIxMHB4XCJ9fSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJHb1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICksIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMlwiLCBpZDogXCJnb29nbGUtbWFwXCIsIHJlZjogXCJnb29nbGVNYXBcIn0pXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuY3VycmVudFN0YXRlID09IHRoaXMuQ3VycmVudFN0YXRlLkZJTkRJTkcpIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChTcGlubmVyV2F2ZSwge3RleHQ6IFwiRmluZGluZyBQb2RzIFJlcXVlc3RzIC4uLlwifSlcbiAgICAgICAgICAgIClcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXRlLmN1cnJlbnRTdGF0ZSA9IHRoaXMuQ3VycmVudFN0YXRlLlJFU1VMVCkge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KERvbmF0aW9uUmVzdWx0LCB7ZG9uYXRpb25zOiB0aGlzLnN0YXRlLmRvbmF0aW9uc30pXG4gICAgICAgICAgICApXG4gICAgICAgIH1cbiAgICB9XG59KVxuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIzLzA1LzE1LlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHZhbHVlOiAodGhpcy5wcm9wcy52YWx1ZSk/IHRoaXMucHJvcHMudmFsdWUgOiAwXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICB0aGlzLmFjdGl2YXRlU2xpZGVyKClcbiAgICB9LFxuXG4gICAgc2V0VmFsdWU6IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe3ZhbHVlOiB2YWx1ZX0pXG4gICAgfSxcblxuICAgIGFjdGl2YXRlU2xpZGVyOiAgZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBtaW4gPSAodGhpcy5wcm9wcy5taW4pPyB0aGlzLnByb3BzLm1pbiA6IDBcbiAgICAgICAgdmFyIG1heCA9ICh0aGlzLnByb3BzLm1heCk/IHRoaXMucHJvcHMubWF4IDogNTBcbiAgICAgICAgdmFyIG5vZGUgPSAkKHRoaXMucmVmcy5zbGlkZXIuZ2V0RE9NTm9kZSgpKVxuICAgICAgICBub2RlLm5vVWlTbGlkZXIoe1xuICAgICAgICAgICAgc3RhcnQ6IHRoaXMuc3RhdGUudmFsdWUsXG4gICAgICAgICAgICBjb25uZWN0OiBcImxvd2VyXCIsXG4gICAgICAgICAgICBvcmllbnRhdGlvbjogXCJob3Jpem9udGFsXCIsXG4gICAgICAgICAgICBkaXJlY3Rpb246IFwibHRyXCIsXG4gICAgICAgICAgICBiZWhhdmlvdXI6IFwidGFwLWRyYWdcIixcbiAgICAgICAgICAgIHJhbmdlOiB7XG4gICAgICAgICAgICAgICAgJ21pbic6IG1pbixcbiAgICAgICAgICAgICAgICAnbWF4JzogbWF4XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZm9ybWF0OiB3TnVtYih7XG4gICAgICAgICAgICAgICAgZGVjaW1hbHM6IDBcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pO1xuICAgICAgICB2YXIgcGFyZW50ID0gdGhpc1xuICAgICAgICBub2RlLm9uKCdzbGlkZScsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcGFyZW50LnNldFZhbHVlKCQodGhpcykudmFsKCkpXG4gICAgICAgIH0pXG5cdG5vZGUub24oJ2NoYW5nZScsIGZ1bmN0aW9uICgpIHtcblx0ICAgIGlmIChwYXJlbnQucHJvcHMuc2xpZGVyUmVsZWFzZSkge1xuXHRcdHBhcmVudC5wcm9wcy5zbGlkZXJSZWxlYXNlKCQodGhpcykudmFsKCkpXG5cdCAgICB9XG5cdH0pXG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgdW5pdCA9ICh0aGlzLnByb3BzLnVuaXQpPyBcIiBcIiArdGhpcy5wcm9wcy51bml0IDogXCJcIlxuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTIgc2xpZGVyLWNvbnRhaW5lclwifSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNsaWRlciBjb2wgczEwXCIsIHJlZjogXCJzbGlkZXJcIn0pLCBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMyIHNsaWRlci12YWx1ZVwifSwgdGhpcy5zdGF0ZS52YWx1ZSArIHVuaXQpXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KVxuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIwLzA1LzE1LlxuICovXG5cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNvbCBzMTIgbTggb2Zmc2V0LW0yIGw2IG9mZnNldC1sM1wifSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImNhcmQtcGFuZWwgZ3JleSBsaWdodGVuLTUgei1kZXB0aC0xXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInJvdyB2YWxpZ24td3JhcHBlclwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMyXCJ9LCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW1nXCIsIHtzcmM6IHRoaXMucHJvcHMuZG9uYXRpb24ucHJvZmlsZUltZywgYWx0OiBcIlByb2ZpbGUgUGljdHVyZVwiLCBjbGFzc05hbWU6IFwiY2lyY2xlIHJlc3BvbnNpdmUtaW1nXCJ9KVxuICAgICAgICAgICAgICAgICAgICAgICAgKSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMFwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwgbnVsbCksIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwXCIsIHtjbGFzc05hbWU6IFwiYmxhY2stdGV4dFwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZG9uYXRpb24uZGVzY3JpcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qKlxuICogQ3JlYXRlZCBieSBkdW9uZ25odSBvbiAyMC8wNS8xNS5cbiAqL1xuXG52YXIgRG9uYXRpb25DYXJkID0gcmVxdWlyZShcIi4vRG9uYXRpb25DYXJkXCIpXG5cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcblxuICAgIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBkb25hdGlvbnM6ICh0aGlzLnByb3BzLmRvbmF0aW9ucyk/IHRoaXMucHJvcHMuZG9uYXRpb25zIDogW11cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzOiBmdW5jdGlvbihuZXh0UHJvcHMpIHtcbiAgICAgICAgaWYgKG5leHRQcm9wcy5kb25hdGlvbnMgJiYgSlNPTi5zdHJpbmdpZnkobmV4dFByb3BzLmRvbmF0aW9ucykgIT0gSlNPTi5zdHJpbmdpZnkodGhpcy5wcm9wcy5kb25hdGlvbnMpKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtkb25hdGlvbnM6IG5leHRQcm9wcy5kb25hdGlvbnN9KVxuICAgICAgICB9XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLmRvbmF0aW9ucy5sZW5ndGggPT0gMCkge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaDNcIiwgbnVsbCwgXG4gICAgICAgICAgICAgICAgICAgIFwiTm8gZG9uYXRpb24gcmVxdWVzdHMgYXJlIGZvdW5kIGFyb3VuZCB5b3VyIGxvY2F0aW9uIVwiXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiYnVza2VyLWxpc3RcIn0sIFxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuZG9uYXRpb25zLm1hcChmdW5jdGlvbihpdGVtLCBpZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRG9uYXRpb25DYXJkLCB7ZG9uYXRpb246IGl0ZW0sIGtleTogaWR9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgfVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b25nbmh1IG9uIDIzLzA1LzE1LlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG5cbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHNlYXJjaEJveDogdW5kZWZpbmVkXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7c2VhcmNoQm94OiBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLlNlYXJjaEJveCh0aGlzLnJlZnMuaW5wdXQuZ2V0RE9NTm9kZSgpKX0pXG4gICAgfSxcblxuICAgIGdldFZhbHVlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlZnMuaW5wdXQuZ2V0RE9NTm9kZSgpLnZhbHVlXG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInJvd1wifSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcImlucHV0LWZpZWxkIGNvbCBzMTJcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiaW5wdXRcIiwge3R5cGU6IFwidGV4dFwiLCBwbGFjZWhvbGRlcjogXCJDaG9vc2UgYSBsb2NhdGlvblwiLCBjbGFzc05hbWU6IFwidmFsaWRhdGVcIiwgcmVmOiBcImlucHV0XCIsIGRlZmF1bHRWYWx1ZTogdGhpcy5wcm9wcy5pbml0QWRkcmVzc30pXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjMvMDUvMTUuXG4gKi9cbnZhciBTbGlkZXIgPSByZXF1aXJlKFwiLi4vYnV0dG9ucy9TbGlkZXJcIilcbnZhciBMb2NhdGlvbklucHV0ID0gcmVxdWlyZShcIi4vTG9jYXRpb25JbnB1dFwiKVxuXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogXCJleHBvcnRzXCIsXG5cbiAgICBjb21wb25lbnRXaWxsVXBkYXRlOiBmdW5jdGlvbiAobmV4dFByb3BzKSB7XG4gICAgICAgIGlmIChuZXh0UHJvcHMuaW5pdEFkZHJlc3MgJiYgbmV4dFByb3BzLmluaXRBZGRyZXNzICE9IHRoaXMucHJvcHMuaW5pdEFkZHJlc3MpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgfSxcblxuICAgIGdldFZhbHVlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBhZGRyZXNzOiB0aGlzLnJlZnMuYWRkcmVzcy5nZXRWYWx1ZSgpLFxuICAgICAgICAgICAgcmFkaXVzOiB0aGlzLnJlZnMucmFkaXVzLnN0YXRlLnZhbHVlXG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwicm93XCIsIGlkOiBcImxvY2F0aW9uLXNldHRpbmdcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJmb3JtXCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMlwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJyb3dcIn0sIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChMb2NhdGlvbklucHV0LCB7aW5pdEFkZHJlc3M6IHRoaXMucHJvcHMuaW5pdEFkZHJlc3MsIHJlZjogXCJhZGRyZXNzXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwiY29sIHMxMiBsYWJlbFwifSwgXCJSYWRpdXNcIiksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChTbGlkZXIsIHtyZWY6IFwicmFkaXVzXCIsIHZhbHVlOiAxMCwgdW5pdDogXCJrbVwifSlcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgKVxuICAgIH1cbn0pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IGR1b2dubmh1IG9uIDI2LzA5LzE1LlxuICovXG52YXIgZ0FwaSA9IHJlcXVpcmUoXCIuLi8uLi91dGlscy9nb29nbGVcIilcbm1vZHVsZS5leHBvcnRzID0ge1xuICAgIGdldE1hcmtlcnM6IGZ1bmN0aW9uIChjdXJyZW50TG9jYXRpb24sIHBvZHMsIGRvbSwgcmFkaXVzLCBpY29uKSB7XG4gICAgICAgIGlmICghd2luZG93Lm1hcCkge1xuICAgICAgICAgICAgd2luZG93Lm1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9tLCB7XG4gICAgICAgICAgICAgICAgY2VudGVyOiBjdXJyZW50TG9jYXRpb24sXG4gICAgICAgICAgICAgICAgem9vbTogY29uZmlnLmdvb2dsZU1hcC56b29tLFxuICAgICAgICAgICAgICAgIG1hcFR5cGVJZDogZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLlRFUlJBSU4sXG4gICAgICAgICAgICAgICAgbWFwVHlwZUNvbnRyb2w6IGZhbHNlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBnb29nbGUubWFwcy52aXN1YWxSZWZyZXNoID0gdHJ1ZTtcbiAgICAgICAgdmFyIGN1cnJlbnRMb2NhdGlvbk1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgICAgcG9zaXRpb246IGN1cnJlbnRMb2NhdGlvbixcbiAgICAgICAgICAgIHRpdGxlOiAnWW91IGFyZSBoZXJlJyxcbiAgICAgICAgICAgIGljb246IGNvbmZpZy51c2VyX2N1cnJlbnRfbG9jYXRpb25faWNvblxuICAgICAgICB9KTtcbiAgICAgICAgY3VycmVudExvY2F0aW9uTWFya2VyLnNldE1hcChtYXApO1xuICAgICAgICB2YXIgY3VycmVudExvY2F0aW9uSW5mb1dpbmRvdyA9IG5ldyBnb29nbGUubWFwcy5JbmZvV2luZG93KHtcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiPHA+WW91IGFyZSBoZXJlITwvcD5cIlxuICAgICAgICB9KTtcbiAgICAgICAgY3VycmVudExvY2F0aW9uTWFya2VyLmFkZExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGN1cnJlbnRMb2NhdGlvbkluZm9XaW5kb3cub3BlbihtYXAsIGN1cnJlbnRMb2NhdGlvbk1hcmtlcik7XG4gICAgICAgIH0pXG5cbiAgICAgICAgcG9kcy5mb3JFYWNoKGZ1bmN0aW9uIChwb2QsIGlkKSB7XG4gICAgICAgICAgICB2YXIgY29vcmRpbmF0ZXMgPSBwb2QuY29vcmRpbmF0ZXMuY29vcmRpbmF0ZXNcbiAgICAgICAgICAgIHZhciBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjoge1xuICAgICAgICAgICAgICAgICAgICBsYXQ6IHBhcnNlRmxvYXQoY29vcmRpbmF0ZXNbXCJsYXRcIl0pLFxuICAgICAgICAgICAgICAgICAgICBsbmc6IHBhcnNlRmxvYXQoY29vcmRpbmF0ZXNbXCJsb25cIl0pXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBpY29uOiAoaWNvbikgPyBpY29uIDogY29uZmlnLmJpa2VfaWNvblxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIG1hcmtlci5hZGRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgd2luZG93LnBvcHVwUG9kRGV0YWlscyhwb2QpXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgbWFya2VyLnNldE1hcChtYXApXG4gICAgICAgIH0pXG4gICAgICAgIG5ldyBnb29nbGUubWFwcy5DaXJjbGUoe1xuICAgICAgICAgICAgc3Ryb2tlQ29sb3I6ICdncmVlbicsXG4gICAgICAgICAgICBzdHJva2VPcGFjaXR5OiAwLjgsXG4gICAgICAgICAgICBzdHJva2VXZWlnaHQ6IDIsXG4gICAgICAgICAgICBmaWxsT3BhY2l0eTogMC4wLFxuICAgICAgICAgICAgbWFwOiBtYXAsXG4gICAgICAgICAgICBjZW50ZXI6IGN1cnJlbnRMb2NhdGlvbixcbiAgICAgICAgICAgIHJhZGl1czogMTAwMFxuICAgICAgICB9KTtcbiAgICB9LFxuXG4gICAgaW5pdFNlYXJjaDogZnVuY3Rpb24gKGN1cnJlbnRMb2NhdGlvbiwgZG9tLCBkYXRhKSB7XG4gICAgICAgIHZhciBtYXJrZXJzID0gW11cbiAgICAgICAgaWYgKCF3aW5kb3cubWFwKSB7XG4gICAgICAgICAgICB3aW5kb3cubWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcChkb20sIHtcbiAgICAgICAgICAgICAgICBjZW50ZXI6IGN1cnJlbnRMb2NhdGlvbixcbiAgICAgICAgICAgICAgICB6b29tOiBjb25maWcuZ29vZ2xlTWFwLnpvb20sXG4gICAgICAgICAgICAgICAgbWFwVHlwZUlkOiBnb29nbGUubWFwcy5NYXBUeXBlSWQuVEVSUkFJTixcbiAgICAgICAgICAgICAgICBtYXBUeXBlQ29udHJvbDogZmFsc2VcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGdvb2dsZS5tYXBzLnZpc3VhbFJlZnJlc2ggPSB0cnVlO1xuICAgICAgICB2YXIgY3VycmVudExvY2F0aW9uTWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICBwb3NpdGlvbjogY3VycmVudExvY2F0aW9uLFxuICAgICAgICAgICAgdGl0bGU6ICdZb3UgYXJlIGhlcmUnLFxuICAgICAgICAgICAgaWNvbjogY29uZmlnLnVzZXJfY3VycmVudF9sb2NhdGlvbl9pY29uXG4gICAgICAgIH0pO1xuICAgICAgICBjdXJyZW50TG9jYXRpb25NYXJrZXIuc2V0TWFwKG1hcCk7XG4gICAgICAgIHZhciBjdXJyZW50TG9jYXRpb25JbmZvV2luZG93ID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3coe1xuICAgICAgICAgICAgY29udGVudDogXCI8cD5Zb3UgYXJlIGhlcmUhPC9wPlwiXG4gICAgICAgIH0pO1xuICAgICAgICBjdXJyZW50TG9jYXRpb25NYXJrZXIuYWRkTGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY3VycmVudExvY2F0aW9uSW5mb1dpbmRvdy5vcGVuKG1hcCwgY3VycmVudExvY2F0aW9uTWFya2VyKTtcbiAgICAgICAgfSlcblxuICAgICAgICBkYXRhLml0ZW1zLmZvckVhY2goZnVuY3Rpb24gKHBvaW50KSB7XG4gICAgICAgICAgICB2YXIgY29vcmRpbmF0ZXMgPSBwb2ludC5jb29yZGluYXRlcy5jb29yZGluYXRlc1xuICAgICAgICAgICAgdmFyIG1PcHRzID0ge1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiB7XG4gICAgICAgICAgICAgICAgICAgIGxhdDogcGFyc2VGbG9hdChjb29yZGluYXRlc1tcImxhdFwiXSksXG4gICAgICAgICAgICAgICAgICAgIGxuZzogcGFyc2VGbG9hdChjb29yZGluYXRlc1tcImxvblwiXSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoZGF0YS5pY29uKSB7XG4gICAgICAgICAgICAgICAgbU9wdHNbXCJpY29uXCJdID0gZGF0YS5pY29uXG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHZhciBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKG1PcHRzKVxuICAgICAgICAgICAgbWFya2VyLnNldE1hcChtYXApXG4gICAgICAgICAgICBtYXJrZXJzLnB1c2gobWFya2VyKVxuICAgICAgICB9KVxuICAgICAgICBuZXcgZ29vZ2xlLm1hcHMuQ2lyY2xlKHtcbiAgICAgICAgICAgIHN0cm9rZUNvbG9yOiAnZ3JlZW4nLFxuICAgICAgICAgICAgc3Ryb2tlT3BhY2l0eTogMC44LFxuICAgICAgICAgICAgc3Ryb2tlV2VpZ2h0OiAyLFxuICAgICAgICAgICAgZmlsbE9wYWNpdHk6IDAuMCxcbiAgICAgICAgICAgIG1hcDogbWFwLFxuICAgICAgICAgICAgY2VudGVyOiBjdXJyZW50TG9jYXRpb24sXG4gICAgICAgICAgICByYWRpdXM6IDEwMDBcbiAgICAgICAgfSk7XG4gICAgICAgIHZhciBzZWFyY2hQbGFjZXNNYXJrZXJzID0gW11cbiAgICAgICAgaWYgKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwbGFjZS1zZWFyY2gnKSkge1xuICAgICAgICAgICAgLy8gQ3JlYXRlIHRoZSBzZWFyY2ggYm94IGFuZCBsaW5rIGl0IHRvIHRoZSBVSSBlbGVtZW50LlxuICAgICAgICAgICAgdmFyIGlucHV0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3BsYWNlLXNlYXJjaCcpO1xuICAgICAgICAgICAgdmFyIHNlYXJjaEJveCA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuU2VhcmNoQm94KGlucHV0KTtcbiAgICAgICAgICAgIC8vbWFwLmNvbnRyb2xzW2dvb2dsZS5tYXBzLkNvbnRyb2xQb3NpdGlvbi5UT1BfTEVGVF0ucHVzaChpbnB1dCk7XG5cbiAgICAgICAgICAgIHZhciBkaXJlY3Rpb25zRGlzcGxheSA9IG5ldyBnb29nbGUubWFwcy5EaXJlY3Rpb25zUmVuZGVyZXIoe1xuICAgICAgICAgICAgICAgIHByZXNlcnZlVmlld3BvcnQ6IHRydWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGRpcmVjdGlvbnNEaXNwbGF5LnNldE1hcChtYXApXG4gICAgICAgICAgICBzZWFyY2hCb3guYWRkTGlzdGVuZXIoJ3BsYWNlc19jaGFuZ2VkJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHZhciBwbGFjZXMgPSBzZWFyY2hCb3guZ2V0UGxhY2VzKCk7XG5cbiAgICAgICAgICAgICAgICBpZiAocGxhY2VzLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgc2VhcmNoUGxhY2VzTWFya2Vycy5mb3JFYWNoKGZ1bmN0aW9uIChtYXJrZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgbWFya2VyLnNldE1hcChudWxsKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLy8gRm9yIGVhY2ggcGxhY2UsIGdldCB0aGUgaWNvbiwgbmFtZSBhbmQgbG9jYXRpb24uXG4gICAgICAgICAgICAgICAgdmFyIGJvdW5kcyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmdCb3VuZHMoKTtcbiAgICAgICAgICAgICAgICBwbGFjZXMuZm9yRWFjaChmdW5jdGlvbiAocGxhY2UpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFwbGFjZS5nZW9tZXRyeSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJSZXR1cm5lZCBwbGFjZSBjb250YWlucyBubyBnZW9tZXRyeVwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB2YXIgaWNvbiA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybDogcGxhY2UuaWNvbixcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemU6IG5ldyBnb29nbGUubWFwcy5TaXplKDcxLCA3MSksXG4gICAgICAgICAgICAgICAgICAgICAgICBvcmlnaW46IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGFuY2hvcjogbmV3IGdvb2dsZS5tYXBzLlBvaW50KDE3LCAzNCksXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2FsZWRTaXplOiBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSgyNSwgMjUpXG4gICAgICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gQ3JlYXRlIGEgbWFya2VyIGZvciBlYWNoIHBsYWNlLlxuICAgICAgICAgICAgICAgICAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXA6IG1hcCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBwbGFjZS5uYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaFBsYWNlc01hcmtlcnMucHVzaChtYXJrZXIpXG4gICAgICAgICAgICAgICAgICAgIHZhciBtTG9jYXRpb25JbmZvID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3coe1xuICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudDogXCI8cD5cIiArIHBsYWNlLm5hbWUgKyBcIjwvcD5cIlxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgbWFya2VyLmFkZExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1Mb2NhdGlvbkluZm8ub3BlbihtYXAsIG1hcmtlcik7XG4gICAgICAgICAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHBsYWNlLmdlb21ldHJ5LnZpZXdwb3J0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBPbmx5IGdlb2NvZGVzIGhhdmUgdmlld3BvcnQuXG4gICAgICAgICAgICAgICAgICAgICAgICBib3VuZHMudW5pb24ocGxhY2UuZ2VvbWV0cnkudmlld3BvcnQpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgYm91bmRzLmV4dGVuZChwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbik7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICAgICAgICAgIGdBcGkubWFwcy5nZXREaXIoY3VycmVudExvY2F0aW9uLCBwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbiwgZnVuY3Rpb24gKGRpcmVjdGlvbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aW9uc0Rpc3BsYXkuc2V0RGlyZWN0aW9ucyhkaXJlY3Rpb24pXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgY3VycmVudExvY2F0aW9uTWFya2VyLnNldE1hcChudWxsKVxuICAgICAgICAgICAgICAgIG1hcC5maXRCb3VuZHMoYm91bmRzKTtcblxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG59XG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjAvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICByZW5kZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNwaW5uZXItd3JhcHBlclwifSwgXG4gICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNwaW5uZXItY2VsbFwifSwgXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1zcGlubmVyIHNrLXNwaW5uZXItd2F2ZVwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stcmVjdDFcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1yZWN0MlwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLXJlY3QzXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stcmVjdDRcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1yZWN0NVwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcInBcIiwgbnVsbCwgdGhpcy5wcm9wcy50ZXh0KVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxufSkiLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjMvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiBcImV4cG9ydHNcIixcbiAgICByZW5kZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzcGlubmVyLXdyYXBwZXJcIn0sIFxuICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzcGlubmVyLWNlbGxcIn0sIFxuICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stc3Bpbm5lciBzay1zcGlubmVyLXRocmVlLWJvdW5jZVwifSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtjbGFzc05hbWU6IFwic2stYm91bmNlMVwifSksIFxuICAgICAgICAgICAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7Y2xhc3NOYW1lOiBcInNrLWJvdW5jZTJcIn0pLCBcbiAgICAgICAgICAgICAgICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge2NsYXNzTmFtZTogXCJzay1ib3VuY2UzXCJ9KSwgXG4gICAgICAgICAgICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFwicFwiLCBudWxsLCB0aGlzLnByb3BzLnRleHQpXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIClcbiAgICB9XG59KSIsIi8qXG4gKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4gKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiAqIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4gKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4gKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4gKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2VcbiAqIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4gKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuICogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiAqIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuICogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuICogdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxudmFyIHJvdXRlciA9IHJlcXVpcmUoXCIuL3JvdXRlci9yb3V0ZXJcIilcblxudmFyIGFwcCA9IHtcbiAgICAvLyBBcHBsaWNhdGlvbiBDb25zdHJ1Y3RvclxuICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICB0aGlzLmJpbmRFdmVudHMoKTtcbiAgICB9LFxuICAgIC8vIEJpbmQgRXZlbnQgTGlzdGVuZXJzXG4gICAgLy9cbiAgICAvLyBCaW5kIGFueSBldmVudHMgdGhhdCBhcmUgcmVxdWlyZWQgb24gc3RhcnR1cC4gQ29tbW9uIGV2ZW50cyBhcmU6XG4gICAgLy8gJ2xvYWQnLCAnZGV2aWNlcmVhZHknLCAnb2ZmbGluZScsIGFuZCAnb25saW5lJy5cbiAgICBiaW5kRXZlbnRzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignZGV2aWNlcmVhZHknLCB0aGlzLm9uRGV2aWNlUmVhZHksIGZhbHNlKTtcbiAgICB9LFxuICAgIC8vIGRldmljZXJlYWR5IEV2ZW50IEhhbmRsZXJcbiAgICAvL1xuICAgIC8vIFRoZSBzY29wZSBvZiAndGhpcycgaXMgdGhlIGV2ZW50LiBJbiBvcmRlciB0byBjYWxsIHRoZSAncmVjZWl2ZWRFdmVudCdcbiAgICAvLyBmdW5jdGlvbiwgd2UgbXVzdCBleHBsaWNpdGx5IGNhbGwgJ2FwcC5yZWNlaXZlZEV2ZW50KC4uLik7J1xuICAgIG9uRGV2aWNlUmVhZHk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAvL2FwcC5yZWNlaXZlZEV2ZW50KCdkZXZpY2VyZWFkeScpO1xuICAgICAgICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJvdXRlci5pbml0KFwiL2ZpbmRfZG9uYXRpb25cIilcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJChcIi5zcGxhc2gtc2NyZWVuXCIpLmZhZGVPdXQoNTAwKVxuICAgICAgICAgICAgfSwgMzAwMClcbiAgICAgICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJiYWNrYnV0dG9uXCIsIG9uQmFja0tleURvd24sIGZhbHNlKTtcbiAgICAgICAgICAgIGZ1bmN0aW9uIG9uQmFja0tleURvd24oKSB7XG4gICAgICAgICAgICAgICAgUmVhY3QudW5tb3VudENvbXBvbmVudEF0Tm9kZShkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBvZC1kZXRhaWxzXCIpKVxuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuICAgIH1cbn07XG5cbmFwcC5pbml0aWFsaXplKCk7XG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjAvMDUvMTUuXG4gKi9cbnZhciBGaW5kQmlrZVBvZCA9IHJlcXVpcmUoXCIuLi9jb21wb25lbnRzL0ZpbmRCaWtlUG9kLmpzXCIpXG52YXIgUG9zdFJlcXVlc3QgPSByZXF1aXJlKFwiLi4vY29tcG9uZW50cy9Qb3N0UmVxdWVzdFwiKVxudmFyIFdoZXJlVG9HbyA9IHJlcXVpcmUoXCIuLi9jb21wb25lbnRzL1doZXJlVG9Hb1wiKVxudmFyIFNpZGVOYXYgPSByZXF1aXJlKFwiLi4vY29tcG9uZW50cy9TaWRlTmF2XCIpXG52YXIgUmV3YXJkcyA9IHJlcXVpcmUoXCIuLi9jb21wb25lbnRzL1Jld2FyZHNcIilcblxud2luZG93LnJlbmRlckZpbmREb25hdGlvbiA9IGZ1bmN0aW9uICgpIHtcbiAgICB3aW5kb3cubWFwID0gbnVsbFxuICAgIFJlYWN0LnVubW91bnRDb21wb25lbnRBdE5vZGUoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYWluLWNvbnRlbnRcIikpXG4gICAgJChcIiNuYXYtYmFyXCIpLnNob3coKVxuICAgICAgICAkKFwiLm1vZGFsXCIpLmhpZGUoKVxuXG4gICAgUmVhY3QucmVuZGVyKFJlYWN0LmNyZWF0ZUVsZW1lbnQoU2lkZU5hdiwge2Rpc3BhdGNoZXI6IGRpc3BhdGNoZXJ9KSwgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJuYXYtYmFyXCIpKVxuICAgIGlmIChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBvZC1kZXRhaWxzXCIpKSB7XG4gICAgICAgIFJlYWN0LnVubW91bnRDb21wb25lbnRBdE5vZGUoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwb2QtZGV0YWlsc1wiKSlcbiAgICB9XG4gICAgUmVhY3QucmVuZGVyKFJlYWN0LmNyZWF0ZUVsZW1lbnQoRmluZEJpa2VQb2QsIHtjdXJyZW50U3RhdGU6IFwiaW5pdGlhbFwiLCBkaXNwYXRjaGVyOiBkaXNwYXRjaGVyfSksIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFpbi1jb250ZW50XCIpKVxufVxuXG53aW5kb3cucmVuZGVyV2hlcmVHbyA9IGZ1bmN0aW9uICgpIHtcbiAgICB3aW5kb3cubWFwID0gbnVsbFxuICAgICQoXCIjbmF2LWJhclwiKS5oaWRlKClcbiAgICAkKFwiLm1vZGFsXCIpLmhpZGUoKVxuICAgICQoXCIubGVhbi1vdmVybGF5XCIpLmhpZGUoKVxuICAgICQoXCIjbGVhbi1vdmVybGF5XCIpLmhpZGUoKVxuXG4gICAgUmVhY3QudW5tb3VudENvbXBvbmVudEF0Tm9kZShkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbiAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9kLWRldGFpbHNcIikpXG4gICAgUmVhY3QucmVuZGVyKFJlYWN0LmNyZWF0ZUVsZW1lbnQoV2hlcmVUb0dvLCB7Y3VycmVudFN0YXRlOiBcImluaXRpYWxcIiwgZGlzcGF0Y2hlcjogZGlzcGF0Y2hlcn0pLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbn1cblxud2luZG93LnJld2FyZHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgJChcIiNwZXMtY29udGFpbmVyXCIpLmhpZGUoKVxuICAgICBSZWFjdC51bm1vdW50Q29tcG9uZW50QXROb2RlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFpbi1jb250ZW50XCIpKVxuICAgIFJlYWN0LnVubW91bnRDb21wb25lbnRBdE5vZGUoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwb2QtZGV0YWlsc1wiKSlcbiAgICBSZWFjdC5yZW5kZXIoUmVhY3QuY3JlYXRlRWxlbWVudChSZXdhcmRzLCB7Y3VycmVudFN0YXRlOiBcImluaXRpYWxcIiwgZGlzcGF0Y2hlcjogZGlzcGF0Y2hlcn0pLCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1haW4tY29udGVudFwiKSlcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgaW5pdDogZnVuY3Rpb24ocGF0aCkge1xuICAgICAgICB2YXIgcm91dGVzID0ge1xuICAgICAgICAgICAgXCIvZmluZF9kb25hdGlvblwiOiByZW5kZXJGaW5kRG9uYXRpb24sXG4gICAgICAgICAgICBcIi93aGVyZS1nb1wiOiByZW5kZXJXaGVyZUdvLFxuICAgICAgICAgICAgXCIvcmV3YXJkc1wiOiByZXdhcmRzXG4gICAgICAgIH1cbiAgICAgICAgd2luZG93LnJvdXRlciA9IFJvdXRlcihyb3V0ZXMpO1xuICAgICAgICByb3V0ZXIuaW5pdChwYXRoKVxuICAgIH1cbn0iLCIvKipcbiAqIENyZWF0ZWQgYnkgZHVvbmduaHUgb24gMjMvMDUvMTUuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0ge1xuICAgIG1hcHM6IHtcbiAgICAgICAgY2hlY2tDb25uZWN0aW9uOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgbmV0d29ya1N0YXRlID0gbmF2aWdhdG9yLmNvbm5lY3Rpb24udHlwZVxuICAgICAgICAgICAgY29uc29sZS5sb2cobmV0d29ya1N0YXRlKVxuICAgICAgICAgICAgaWYgKG5ldHdvcmtTdGF0ZSA9PSBDb25uZWN0aW9uLk5PTkUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH0sXG5cbiAgICAgICAgcmV2ZXJzZUdlb2NvZGluZzogZnVuY3Rpb24gKGxhdCwgbG5nLCBzdWNjZXNzLCBlcnJvcikge1xuICAgICAgICAgICAgdmFyIGdlb2NvZGVyID0gbmV3IGdvb2dsZS5tYXBzLkdlb2NvZGVyO1xuICAgICAgICAgICAgdmFyIGxhdGxuZyA9IHtsYXQ6IHBhcnNlRmxvYXQobGF0KSwgbG5nOiBwYXJzZUZsb2F0KGxuZyl9O1xuICAgICAgICAgICAgZ2VvY29kZXIuZ2VvY29kZSh7J2xvY2F0aW9uJzogbGF0bG5nfSwgZnVuY3Rpb24gKHJlc3VsdHMsIHN0YXR1cykge1xuICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPT09IGdvb2dsZS5tYXBzLkdlb2NvZGVyU3RhdHVzLk9LKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHRzWzBdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3VsdHNbMF0uZm9ybWF0dGVkX2FkZHJlc3MpXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcignTm8gcmVzdWx0cyBmb3VuZCcpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IoJ0dlb2NvZGVyIGZhaWxlZCBkdWUgdG86ICcgKyBzdGF0dXMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdlb2NvZGluZzogZnVuY3Rpb24gKGFkZHJlc3MsIHN1Y2Nlc3MsIGVycm9yKSB7XG4gICAgICAgICAgICB2YXIgZ2VvY29kZXIgPSBuZXcgZ29vZ2xlLm1hcHMuR2VvY29kZXI7XG4gICAgICAgICAgICBnZW9jb2Rlci5nZW9jb2RlKHsnYWRkcmVzcyc6IGFkZHJlc3N9LCBmdW5jdGlvbiAocmVzdWx0cywgc3RhdHVzKSB7XG4gICAgICAgICAgICAgICAgaWYgKHN0YXR1cyA9PT0gZ29vZ2xlLm1hcHMuR2VvY29kZXJTdGF0dXMuT0spIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdHNbMF0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzdWx0c1swXS5nZW9tZXRyeSlcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yKCdObyByZXN1bHRzIGZvdW5kJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBlcnJvcignR2VvY29kZXIgZmFpbGVkIGR1ZSB0bzogJyArIHN0YXR1cyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0RGlyOiBmdW5jdGlvbiAob3JpZ2luLCBkZXN0aW5hdGlvbiwgc3VjY2Vzcykge1xuICAgICAgICAgICAgdmFyIGRpcmVjdGlvbnNTZXJ2aWNlID0gbmV3IGdvb2dsZS5tYXBzLkRpcmVjdGlvbnNTZXJ2aWNlKCk7XG4gICAgICAgICAgICB2YXIgZGlyZWN0aW9uc1JlcXVlc3QgPSB7XG4gICAgICAgICAgICAgICAgXCJvcmlnaW5cIjogb3JpZ2luW1wibGF0XCJdICsgXCIsXCIgKyBvcmlnaW5bXCJsbmdcIl0sXG4gICAgICAgICAgICAgICAgXCJkZXN0aW5hdGlvblwiOiBvcmlnaW5bXCJsYXRcIl0gKyBcIixcIiArIG9yaWdpbltcImxuZ1wiXSxcbiAgICAgICAgICAgICAgICBvcHRpbWl6ZVdheXBvaW50czogdHJ1ZSxcbiAgICAgICAgICAgICAgICB0cmF2ZWxNb2RlOiAnQklDWUNMSU5HJ1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGRpcmVjdGlvbnNTZXJ2aWNlLnJvdXRlKGRpcmVjdGlvbnNSZXF1ZXN0LCBmdW5jdGlvbiAocmVzcG9uc2UsIHN0YXR1cykge1xuICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPT0gZ29vZ2xlLm1hcHMuRGlyZWN0aW9uc1N0YXR1cy5PSykge1xuICAgICAgICAgICAgICAgICAgICAvL2RvIHdvcmsgd2l0aCByZXNwb25zZSBkYXRhXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKVxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3BvbnNlKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgLy9FcnJvciBoYXMgb2NjdXJlZFxuICAgICAgICAgICAgICAgICAgICBlcnJvcihyZXNwb25zZSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuXG4gICAgICAgIH1cbiAgICB9XG59Il19
